//
//  DataController.swift
//  WMG
//
//  Created by Coolasia on 23/3/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//
import UIKit
import CoreData
class DataController: NSObject {
    var managedObjectContext: NSManagedObjectContext
    override init() {
        // This resource is the same name as your xcdatamodeld contained in your project.
        guard let modelURL = Bundle.main.url(forResource: "modeldb", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        let mom = NSManagedObjectModel(contentsOf: modelURL) 
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom!)
        managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = psc
       
   //         let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
   //         let docURL = urls[urls.endIndex-1]
  //          let storeURL = docURL.URLByAppendingPathComponent("modeldb.sqlite")
  //          let storeURL = NSURL(string:"modeldb.sqlite", relativeTo:docURL)
        
        let documentDirectory = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let storeURL = documentDirectory.appendingPathComponent("modeldb.sqlite") // now it's NSURL
        
        //for delete sqlite-wal, to make sure data insert into sqlite file
        let dict = [NSSQLitePragmasOption: ["journal_mode":"DELETE"]]
        
        
            do {
                try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL as URL?, options: dict)
            } catch {
                fatalError("Error migrating store: \(error)")
            }
        }
    }
