//
//  OrderAttemptSectionTableViewCell.swift
//  WMG
//
//  Created by Coolasia on 6/1/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher

class OrderAttemptSectionTableViewCell: UITableViewCell {

    let label = UILabel()
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var cell: UIView!
    
    @IBOutlet weak var orderAttemptDate: UILabel!
    @IBOutlet weak var orderAttemptTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        
//        contentView.addSubview(label)
//    }
    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
    // MARK: - Base Class Overrides
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//        label.frame = contentView.bounds
//        label.frame.origin.x += 12
//    }

}
