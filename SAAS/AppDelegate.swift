//
//  AppDelegate.swift
//  SampleIOSNew
//
//  Created by Coolasia on 19/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import SwiftMessages
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import CoreData
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var locationManager: CLLocationManager?
    
    var backGround = LocationTracker_ViewController()
    
    
    func showLogin(){
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "Login", bundle: nil)
        let viewcontroller : UIViewController = mainView.instantiateViewController(withIdentifier: "vcLogin") as UIViewController
        self.window!.rootViewController = viewcontroller
        
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
        var mainView: UIStoryboard!
        
        /* This code is to avoid duplicate call calendar popup */
        UserDefaults.standard.setValue(true, forKey: "setupForCalendarFirst")
        print("setupForCalendarFirst true")
        
        
        if let firstTimeUserOnBoarding = UserDefaults.standard.object(forKey: "firstTimeUserOnBoarding") {
            
            print("firstTimeUserOnBoarding \(firstTimeUserOnBoarding)")
            
            if let token = UserDefaults.standard.string(forKey: "Token") {
                
                print(token)
                print(token.characters.count)
                
                
                if(UserDefaults.standard.value(forKey: "Token") == nil || token.characters.count==0){
                    mainView = UIStoryboard(name: "Login", bundle: nil)
                    let viewcontroller : UIViewController = mainView.instantiateViewController(withIdentifier: "vcLogin") as UIViewController
                    self.window!.rootViewController = viewcontroller
                }
                else{
                    
                   // let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
                   // print("printisOnline \(isOnline)")
                    //                    if(isOnline){
                    //                        self.turnOnLocationTracker()
                    //                    }else{
                    //                        self.turnOffLocationTracker()
                    //                    }
                    
                    UserDefaults.standard.register(defaults: ["Relogin" : false])
                    print("User Already Login \(UserDefaults.standard.value(forKey: "Token")!)")
                    mainView = UIStoryboard(name: "Main", bundle: nil)
                    let viewcontroller : UIViewController = mainView.instantiateViewController(withIdentifier: "vcRoot") as UIViewController
                    self.window!.rootViewController = viewcontroller
                    
                }
                
            }
            else{
                mainView = UIStoryboard(name: "Login", bundle: nil)
                let viewcontroller : UIViewController = mainView.instantiateViewController(withIdentifier: "vcLogin") as UIViewController
                self.window!.rootViewController = viewcontroller
            }
            
        }
        else{
            print("firstTimeUserOnBoarding 111")
            
            window = UIWindow(frame: UIScreen.main.bounds)
            let mainController = OnBoardViewController() as UIViewController
            let navigationController = UINavigationController(rootViewController: mainController)
            navigationController.navigationBar.isTranslucent = false
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
            
        }
        
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        FirebaseApp.configure()
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.InstanceIDTokenRefresh,
                                               object: nil)
        
        IQKeyboardManager.sharedManager().enable = true
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        print("URLPathSqlite \(urls[urls.count-1] as URL)") // look for doc. directory
        
        UIApplication.shared.statusBarStyle = .lightContent
        
      //  locationManager = CLLocationManager()
      //  locationManager?.requestWhenInUseAuthorization()
        
        
        return true
    }
    
    func trackingLocation(){
        if let firstTimeUserOnBoarding = UserDefaults.standard.object(forKey: "isOnline"){
            let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
            print("printisOnlineLaunchOptions \(isOnline)")
            if(isOnline){
                self.turnOnLocationTracker()
            }else{
                self.turnOffLocationTracker()
            }
        }
    }
    
    func turnOffLocationTracker(){
        backGround.stopTimer()
        print("stopTimer")
    }
    
    func turnOnLocationTracker(){
        backGround.viewDidLoad()
    }
    
    
    
    //Update for Use Core Data
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "modeldb")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID 11: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID 22: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    // [START refresh_token]
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    
    // [START connect_to_fcm]
    func connectToFcm() {
        // Won't connect since there is no token
        guard InstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().disconnect()
        
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    // [END connect_to_fcm]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    
    // [START connect_on_active]
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
       // trackingLocation()
    }
    
    
    // [END connect_on_active]
    
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().disconnect()
        print("Disconnected from FCM.")
        
        
        
        
    }
    // [END disconnect_from_fcm]
    
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID 33: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        
        if let aps = userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                
                
                
                let message = alert["body"] as? String
                
                let titleString = alert["title"] as? String
                
                let warning = MessageView.viewFromNib(layout: .cardView)
                warning.configureTheme(.warning)
                warning.configureDropShadow()
                
                warning.configureContent(title: titleString!, body: message!, iconText: "")
                warning.button?.isHidden = true
                var warningConfig = SwiftMessages.defaultConfig
                warningConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
                
                SwiftMessages.show(config: warningConfig, view: warning)
                
                
            } else if let alert = aps["alert"] as? NSString {
                //Do stuff
                print(alert)
            }
        }
        
        
        
        
        
        // Change this to your preferred presentation option
        completionHandler([])
        
        
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RELOAD_JOB_AFTER_NOTIFICATION_MAIN"), object: nil)
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        
        if let message = userInfo["order_id"] as? NSString {
            print("Message Order ID : \(message)")
            
            
            if let orderNumber = userInfo["reference_no"] as? NSString {
                print("Message reference_no : \(orderNumber)")
                
                UserDefaults.standard.setValue(true, forKey: "fromPushNotification")
                UserDefaults.standard.setValue(Int(message as String), forKey: "orderIDPushNotification")
                UserDefaults.standard.setValue(orderNumber as String, forKey: "orderNumberPushNotification")
                
                var mainView: UIStoryboard!
                
                mainView = UIStoryboard(name: "Main", bundle: nil)
                let viewcontroller : UIViewController = mainView.instantiateViewController(withIdentifier: "vcRoot") as UIViewController
                self.window!.rootViewController = viewcontroller
                
//                let storyboard = UIStoryboard(name: "Assigned", bundle: nil)
//                let levelController = storyboard.instantiateViewController(withIdentifier: "Assigned") as! AssignedViewController
//                levelController.fromPushNotification = true
//                levelController.navigationController?.setNavigationBarHidden(false, animated: false)
//                levelController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
//                // window = UIWindow(frame: UIScreen.main.bounds)
//                self.window?.rootViewController = levelController;
                // self.window?.rootViewController?.navigationController?.present(levelController, animated: true) {() -> Void in }
                
                
                //                if let controller = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailsManyStep") as? OrderDetailsManyStepViewController {
                //                    if let window = self.window, let rootViewController = window.rootViewController {
                //                        var currentController = rootViewController
                //                        controller.orderID = Int(message as String)
                //                        controller.orderNumber = orderNumber as String
                //                        while let presentedController = currentController.presentedViewController {
                //                            currentController = presentedController
                //                        }
                //                        currentController.present(controller, animated: true, completion: nil)
                //                    }
                //                }
                
                //                let storyboard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
                //
                //                let destinationViewController = storyboard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as! OrderDetailsManyStepViewController
                //
                //                destinationViewController.orderID = Int(message as String)
                //                destinationViewController.orderNumber = orderNumber as String
                //                let navigationController = self.window?.rootViewController as! UIViewController
                //
                //                navigationController.showDetailViewController(destinationViewController, sender: Any?.self)
                
                
                
                //                //  Converted to Swift 4 by Swiftify v4.1.6669 - https://objectivec2swift.com/
                //                var storyBoard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
                //                var viewcontroller = storyBoard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as? OrderDetailsManyStepViewController
                //                var navController: UINavigationController? = nil
                //                if let aViewcontroller = viewcontroller {
                //                    navController = UINavigationController(rootViewController: aViewcontroller)
                //                }
                
                //                let mainStoryboard: UIStoryboard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
                //                let destinationViewController = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as! OrderDetailsManyStepViewController
                //                destinationViewController.orderID = Int(message as String)
                //                destinationViewController.orderNumber = orderNumber as String
                //                var rootViewController = self.window!.rootViewController as! UIViewController
                //                rootViewController.pushViewController(destinationViewController, animated: true)
                
                
                
                //                var navigationController = window?.rootViewController as? UINavigationController
                //                var mainStoryboard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
                //                var controller = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as? OrderDetailsManyStepViewController
                //                controller?.orderID = Int(message as String)
                //                controller?.orderNumber = orderNumber as String
                //
                //                if let aController = controller {
                //                    navigationController?.pushViewController(aController, animated: true)
                //                }
                
                //                var rootViewController = self.window!.rootViewController as! UINavigationController
                //                let mainStoryboard: UIStoryboard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
                //                var profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as! OrderDetailsManyStepViewController
                //                rootViewController.pushViewController(profileViewController, animated: true)
                
                
                
            }
            
        }
        
        
        completionHandler()
    }
}


// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
extension AppDelegate : MessagingDelegate {
    /// This method will be called whenever FCM receives a new, default FCM token for your
    /// Firebase project's Sender ID.
    /// You can send this token to your application server to send notifications to this device.
    public func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    
    // Receive data message on iOS 10 devices while app is in the foreground.
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
        
    }
}
// [END ios_10_data_message_handling]








