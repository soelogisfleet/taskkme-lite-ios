//
//  JobCardTableViewCell.swift
//  LEEWAY
//
//  Created by Coolasia on 13/9/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import MarqueeLabel
import M13Checkbox

class JobCardTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var btn_callDropOff: UIButton!
    @IBOutlet weak var lbl_dropOffPic: UILabel!
    @IBOutlet weak var lbl_orderStatus: UILabel!
    @IBOutlet weak var lbl_dropOffDate: UILabel!
    @IBOutlet weak var lbl_doReferenceNumber: UILabel!
    @IBOutlet weak var lbl_customerName: UILabel!
    @IBOutlet weak var layoutCustomerName: UIView!
    @IBOutlet weak var cardJob: UIView!
    @IBOutlet weak var lbl_dropOffAdrress: UILabel!
    @IBOutlet weak var lbl_step_status: UILabel!

    @IBOutlet weak var lbl_dropOffTime: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        let path = UIBezierPath(roundedRect:layoutCustomerName.bounds,
//                                byRoundingCorners:[.topLeft, .topRight],
//                                cornerRadii: CGSize(width: 10, height:  10))
//        
//        let maskLayer = CAShapeLayer()
//        
//        maskLayer.path = path.cgPath
//        layoutCustomerName.layer.mask = maskLayer
        
        
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        //backgroundColor = Colors.colorClear
        
        self.cardJob.layer.borderWidth = 1
        self.cardJob.layer.cornerRadius = 10
        self.cardJob.layer.borderColor = UIColor.clear.cgColor
        self.cardJob.layer.masksToBounds = true
        
        self.layer.shadowOpacity = 0.20
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 8
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
