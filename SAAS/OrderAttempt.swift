//
//  OrderAttempt.swift
//  WMG
//
//  Created by jing jie wong on 23/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit


class OrderAttempt: NSObject {
    
    var id: Int?
    var receivedBy: String?
    var reason: String?
    var note: String?
    var submittedTime: String?
    var latitude: Double?
    var longitude: Double?
    var orderAttemptImageList: [OrderAttemptImage] = []
    
    //add new field param
    var nric: String?
    var srno: String?
    
    override init() {
        
        self.id = 0
        self.receivedBy = ""
        self.reason = ""
        self.note = ""
        self.submittedTime = ""
        self.latitude = 0
        self.longitude = 0
        self.nric = ""
        self.srno = ""
        super.init()

    }

    init(id: Int?,receivedBy: String?, reason: String?,note:String?,submittedTime: String?,latitude: Double?,longitude:Double?,orderAttemptImageList: [OrderAttemptImage]?, nric:String?, srno:String?) {
        self.id = id
        self.receivedBy = receivedBy
        self.reason = reason
        self.note = note
        self.submittedTime = submittedTime
        self.latitude = latitude
        self.longitude = longitude
        self.orderAttemptImageList = orderAttemptImageList!
        self.nric = nric
        self.srno = srno
        super.init()
    }
    
    func toJson()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        //orderAttemptDic.setValue(self.id, forKey: "id")
        orderAttemptDic.setValue(self.receivedBy, forKey: "received_by")
        orderAttemptDic.setValue(self.reason, forKey: "reason")
        orderAttemptDic.setValue(self.note, forKey: "note")
        orderAttemptDic.setValue(self.latitude, forKey: "latitude")
        orderAttemptDic.setValue(self.longitude, forKey: "longitude")
      //  orderAttemptDic.setValue(self.nric, forKey: "nric")
       // orderAttemptDic.setValue(self.srno, forKey: "srno")
        
        let orderAttemptImageArray:NSMutableArray = NSMutableArray()

        for orderAttemptImage in self.orderAttemptImageList {
            
            let orderAttemptImageDic:NSMutableDictionary = NSMutableDictionary()
            orderAttemptImageDic.setValue(orderAttemptImage.mUrl, forKey: "image_url")
            orderAttemptImageDic.setValue(orderAttemptImage.isSignature, forKey: "is_signature")
            orderAttemptImageDic.setValue(orderAttemptImage.mNote, forKey: "description")

          //  orderAttemptImageArray.add(orderAttemptImage.mBase64!)
            orderAttemptImageArray.add(orderAttemptImageDic)
        }
        
       // orderAttemptDic.setValue(orderAttemptImageArray, forKey: "order_attempt_image_base64")
        orderAttemptDic.setValue(orderAttemptImageArray, forKey: "order_attempt_image")
        
        return orderAttemptDic

    }
    
    
    func toJsonWithIDRepeatImage()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        orderAttemptDic.setValue(self.id, forKey: "order_attempt_id")
        
        let orderAttemptImageArray:NSMutableArray = NSMutableArray()
        
        for orderAttemptImage in self.orderAttemptImageList {
            
            let orderAttemptImageDic:NSMutableDictionary = NSMutableDictionary()
            orderAttemptImageDic.setValue(orderAttemptImage.mBase64, forKey: "raw")
            orderAttemptImageDic.setValue(orderAttemptImage.isSignature, forKey: "is_signature")
            orderAttemptImageDic.setValue(orderAttemptImage.mNote, forKey: "description")
            
            //  orderAttemptImageArray.add(orderAttemptImage.mBase64!)
            orderAttemptImageArray.add(orderAttemptImageDic)
        }
        
        orderAttemptDic.setValue(orderAttemptImageArray, forKey: "order_attempt_image_base64")
        
        return orderAttemptDic
        
    }
    
    func toJsonUploadPhotoOnly()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        let orderAttemptImageArray:NSMutableArray = NSMutableArray()
        
        for orderAttemptImage in self.orderAttemptImageList {
            
            let orderAttemptImageDic:NSMutableDictionary = NSMutableDictionary()
            orderAttemptImageDic.setValue(orderAttemptImage.isSignature, forKey: "is_signature")
            orderAttemptImageDic.setValue(orderAttemptImage.mNote, forKey: "description")
            orderAttemptImageDic.setValue(orderAttemptImage.mBase64, forKey: "raw")
            
            orderAttemptImageArray.add(orderAttemptImageDic)
        }
        
        orderAttemptDic.setValue(orderAttemptImageArray, forKey: "order_attempt_image_base64")
        
        return orderAttemptDic
        
    }
    
    func toJsonEditDetailOnly()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        orderAttemptDic.setValue(self.id, forKey: "order_attempt_id")
        orderAttemptDic.setValue(self.receivedBy, forKey: "received_by")
  //      orderAttemptDic.setValue(self.reason, forKey: "reason")
        orderAttemptDic.setValue(self.note, forKey: "note")
        orderAttemptDic.setValue(self.latitude, forKey: "latitude")
        orderAttemptDic.setValue(self.longitude, forKey: "longitude")
   //     orderAttemptDic.setValue(self.nric, forKey: "nric")
    //    orderAttemptDic.setValue(self.srno, forKey: "srno")
        
        return orderAttemptDic
        
    }
    
    func toJsonRemoveImage()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        orderAttemptDic.setValue(self.id, forKey: "order_attempt_id")
        
        let orderAttemptImageArray:NSMutableArray = NSMutableArray()
        
        for orderAttemptImage in self.orderAttemptImageList {
            
            let orderAttemptImageDic:NSMutableDictionary = NSMutableDictionary()
            orderAttemptImageDic.setValue(orderAttemptImage.id, forKey: "attempt_image_id")
            orderAttemptImageDic.setValue(orderAttemptImage.isRemove, forKey: "remove")
            orderAttemptImageDic.setValue(orderAttemptImage.isSignature, forKey: "is_signature")
            
            //  orderAttemptImageArray.add(orderAttemptImage.mBase64!)
            orderAttemptImageArray.add(orderAttemptImageDic)
        }
        
        orderAttemptDic.setValue(orderAttemptImageArray, forKey: "order_attempt_image")
        
        return orderAttemptDic
        
    }
    
    func toJsonEditDescriptionOnly()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        orderAttemptDic.setValue(self.id, forKey: "order_attempt_id")
        
        let orderAttemptImageArray:NSMutableArray = NSMutableArray()
        
        for orderAttemptImage in self.orderAttemptImageList {
            
            let orderAttemptImageDic:NSMutableDictionary = NSMutableDictionary()
            orderAttemptImageDic.setValue(orderAttemptImage.id, forKey: "attempt_image_id")
            orderAttemptImageDic.setValue(orderAttemptImage.isRemove, forKey: "remove")
            orderAttemptImageDic.setValue(orderAttemptImage.mNote, forKey: "description")
            
            //  orderAttemptImageArray.add(orderAttemptImage.mBase64!)
            orderAttemptImageArray.add(orderAttemptImageDic)
        }
        
        orderAttemptDic.setValue(orderAttemptImageArray, forKey: "order_attempt_image_base")
        
        return orderAttemptDic
        
    }
    
    func toJsonEditDescriptionAndRemoveImage()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        orderAttemptDic.setValue(self.id, forKey: "order_attempt_id")
        
        let orderAttemptImageArray:NSMutableArray = NSMutableArray()
        
        for orderAttemptImage in self.orderAttemptImageList {
            
            let orderAttemptImageDic:NSMutableDictionary = NSMutableDictionary()
            orderAttemptImageDic.setValue(orderAttemptImage.id, forKey: "attempt_image_id")
            orderAttemptImageDic.setValue(orderAttemptImage.isRemove, forKey: "remove")
            orderAttemptImageDic.setValue(orderAttemptImage.mNote, forKey: "description")
            orderAttemptImageDic.setValue(orderAttemptImage.isSignature, forKey: "is_signature")
            
            //  orderAttemptImageArray.add(orderAttemptImage.mBase64!)
            orderAttemptImageArray.add(orderAttemptImageDic)
        }
        
        orderAttemptDic.setValue(orderAttemptImageArray, forKey: "order_attempt_image_base")
        
        return orderAttemptDic
        
    }
    
    
    func toJsonUploadPhotoOnlyWithURL()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        orderAttemptDic.setValue(self.id, forKey: "order_attempt_id")
        
        let orderAttemptImageArray:NSMutableArray = NSMutableArray()
        
        for orderAttemptImage in self.orderAttemptImageList {
            
            let orderAttemptImageDic:NSMutableDictionary = NSMutableDictionary()
            orderAttemptImageDic.setValue(orderAttemptImage.isSignature, forKey: "is_signature")
            orderAttemptImageDic.setValue(orderAttemptImage.mNote, forKey: "description")
            orderAttemptImageDic.setValue(orderAttemptImage.mUrl, forKey: "image_url")
            
            orderAttemptImageArray.add(orderAttemptImageDic)
        }
        
        orderAttemptDic.setValue(orderAttemptImageArray, forKey: "order_attempt_image")
        
        return orderAttemptDic
        
    }
    
    
    
    
    
}


extension OrderAttempt {
    convenience init?(json: JSON) {
        
        
        
        let id: Int? = json["id"].int
        let receivedBy: String? = json["received_by"].string
        let reason: String? = json["reason"].string
        let note: String? = json["note"].string
        let submittedTime: String? = json["submitted_time"].string
        let latitude: Double? = json["latitude"].double
        let longitude: Double? = json["longitude"].double
        let nric: String? = json["nric"].string
        let srno: String? = json["srno"].string
        
        var orderAttemptImageList: [OrderAttemptImage] = []
        
        if let items = json["order_attempts_images"].array {
            for item in items {
                if let orderAttemptImage = OrderAttemptImage(json: item) {
                    
                    orderAttemptImageList.append(orderAttemptImage)
                }

            }
        }
    
        
        
        self.init(id: id,receivedBy: receivedBy, reason: reason,note:note,submittedTime: submittedTime,latitude: latitude,longitude:longitude,orderAttemptImageList:orderAttemptImageList,nric:nric,srno:srno)
        
    }
}




