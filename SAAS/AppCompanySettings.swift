//
//  AppCompanySettings.swift
//  SAAS
//
//  Created by Coolasia on 22/1/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit


class AppCompanySettings: NSObject {
    
    var id: Int?
    var rule: String?
    var jsonData: String?
    var enable: Bool?
    var current_status_general_id: Int?
    var to_status_general_id: Int?
    var is_man_power_worker: Bool?
    var is_scan_required: Bool?
    
    override init() {
        
        self.id = 0
        self.rule = ""
        self.jsonData = ""
        self.enable = false
        self.current_status_general_id = 0
        self.to_status_general_id = 0
        self.is_man_power_worker = false
        self.is_scan_required = false
        
        super.init()
        
    }
    
    init(id: Int?, rule: String?, jsonData: String?, enable: Bool?, current_status_general_id: Int?, to_status_general_id: Int?, is_man_power_worker:Bool?, is_scan_required: Bool? ) {
        
        self.id = id
        self.rule = rule
        self.jsonData = jsonData
        self.enable = enable
        self.current_status_general_id = current_status_general_id
        self.to_status_general_id = to_status_general_id
        self.is_man_power_worker = is_man_power_worker
        self.is_scan_required = is_scan_required
        
        super.init()
    }
    
}


extension AppCompanySettings {
    convenience init?(json: JSON) {
        
        let id: Int? = json["id"].int
        let rule: String? = json["rule"].string
        let jsonData: String? = json["json_data"].string
        let enable: Bool? = json["enable"].bool
        let current_status_general_id: Int? = json["current_status_general_id"].int
        let to_status_general_id: Int? = json["to_status_general_id"].int
        let is_man_power_worker: Bool? = json["is_man_power_worker"].bool
        let is_scan_required: Bool? = json["is_scan_required"].bool
        
        self.init(id: id, rule:rule, jsonData:jsonData, enable:enable, current_status_general_id:current_status_general_id, to_status_general_id:to_status_general_id, is_man_power_worker:is_man_power_worker, is_scan_required:is_scan_required)
        
    }
}

