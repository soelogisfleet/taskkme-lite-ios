//
//  StepsListTableViewCell.swift
//  SAAS
//
//  Created by Coolasia on 5/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit
import MarqueeLabel

class StepsListTableViewCell: UITableViewCell {

    @IBOutlet weak var topLine: UIView!
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var stepNumber: UIButton!
    @IBOutlet weak var stepName: UILabel!
    @IBOutlet weak var stepAddress: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        stepNumber.layer.cornerRadius = stepNumber.frame.width/2
        stepNumber.layer.masksToBounds = true
  
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        stepNumber.layer.cornerRadius = stepNumber.frame.width/2
        stepNumber.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
