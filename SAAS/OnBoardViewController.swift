//
//  OnBoardViewController.swift
//  SAAS
//
//  Created by Coolasia on 18/3/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import Foundation
import UIKit
import paper_onboarding
import AVFoundation
import Photos
import CoreLocation
import SCLAlertView

class OnBoardViewController: UIViewController, CLLocationManagerDelegate {
    
    var totalPageCount = 6
    var doneLabel: UILabel!
    
    //create last page
    var oneLastThingLabel: UILabel!
    var descLastThingLabel: UILabel!
    var cameraDescLabel: UILabel!
    var cameraButton: UIButton!
    
    var photoDescLabel: UILabel!
    var photoButton: UIButton!
    
    var locationDescLabel: UILabel!
    var locationButton: UIButton!
    
    var allowCameraAccess: Bool!
    var allowPhotoAccess: Bool!
    var allowLocationAccess: Bool!
    
    
    var cameraPermission: Int!
    var photoPermission: Int!
    var locationPermission: Int!
    
    
    var firstClickCamera: Bool!
    var firstClickPhoto: Bool!
    var firstClickLocation: Bool!
    
    /// NSUserDefaults standardDefaults lazy var
    lazy var defaults:UserDefaults = {
        return .standard
    }()
    
    
    // MARK: - Various lazy managers
    lazy var locationManager:CLLocationManager = {
        let lm = CLLocationManager()
        lm.delegate = self
        return lm
    }()
    //create last page
    
   //@IBOutlet var skipButton: UIButton!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   skipButton.isHidden = true
        
        self.allowCameraAccess = false
        self.allowPhotoAccess = false
        self.allowLocationAccess = false
        
         self.firstClickCamera = true
         self.firstClickPhoto = true
         self.firstClickLocation = true
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        self.navigationController?.navigationBar.topItem?.title=""
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        
        
//        button = UIButton(frame: CGRect(x: 300, y: 500, width: 100, height: 50))
//        button.backgroundColor = .green
//        button.setTitle("DONE", for: .normal)
//        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
//        var button = UIButton(type: UIButtonType.system) as UIButton
//        button.backgroundColor = .green
//        button.frame = CGRect(x: 300, y: 500, width: 100, height: 50)
//        button.setTitle("DONE", for: .normal)
//        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        self.doneLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        self.doneLabel.center = CGPoint(x: 50, y: 500)
        self.doneLabel.center.x = self.view.center.x
        self.doneLabel.textAlignment = .center
        self.doneLabel.text = "DONE"
        self.doneLabel.translatesAutoresizingMaskIntoConstraints = false
        self.doneLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        self.doneLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.doneLabelAction(_:)))
        self.doneLabel.isUserInteractionEnabled = true
        self.doneLabel.addGestureRecognizer(tap)
        
        //Create last page
        self.oneLastThingLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 30))
        self.oneLastThingLabel.center = CGPoint(x: 50, y: 70)
        self.oneLastThingLabel.center.x = self.view.center.x
        self.oneLastThingLabel.textAlignment = .center
        self.oneLastThingLabel.font = OnBoardViewController.titleOneLast
        self.oneLastThingLabel.text = "One last thing..."
        self.oneLastThingLabel.translatesAutoresizingMaskIntoConstraints = false
        self.oneLastThingLabel.widthAnchor.constraint(equalToConstant: 400).isActive = true
        self.oneLastThingLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        self.descLastThingLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 60))
        self.descLastThingLabel.center = CGPoint(x: 50, y: 110)
        self.descLastThingLabel.center.x = self.view.center.x
        self.descLastThingLabel.textAlignment = .center
        self.descLastThingLabel.lineBreakMode = .byWordWrapping
        self.descLastThingLabel.numberOfLines = 2
        self.descLastThingLabel.font = OnBoardViewController.descriptionFont
        self.descLastThingLabel.text = "These features are required or else the application will not work."
        self.descLastThingLabel.translatesAutoresizingMaskIntoConstraints = false
        self.descLastThingLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        self.descLastThingLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        
        self.cameraDescLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 60))
        self.cameraDescLabel.center = CGPoint(x: 50, y: 170)
        self.cameraDescLabel.center.x = self.view.center.x
        self.cameraDescLabel.textAlignment = .center
        self.cameraDescLabel.lineBreakMode = .byWordWrapping
        self.cameraDescLabel.numberOfLines = 2
        self.cameraDescLabel.font = OnBoardViewController.descriptionFont
        self.cameraDescLabel.text = "Camera access to scan barcode & snap photos upon proof of delivery."
        self.cameraDescLabel.translatesAutoresizingMaskIntoConstraints = false
        self.cameraDescLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        self.cameraDescLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        
        
        self.cameraButton = UIButton(frame: CGRect(x: 50, y: 200, width: 400, height: 40))
        self.cameraButton.setTitle("ALLOW CAMERA ACCESS", for: .normal)
        self.cameraButton.addTarget(self, action: #selector(cameraAction), for: .touchUpInside)
        self.cameraButton.backgroundColor = .clear
        self.cameraButton.center.x = self.view.center.x
        self.cameraButton.setTitleColor(.blue, for: .normal)
        self.cameraButton.layer.cornerRadius = 15
        self.cameraButton.layer.borderWidth = 1
        self.cameraButton.layer.borderColor = UIColor.blue.cgColor
        self.cameraButton.translatesAutoresizingMaskIntoConstraints = false
        self.cameraButton.widthAnchor.constraint(equalToConstant: 400).isActive = true
        self.cameraButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        var statusPermissionCamera = statusCamera()
        if(statusPermissionCamera == 0){
            self.cameraButton.setTitle("ALLOW CAMERA ACCESS", for: .normal)
            self.cameraButton.backgroundColor = .clear
            self.cameraButton.setTitleColor(.blue, for: .normal)
            self.cameraButton.layer.borderColor = UIColor.blue.cgColor
        }else if(statusPermissionCamera == 1){
            self.cameraButton.setTitle("ALLOWED CAMERA ACCESS", for: .normal)
            self.cameraButton.backgroundColor = .blue
            self.cameraButton.setTitleColor(.white, for: .normal)
            self.cameraButton.layer.borderColor = UIColor.blue.cgColor
        }else if(statusPermissionCamera == -1){
            self.cameraButton.setTitle("DENIED CAMERA ACCESS", for: .normal)
            self.cameraButton.backgroundColor = .orange
            self.cameraButton.layer.borderColor = UIColor.orange.cgColor
            self.cameraButton.setTitleColor(.white, for: .normal)
        }
        
        
        
        self.photoDescLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 60))
        self.photoDescLabel.center = CGPoint(x: 50, y: 270)
        self.photoDescLabel.center.x = self.view.center.x
        self.photoDescLabel.textAlignment = .center
        self.photoDescLabel.lineBreakMode = .byWordWrapping
        self.photoDescLabel.numberOfLines = 2
        self.photoDescLabel.font = OnBoardViewController.descriptionFont
        self.photoDescLabel.text = "Gallery access to save photos taken by camera."
        self.photoDescLabel.translatesAutoresizingMaskIntoConstraints = false
        self.photoDescLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        self.photoDescLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.photoDescLabel.topAnchor.constraint(equalTo: cameraButton.bottomAnchor, constant: 3000)
        
        
        self.photoButton = UIButton(frame: CGRect(x: 50, y: 300, width: 250, height: 40))
        self.photoButton.setTitle("ALLOW PHOTOS ACCESS", for: .normal)
        self.photoButton.addTarget(self, action: #selector(photoAction), for: .touchUpInside)
        self.photoButton.backgroundColor = .clear
        self.photoButton.center.x = self.view.center.x
        self.photoButton.setTitleColor(.blue, for: .normal)
        self.photoButton.layer.cornerRadius = 15
        self.photoButton.layer.borderWidth = 1
        self.photoButton.layer.borderColor = UIColor.blue.cgColor
        self.photoButton.translatesAutoresizingMaskIntoConstraints = false
        self.photoButton.widthAnchor.constraint(equalToConstant: 250).isActive = true
        self.photoButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        var statusPermissionPhoto = checkPhotoLibraryPermission()
        if(statusPermissionPhoto == 0){
            self.photoButton.setTitle("ALLOW PHOTO ACCESS", for: .normal)
            self.photoButton.backgroundColor = .clear
            self.photoButton.setTitleColor(.blue, for: .normal)
            self.photoButton.layer.borderColor = UIColor.blue.cgColor
        }else if(statusPermissionPhoto == 1){
            self.photoButton.setTitle("ALLOWED PHOTO ACCESS", for: .normal)
            self.photoButton.backgroundColor = .blue
            self.photoButton.setTitleColor(.white, for: .normal)
            self.photoButton.layer.borderColor = UIColor.blue.cgColor
        }else if(statusPermissionPhoto == -1){
            self.photoButton.setTitle("DENIED PHOTO ACCESS", for: .normal)
            self.photoButton.backgroundColor = .orange
            self.photoButton.layer.borderColor = UIColor.orange.cgColor
            self.photoButton.setTitleColor(.white, for: .normal)
        }
        
    
        
        self.locationDescLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 60))
        self.locationDescLabel.center = CGPoint(x: 50, y: 380)
        self.locationDescLabel.center.x = self.view.center.x
        self.locationDescLabel.textAlignment = .center
        self.locationDescLabel.lineBreakMode = .byWordWrapping
        self.locationDescLabel.numberOfLines = 2
        self.locationDescLabel.font = OnBoardViewController.descriptionFont
        self.locationDescLabel.text = "Background tracking to notify admin your current location."
        self.locationDescLabel.translatesAutoresizingMaskIntoConstraints = false
        self.locationDescLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        self.locationDescLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.locationDescLabel.topAnchor.constraint(equalTo: photoButton.bottomAnchor, constant: 5555550)
        
        
        
        self.locationButton = UIButton(frame: CGRect(x: 50, y: 410, width: 280, height: 40))
        self.locationButton.setTitle("ALLOW LOCATION SERVICES", for: .normal)
        self.locationButton.addTarget(self, action: #selector(locationAction), for: .touchUpInside)
        self.locationButton.backgroundColor = .clear
        self.locationButton.center.x = self.view.center.x
        self.locationButton.setTitleColor(.blue, for: .normal)
        self.locationButton.layer.cornerRadius = 15
        self.locationButton.layer.borderWidth = 1
        self.locationButton.layer.borderColor = UIColor.blue.cgColor
        self.locationButton.translatesAutoresizingMaskIntoConstraints = false
        self.locationButton.widthAnchor.constraint(equalToConstant: 280).isActive = true
        self.locationButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
     
        
        let statusPermissionLocation = statusLocationAlways()
        if(statusPermissionLocation == 0){
            self.locationButton.setTitle("ALLOW LOCATION SERVICES", for: .normal)
            self.locationButton.backgroundColor = .clear
            self.locationButton.setTitleColor(.blue, for: .normal)
            self.locationButton.layer.borderColor = UIColor.blue.cgColor
        }else if(statusPermissionLocation == 1){
            self.locationButton.setTitle("ALLOWED LOCATION SERVICES", for: .normal)
            self.locationButton.backgroundColor = .blue
            self.locationButton.setTitleColor(.white, for: .normal)
            self.locationButton.layer.borderColor = UIColor.blue.cgColor
        }else if(statusPermissionLocation == -1){
            self.locationButton.setTitle("DENIED LOCATION SERVICES", for: .normal)
            self.locationButton.backgroundColor = .orange
            self.locationButton.layer.borderColor = UIColor.orange.cgColor
            self.locationButton.setTitleColor(.white, for: .normal)
        }
        
        //Create last page
        
        
        setupPaperOnboardingView()
        
        
        let stackView = createStackView(with: UILayoutConstraintAxis.vertical)
        
        stackView.addArrangedSubview(oneLastThingLabel)
        stackView.addArrangedSubview(descLastThingLabel)
       
        
        stackView.addArrangedSubview(cameraDescLabel)
        stackView.addArrangedSubview(cameraButton)
       
        
        stackView.addArrangedSubview(photoDescLabel)
        stackView.addArrangedSubview(photoButton)
        
        stackView.addArrangedSubview(locationDescLabel)
        stackView.addArrangedSubview(locationButton)
        stackView.addArrangedSubview(doneLabel)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
    //    stackView.setCustomSpacing(10.0, after: descLastThingLabel)
        
//        let bottomLocationButtonConstrain = photoDescLabel.topAnchor.constraint(equalTo: cameraButton.bottomAnchor, constant: 20)
//        bottomLocationButtonConstrain.isActive = true
//
//        let bottomLocationButtonConstrain1 = cameraDescLabel.topAnchor.constraint(equalTo: descLastThingLabel.bottomAnchor, constant: 20)
//        bottomLocationButtonConstrain1.isActive = true
        
       
        self.view.addSubview(stackView)
        
       
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        stackView.widthAnchor.constraint(equalToConstant: view.frame.width-40).isActive = true
        self.view = view
        
        
        
        //self.view.bringSubview(toFront: self.oneLastThingLabel)
//        self.view.addSubview(self.oneLastThingLabel)
//        self.view.addSubview(self.descLastThingLabel)
//
//        self.view.addSubview(self.cameraDescLabel)
//        self.view.addSubview(self.cameraButton)
//
//        self.view.addSubview(self.photoDescLabel)
//        self.view.addSubview(self.photoButton)
//
//        self.view.addSubview(self.locationDescLabel)
//        self.view.addSubview(self.locationButton)
//
//        self.view.addSubview(self.doneLabel)
        
        
////        self.view.addConstraint(NSLayoutConstraint(item: descLastThingLabel, attribute: .top, relatedBy: .equal, toItem: oneLastThingLabel, attribute: .bottom, multiplier: 1, constant: 34))
//
//        self.view.addConstraint(NSLayoutConstraint(item: cameraDescLabel, attribute: .top, relatedBy: .equal, toItem: descLastThingLabel, attribute: .bottom, multiplier: 1, constant: 34))
//
//        self.view.addConstraint(NSLayoutConstraint(item: cameraButton, attribute: .top, relatedBy: .equal, toItem: cameraDescLabel, attribute: .bottom, multiplier: 1, constant: 34))
//
//        self.view.addConstraint(NSLayoutConstraint(item: cameraButton, attribute: .top, relatedBy: .equal, toItem: cameraDescLabel, attribute: .bottom, multiplier: 1, constant: 34))
//
//        self.view.addConstraint(NSLayoutConstraint(item: doneLabel, attribute: .top, relatedBy: .equal, toItem: locationButton, attribute: .bottom, multiplier: 1, constant: 34))
//
//        // assuming here you have added the self.imageView to the main view and it was declared before.
//        self.oneLastThingLabel.translatesAutoresizingMaskIntoConstraints = true
//
//        // create the constraints with the constant value you want.
//        var verticalSpace = NSLayoutConstraint(item: self.descLastThingLabel, attribute: .top, relatedBy: .equal, toItem: self.oneLastThingLabel, attribute: .bottom, multiplier: 1, constant: 100)
//
//        // activate the constraints
//        NSLayoutConstraint.activate([verticalSpace])
//
//        let top = NSLayoutConstraint(item: self.oneLastThingLabel,
//                                     attribute: .bottom,
//                                     relatedBy: .equal,
//                                     toItem: self.descLastThingLabel,
//                                     attribute: .top,
//                                     multiplier: 1.0,
//                                     constant: 1000.0)
//        self.view.addConstraint(top)
//        NSLayoutConstraint.activate([top])


    }
    
    func createStackView(with layout: UILayoutConstraintAxis) -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = layout
        stackView.distribution = .equalSpacing
        stackView.spacing = 0
        return stackView
    }
    
    func doneLabelAction(_ recognizer: UITapGestureRecognizer) {
        
        print("Button tapped")
        var window: UIWindow? = UIApplication.shared.keyWindow
        var stryBoard = UIStoryboard(name: "Login", bundle: nil)
        window?.rootViewController = stryBoard.instantiateInitialViewController()
        
    }
    
    func showUIAlertDialog(permissionCameraPhotoLocation: Int){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("Settings") {
            print("YES button tapped")
            DispatchQueue.main.async {
                if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(settingsURL)
                }
            }
        }
        alertView.addButton("Cancel") {
            print("NO button tapped")
        }
        
        var subTitle = ""
        var titleString = ""
        
        switch permissionCameraPhotoLocation {
        case 1:
            subTitle = "Enable camera access will allow you to snap photos and use scanner barcode"
            titleString = "Camera Access"
        case 2:
            subTitle = "Enable photo access will allow you to use photos gallery as proof of delivery"
            titleString = "Photo Access"
        case 3:
            subTitle = "Enable location services will improve location accurancy"
            titleString = "Location Services"
        default:
            break
        }
        
        alertView.showEdit(titleString, subTitle: subTitle)
    }
    
    func checkPhotoLibraryPermission() -> Int {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            photoPermission = 1
            return photoPermission
        case .denied, .restricted :
             photoPermission = -1
            return photoPermission
        case .notDetermined:
            photoPermission = 0
            return photoPermission
        }
    }
    
    
    public func statusCamera() -> Int {
        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch status {
        case .authorized:
            cameraPermission = 1
            return cameraPermission
        case .restricted, .denied:
            cameraPermission = -1
            return cameraPermission
        case .notDetermined:
            cameraPermission = 0
            return cameraPermission
        }
    }
   
    
    func cameraAction(sender: UIButton!) {
        print("cameraAction tapped")
        
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo,
                                          completionHandler: { granted in
                                            if granted {
                                                print("Granted access to")
                                                DispatchQueue.main.async {
                                                    self.cameraButton.setTitle("ALLOWED CAMERA ACCESS", for: .normal)
                                                    self.allowCameraAccess = true
                                                    self.cameraButton.backgroundColor = .blue
                                                    self.cameraButton.layer.borderColor = UIColor.blue.cgColor
                                                    self.cameraButton.setTitleColor(.white, for: .normal)
                                                    
                                                    
                                                    if(self.firstClickCamera == false){
                                                        self.showUIAlertDialog(permissionCameraPhotoLocation: 1)
                                                    }
                                                    self.firstClickCamera = false
                                                    
                                                    
                                                    
//                                                    let alert = UIAlertController(title: "Error", message: "This app is not authorized to use Camera.", preferredStyle: .alert)
//
//                                                    alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
//                                                        DispatchQueue.main.async {
//                                                            if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
//                                                                UIApplication.shared.openURL(settingsURL)
//                                                            }
//                                                        }
//                                                    }))
//                                                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//                                                    self.present(alert, animated: true, completion: nil)
                                                }
                                                
                                            } else {
                                                print("Denied access to")
                                                DispatchQueue.main.async {
                                                    self.cameraButton.setTitle("DENIED CAMERA ACCESS", for: .normal)
                                                    self.allowCameraAccess = false
                                                    self.cameraButton.backgroundColor = .orange
                                                     self.cameraButton.layer.borderColor = UIColor.orange.cgColor
                                                    self.cameraButton.setTitleColor(.white, for: .normal)
                                                    
                                                    if(self.firstClickCamera == false){
                                                        self.showUIAlertDialog(permissionCameraPhotoLocation: 1)
                                                    }
                                                    self.firstClickCamera = false
                                                    
                                                    
                                                }
                                                
                                            }
            })
        
        
        
    }
    
    func photoAction(sender: UIButton!) {
        print("photoAction tapped")
        
        PHPhotoLibrary.requestAuthorization({ status in
            switch status {
            case .authorized:
                 print("cameraAction tapped")
                 
                 DispatchQueue.main.async {
                    
                    self.allowPhotoAccess = true
                    self.photoButton.setTitle("ALLOWED PHOTO ACCESS", for: .normal)
                    self.photoButton.backgroundColor = .blue
                    self.photoButton.setTitleColor(.white, for: .normal)
                    
                    if(self.firstClickPhoto == false){
                        self.showUIAlertDialog(permissionCameraPhotoLocation: 2)
                    }
                    self.firstClickPhoto = false
                    
                    
                    
                 }

            case .denied, .restricted:
                DispatchQueue.main.async {
                    
                    self.allowPhotoAccess = false
                    self.photoButton.setTitle("DENIED PHOTO ACCESS", for: .normal)
                    self.photoButton.backgroundColor = .orange
                    self.photoButton.setTitleColor(.white, for: .normal)
                    
                    
                    if(self.firstClickPhoto == false){
                        self.showUIAlertDialog(permissionCameraPhotoLocation: 2)
                    }
                    self.firstClickPhoto = false
                    
                    
                }
            case .notDetermined:
                 self.allowPhotoAccess = false
                 self.photoButton.backgroundColor = .clear
                 self.photoButton.setTitleColor(.blue, for: .normal)
                 self.photoButton.setTitle("ALLOW PHOTO ACCESS", for: .normal)
                
                 if(self.firstClickPhoto == false){
                    self.showUIAlertDialog(permissionCameraPhotoLocation: 2)
                 }
                 self.firstClickPhoto = false
            }
        })
        
    }
    
    public func statusLocationAlways() -> Int {
        guard CLLocationManager.locationServicesEnabled() else {
            return 0
        }
        
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways:
            locationPermission = 1
            return locationPermission
        case .restricted, .denied:
            locationPermission = -1
            return locationPermission
        case .authorizedWhenInUse:
            // Curious why this happens? Details on upgrading from WhenInUse to Always:
            // [Check this issue](https://github.com/nickoneill/PermissionScope/issues/24)
            if defaults.bool(forKey: ConstantBoard.NSUserDefaultsKeys.requestedInUseToAlwaysUpgrade) {
                locationPermission = 1
                return locationPermission
            } else {
                locationPermission = 0
                return locationPermission
            }
        case .notDetermined:
            locationPermission = 0
            return locationPermission
        }
    }
    
    
    func locationAction(sender: UIButton!) {
        print("locationAction tapped")
        
//        let hasAlwaysKey:Bool = !(Bundle.main
//            .object(forInfoDictionaryKey: ConstantBoard.InfoPlistKeys.locationAlways) != nil)
//        assert(hasAlwaysKey, ConstantBoard.InfoPlistKeys.locationAlways + " not found in Info.plist.")
        
        if (CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .denied){
            defaults.set(true, forKey: ConstantBoard.NSUserDefaultsKeys.requestedInUseToAlwaysUpgrade)
            defaults.synchronize()
            locationManager.requestAlwaysAuthorization()
            
            if(self.firstClickLocation == false){
                self.showUIAlertDialog(permissionCameraPhotoLocation: 3)
            }
            self.firstClickLocation = false
        }
        
        let status = statusLocationAlways()
        switch status {
        case 1:
            self.locationButton.setTitle("ALLOWED LOCATION SERVICES", for: .normal)
            self.locationButton.backgroundColor = .blue
            self.locationButton.setTitleColor(.white, for: .normal)
            self.locationButton.layer.borderColor = UIColor.blue.cgColor
        case -1:
            self.locationButton.setTitle("DENIED LOCATION SERVICES", for: .normal)
            self.locationButton.backgroundColor = .orange
            self.locationButton.layer.borderColor = UIColor.orange.cgColor
            self.locationButton.setTitleColor(.white, for: .normal)
        case 0:
            self.locationButton.setTitle("ALLOW LOCATION SERVICES", for: .normal)
            self.locationButton.backgroundColor = .clear
            self.locationButton.setTitleColor(.blue, for: .normal)
            self.locationButton.layer.borderColor = UIColor.blue.cgColor
        default:
            break
        }
        
    }
    
    private func setupPaperOnboardingView() {
        
        let onboarding = PaperOnboarding()
        onboarding.delegate = self
        onboarding.dataSource = self
        onboarding.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(onboarding)
        
        // Add constraints
        for attribute: NSLayoutAttribute in [.left, .right, .top, .bottom] {
            let constraint = NSLayoutConstraint(item: onboarding,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: 0)
            view.addConstraint(constraint)
        }
    }
}

// MARK: Actions
extension OnBoardViewController {
    
//    @IBAction func skipButtonTapped(_: UIButton) {
//        print(#function)
//    }
}

// MARK: PaperOnboardingDelegate
extension OnBoardViewController: PaperOnboardingDelegate {
    
    func onboardingWillTransitonToIndex(_ index: Int) {
     //   skipButton.isHidden = index == 2 ? false : true
        print("index is \(index)")
        
        if(index == 0){
            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.navigationBar.isHidden = true
//            self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        }else if(index == 1){
            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.navigationBar.isHidden = true
//            self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:"4BB793")
        }else if(index == 2){
            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.navigationBar.isHidden = true
//            self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:"8962B3")
        }else if(index == 3){
            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.navigationBar.isHidden = true
//            self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:"F8AA60")
        }else if(index == 4){
            UIApplication.shared.statusBarStyle = .lightContent
            self.navigationController?.navigationBar.isHidden = true
//            self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:"E06060")
        }else if(index == 5){
            UIApplication.shared.statusBarStyle = .default
            self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
            var window: UIWindow? = UIApplication.shared.keyWindow
            var stryBoard = UIStoryboard(name: "PermissionOnBoarding", bundle: nil)
            window?.rootViewController = stryBoard.instantiateInitialViewController()
        }
        
    }
    
    func onboardingDidTransitonToIndex(_: Int) {
    }
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
        //item.titleLabel?.backgroundColor = .redColor()
        //item.descriptionLabel?.backgroundColor = .redColor()
        //item.imageView = ...
        print("index22 is \(index)")
        if(index+1 == self.totalPageCount){
            self.doneLabel.isHidden = false
            self.oneLastThingLabel.isHidden = false
            self.descLastThingLabel.isHidden = false
            self.cameraDescLabel.isHidden = false
            self.cameraButton.isHidden = false
            self.photoDescLabel.isHidden = false
            self.photoButton.isHidden = false
            self.locationDescLabel.isHidden = false
            self.locationButton.isHidden = false
           
            print("isHidden is \(false)")
        }else{
            self.doneLabel.isHidden = true
            self.oneLastThingLabel.isHidden = true
            self.descLastThingLabel.isHidden = true
            self.cameraDescLabel.isHidden = true
            self.cameraButton.isHidden = true
            self.photoDescLabel.isHidden = true
            self.photoButton.isHidden = true
            self.locationDescLabel.isHidden = true
            self.locationButton.isHidden = true
            print("isHidden is \(true)")
        }
        
        
        
    }
}

// MARK: PaperOnboardingDataSource
extension OnBoardViewController: PaperOnboardingDataSource {
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        return [OnboardingItemInfo(informationImage: UIImage(named: "onboarding-logo")!,
                                   title: "Welcome!",
                                   description: "Here's a brief intro before you get started",
                                   pageIcon: UIImage(named: "nav-step1")!,
                                   color: Constants.hexStringToUIColor(hex:"4C84BF"),
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: OnBoardViewController.titleFont,
                                   descriptionFont: OnBoardViewController.descriptionFont),
                
                OnboardingItemInfo(informationImage: UIImage(named: "phone-2")!,
                                   title: "Daily Jobs",
                                   description: "Jobs are given out by your administrator. If the job does not appear for that day, click the refresh button or contact your administrator.",
                                   pageIcon: UIImage(named: "nav-step2")!,
                                   color: Constants.hexStringToUIColor(hex:"4BB793"),
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: OnBoardViewController.titleFont,
                                   descriptionFont: OnBoardViewController.descriptionFont),
                
                OnboardingItemInfo(informationImage: UIImage(named: "phone-3")!,
                                   title: "Job with steps",
                                   description: "If the job have stops or detours in between, it will break down to several steps before you could finish that job.",
                                   pageIcon: UIImage(named: "nav-step3")!,
                                   color: Constants.hexStringToUIColor(hex:"8962B3"),
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: OnBoardViewController.titleFont,
                                   descriptionFont: OnBoardViewController.descriptionFont),
                
                OnboardingItemInfo(informationImage: UIImage(named: "phone-4")!,
                                   title: "Proof Of Delivery",
                                   description: "Upon completing the job, you are required to get the recipient's signature, if required. Barcode scanning or photos are optional.",
                                   pageIcon: UIImage(named: "nav-step4")!,
                                   color: Constants.hexStringToUIColor(hex:"F8AA60"),
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: OnBoardViewController.titleFont,
                                   descriptionFont: OnBoardViewController.descriptionFont),
                
                OnboardingItemInfo(informationImage: UIImage(named: "phone-5")!,
                                   title: "Availability Status",
                                   description: "You can turn off your availability after work from the menu. This will stop the location tracking until you activate it back next time. If you are offline mode, you cannot update jobs.",
                                   pageIcon: UIImage(named: "nav-step5")!,
                                   color: Constants.hexStringToUIColor(hex:"E06060"),
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: OnBoardViewController.titleFont,
                                   descriptionFont: OnBoardViewController.descriptionFont),
                
                OnboardingItemInfo(informationImage: UIImage(named: "blank")!,
                                   title: "",
                                   description: "",
                                   pageIcon: UIImage(named: "blank")!,
                                   color: Constants.hexStringToUIColor(hex:"ffffff"),
                                   titleColor: UIColor.white,
                                   descriptionColor: UIColor.white,
                                   titleFont: OnBoardViewController.titleFont,
                                   descriptionFont: OnBoardViewController.descriptionFont)][index]
    }
    
 
    
    func onboardingItemsCount() -> Int {
        return self.totalPageCount
    }
    
    //    func onboardinPageItemRadius() -> CGFloat {
    //        return 2
    //    }
    //
    //    func onboardingPageItemSelectedRadius() -> CGFloat {
    //        return 10
    //    }
    //    func onboardingPageItemColor(at index: Int) -> UIColor {
    //        return [UIColor.white, UIColor.red, UIColor.green][index]
    //    }
}


//MARK: Constants
extension OnBoardViewController {
    
    public static let titleFont = UIFont.boldSystemFont(ofSize: 24.0)
    public static let titleOneLast = UIFont.boldSystemFont(ofSize: 24.0)
    public static let descriptionFont = UIFont.systemFont(ofSize: 14.0)
    
//    public static let titleFont = UIFont(name: "Nunito-Bold", size: 36.0) ?? UIFont.boldSystemFont(ofSize: 36.0)
//    public static let titleOneLast = UIFont(name: "Nunito-Bold", size: 26.0) ?? UIFont.boldSystemFont(ofSize: 26.0)
//    public static let descriptionFont = UIFont(name: "OpenSans-Regular", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
}
