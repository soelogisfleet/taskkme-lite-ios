//
//  ListOfCustomer_ViewController.swift
//  SAAS
//
//  Created by Coolasia on 27/6/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON

class ListOfCustomer_ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var heightConstraintTableView: NSLayoutConstraint!
    
    var countRow: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundView = nil;
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON ORDER ID ======== */
    func getOrderDetails(){
        
        SwiftSpinner.show("Getting List of Customer...")
        
        let message   = ""
        
        /* ====== FETCH JOB ======= */
        AlamofireRequest.fetchDetailsOfJob(url: message) { responseObject, statusCode, error in
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    if let job = Job(json: swiftyJsonVar) {
                        
                        self.tableView.reloadData()
                        //  self.sortOrderSequenceList()
                    }
                    
                    
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    
                }
            }
        }
        /* ====== FETCH JOB ======= */
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON ORDER ID ======== */
    
    /* ======== TABLE VIEW STEP LIST ====== */
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraintTableView.constant = tableView.contentSize.height
        tableView.layoutIfNeeded()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension;
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension
    }
    
    
    // TableView
    //MARK: - Tableview Delegate & Datasource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        
        //countRow =  self.jobdetail.orderDetailList.count
        return countRow!
        
        
    }
    private func numberOfSectionsInTableView(tableViewOrderItem: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:ListItemOrderTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ListItemOrderTableViewCell") as! ListItemOrderTableViewCell
        
//        print("Item \(self.jobdetail.orderDetailList[indexPath.row].descriptions)")
//        print("remarks \(self.jobdetail.orderDetailList[indexPath.row].remarks)")
//        print("quantity \(self.jobdetail.orderDetailList[indexPath.row].quantity)")
//
//        if(self.jobdetail.orderDetailList[indexPath.row].descriptions == ""){
//            cell.itemName.text = "Unknown item name"
//        }else{
//            cell.itemName.text = self.jobdetail.orderDetailList[indexPath.row].descriptions
//        }
        
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    /* ======== TABLE VIEW STEP LIST ====== */
    

}
