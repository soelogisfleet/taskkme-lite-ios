//
//  JobSteps.swift
//  SAAS
//
//  Created by Coolasia on 6/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit


class JobSteps: NSObject {
    
    var order_id: Int?
    var id: Int?
    var worker_id: Int?
    var location: String?
    var latitude: Double?
    var longitude: Double?
    var updated_at: String?
    var order_sequence: Int?
    var job_step_status_id: Int?
    var job_step_type_id: Int?
    var descriptions: String?
    var job_step_name: String?
    var job_step_pic: String?
    var job_step_pic_contact: String?
    var is_signature_required: Bool?
    var is_scan_required: Bool?
    var order_detail_ids: String?
    var jobStepStatus = JobStepStatus()
    var orderAttemptList: [OrderAttempt] = []
    
    
    override init() {
        
        self.order_id = 0
        self.id = 0
        self.worker_id = 0
        self.location = ""
        self.latitude = 0.0
        self.longitude = 0.0
        self.updated_at = ""
        self.order_sequence = 0
        self.job_step_status_id = 0
        self.job_step_type_id = 0
        self.descriptions = ""
        self.job_step_name = ""
        self.job_step_pic = ""
        self.job_step_pic_contact = ""
        self.is_signature_required = false
        self.is_scan_required = false
        self.order_detail_ids = ""
        
        super.init()
        
    }
    
    init(order_id: Int?, id: Int?, worker_id:Int?, location: String?, latitude:Double?, longitude:Double?, updated_at:String?, order_sequence: Int?, job_step_status_id: Int?, job_step_type_id: Int?,
         descriptions:String?, job_step_name:String?, job_step_pic:String?, job_step_pic_contact:String?,is_signature_required:Bool?, is_scan_required:Bool?, order_detail_ids:String?, jobStepStatus: JobStepStatus,orderAttemptList:[OrderAttempt] ) {
        
        self.order_id = order_id
        self.id = id
        self.worker_id = worker_id
        self.location = location
        self.latitude = latitude
        self.longitude = longitude
        self.updated_at = updated_at
        self.order_sequence = order_sequence
        self.job_step_status_id = job_step_status_id
        self.job_step_type_id = job_step_type_id
        self.descriptions = descriptions
        self.job_step_name = job_step_name
        self.job_step_pic = job_step_pic
        self.job_step_pic_contact = job_step_pic_contact
        self.is_signature_required = is_signature_required
        self.is_scan_required = is_scan_required
        self.order_detail_ids = order_detail_ids
        self.jobStepStatus = jobStepStatus
        self.orderAttemptList = orderAttemptList
        
        
        super.init()
    }
    
    /* This func to initialize Json data need to be send. Used for update job step status only */
    func toJsonUpdateJobStepStatus()->NSMutableDictionary{
        let jobSteps:NSMutableDictionary = NSMutableDictionary()
        jobSteps.setValue(self.id, forKey: "job_step_id")
//        jobSteps.setValue(self.order_sequence, forKey: "order_sequence")
//        jobSteps.setValue(self.worker_id, forKey: "worker_id")
//        jobSteps.setValue(self.location, forKey: "location")
//        jobSteps.setValue(self.latitude, forKey: "latitude")
//        jobSteps.setValue(self.longitude, forKey: "longitude")
        jobSteps.setValue(self.job_step_status_id, forKey: "job_step_status_id")
//        jobSteps.setValue(self.job_step_type_id, forKey: "job_step_type_id")
//        jobSteps.setValue(self.descriptions, forKey: "description")
//        jobSteps.setValue(self.job_step_name, forKey: "job_step_name")
//        jobSteps.setValue(self.job_step_pic, forKey: "job_step_pic")
//        jobSteps.setValue(self.job_step_pic_contact, forKey: "job_step_pic_contact")
        

//        jobSteps.setValue(self.is_scan_required, forKey: "is_scan_required")

//        jobSteps.setValue(self.is_signature_required, forKey: "is_signature_required")
        
        return jobSteps
        
    }
    
    
//    func toJsonAddOrderItems()->NSMutableDictionary{
//        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
//        orderAttemptDic.setValue(self.descriptions, forKey: "description")
//        orderAttemptDic.setValue(self.quantity, forKey: "quantity")
//        orderAttemptDic.setValue(self.weight, forKey: "weight")
//        orderAttemptDic.setValue(self.remarks, forKey: "remarks")
//
//        return orderAttemptDic
//
//    }
    
}


extension JobSteps {
    convenience init?(json: JSON) {
        
        let order_id: Int? = json["order_id"].int
        let id: Int? = json["id"].int
        let worker_id: Int? = json["worker_id"].int
        
        var location: String? = json["location"].string
        if(location == nil){
            location = ""
        }
        
        var latitude: Double? = json["latitude"].double
        if(latitude == nil){
            latitude = 0.0
        }
        
        var longitude: Double? = json["longitude"].double
        if(longitude == nil){
            longitude = 0.0
        }
        
        var updated_at: String? = json["updated_at"].string
        if(updated_at == nil){
            updated_at = ""
        }
        
        let order_sequence: Int? = json["order_sequence"].int
        let job_step_status_id: Int? = json["job_step_status_id"].int
        let job_step_type_id: Int? = json["job_step_type_id"].int
        
        var descriptions: String? = json["description"].string
        if(descriptions == nil){
            descriptions = ""
        }
        
        var job_step_name: String? = json["job_step_name"].string
        if(job_step_name == nil){
            job_step_name = ""
        }
        
        var job_step_pic: String? = json["job_step_pic"].string
        if(job_step_pic == nil){
            job_step_pic = ""
        }
        
        var job_step_pic_contact: String? = json["job_step_pic_contact"].string
        if(job_step_pic_contact == nil){
            job_step_pic_contact = ""
        }
        
        let is_signature_required: Bool? = json["is_signature_required"].bool
        let is_scan_required: Bool? = json["is_scan_required"].bool
        let order_detail_ids: String? = json["order_detail_ids"].string
        
        var jobStepStatus = JobStepStatus()
        jobStepStatus = JobStepStatus(json: json["job_step_status"])!
        
        var orderAttemptList: [OrderAttempt] = []
        
        if let items = json["order_attempts"].array {
            for item in items {
                if let orderAttempt = OrderAttempt(json: item) {
                    print("order attempt 11\(String(describing: orderAttempt.id))")
                    orderAttemptList.append(orderAttempt)
                }
                
            }
        }
            
        
        self.init(order_id: order_id, id: id, worker_id: worker_id, location: location, latitude: latitude, longitude: longitude, updated_at: updated_at, order_sequence: order_sequence, job_step_status_id: job_step_status_id, job_step_type_id: job_step_type_id,
        descriptions: descriptions, job_step_name: job_step_name, job_step_pic: job_step_pic, job_step_pic_contact: job_step_pic_contact, is_signature_required: is_signature_required, is_scan_required: is_scan_required, order_detail_ids: order_detail_ids, jobStepStatus: jobStepStatus,orderAttemptList:orderAttemptList )
        
    }
}
