//
//
//
//
//  Created by lbxia on 15/10/21.
//  Copyright © 2015年 lbxia. All rights reserved.
//

#import "SubLBXScanViewController.h"
#import "LBXScanResult.h"
#import "LBXScanWrapper.h"
#import "LBXScanVideoZoomView.h"

@interface SubLBXScanViewController ()
@property (nonatomic, strong) LBXScanVideoZoomView *zoomView;
@end

@implementation SubLBXScanViewController

@synthesize delegate; //synthesise  MyClassDelegate delegate
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO animated:true];

//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//    }
    self.view.backgroundColor = [UIColor blackColor];
    
    
    //设置扫码后需要扫码图像
   // self.isNeedScanImage = YES;
    
//    UIBarButtonItem *barConfirm = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone target:self action:@selector(confirmScan:)];
    
    ////self.navigationItem.rightBarButtonItem = barConfirm;
    
    _flashTurnOn = false;
   
    UIBarButtonItem *barConfirm = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone target:self action:@selector(openFlash:)];
    
    
    // Flash button icon
    UIImage *imageFlash = [UIImage imageNamed:@"flash_off"];
    UIButton *myCustomButtonFlash = [UIButton buttonWithType:UIButtonTypeCustom];
    myCustomButtonFlash.bounds = CGRectMake( 0, 0, imageFlash.size.width, imageFlash.size.height );
    [myCustomButtonFlash setImage:imageFlash forState:UIControlStateNormal];
    [myCustomButtonFlash addTarget:self action:@selector(openFlash:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *buttonFlash = [[UIBarButtonItem alloc] initWithCustomView:myCustomButtonFlash];
    self.navigationItem.rightBarButtonItem = buttonFlash;
    // Flash button icon
    
    UIImage *image = [UIImage imageNamed:@"keyboard"];
    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    myCustomButton.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
    [myCustomButton setImage:image forState:UIControlStateNormal];
    [myCustomButton addTarget:self action:@selector(keyboardClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
   // UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
    _button = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
    self.navigationItem.rightBarButtonItem = _button;
    
    NSArray *tempArray2= [[NSArray alloc] initWithObjects:buttonFlash,_button,nil];
   // NSArray *tempArray2= [[NSArray alloc] initWithObjects:button,nil];
    
    self.navigationItem.rightBarButtonItems = tempArray2;
    
    
    self.navigationItem.title = @"Scan Barcode";
    
//   self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.0f];
//    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"trans"]];
//
//
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.opaque = NO;
    
    
////    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:10.0];
//
////    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
////    [[UINavigationBar appearance] setTranslucent:YES];
////    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:10.0]];
//
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"trans.png"]
//                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
//    self.navigationController.navigationBar.shadowImage = [UIImage imageNamed:@"trans.png"];////UIImageNamed:@"transparent.png"
//    self.navigationController.navigationBar.translucent = YES;
//
//    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.0];

    
    if(_orderScanList.count==0)
    {
        _orderScanList = [[NSMutableArray alloc] init];
    }
    else{
        _initialOrderScanList = [[NSMutableArray alloc] initWithArray:_orderScanList copyItems:YES];

    }
    NSLog(@"inital order scan list count 00: %ld, istransferboolean: %hhu",_initialOrderScanList.count,_isTransfer );


}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (_isQQSimulator) {
        
         [self drawBottomItems];
         [self drawTitle];
         [self.view bringSubviewToFront:_topTitle];
        [self.tableView reloadData];

    }
    else
        _topTitle.hidden = YES;
    
    

}

//绘制扫描区域
- (void)drawTitle
{
    if (!_topTitle)
    {
        self.topTitle = [[UILabel alloc]init];
        _topTitle.bounds = CGRectMake(0, 0, 300, 60);
        _topTitle.center = CGPointMake(CGRectGetWidth(self.view.frame)/2, 50);
        
        //3.5inch iphone
        if ([UIScreen mainScreen].bounds.size.height <= 568 )
        {
            _topTitle.center = CGPointMake(CGRectGetWidth(self.view.frame)/2, 38);
            _topTitle.font = [UIFont systemFontOfSize:14];
        }
        
        
        _topTitle.textAlignment = NSTextAlignmentCenter;
        _topTitle.numberOfLines = 0;
        _topTitle.text = @"Scan the barcode inside this frame.";
        _topTitle.textColor = [UIColor whiteColor];
        [self.view addSubview:_topTitle];
    }    
}

- (void)cameraInitOver
{
    if (self.isVideoZoom) {
        [self zoomView];
    }
}

- (LBXScanVideoZoomView*)zoomView
{
    if (!_zoomView)
    {
      
        CGRect frame = self.view.frame;
        
        int XRetangleLeft = self.style.xScanRetangleOffset;
        
        CGSize sizeRetangle = CGSizeMake(frame.size.width - XRetangleLeft*2, frame.size.width - XRetangleLeft*2);
        
        if (self.style.whRatio != 1)
        {
            CGFloat w = sizeRetangle.width;
            CGFloat h = w / self.style.whRatio;
            
            NSInteger hInt = (NSInteger)h;
            h  = hInt;
            
            sizeRetangle = CGSizeMake(w, h);
        }
        
        CGFloat videoMaxScale = [self.scanObj getVideoMaxScale];
        
        //扫码区域Y轴最小坐标
        CGFloat YMinRetangle = frame.size.height / 2.0 - sizeRetangle.height/2.0 - self.style.centerUpOffset;
        CGFloat YMaxRetangle = YMinRetangle + sizeRetangle.height;
        
        CGFloat zoomw = sizeRetangle.width + 40;
        _zoomView = [[LBXScanVideoZoomView alloc]initWithFrame:CGRectMake((CGRectGetWidth(self.view.frame)-zoomw)/2, YMaxRetangle + 40, zoomw, 18)];
        
        [_zoomView setMaximunValue:videoMaxScale/4];
        
        
        __weak __typeof(self) weakSelf = self;
        _zoomView.block= ^(float value)
        {            
            [weakSelf.scanObj setVideoScale:value];
        };
     //   [self.view addSubview:_zoomView];
//                
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
//        [self.view addGestureRecognizer:tap];
    }
    
    return _zoomView;
   
}

- (void)tap
{
    _zoomView.hidden = !_zoomView.hidden;
}

- (void)drawBottomItems
{
    if (_bottomItemsView) {
        
        return;
    }
    
    self.bottomItemsView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.view.frame)-145,
                                                                      CGRectGetWidth(self.view.frame), 136)];
    _bottomItemsView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.view.frame)-145,
                                                                   CGRectGetWidth(self.view.frame), 136)];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.scrollEnabled = false;
    self.tableView.allowsSelection = false;
    [self.view addSubview:_tableView];
    
    CGSize size = CGSizeMake(65, 87);
    self.btnFlash = [[UIButton alloc]init];
    _btnFlash.bounds = CGRectMake(0, 0, size.width, size.height);
    _btnFlash.center = CGPointMake(CGRectGetWidth(_bottomItemsView.frame)/2, CGRectGetHeight(_bottomItemsView.frame)/2);
     [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_flash_nor"] forState:UIControlStateNormal];
    [_btnFlash addTarget:self action:@selector(openOrCloseFlash) forControlEvents:UIControlEventTouchUpInside];
    
    self.btnPhoto = [[UIButton alloc]init];
    _btnPhoto.bounds = _btnFlash.bounds;
    _btnPhoto.center = CGPointMake(CGRectGetWidth(_bottomItemsView.frame)/4, CGRectGetHeight(_bottomItemsView.frame)/2);
    [_btnPhoto setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_photo_nor"] forState:UIControlStateNormal];
    [_btnPhoto setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_photo_down"] forState:UIControlStateHighlighted];
    [_btnPhoto addTarget:self action:@selector(openPhoto) forControlEvents:UIControlEventTouchUpInside];
    
    self.btnMyQR = [[UIButton alloc]init];
    _btnMyQR.bounds = _btnFlash.bounds;
    _btnMyQR.center = CGPointMake(CGRectGetWidth(_bottomItemsView.frame) * 3/4, CGRectGetHeight(_bottomItemsView.frame)/2);
    [_btnMyQR setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_myqrcode_nor"] forState:UIControlStateNormal];
    [_btnMyQR setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_myqrcode_down"] forState:UIControlStateHighlighted];
    //[_btnMyQR addTarget:self action:@selector(myQRCode) forControlEvents:UIControlEventTouchUpInside];
    
    [_bottomItemsView addSubview:_btnFlash];
    [_bottomItemsView addSubview:_btnPhoto];
    [_bottomItemsView addSubview:_btnMyQR];   
    
}







- (void)showError:(NSString*)str
{
    [LBXAlertAction showAlertWithTitle:@"提示" msg:str chooseBlock:nil buttonsStatement:@"知道了",nil];
}



- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    
    if (array.count < 1)
    {
        [self popAlertMsgWithScanResult:nil];
     
        return;
    }
    
    //经测试，可以同时识别2个二维码，不能同时识别二维码和条形码
    for (LBXScanResult *result in array) {
        
        NSLog(@"scanResult:%@",result.strScanned);
    }
    
    LBXScanResult *scanResult = array[0];
    
    NSString*strResult = scanResult.strScanned;
    
    self.scanImage = scanResult.imgScanned;
    
    if (!strResult) {
        
//        [self popAlertMsgWithScanResult:nil];
//        
//        return;
    }
    
    //震动提醒
    [LBXScanWrapper systemVibrate];
    //声音提醒
    [LBXScanWrapper systemSound];
    
    [self showNextVCWithScanResult:scanResult];
   
}

- (void)popAlertMsgWithScanResult:(NSString*)strResult
{
    if (!strResult) {
        
        strResult = @"识别失败";
    }
    
    __weak __typeof(self) weakSelf = self;
    [LBXAlertAction showAlertWithTitle:@"扫码内容" msg:strResult chooseBlock:^(NSInteger buttonIdx) {
        
        //点击完，继续扫码
        [weakSelf reStartDevice];
    } buttonsStatement:@"知道了",nil];
}

- (void)showNextVCWithScanResult:(LBXScanResult*)strResult
{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Succesfully Scan"
                                 message:[NSString stringWithFormat:@"Tracking Number: %@",strResult.strScanned]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    Boolean correctOrderBool = false;
    
    NSLog(@" order scan list count 11 : %ld",_orderScanList.count);
    
    for(int i=0;i<_orderScanList.count;i++)
    {
        
        if([[_orderScanList objectAtIndex:i] isEqualToString:strResult.strScanned])
        {
            correctOrderBool=true;
            [_orderScanList removeObjectAtIndex:i];
            [_tableView reloadData];
            
            NSLog(@"inital order scan list count 11: %ld",_initialOrderScanList.count);
            
            NSLog(@"scanner sama");
            
            break;
        }
        
    }
    
    
    if(correctOrderBool)
    {
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else{
        
        Boolean isExist = false;
        
        NSLog(@"inital order scan list count 11: %ld",_initialOrderScanList.count);
        
        NSLog(@"scanner tak sama");
        
        alert.title = @"Error";
        alert.message = @"Wrong Barcode Scan";
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    // Vibrate
    //AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    NSLog(@" order scan list count: %ld",_orderScanList.count);
    
    if(_orderScanList.count==0)
    {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [alert dismissViewControllerAnimated:YES completion:^{
            }];
            [self.navigationController popViewControllerAnimated:YES];
            
            [self.delegate scanDelegateMethod:self.orderScanList isTransfer:true]; //this will call the method implemented in your other class
            
            // code here
            
        });
        
    }
    else{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:^{
            }];
        });
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            __weak __typeof(self) weakSelf = self;
            [weakSelf reStartDevice];
        });
        
        
        
        
        
    }

}


#pragma mark -底部功能项
//打开相册
- (void)openPhoto
{
    if ([LBXScanWrapper isGetPhotoPermission])
        [self openLocalPhoto];
    else
    {
        [self showError:@"      请到设置->隐私中开启本程序相册权限     "];
    }
}

//开关闪光灯
- (void)openOrCloseFlash
{
    
    [super openOrCloseFlash];
   
    
    if (self.isOpenFlash)
    {
        [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_flash_down"] forState:UIControlStateNormal];
    }
    else
        [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_flash_nor"] forState:UIControlStateNormal];
}


- (IBAction)keyboardClick:(id)sender
{
    //[self dismissViewControllerAnimated:true completion:nil];
    // code here
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Enter Barcode"
                               message:@"Key in the last 4 digit of the barcode."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                                   //Do Some action here
                                                   UITextField *textField = alert.textFields[0];
                                                   if(textField.text.length >= 4)
                                                   {
                                                       NSString *userInputCode = [textField.text substringFromIndex: [textField.text length] - 4];
                                                       
                                                       for(int i=0;i<_orderScanList.count;i++)
                                                       {
                                                           
                                                           NSString *originalCode = [[_orderScanList objectAtIndex:i] substringFromIndex: [[_orderScanList objectAtIndex:i] length] - 4];
                                                           
                                                           if([originalCode isEqualToString:userInputCode])
                                                           {
                                                               [_orderScanList removeObjectAtIndex:i];
                                                               [_tableView reloadData];
                                                               
                                                               UIAlertController * alert = [UIAlertController
                                                                                            alertControllerWithTitle:@"Succesfully Scan"
                                                                                            message:[NSString stringWithFormat:@"Tracking Number: %@",userInputCode]
                                                                                            preferredStyle:UIAlertControllerStyleAlert];
                                                               
                                                               
                                                               [self presentViewController:alert animated:YES completion:nil];
                                                               
                                                               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                                   
                                                                   [alert dismissViewControllerAnimated:YES completion:^{
                                                                   }];
                                                                   [self.navigationController popViewControllerAnimated:YES];
                                                                   
                                                                   [self.delegate scanDelegateMethod:self.orderScanList isTransfer:true]; //this will call the method implemented in your other class
                                                                   
                                                                   // code here
                                                                   
                                                               });
                                                               
                                                              // break;
                                                           }
                                                           
                                                       }
                                                   }
                                                   
                                                   
                                                   
                                                   
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


- (IBAction)dismissScan:(id)sender
{
     NSLog(@" dismissScan 11");
    [self dismissViewControllerAnimated:true completion:nil];
    // code here
}

- (IBAction)openFlash:(id)sender
{
    [self openOrCloseFlash];
//    CGSize size = CGSizeMake(65, 87);
//    self.btnFlash = [[UIButton alloc]init];
//    _btnFlash.bounds = CGRectMake(0, 0, size.width, size.height);
//    _btnFlash.center = CGPointMake(CGRectGetWidth(_bottomItemsView.frame)/2, CGRectGetHeight(_bottomItemsView.frame)/2);
//    [_btnFlash setImage:[UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_btn_flash_nor"] forState:UIControlStateNormal];
//    [_btnFlash addTarget:self action:@selector(openOrCloseFlash) forControlEvents:UIControlEventTouchUpInside];
    
    if(_flashTurnOn){
        _flashTurnOn = false;
        _imageFlash = [UIImage imageNamed:@"flash_off"];
        UIButton *myCustomButtonFlash = [UIButton buttonWithType:UIButtonTypeCustom];
        myCustomButtonFlash.bounds = CGRectMake( 0, 0, _imageFlash.size.width, _imageFlash.size.height );
        [myCustomButtonFlash setImage:_imageFlash forState:UIControlStateNormal];
        [myCustomButtonFlash addTarget:self action:@selector(openFlash:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *buttonFlash = [[UIBarButtonItem alloc] initWithCustomView:myCustomButtonFlash];
        self.navigationItem.rightBarButtonItem = buttonFlash;
        // Flash button icon
        
        NSArray *tempArray2= [[NSArray alloc] initWithObjects:buttonFlash,_button,nil];
        self.navigationItem.rightBarButtonItems = tempArray2;
    }else{
        _flashTurnOn = true;
        _imageFlash = [UIImage imageNamed:@"flash_on"];
        UIButton *myCustomButtonFlash = [UIButton buttonWithType:UIButtonTypeCustom];
        myCustomButtonFlash.bounds = CGRectMake( 0, 0, _imageFlash.size.width, _imageFlash.size.height );
        [myCustomButtonFlash setImage:_imageFlash forState:UIControlStateNormal];
        [myCustomButtonFlash addTarget:self action:@selector(openFlash:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *buttonFlash = [[UIBarButtonItem alloc] initWithCustomView:myCustomButtonFlash];
        self.navigationItem.rightBarButtonItem = buttonFlash;
        // Flash button icon
        
       
        NSArray *tempArray2= [[NSArray alloc] initWithObjects:buttonFlash,_button,nil];
        self.navigationItem.rightBarButtonItems = tempArray2;
    }
}

- (IBAction)confirmScan:(id)sender
{
    [self.delegate scanDelegateMethod:self.orderScanList isTransfer:_isTransfer]; //this will call the method implemented in your other class
    [self.navigationController popViewControllerAnimated:YES];
    // code here
}
#pragma uitableview delegate


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    _remainingLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 20, 18)];
    [_remainingLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [_remainingLabel setTextColor:[UIColor redColor]];
    [_remainingLabel setText:[NSString stringWithFormat:@"%ld",_orderScanList.count]];

    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    
    
    NSString *string =@"remaining to scan";
    /* Section header is in 0th index... */
    [label setText:string];

    [view addSubview:_remainingLabel];
    [view addSubview:label];

    [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; //your background color...
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(!_isTransfer)
    {
        return 30;

    }
    else{
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_orderScanList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"OrderItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [_orderScanList objectAtIndex:indexPath.row];
    return cell;
}




#pragma mark -底部功能项


//- (void)myQRCode
//{
//    MyQRViewController *vc = [MyQRViewController new];
//    [self.navigationController pushViewController:vc animated:YES];
//}



@end
