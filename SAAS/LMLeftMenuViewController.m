//
//  LMMenuViewController.m
//  LMSideBarControllerDemo
//
//  Created by LMinh on 10/11/15.
//  Copyright © 2015 LMinh. All rights reserved.
//

#import "LMLeftMenuViewController.h"
#import "UIViewController+LMSideBarController.h"
#import "LMMainNavigationController.h"



@class AboutViewController;
@class AccountSettingViewController;


@interface LMLeftMenuViewController ()

@property (nonatomic, strong) NSArray *menuTitles;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@property (weak, nonatomic) IBOutlet UILabel *username;
    
@end

@implementation LMLeftMenuViewController

#pragma mark - VIEW LIFECYCLE

-(void)viewWillAppear:(BOOL)animated{
    
    BOOL onlineStatus = [[NSUserDefaults standardUserDefaults] boolForKey:@"isOnline"];
    if(onlineStatus)
    {
        [self.switchBtn setOn:true];
        self.onlineOffline_lbl.text = @"Online";
    }
    else{
        [self.switchBtn setOn:false];
        self.onlineOffline_lbl.text = @"Offline";
    }
        
}

- (IBAction)isOnline:(UISwitch*)sender{
    
    if(sender.isOn){
        self.onlineOffline_lbl.text = @"Online";
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"TurnOnLocation"
         object:self];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isOnline"];

    }
    else{
        self.onlineOffline_lbl.text = @"Offline";
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"TurnOffLocation"
         object:self];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isOnline"];


    }
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.menuTitles = @[@"Job", @"About", @"Logout"];
    
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.layer.cornerRadius = 2;
    self.avatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.avatarImageView.layer.borderWidth = 3.0f;
    self.avatarImageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.avatarImageView.layer.shouldRasterize = YES;
    
    NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@"Email"];
    self.username.text = value;
}


#pragma mark - TABLE VIEW DATASOURCE

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = self.menuTitles[indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithWhite:0.11 alpha:1];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}


#pragma mark - TABLE VIEW DELEGATE

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    LMMainNavigationController *mainNavigationController = (LMMainNavigationController *)self.sideBarController.contentViewController;
    NSString *menuTitle = self.menuTitles[indexPath.row];
    if ([menuTitle isEqualToString:@"Job"]) {
        
        
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"≈" bundle:nil];
        window.rootViewController=[stryBoard instantiateInitialViewController];
    }
    
    else if ([menuTitle isEqualToString:@"About"]) {

        
        
        AboutViewController *vc = (AboutViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewController"];
        
        [mainNavigationController pushViewController:vc animated:YES];

        
        
    }
    
//    else if ([menuTitle isEqualToString:@"Settings"]) {
//        
//        AccountSettingViewController *vcAccount = (AccountSettingViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"AccountSettingViewController"];
//        
//        [mainNavigationController pushViewController:vcAccount animated:YES];
//        
//        
//        
//    }
    
    else if ([menuTitle isEqualToString:@"Logout"]) {
        
        //code original
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        [defaults setValue:@"" forKey:@"Token"];
//        [defaults setValue:@"" forKey:@"Username"];
//        [defaults synchronize];
//
//        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isOnline"];
//
//        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
//        [[NSUserDefaults standardUserDefaults] synchronize];
         //code original
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"driverLogout"
         object:self];
        
        // swift code
       // NSUserDefaults.standardUserDefaults().removePersistentDomainForName(NSBundle.mainBundle().bundleIdentifier!)
       // NSUserDefaults.standardUserDefaults().synchronize()

//        UIWindow *window = [UIApplication sharedApplication].keyWindow;
//        UIStoryboard *stryBoard=[UIStoryboard storyboardWithName:@"Login" bundle:nil];
//        window.rootViewController=[stryBoard instantiateInitialViewController];
        
        
    }
//    else {
//        mainNavigationController.othersViewController.title = menuTitle;
//        [mainNavigationController showOthersViewController];
//    }
    
    [self.sideBarController hideMenuViewController:YES];
}


@end
