//
//  ListOrderAttemptViewController.swift
//  SAAS
//
//  Created by Coolasia on 7/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON

class ListOrderAttemptViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var heightConstraintTableView: NSLayoutConstraint!
    
    var countRow: Int?
    var orderID : Int?
    var orderStatusID : Int?
    var jobStepID : Int?
    var jobStepDetail = JobSteps()
    
    var reachInternetConnection = Reachability()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundView = nil;
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("ListOrderAttempt Internet connection OK")
            getOrderDetails()
        } else {
            print("ListOrderAttempt Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Attempt in this step"
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON JOB STEP ID ======== */
    func getOrderDetails(){
        
        SwiftSpinner.show("Getting List Order Attempt Details...")
        
        let message   = String(format: Constants.api_get_job_step_details, arguments: [String(describing: jobStepID!)])
        
        print("meesageGetOrderDetail \(message)")
        
        /* ====== FETCH JOB STEP ======= */
        AlamofireRequest.fetchJobStepDetails(url: message) { responseObject, statusCode, error in
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    let resultJson = swiftyJsonVar["result"]
                    if let jobStep = JobSteps(json: resultJson) {
                        self.jobStepDetail = jobStep
                        self.sortTableViewList()
                    }
                    
                }
                
                
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== FETCH JOB STEP ======= */
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON JOB STEP ID ======== */
    
    /* ======== TABLE VIEW ORDER ATTEMPT ====== */
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraintTableView.constant = tableView.contentSize.height
        tableView.layoutIfNeeded()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension;
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension
    }
    
    
    // TableView
    //MARK: - Tableview Delegate & Datasource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        
        countRow =  self.jobStepDetail.orderAttemptList.count
        return countRow!
        
        
    }
    private func numberOfSectionsInTableView(tableViewOrderItem: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:ListOrderAttemptTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ListOrderAttemptTableViewCell") as! ListOrderAttemptTableViewCell
        
        cell.noOrderAttempt.text = ("Attempt \(indexPath.row + 1)")
        if(self.jobStepDetail.orderAttemptList[indexPath.row].submittedTime != "" || self.jobStepDetail.orderAttemptList[indexPath.row].submittedTime != nil){
            cell.dateOrderAttempt.text = Constants.formatDateWithTime(date: self.jobStepDetail.orderAttemptList[indexPath.row].submittedTime!)
        }else{
            cell.dateOrderAttempt.text = "Unknown Date"
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("ListOrderAttempt Internet connection OK")
            let indexPath = tableView.indexPathForSelectedRow
            let storyboard = UIStoryboard(name: "OrderAttemptDetails", bundle: nil)
            let navController = storyboard.instantiateViewController(withIdentifier: "OrderAttemptDetails_ViewController") as! UINavigationController
            let levelController = navController.viewControllers[0] as! OrderAttemptDetails_ViewController
            
            levelController.orderID = self.orderID
            levelController.orderStatusID = self.orderStatusID!
            levelController.jobStepID = self.jobStepDetail.id
            levelController.jobStepStatusID = self.jobStepDetail.job_step_status_id
            levelController.orderAttemptID = self.jobStepDetail.orderAttemptList[indexPath!.row].id!
            levelController.orderAttemptArray = [self.jobStepDetail.orderAttemptList[indexPath!.row]]
            self.present(navController, animated: true, completion: nil)
        } else {
            print("ListOrderAttempt Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    /* ======== TABLE VIEW ORDER ATTEMPT ====== */
    
    /* === FUNCTION TO SORT LATEST ORDER ATTEMPT IN TABLEVIEW ======= */
    func sortTableViewList() {
        self.jobStepDetail.orderAttemptList.sort() { $0.self.submittedTime! < $1.self.submittedTime! }
        self.tableView.reloadData()
    }
    /* === FUNCTION TO SORT LATEST ORDER ATTEMPT IN TABLEVIEW ======= */
}


extension ListOrderAttemptViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}

