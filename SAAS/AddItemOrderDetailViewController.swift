//
//  AddItemOrderDetailViewController.swift
//  LEEWAY
//
//  Created by Coolasia on 27/9/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SwiftSpinner
import CoreData
import SCLAlertView

class AddItemOrderDetailViewController: UIViewController,UITextViewDelegate {
    
    
    @IBOutlet weak var lblRemark: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var tittlePage: UINavigationItem!
    @IBOutlet weak var txtViewQuantity: UITextView!
    @IBOutlet weak var txViewWeight: UITextView!
    
    @IBOutlet weak var heightConstraintDescription: NSLayoutConstraint!
    
    
    let button = UIButton(type: .system)
    open var tintColor = UIColor.defaultTintColor()
    open var tintColorDisable = Constants.hexStringToUIColor(hex:"80FFFFFF")
    var orderDetailsItemArray: [OrderDetail] = []
    var orderDetailsItemAdd: OrderDetail = OrderDetail()
    var isEdit: Bool?
    var orderId = Int()
    var orderStatusId = Int()
    var request: URLRequest!
    let moc = DataController().managedObjectContext
    var orderStatusNameFromDB: String?
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        self.navigationController?.navigationBar.topItem?.title="Upload Photos"
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        self.txtViewQuantity.textContainer.maximumNumberOfLines = 1
        self.txViewWeight.textContainer.maximumNumberOfLines = 1
        
        self.txtViewQuantity.delegate = self
        self.txViewWeight.delegate = self
        
        if(self.isEdit)!{
            print("detailOrderQuantity \(String(describing: self.orderDetailsItemArray[0].quantity))")
            print("detailOrderQuantity \(String(describing: self.orderDetailsItemArray[0].weight))")
            print("detailOrderQuantity \(String(describing: self.orderDetailsItemArray[0].descriptions))")
            print("detailOrderQuantity \(String(describing: self.orderDetailsItemArray[0].remarks))")
            print("detailOrderQuantity \(String(describing: isEdit))")
            
            /*To check if job order status id already completed, cannot be edit*/
//            if(self.orderStatusNameFromDB == Constants.COMPLETE_TEXT){
//                self.tittlePage.title = "Details Item"
//            }else{
//               self.tittlePage.title = "Edit Item"
//            }
            
            self.tittlePage.title = "Details Item"
            
            if(self.orderDetailsItemArray[0].descriptions == nil){
                self.lblDescription.text = ""
            }else{
                self.lblDescription.text = self.orderDetailsItemArray[0].descriptions
            }
            
            if(self.orderDetailsItemArray[0].quantity == nil){
                self.txtViewQuantity.text = "0"
            }else{
                self.txtViewQuantity.text = String(describing: self.orderDetailsItemArray[0].quantity!)
            }
            
            if(self.orderDetailsItemArray[0].weight == nil){
                self.txViewWeight.text = "0"
            }else{
                self.txViewWeight.text = String(describing: self.orderDetailsItemArray[0].weight!)
            }
            
            if(self.orderDetailsItemArray[0].remarks == nil){
                self.lblRemark.text = ""
            }else{
                self.lblRemark.text = self.orderDetailsItemArray[0].remarks
            }
            
            
            
            self.txtViewQuantity.isEditable =  false
            self.txViewWeight.isEditable =  false
            
            self.lblDescription.textColor = Constants.hexStringToUIColor(hex: Constants.DARKEY_GREY_COLOR)
            self.txtViewQuantity.textColor = Constants.hexStringToUIColor(hex: Constants.DARKEY_GREY_COLOR)
            self.txViewWeight.textColor = Constants.hexStringToUIColor(hex: Constants.DARKEY_GREY_COLOR)
            self.lblRemark.textColor = Constants.hexStringToUIColor(hex: Constants.DARKEY_GREY_COLOR)
            
            /*To check if job order status id already completed, cannot be edit*/
//            if(self.orderStatusNameFromDB != Constants.COMPLETE_TEXT){
//                let editButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.edit, target: self, action: #selector(AddItemOrderDetailViewController.onTouchEditButton))
//                self.navigationItem.rightBarButtonItem = editButton
//            }
            
            
            
            
        }
//        else{
//            self.txtViewDescription.textColor = UIColor.lightGray
//            self.txtViewQuantity.textColor = UIColor.lightGray
//            self.txViewWeight.textColor = UIColor.lightGray
//            self.txtViewRemarks.textColor = UIColor.lightGray
//
//            self.tittlePage.title = "Add Item"
//
////            let doneAddButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(AddItemOrderDetailViewController.onTouchDoneAddButton))
//
//          //  let button = UIButton(type: .system)
//          //  button.setImage(UIImage(named: "plus"), for: .normal)
//          //  button.sizeToFit()
//            button.setTitle("Add", for: .normal)
//            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
//            button.addTarget(self, action:#selector(AddItemOrderDetailViewController.onTouchDoneAddButton), for: .touchUpInside)
//           // button.tintColor = tintColorDisable
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
//            self.navigationItem.rightBarButtonItem?.isEnabled = false
//
//           // self.navigationItem.rightBarButtonItem = doneAddButton
//
//
//        }
      
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(AddItemOrderDetailViewController.onTouchCancelButton))
        //cancelButton.tintColor = tintColor
        self.navigationItem.leftBarButtonItem = cancelButton
        
     
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(!self.isEdit!){
            if textView.textColor == Constants.hexStringToUIColor(hex: Constants.DARKEY_GREY_COLOR) {
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("txtViewQuantity \(self.txtViewQuantity.text)")
        print("txViewWeight \(self.txViewWeight.text)")

        
        
        if( (self.txtViewQuantity.text != "Amount" && self.txtViewQuantity.text != "") &&
            (self.txViewWeight.text != "Weight" && self.txViewWeight.text != "")){

            self.navigationItem.rightBarButtonItem?.isEnabled = true

        }else{
            self.navigationItem.rightBarButtonItem?.isEnabled = false

        }
    }
    
    func showUIAlertDialog(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") {
            print("YES button tapped")
            alertView.dismiss(animated: true, completion: nil)
        }
        
        alertView.showError("Missing input field", subTitle: "Please enter all field")
    }
    
    func onTouchCancelButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func onTouchDoneAddButton(){
        if(self.txtViewQuantity.text.count != 0 && self.txViewWeight.text.count != 0  ){
            self.doneAddorEditDetails(doneAdd: true)
           // dismiss(animated: true, completion: nil)
        }else{
            self.showUIAlertDialog()
        }
        
        
    }
    
    func onTouchDoneButton(){
        if(self.txtViewQuantity.text.count != 0 && self.txViewWeight.text.count != 0 ){
            self.doneAddorEditDetails(doneAdd: false)
          //  dismiss(animated: true, completion: nil)
        }else{
            self.showUIAlertDialog()
        }
    }
    
    func onTouchEditButton(){
        self.txtViewQuantity.isEditable =  true
        self.txViewWeight.isEditable =  true
        
        self.txtViewQuantity.textColor = UIColor.black
        self.txViewWeight.textColor = UIColor.black
        
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(AddItemOrderDetailViewController.onTouchDoneButton))
        self.navigationItem.rightBarButtonItem = doneButton
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Process
    func doneAddorEditDetails(doneAdd: Bool?) {
        
        
        let batchOrder: BatchOrder = BatchOrder()
        
        let para:NSMutableDictionary = NSMutableDictionary()
        let prodArray:NSMutableArray = NSMutableArray()
        
        
        
        batchOrder.orderIds.append(String(self.orderId))
        batchOrder.orderStatus = String(self.orderStatusId)
        
        
        para.setValue(batchOrder.orderIds, forKey: "id")
        para.setValue(batchOrder.orderStatus, forKey: "order_status_id")
        
        
        
        if(doneAdd)!{
            orderDetailsItemAdd = OrderDetail()
            self.orderDetailsItemAdd.descriptions = self.lblDescription.text
            self.orderDetailsItemAdd.quantity = Int(self.txtViewQuantity.text)
            self.orderDetailsItemAdd.weight = Double(self.txViewWeight.text)
            self.orderDetailsItemAdd.remarks = self.lblRemark.text
            prodArray.add(self.orderDetailsItemAdd.toJsonAddOrderItems())
        }else{
            self.orderDetailsItemArray[0].descriptions = self.lblDescription.text
            self.orderDetailsItemArray[0].quantity = Int(self.txtViewQuantity.text)
            self.orderDetailsItemArray[0].weight = Double(self.txViewWeight.text)
            self.orderDetailsItemArray[0].remarks = self.lblRemark.text
            
            prodArray.add(self.orderDetailsItemArray[0].toJsonEditOrderItems())
        }
        
        
        
        
        para.setObject(prodArray, forKey: "order_details" as NSCopying)
        
        let dataPara:NSMutableDictionary = NSMutableDictionary()
        let dataArray:NSMutableArray = NSMutableArray()
        
        dataArray.add(para)
        dataPara.setValue(dataArray, forKey: "data")
        
        SwiftSpinner.show("Update Order Details ...")
        
        self.prettyJSONandCallServer(dataPara: dataPara)
        
    }
    
    // Func to create pretyJSON and call server
    func prettyJSONandCallServer(dataPara: NSMutableDictionary){
        
        do{
            let jsonData = try JSONSerialization.data(withJSONObject: dataPara, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            
            if let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) {
                
                print("dataPara \(jsonString)")
                
                let url = NSURL(string: Constants.web_api+Constants.api_batch_update_order)

                request = URLRequest(url: url! as URL)
                request.httpMethod = "POST" //set http method as POST

                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: dataPara, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body

                } catch let error {
                    print(error.localizedDescription)
                }


                //HTTP Headers
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.addValue("bearer \(UserDefaults.standard.value(forKey: "Token")!)", forHTTPHeaderField:"Authorization")


                let urlconfig = URLSessionConfiguration.default
                urlconfig.timeoutIntervalForRequest = 20
                urlconfig.timeoutIntervalForResource = 30
                let session = URLSession(configuration: urlconfig)
                print(request.allHTTPHeaderFields!)

                //create dataTask using the session object to send data to the server
                let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                    guard error == nil else {

                        do{
                            try self.moc.save()
                        }catch{
                            fatalError("errorInsert: \(error)")
                        }
                        DispatchQueue.main.async {

                            self.navigationController!.popViewController(animated:true)

                        }

                        return
                    }

                    guard let data = data else {
                        return
                    }

                    do {
                        //create json object from data
                        print("response is \(response!)")
                        print("dataAfterResponse \(data)")


                        if let httpResponse = response as? HTTPURLResponse {
                            print("error \(httpResponse.statusCode)")
                            if(httpResponse.statusCode==200)
                            {
                                
                                DispatchQueue.main.async {
                                    self.dismiss(animated: true, completion: nil)
                                    SwiftSpinner.hide()
                                }

                            }
                            else{
                                DispatchQueue.main.async {
                                    self.dismiss(animated: true, completion: nil)
                                    SwiftSpinner.hide()
                                }
                            }

                        }

                    } catch let error {
                        print(error.localizedDescription)
                    }

                })

                task.resume()
            }
            
        }catch{
            
        }
        
    }
    
}
