//
//  AlamofireRequestRefreshToken.swift
//  SAAS
//
//  Created by Coolasia on 25/1/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import UIKit


struct AlamofireRequestRefreshToken {
    
    
    /* =========== USER LOGIN ======= */
    static func loginUserRefreshToken(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        let api_sign_in = "/api/login"
        print(api_sign_in)
        print(parameters)
        
        
        self.postRequest(httpMethod: .post, urlString: Constants.web_api_auth+api_sign_in,
                         paramDict: parameters as? Dictionary<String, AnyObject>,
                         completionHandler: completionHandler )
    }
    /* =========== USER LOGIN ======= */
    
    /* =========== MAIN REQUEST REFRESH TOKEN =========== */
    static func postRequest(httpMethod: HTTPMethod, urlString: String, paramDict:Dictionary<String, AnyObject>? = nil,
                            completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 180
        manager.session.configuration.timeoutIntervalForResource = 180
        
        manager.request(urlString, method: httpMethod, parameters: paramDict, encoding: JSONEncoding.default,headers: nil)
            .validate()
            .responseJSON { response in
                let responseResult = JSON(response.data)
                print("responseResult REFRESH TOKEN:  \(responseResult)")
                switch response.result {
                case .success(let JSON):
                    print("success postRequest REFRESH TOKEN: \(JSON)")
                    completionHandler(responseResult, response.response?.statusCode, nil )
                case .failure(let error):
                    print("failure postRequest REFRESH TOKEN: \(error)")
                    completionHandler(responseResult, response.response?.statusCode, error as NSError )
                }
        }
    }
    /* =========== MAIN REQUEST REFRESH TOKEN =========== */
    
}
