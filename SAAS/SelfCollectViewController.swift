//
//  SelfCollectViewController.swift
//  LEEWAY
//
//  Created by Coolasia on 18/9/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Gloss
import SwiftSpinner
import MarqueeLabel
import AVFoundation
import AnimatableReload

class SelfCollectViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,UISearchBarDelegate, AVCaptureMetadataOutputObjectsDelegate {
    
    // scanner barcode
    var captureSession: AVCaptureSession!
    var captureDevice: AVCaptureDevice!
    var captureDeviceInput: AVCaptureDeviceInput!
    var captureDeviceOutput: AVCaptureMetadataOutput!
    var capturePreviewLayer: AVCaptureVideoPreviewLayer!
    var alertController: UIAlertController!
    // scanner barcode
    
    
    var jobList: [Job] = []
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .custom)
    var itemTrackingSearch = String()
    var selectedIndexTabStatus = Int()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbl_selectedDate: UILabel!
    
    @IBOutlet weak var calendar_outerview: UIView!
    var reachInternetConnection = Reachability()
    
    
    @IBAction func leftMenuButtonTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LEFT_MENU_TAPPED"), object: nil, userInfo: nil)
    }
    
    
    
    func calendarDaySelected(notification: NSNotification){
        
        var tabBarController: UITabBarController
        tabBarController = self.parent?.parent as!UITabBarController
        selectedIndexTabStatus = tabBarController.selectedIndex
        print("selectedIndexTabStatus \(selectedIndexTabStatus)")
        
        if(selectedIndexTabStatus != Constants.SELECTED_TAB_ASSIGNED){
            if let selectedDate = notification.userInfo?["date"] as? Date {
                print(selectedDate)
                
                if reachInternetConnection.isConnectedToNetwork() == true {
                    print("SelfCollect File Internet connection OK")
                    reloadJobList(date: selectedDate.string(format: "yyyy-MM-dd"))
                } else {
                    print("SelfCollect File Internet connection FAILED")
                    var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
                
                
                
            }
        }
    }
    
    
    func stringFormatDate(dropOffDate: String,dateformat: String, dateString: String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = dateformat
        let dateNew = formatter.date(from:dropOffDate)
        let returnDateString = dateNew?.string(format: dateString)
        return returnDateString!
    }
    
    func reloadJobList(date: String){
        var tabBarController: UITabBarController
        tabBarController = self.parent?.parent as!UITabBarController
        selectedIndexTabStatus = tabBarController.selectedIndex
        print("selectedIndexTabStatus \(selectedIndexTabStatus)")
        
        var message = String()
        let dateString = self.stringFormatDate(dropOffDate: date, dateformat: "yyyy-MM-dd", dateString: "dd MMMM yyyy")
        
        print("dateString \(dateString)")
        
        lbl_selectedDate.text = dateString
        
        /* ==== Set status for fetch Job ===== */
        if(selectedIndexTabStatus == Constants.SELECTED_TAB_ASSIGNED){
            var orderStatusIDFromOrderStatusDB: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ASSIGNED_TEXT)
            
           message =  String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
            SwiftSpinner.show("Fetching Assigned Job List...")
        }
        
        else if(selectedIndexTabStatus == Constants.SELECTED_TAB_ACKNOWLEDGED){
            var orderStatusIDFromOrderStatusDB: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ACKNOWLEDGED_TEXT)
            message =  String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
            SwiftSpinner.show("Fetching Acknowledged Job List...")
        }
        
        else if(selectedIndexTabStatus == Constants.SELECTED_TAB_INPROGRESS){
            var orderStatusIDFromOrderStatusDB: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.INPROGRESS_TEXT)
            message =  String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
            SwiftSpinner.show("Fetching In Progress Job List...")
        }
        else if(selectedIndexTabStatus == Constants.SELECTED_TAB_COMPLETE){
            var orderStatusIDFromOrderStatusDB: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.COMPLETE_TEXT)
            message =  String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
            SwiftSpinner.show("Fetching Completed Job List...")
        }
//        else if(selectedIndexTabStatus == Constants.SELECTED_TAB_CANCELLED){
//            var orderStatusIDFromOrderStatusDB: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.CANCELLED_TEXT)
//            message =  String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
//            SwiftSpinner.show("Fetching Cancelled Job List...")
//        }
        else if(selectedIndexTabStatus == Constants.SELECTED_TAB_FAILED){
            var orderStatusIDFromOrderStatusDB: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.FAILED_TEXT)
            message =  String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
            SwiftSpinner.show("Fetching Failed Job List...")
        }
        else if(selectedIndexTabStatus == Constants.SELECTED_TAB_SELF_COLLECT){
            var orderStatusIDFromOrderStatusDB: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.SELF_COLLECT_TEXT)
            message =  String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
            SwiftSpinner.show("Fetching Self Collect Job List...")
        }
        /* ==== Set status for fetch Job ===== */
        
        
        // SwiftSpinner.show("Failed to connect, waiting...", animated: false)
        
        
        /* ====== FETCH JOB ======= */
        AlamofireRequest.fetchJobList(url: message) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            if(statusCode == 200){
                if(responseObject == nil)
                {
                
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    
                    self.jobList.removeAll()
                    
                    for subJson in swiftyJsonVar.array! {
                        for jobsJson  in subJson["jobs"].array! {
                            
                            for jobJson in jobsJson["job"].array! {
                                if let job = Job(json: jobJson) {
                                    
                                    self.jobList.append(job)
                                }
                                
                            }
                        }
                    }
                    
                    AnimatableReload.reload(tableView: self.tableView, animationDirection: "down")
//                    self.tableView.beginUpdates()
//                    self.tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .fade)
//                    self.tableView.endUpdates()
                    
                   // self.tableView.reloadData()
                   // self.tableView.reloadData()
//                    UIView.transition(with: self.tableView, duration: 1.0, options: .transitionCrossDissolve, animations: {self.tableView.reloadData()}, completion: nil)
                    
                    var tabBarController: UITabBarController
                    tabBarController = self.parent?.parent as!UITabBarController
                    
                    /* Assinged Tab */
                    if(self.selectedIndexTabStatus == Constants.SELECTED_TAB_ASSIGNED){
                        if(self.jobList.count != 0)
                        {
                            tabBarController.tabBar.items?[0].badgeValue = String(self.jobList.count)   // this will add "1" badge to
                        }
                        else{
                            tabBarController.tabBar.items?[0].badgeValue = nil
                        }
                    }
                    /* Acknowledged Tab */
                    else if(self.selectedIndexTabStatus == Constants.SELECTED_TAB_ACKNOWLEDGED){
                        if(self.jobList.count != 0)
                        {
                            tabBarController.tabBar.items?[1].badgeValue = String(self.jobList.count)   // this will add "1" badge to
                        }
                        else{
                            tabBarController.tabBar.items?[1].badgeValue = nil
                        }
                    }
                    /* In Progress Tab */
                    else if(self.selectedIndexTabStatus == Constants.SELECTED_TAB_INPROGRESS){
                        if(self.jobList.count != 0)
                        {
                            tabBarController.tabBar.items?[2].badgeValue = String(self.jobList.count)   // this will add "1" badge to
                        }
                        else{
                            tabBarController.tabBar.items?[2].badgeValue = nil
                        }
                    }
                    /* Completed Tab */
                    else if(self.selectedIndexTabStatus == Constants.SELECTED_TAB_COMPLETE ){
                        if(self.jobList.count != 0)
                        {
                            tabBarController.tabBar.items?[3].badgeValue = String(self.jobList.count)   // this will add "1" badge to
                        }
                        else{
                            tabBarController.tabBar.items?[3].badgeValue = nil
                        }
                    }
                    /* Cancelled Tab , Failed , Self Collect*/
//                    else if(self.selectedIndexTabStatus == Constants.SELECTED_TAB_CANCELLED || self.selectedIndexTabStatus == Constants.SELECTED_TAB_FAILED || self.selectedIndexTabStatus == Constants.SELECTED_TAB_SELF_COLLECT){
//                        if(self.jobList.count != 0)
//                        {
//                            tabBarController.tabBar.items?[4].badgeValue = String(self.jobList.count)   // this will add "1" badge to
//                        }
//                        else{
//                            tabBarController.tabBar.items?[4].badgeValue = nil
//                        }
//                    }
                    
                    
                }
                
                
            }else{
                SwiftSpinner.hide()
                if(responseObject == nil || statusCode == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                   self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== FETCH JOB ======= */
        
    }
    
    // Show when no data exist
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Click Refresh button or Select another date."
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "No Order on this date."
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.parent?.navigationController?.isNavigationBarHidden = true
        
        //button search
        
        
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchButton.addTarget(self, action:#selector(self.createSearchBar), for: .touchUpInside)
        let searchBarButton = UIBarButtonItem(customView: searchButton)
        
        
        let refreshButton = UIButton(type: .custom)
        refreshButton.setImage(UIImage(named: "refresh"), for: .normal)
        refreshButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        refreshButton.addTarget(self, action:#selector(self.refreshJob), for: .touchUpInside)
        let refreshBarButton = UIBarButtonItem(customView: refreshButton)
        
        self.navigationController?.navigationBar.topItem?.setRightBarButtonItems([refreshBarButton,searchBarButton], animated: true)
        
        var tabBarController: UITabBarController
        tabBarController = self.parent?.parent as!UITabBarController
        selectedIndexTabStatus = tabBarController.selectedIndex
        print("selectedIndexTabStatus \(selectedIndexTabStatus)")
        
        /* ===== Set Title Page ==== */
        if(selectedIndexTabStatus == Constants.SELECTED_TAB_ASSIGNED){
            self.navigationController?.navigationBar.topItem?.title = Constants.ASSIGNED_TEXT
        }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_ACKNOWLEDGED){
            self.navigationController?.navigationBar.topItem?.title = Constants.ACKNOWLEDGED_TEXT
        }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_INPROGRESS){
            self.navigationController?.navigationBar.topItem?.title = Constants.INPROGRESS_TEXT
        }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_COMPLETE){
            self.navigationController?.navigationBar.topItem?.title = Constants.COMPLETE_TEXT
        
//        }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_CANCELLED){
//            self.navigationController?.navigationBar.topItem?.title = Constants.CANCELLED_TEXT
        }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_FAILED){
            self.navigationController?.navigationBar.topItem?.title = Constants.FAILED_TEXT
        }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_SELF_COLLECT){
            self.navigationController?.navigationBar.topItem?.title = Constants.SELF_COLLECT_TEXT
        }
        /* ===== Set Title Page ==== */
        
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
//        /* ===== Check which tab status is press ====== */
//        let application = UIApplication.shared.delegate as! AppDelegate
//        let tabbarController = application.window?.rootViewController as! UITabBarController
//        let selectedIndexTabStatus = tabbarController.selectedIndex
//        print("selectedIndexTabStatus \(selectedIndexTabStatus)")
//        /* ===== Check which tab status is press ====== */
        
        
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Self Collect File 22 Internet connection OK")
            reloadJobList(date: Constants.current_date.string(format: "yyyy-MM-dd"))
            
        } else {
            print("Self Collect File 22 Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        var tabBarController: UITabBarController
//        tabBarController = self.parent?.parent as!UITabBarController
//        tabBarController.navigationController?.navigationBar.topItem?.title="Self Collect"
//        self.tableView.estimatedRowHeight = 164
//        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        self.tableView.backgroundView = nil;
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        print("tap \(tap)")
        print("calendar outer view \(self.calendar_outerview)")
        
        self.calendar_outerview.isUserInteractionEnabled = true
        self.calendar_outerview.addGestureRecognizer(tap)

        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.calendarDaySelected),
            name: NSNotification.Name(rawValue: "CALENDAR_DAY_SELECTED"),
            object: nil)
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadJobAfterNotification),
            name: NSNotification.Name(rawValue: "RELOAD_JOB_AFTER_NOTIFICATION_OTHER"),
            object: nil)
        
        
    }
    
    func reloadJobAfterNotification(notification: NSNotification){
        print("reloadJobAfterNotification other")
        
        var currentViewController: UIViewController? = navigationController?.visibleViewController
        if currentViewController == self {
            reloadJobList(date: Constants.current_date.string(format: "yyyy-MM-dd"))
        } else {
            print("not inside this page")
        }
        
        
    }
    
    func refreshJob(){
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "REFRESH_JOB"), object: nil, userInfo: nil)
    }
    
    
    //func create search bar
    func createSearchBar(){
        
//        if(self.navigationItem.titleView == searchBar)
//        {
//
//            searchButton.setImage(UIImage(named: "search"), for: .normal)
//            self.navigationItem.titleView=nil
//            if(selectedIndexTabStatus == Constants.SELECTED_TAB_ASSIGNED){
//                self.navigationController?.navigationBar.topItem?.title = Constants.ASSIGNED_TEXT
//            }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_ACKNOWLEDGED){
//                self.navigationController?.navigationBar.topItem?.title = Constants.ACKNOWLEDGED_TEXT
//            }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_INPROGRESS){
//                self.navigationController?.navigationBar.topItem?.title = Constants.INPROGRESS_TEXT
//            }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_COMPLETE){
//                self.navigationController?.navigationBar.topItem?.title = Constants.COMPLETE_TEXT
//            }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_CANCELLED){
//                self.navigationController?.navigationBar.topItem?.title = Constants.CANCELLED_TEXT
//            }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_FAILED){
//                self.navigationController?.navigationBar.topItem?.title = Constants.FAILED_TEXT
//            }else if(selectedIndexTabStatus == Constants.SELECTED_TAB_SELF_COLLECT){
//                self.navigationController?.navigationBar.topItem?.title = Constants.SELF_COLLECT_TEXT
//            }
//
//            let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
//            self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
//
//            self.navigationController?.navigationBar.tintColor = UIColor.white;
//
//        }
//        else{
//            searchBar.showsCancelButton = false
//            searchBar.placeholder = "Enter tracking number"
//            searchBar.delegate = self
//            searchBar.text = ""
//            self.navigationItem.titleView = searchBar
//            searchButton.setImage(UIImage(named: "cancel-search"), for: .normal)
//        }
        
        let storyboard = UIStoryboard(name: "SearchOrder", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "SearchOrderViewController") as! UINavigationController
        let levelController = navController.viewControllers[0] as! SearchOrderViewController
        levelController.itemTracking = itemTrackingSearch
        self.present(navController, animated: true, completion: nil)
        
        
        
        
    }
    //func create search bar
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing \(itemTrackingSearch)")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing \(itemTrackingSearch)")
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
        // getSearchJobList(itemTrackingSearch: itemTrackingSearch)
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        print("searchBarSearchButtonClicked \(itemTrackingSearch)")
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
        
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        self.navigationItem.titleView=nil
        self.navigationController?.navigationBar.topItem?.title="Self Collect"
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        let storyboard = UIStoryboard(name: "SearchOrder", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "SearchOrderViewController") as! UINavigationController
        let levelController = navController.viewControllers[0] as! SearchOrderViewController
        levelController.itemTracking = itemTrackingSearch
        
        var tabBarController: MainVC
        tabBarController = self.parent?.parent as!MainVC
        // tabBarController.navigationController?.pushViewController(navController, animated: true)
        
        tabBarController.present(navController, animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        itemTrackingSearch = searchText
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SHOW_CALENDAR"), object: nil, userInfo: nil)
    }
    
    //button call dropOff
    func dropOffcallButtonClicked(button: UIButton) {
        
        
        var dropOffCall = String()
        dropOffCall = self.jobList[button.tag].dropOffContact!
        let alert = UIAlertController(title: dropOffCall, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: { action in
            print("call \(dropOffCall)")
            
            if let url = URL(string: "tel://\(dropOffCall)") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            
            //   UIApplication.shared.openURL(NSURL(string: "tel://\(self.dropOffcall)") as! URL)
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    //button call pickup
    func pickupcallButtonClicked(button: UIButton) {
        
        
        var pickupCall = String()
        pickupCall = self.jobList[button.tag].pickUpContact!
        let alert = UIAlertController(title: pickupCall, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: { action in
            print("call \(pickupCall)")
            
            if let url = URL(string: "tel://\(pickupCall)") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            
            //   UIApplication.shared.openURL(NSURL(string: "tel://\(self.dropOffcall)") as! URL)
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    // show popup if number call empty
    func popupErrorNumberCall(button: UIButton) {
        
        let alertController = UIAlertController(title: "Error Number", message: "This number is not available", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // TableView
    //MARK: - Tableview Delegate & Datasource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return self.jobList.count;
        
        
    }
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
            let cell:JobCardTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "JobCardTableViewCell") as! JobCardTableViewCell
            
            // ui for cardJob
            // cell.cardJob.layer.cornerRadius = 10
            cell.selectionStyle = .none
        
            //cell.backgroundColor = UIColor(red:0.81, green:0.81, blue:0.82, alpha:1)
            
            //            cell.cardJob.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:1).cgColor
            //            cell.cardJob.layer.shadowOpacity = 1
            //            cell.cardJob.layer.shadowRadius = 8
            
           // cell.lbl_customerName.text = self.jobList[indexPath.row].companyName
        
            if(self.jobList[indexPath.row].companyName == "" || self.jobList[indexPath.row].companyName == nil){
                cell.lbl_customerName.text = " - "
                
            }else{
                cell.lbl_customerName.text = self.jobList[indexPath.row].companyName
            }
        
            cell.lbl_doReferenceNumber.text = self.jobList[indexPath.row].orderNumber
            
            var stepSequence = 0
            var stepCompletion = 0
            var isFailed = false
            var isCancelled = false
            var firstPending = true;
            
            for (index, jobstep) in  self.jobList[indexPath.row].job_stepsList.enumerated() {
             
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_PENDING || jobstep.job_step_status_id == Constants.JOB_STEP_INPROGRESS)
                {
                    if(firstPending == true)
                    {
                        stepSequence = jobstep.order_sequence!
                        firstPending = false
                    }
                    
                    
                    if(stepSequence >= jobstep.order_sequence!)
                    {
                        print("job step location in progress is \(String(describing: jobstep.location))")

                        if(jobstep.location == "" || jobstep.location == nil){
                            cell.lbl_dropOffAdrress.text = " - "
                        }else{
                            
                            cell.lbl_dropOffAdrress.text = NSString(format:"Step %d: %@", jobstep.order_sequence!,jobstep.location!) as String
                        }
                    }
                    
                    
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_COMPLETED)
                {
                    stepCompletion = stepCompletion + 1
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_CANCELLED)
                {
                    isCancelled = true
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_FAILED)
                {
                    isFailed = true
                }
            }
            
            if(self.jobList[indexPath.row].job_stepsList.count==1)
            {
                
                
                
                if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_PENDING)
                {
                    cell.lbl_step_status.text = "Pending"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.PENDING_COLOR)

                }
                else  if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_INPROGRESS)
                {
                    cell.lbl_step_status.text = "In Progress"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.INPROGRESS_COLOR)

                }
                else  if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_COMPLETED)
                {
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Completed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.COMPLETE_COLOR)
                    
                }
                else  if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_FAILED)
                {
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Failed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.FAILED_COLOR)
                    
                }
                else  if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_CANCELLED)
                {
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Cancelled"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.CANCELLED_COLOR)
                    
                }
            }
            else{
                
                if(stepCompletion==0)
                {
    
                    cell.lbl_step_status.text =  NSString(format:"0 of %d steps completed", self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.PENDING_COLOR)

                }
                else if(stepCompletion == self.jobList[indexPath.row].job_stepsList.count)
                {
                    cell.lbl_step_status.text =  NSString(format:"%d of %d steps completed", self.jobList[indexPath.row].job_stepsList.count,self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.COMPLETE_COLOR)
                }
                else if(isCancelled)
                {
                    cell.lbl_step_status.text =  "Cancelled"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.CANCELLED_COLOR)
                }
                else if(isFailed)
                {
                    cell.lbl_step_status.text =  "Failed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.FAILED_COLOR)
                }
                else {
                    cell.lbl_step_status.text =  NSString(format:"%d of %d steps completed", stepCompletion,self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.INPROGRESS_COLOR)
                }
                
            
                
            }
            
            if(self.jobList[indexPath.row].dropOffTime == nil || self.jobList[indexPath.row].dropOffTime == ""){
                
                cell.lbl_dropOffDate.isHidden = true
            }
            else{
                let dropOffTimeString = self.jobList[indexPath.row].dropOffTime!
                
                print("time is \(dropOffTimeString)")
                
                
//                let timeString = self.stringFormatDate(dropOffDate: dropOffTimeString, dateformat: "HH:mm", dateString: "hh:mm a")
                
//                print("new time is \(timeString)")
                
                
                
                cell.lbl_dropOffDate.text = dropOffTimeString
            }
            
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 148.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Self Collect didSelectRowAt Internet connection OK")
            let storyboard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
            let levelController = storyboard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as! OrderDetailsManyStepViewController
            
            // levelController.jobdetail = self.jobList[(indexPath.row)]
            
            if(self.jobList[indexPath.row].orderId != 0){
                levelController.orderID = self.jobList[indexPath.row].orderId
                levelController.orderNumber = self.jobList[indexPath.row].orderNumber
                
                self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
                
                
                self.navigationController?.pushViewController(levelController, animated: true)
            }else{
                // order id is 0 .. so cannot open other page
            }
        } else {
            print("Self Collect didSelectRowAt Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
    }

}

//extension SelfCollectViewController{
//    public func alert_popup(title: String!, message: String!) {
//        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
//        let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
//            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
//            UserDefaults.standard.synchronize()
//            let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
//            let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//            self.present(newViewController, animated: true, completion: nil)
//        })
//        alertController.addAction(defaultAction)
//        present(alertController, animated: true, completion: nil)
//    }
//}

extension SelfCollectViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}
