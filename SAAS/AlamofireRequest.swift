//
//  AlamofireRequest.swift
//  SAAS
//
//  Created by Coolasia on 21/12/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import UIKit
import Firebase


struct AlamofireRequest {
    
    static var headersBearer = "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
    
    
    static var uploadImageHeaders = [
        "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)",
        "Content-Type":"multipart/form-data", "Accept":"application/json"
    ]
    
    // update location every 5 minute if user enable 'online'
    static func updateLocation(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
       
        self.postRequestWithToken(httpMethod: .post, httpHeader: httpHeader, urlString: Constants.web_api_location+Constants.api_driver_update_location_minute,
                         paramDict: parameters as? Dictionary<String, AnyObject>,
                         completionHandler: completionHandler )
    }
    
    /* =========== USER LOGIN ======= */
    static func loginUser(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        let api_sign_in = "/api/login"
        print(api_sign_in)
        print(parameters)
        
        
        self.postRequest(httpMethod: .post, urlString: Constants.web_api_auth+api_sign_in,
                         paramDict: parameters as? Dictionary<String, AnyObject>,
                         completionHandler: completionHandler )
    }
    /* =========== USER LOGIN ======= */
    
    /* =========== GET PROFILE WORKER ======= */
    static func getProfileDriver(completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: Constants.web_api_auth+Constants.api_get_profile_worker,
                         completionHandler: completionHandler )
    }
    /* =========== GET PROFILE WORKER ======= */
    
    /* =========== GET PROFILE WORKER ======= */
    static func getOrderStatuses(completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: Constants.web_api+Constants.api_get_order_status,
                                  completionHandler: completionHandler )
    }
    /* =========== GET PROFILE WORKER ======= */
    
    
    
    /* =========== GET APP COMPANY SETTINGS ======= */
    static func getAppCompanySettings(completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: Constants.web_api_auth+Constants.api_app_company_settings,
                                  completionHandler: completionHandler )
    }
    /* =========== GET APP COMPANY SETTINGS ======= */
    
    /* =========== FETCH LIST OF JOB ======= */
    static func fetchJobList(url: String, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        var urlString = Constants.web_api + url
        print("fetchJobList url \(urlString)")
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: urlString, completionHandler: completionHandler )
    }
    /* =========== FETCH LIST OF JOB ======= */
    
    
    /* =========== FETCH DETAILS OF JOB BASED ON ORDER ID ======= */
    static func fetchDetailsOfJob(url: String, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        var urlString = Constants.web_api + url
        print("fetchDetailsOfJob url \(urlString)")
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: urlString, completionHandler: completionHandler )
    }
    /* =========== FETCH DETAILS OF JOB BASED ON ORDER ID ======= */
    
    
    /* =========== FETCH LISTING OF CUSTOMER ======= */
    static func fetchListingOfCustomer(url: String, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        var urlString = Constants.web_api_wms + Constants.api_get_customer_listing
        print("fetchListingOfCustomer url \(urlString)")
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: urlString, completionHandler: completionHandler )
    }
    /* =========== FETCH LISTING OF CUSTOMER ======= */
    
    
    /* =========== FETCH JOB STEP DETAILS BASED ON JOB STEP ID ======= */
    static func fetchJobStepDetails(url: String, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        var urlString = Constants.web_api + url
        print("fetchJobStepDetails url \(urlString)")
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: urlString, completionHandler: completionHandler )
    }
    /* =========== FETCH JOB STEP DETAILS BASED ON JOB STEP ID ======= */
    
    
    
    /* =========== FETCH DRIVER LIST ======= */
    static func fetchDriverList(url: String, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        var urlString = Constants.web_api_auth + url
        print("fetchJobStepDetails url \(urlString)")
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: urlString, completionHandler: completionHandler )
    }
    
    
    /* =========== BATCH UPDATE ======= */
    static func batchUpdate(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        let urlString = Constants.web_api + Constants.api_batch_update_order
        print("batchUpdate url \(urlString)")
        print(parameters)
        
        
        self.postRequestWithToken(httpMethod: .post, httpHeader: httpHeader, urlString: urlString, paramDict: parameters as? Dictionary<String, AnyObject>, completionHandler: completionHandler )
    }
    /* =========== BATCH UPDATE ======= */
    
    /* =========== DRIVER LOGOUT ======= */
    static func driverLogout(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        let urlString = Constants.web_api_auth + Constants.api_driver_logout
        print("api_driver_logout url \(urlString)")
        print(parameters)
        
        
        self.postRequestWithToken(httpMethod: .post, httpHeader: httpHeader, urlString: urlString, paramDict: parameters as? Dictionary<String, AnyObject>, completionHandler: completionHandler )
    }
    /* =========== DRIVER LOGOUT ======= */
    
    /* =========== VALIDATE USERNAME ======= */
    static func validateUsername(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        let urlString = Constants.web_api_auth + Constants.api_driver_validate_username
        print("validateUsername url \(urlString)")
        print(parameters)
        
        
        self.postRequest(httpMethod: .post, urlString: urlString,
                         paramDict: parameters as? Dictionary<String, AnyObject>,
                         completionHandler: completionHandler )
    }
    /* =========== VALIDATE USERNAME ======= */
    
    /* =========== FORGOT PASSWORD ======= */
    static func forgotPassword(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        let urlString = Constants.web_api_auth + Constants.api_driver_forget_password
        print("forgotPassword url \(urlString)")
        print(parameters)
        
        
        self.postRequest(httpMethod: .post, urlString: urlString,
                         paramDict: parameters as? Dictionary<String, AnyObject>,
                         completionHandler: completionHandler )
    }
    /* =========== FORGOT PASSWORD ======= */
    
    /* =========== SET NEW PASSWORD ======= */
    static func verificationCodeNewPassword(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        let urlString = Constants.web_api_auth + Constants.api_driver_reset_password
        print("verificationCodeNewPassword url \(urlString)")
        print(parameters)
        
        
        self.postRequest(httpMethod: .put, urlString: urlString,
                         paramDict: parameters as? Dictionary<String, AnyObject>,
                         completionHandler: completionHandler )
    }
    /* =========== SET NEW PASSWORD ======= */
    
    
    
    /* =========== UPLOAD IMGAGE  ======= */
    static func uploadImage(parameters:NSDictionary, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        let urlString = Constants.web_api_auth + Constants.api_upload_image_base64
        
        print("uploadImage url \(urlString)")
        
        self.postRequestWithToken(httpMethod: .post, httpHeader: httpHeader, urlString: urlString, paramDict: parameters as? Dictionary<String, AnyObject>, completionHandler: completionHandler )
        
    }
    /* =========== UPLOAD IMGAGE ======= */
    
    
    
    /* =========== SEARCH JOB LISTING ======= */
    static func searchJobListing(urlString:String, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        var httpHeader = [
            "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        print("searchJobListing url \(urlString)")
        
        
        self.postRequestWithToken(httpMethod: .get, httpHeader: httpHeader, urlString: urlString, completionHandler: completionHandler )
    }
    /* =========== SEARCH JOB LISTING ======= */
    
    
    
    // Main Request without access token
    static func postRequest(httpMethod: HTTPMethod, urlString: String, paramDict:Dictionary<String, AnyObject>? = nil,
                            completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.session.configuration.timeoutIntervalForResource = 120
        
       manager.request(urlString, method: httpMethod, parameters: paramDict, encoding: JSONEncoding.default,headers: nil)
        //Alamofire.request(request)
            .validate()
            .responseJSON { response in
                let responseResult = JSON(response.data)
                print("responseResult:  \(responseResult)")
                switch response.result {
                case .success(let JSON):
                    print("success postRequest: \(JSON)")
                    completionHandler(responseResult, response.response?.statusCode, nil )
                case .failure(let error):
                    print("failure postRequest: \(error)")
                    completionHandler(responseResult, response.response?.statusCode, error as NSError )
                }
        }
    }
    
    // Main Request with access token
    static func postRequestWithToken(httpMethod: HTTPMethod, httpHeader: HTTPHeaders,  urlString: String, paramDict:Dictionary<String, AnyObject>? = nil, completionHandler: @escaping (JSON?, Int?, NSError? ) -> ()) {
        
        /* This code for set requestTimeOut */
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.session.configuration.timeoutIntervalForResource = 120
        
        print("paramDict:  \(paramDict)")
        
        manager.request(urlString, method: httpMethod, parameters: paramDict, encoding: JSONEncoding.default,headers: httpHeader)
            .validate()
            .responseJSON { response in
                 print("headers \(httpHeader)")
                let responseResult = JSON(response.data)
                print("responseResultWithToken:  \(responseResult)")
                switch response.result {
                case .success(let JSON):
                    print("success postRequestWithToken")
                    completionHandler(responseResult, response.response?.statusCode, nil )
                case .failure(let error):
                    print("failure postRequestWithToken : code =  \(response.response?.statusCode)")
                    if(response.response?.statusCode == 401){
                        let isRelogin = UserDefaults.standard.value(forKey: "Relogin") as! Bool
                        
                        print("isRelogin is : \(isRelogin)")
                        print("value is : \(response.result.value)")
                        print("value err : \(response.result.error)")
                        
                        let keyChainDeviceId = KeychainManager()
                       // let deviceId: String = "DB684CD2-EF0F-4A79-A1DA-AA1C5F036947"

                        let deviceId: String = keyChainDeviceId.getDeviceIdentifierFromKeychain()
                        let device_model = UIDevice.current.modelName
                        //let device_manufacturer = UIDevice.current.name
                        print("device_model: \(device_model)")
                       // print("device_manufacturer: \(device_manufacturer)")
                        
                        var token = InstanceID.instanceID().token()!
                        print("fbs token:\(token)")
                        
                        
                        if let iphoneTokenExist = UserDefaults.standard.object(forKey: "iphone_token") {
                            if(token.isEmpty){
                                token = UserDefaults.standard.value(forKey: "iphone_token") as! String
                            }
                        }
                        
                        let loginInfo : Dictionary<String,AnyObject> = ["email" : UserDefaults.standard.value(forKey: "Email") as AnyObject,
                                                                        "password" : UserDefaults.standard.value(forKey: "Password") as AnyObject,
                                                                        "scope" : "admin" as AnyObject,
                                                                        "grant_type" : "password" as AnyObject,
                                                                        "iphone_token" : token as AnyObject,
//                                                                        "iphone_token" : UserDefaults.standard.value(forKey: "iphone_token") as AnyObject,
                                                                        "device_id" : deviceId as AnyObject,
                                                                        "device_manufacturer" : "Apple" as AnyObject,
                                                                        "device_model" : device_model as AnyObject,
                                                                        "device_platform" : "iOS" as AnyObject]
                        
                        AlamofireRequestRefreshToken.loginUserRefreshToken(parameters: loginInfo as NSDictionary) { responseObject, statusCode, error in
                            
                            UserDefaults.standard.setValue(true, forKey: "Relogin")
                            
                            print("relogin isRelogin")
                            print("statusCode is : \(String(describing: statusCode))")
                            
                            let swiftyJsonVar = JSON(responseObject)
                            print("relogin \(String(describing: responseObject))")
                            if(statusCode == 200){
                                
                                if(!token.isEmpty){
                                    UserDefaults.standard.setValue(token, forKey: "iphone_token")
                                }
                                
                                if(swiftyJsonVar["token"]["access_token"].exists()){
                                    print("token ? : \(swiftyJsonVar["token"]["access_token"].exists())")
                                    let access_token = swiftyJsonVar["token"]["access_token"].string
                                    print("access_token \(String(describing: access_token))")
                                    UserDefaults.standard.setValue(access_token!, forKey: "Token")
                                    print("access_tokenNEW \(String(describing: UserDefaults.standard.value(forKey: "Token")))")
                                    
                                    let httpHeader = [
                                        "Authorization": "Bearer \(UserDefaults.standard.value(forKey: "Token")!)"
                                    ]
                                    
                                    self.postRequestWithToken(httpMethod: httpMethod, httpHeader: httpHeader,  urlString: urlString, paramDict: paramDict, completionHandler: completionHandler )
                                }
                                UserDefaults.standard.setValue(false, forKey: "Relogin")
                            }else{
                                completionHandler(swiftyJsonVar, response.response?.statusCode, error as! NSError )
                            }
                            
                        }

//                        if(!isRelogin){
//                        }else{
//                           completionHandler(nil, response.response?.statusCode, error as NSError )
//                        }
                    }else{
                      completionHandler(responseResult, response.response?.statusCode, error as NSError )
                    }
                    
                }
        }
    }
    
    
    
    
}







//// other way to relogin
//if(response.response?.statusCode == 401){
//    print("relogin postRequestWithToken")
//    let error = response.result.error
//    let isRelogin = UserDefaults.standard.value(forKey: "Relogin") as! Bool
//
//    if(!isRelogin){
//        let loginInfo : Dictionary<String,AnyObject> = ["email" : UserDefaults.standard.value(forKey: "Email") as AnyObject,
//                                                        "password" : UserDefaults.standard.value(forKey: "Password") as AnyObject,
//                                                        "scope" : "worker" as AnyObject,
//                                                        "grant_type" : "password" as AnyObject,
//                                                        "iphone_token" : UserDefaults.standard.value(forKey: "iphone_token") as AnyObject]
//
//        AlamofireRequest.loginUser(parameters: loginInfo as NSDictionary) { responseObject, statusCode, error in
//
//            UserDefaults.standard.setValue(true, forKey: "Relogin")
//
//            print("relogin isRelogin")
//
//            let swiftyJsonVar = JSON(responseObject)
//            print("relogin \(responseObject)")
//            if(statusCode == 200){
//                print("relogin success")
//                if(swiftyJsonVar["token"]["access_token"].exists()){
//                    let access_token = swiftyJsonVar["token"]["access_token"].string
//                    UserDefaults.standard.setValue(access_token, forKey: "Token")
//                    self.postRequestWithToken(httpMethod: httpMethod, urlString: urlString, completionHandler: completionHandler )
//                }
//                UserDefaults.standard.setValue(false, forKey: "Relogin")
//            }
//
//        }
//    }
//
//    completionHandler(nil, response.response?.statusCode, error as? NSError )
//
//}else if(response.response?.statusCode == 200){
//    let JSON = response.result.value
//    print("success postRequestWithToken")
//    completionHandler(JSON as? NSDictionary, response.response?.statusCode, nil )
//}else{
//    print("failure postRequestWithToken")
//    let error = response.result.error
//    completionHandler(nil, response.response?.statusCode, error as? NSError )
//}

