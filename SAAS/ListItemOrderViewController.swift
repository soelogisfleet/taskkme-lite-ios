//
//  ListItemOrderViewController.swift
//  SAAS
//
//  Created by Coolasia on 7/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON

class ListItemOrderViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var heightConstraintTableView: NSLayoutConstraint!
    
    var countRow: Int?
    var orderID : Int?
    var jobdetail = Job()
    var orderStatusNameFromDB: String?
    
    var reachInternetConnection = Reachability()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundView = nil;
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("ListItemOrderViewController Internet connection OK")
            getOrderDetails()
        } else {
            print("ListItemOrderViewController Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON ORDER ID ======== */
    func getOrderDetails(){
        
        SwiftSpinner.show("Getting List Item Order Details...")
        
        let message   = String(format: Constants.api_get_order_detail, arguments: [String(describing: orderID!)])
        
        print("meesageGetOrderDetail \(message)")
        
        /* ====== FETCH JOB ======= */
        AlamofireRequest.fetchDetailsOfJob(url: message) { responseObject, statusCode, error in
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    if let job = Job(json: swiftyJsonVar) {
                        self.jobdetail = job
                        
                        self.tableView.reloadData()
                      //  self.sortOrderSequenceList()
                    }
                    
                    
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== FETCH JOB ======= */
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON ORDER ID ======== */
    
    /* ======== TABLE VIEW STEP LIST ====== */
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraintTableView.constant = tableView.contentSize.height
        tableView.layoutIfNeeded()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension;
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension
    }
    
    
    // TableView
    //MARK: - Tableview Delegate & Datasource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        
        countRow =  self.jobdetail.orderDetailList.count
        return countRow!
        
        
    }
    private func numberOfSectionsInTableView(tableViewOrderItem: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:ListItemOrderTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ListItemOrderTableViewCell") as! ListItemOrderTableViewCell
        
        print("Item \(self.jobdetail.orderDetailList[indexPath.row].descriptions)")
        print("remarks \(self.jobdetail.orderDetailList[indexPath.row].remarks)")
         print("quantity \(self.jobdetail.orderDetailList[indexPath.row].quantity)")
        
        if(self.jobdetail.orderDetailList[indexPath.row].descriptions == ""){
            cell.itemName.text = "Unknown item name"
        }else{
            cell.itemName.text = self.jobdetail.orderDetailList[indexPath.row].descriptions
        }
        
        if( self.jobdetail.orderDetailList[indexPath.row].quantity != nil)
        {
            cell.totalUnit.text = String(describing: self.jobdetail.orderDetailList[indexPath.row].quantity!) + " units"
            
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyboard = UIStoryboard(name: "AddItemOrderDetail", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "AddItemOrderDetail") as! UINavigationController
        let levelController = navController.viewControllers[0] as! AddItemOrderDetailViewController
        levelController.orderDetailsItemArray = [self.jobdetail.orderDetailList[indexPath.row]]
        levelController.isEdit = true
        levelController.orderStatusNameFromDB = orderStatusNameFromDB
        levelController.orderStatusId = jobdetail.orderStatusId
        levelController.orderId = jobdetail.orderId
        self.present(navController, animated: true, completion: nil)
    }
    
    /* ======== TABLE VIEW STEP LIST ====== */
    

}

extension ListItemOrderViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
               // UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}
