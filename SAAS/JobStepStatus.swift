//
//  JobStepStatus.swift
//  SAAS
//
//  Created by Coolasia on 6/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit


class JobStepStatus: NSObject {
    
    var id: Int?
    var name: String?
    var stage: String?
    var created_at: String?
    var updated_at: String?
    
    override init() {
        
        self.id = 0
        self.name = ""
        self.stage = ""
        self.created_at = ""
        self.updated_at = ""
        
        super.init()
        
    }
    
    init(id: Int?,name: String?,stage:String?,created_at:String?,updated_at:String?) {
        
        self.id = id
        self.name = name
        self.stage = stage
        self.created_at = created_at
        self.updated_at = updated_at
        
        super.init()
    }
    
}


extension JobStepStatus {
    convenience init?(json: JSON) {
        
        let id: Int? = json["id"].int
        let name: String? = json["name"].string
        let stage: String? = json["stage"].string
        let created_at: String? = json["created_at"].string
        let updated_at: String? = json["updated_at"].string
        
        
        
        self.init(id: id, name:name, stage:stage, created_at:created_at, updated_at:updated_at)
        
    }
}
