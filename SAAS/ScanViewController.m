/*
 * Copyright 2012 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import <AudioToolbox/AudioToolbox.h>
#import "ScanViewController.h"



@interface ScanViewController ()

@property (nonatomic, strong) ZXCapture *capture;
@property (nonatomic, weak) IBOutlet UIView *scanRectView;
@property (nonatomic, weak) IBOutlet UILabel *decodedLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *qrImageView;

@end

@implementation ScanViewController {
	CGAffineTransform _captureSizeTransform;
}
@synthesize delegate; //synthesise  MyClassDelegate delegate


#pragma mark - View Controller Methods

- (void)dealloc {
  [self.capture.layer removeFromSuperlayer];
}

- (void)viewDidLoad {
  [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:true];

    UIBarButtonItem *barCancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemCancel target:self action:@selector(dismissScan:)];
    self.navigationItem.leftBarButtonItem = barCancel;
    
    
    UIBarButtonItem *barConfirm = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone target:self action:@selector(confirmScan:)];
    //self.navigationItem.rightBarButtonItem = barConfirm;
    

    UIImage *image = [UIImage imageNamed:@"search"];
    UIButton *myCustomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    myCustomButton.bounds = CGRectMake( 0, 0, image.size.width, image.size.height );
    [myCustomButton setImage:image forState:UIControlStateNormal];
    [myCustomButton addTarget:self action:@selector(keyboardClick:) forControlEvents:UIControlEventTouchUpInside];

    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithCustomView:myCustomButton];
    self.navigationItem.rightBarButtonItem = button;
    
    NSArray *tempArray2= [[NSArray alloc] initWithObjects:barConfirm,button,nil];

    self.navigationItem.rightBarButtonItems = tempArray2;

    
    
    self.navigationItem.title = @"Scan Barcode";

  self.capture = [[ZXCapture alloc] init];
  self.capture.camera = self.capture.back;
  self.capture.focusMode = AVCaptureFocusModeContinuousAutoFocus;
    self.tableView.delegate=self;
  [self.view.layer addSublayer:self.capture.layer];

  [self.view bringSubviewToFront:self.scanRectView];
  [self.view bringSubviewToFront:self.decodedLabel];
  [self.view bringSubviewToFront:self.tableView];
  [self.view bringSubviewToFront:self.qrImageView];

    NSLog(@"scan order list: %lu",(unsigned long)_orderScanList.count);
    if(_orderScanList.count==0)
    {
        _isTransfer = true;
    }
   [self.tableView reloadData];
}

- (IBAction)keyboardClick:(id)sender
{
    //[self dismissViewControllerAnimated:true completion:nil];
    // code here
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:@"Enter Barcode"
                               message:@"Key in the last 4 digit of the barcode."
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                                   //Do Some action here
                                                   UITextField *textField = alert.textFields[0];
                                                   if(textField.text.length >= 4)
                                                   {
                                                       NSString *userInputCode = [textField.text substringFromIndex: [textField.text length] - 4];
                                                       
                                                       for(int i=0;i<_orderScanList.count;i++)
                                                       {
                                                           
                                                            NSString *originalCode = [[_orderScanList objectAtIndex:i] substringFromIndex: [[_orderScanList objectAtIndex:i] length] - 4];
                                                           
                                                           if([originalCode isEqualToString:userInputCode])
                                                           {
                                                               [_orderScanList removeObjectAtIndex:i];
                                                               [_tableView reloadData];
                                                               break;
                                                           }
                                                           
                                                       }
                                                   }
                                                  

                                                   
                                                   
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                       
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
    

}


- (IBAction)dismissScan:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
    // code here
}


- (IBAction)confirmScan:(id)sender
{
    [self.delegate scanDelegateMethod:self.orderScanList isTransfer:_isTransfer]; //this will call the method implemented in your other class

    [self dismissViewControllerAnimated:true completion:nil];
    // code here
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];

  self.capture.delegate = self;

  [self applyOrientation];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
  return toInterfaceOrientation == UIInterfaceOrientationPortrait;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
	[self applyOrientation];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
	[super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
	[coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
	} completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
	{
		[self applyOrientation];
	}];
}

#pragma mark - Private
- (void)applyOrientation {
	UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	float scanRectRotation;
	float captureRotation;

	switch (orientation) {
		case UIInterfaceOrientationPortrait:
			captureRotation = 0;
			scanRectRotation = 90;
			break;
		case UIInterfaceOrientationLandscapeLeft:
			captureRotation = 90;
			scanRectRotation = 180;
			break;
		case UIInterfaceOrientationLandscapeRight:
			captureRotation = 270;
			scanRectRotation = 0;
			break;
		case UIInterfaceOrientationPortraitUpsideDown:
			captureRotation = 180;
			scanRectRotation = 270;
			break;
		default:
			captureRotation = 0;
			scanRectRotation = 90;
			break;
	}
	[self applyRectOfInterest:orientation];
	CGAffineTransform transform = CGAffineTransformMakeRotation((CGFloat) (captureRotation / 180 * M_PI));
	[self.capture setTransform:transform];
	[self.capture setRotation:scanRectRotation];
	self.capture.layer.frame = self.view.frame;
}



- (void)applyRectOfInterest:(UIInterfaceOrientation)orientation {
    NSLog(@"%s %ld", __PRETTY_FUNCTION__, (long)orientation);
    CGFloat /*scaleVideo,*/ scaleVideoX, scaleVideoY;
    CGFloat videoSizeX, videoSizeY;
    NSLog(@"%s Main frame: at %f, %f size %f x %f", __PRETTY_FUNCTION__,
          self.view.frame.origin.x, self.view.frame.origin.y,
          self.view.frame.size.width, self.view.frame.size.height);
    CGRect transformedVideoRect = self.scanRectView.frame;
    NSLog(@"%s Starting video frame: at %f, %f size %f x %f", __PRETTY_FUNCTION__,
          transformedVideoRect.origin.x, transformedVideoRect.origin.y,
          transformedVideoRect.size.width, transformedVideoRect.size.height);
    NSLog(@"%s Center of video frame is at %f, %f", __PRETTY_FUNCTION__,
          self.scanRectView.center.x, self.scanRectView.center.y);
    CMFormatDescriptionRef format = self.capture.captureDevice.activeFormat.formatDescription;
    CMVideoDimensions dim = CMVideoFormatDescriptionGetDimensions(format);
    videoSizeX = dim.width;
    videoSizeY = dim.height;
    NSLog(@"%s Video size:%f x %f", __PRETTY_FUNCTION__, videoSizeX, videoSizeY);
    if(UIInterfaceOrientationIsPortrait(orientation)) {
        scaleVideoX = self.view.frame.size.width / videoSizeY;
        scaleVideoY = self.view.frame.size.height / videoSizeX;
    } else {
        NSLog(@"%s We don't run in landscape yet", __PRETTY_FUNCTION__);
        abort();
    }
    NSLog(@"%s scaling capture size by %f %f", __PRETTY_FUNCTION__, 1/scaleVideoX, 1/scaleVideoY);
    _captureSizeTransform = CGAffineTransformMakeScale(1/scaleVideoX, 1/scaleVideoY);
    CGRect scanRect = CGRectApplyAffineTransform(transformedVideoRect, _captureSizeTransform);
    self.capture.scanRect = scanRect;
    NSLog(@"%s Final scanRect: at %f, %f size %f x %f within video %f x %f", __PRETTY_FUNCTION__,
          scanRect.origin.x, scanRect.origin.y,
          scanRect.size.width, scanRect.size.height,
          videoSizeX, videoSizeY);
}

//- (void)applyRectOfInterest:(UIInterfaceOrientation)orientation {
//	CGFloat scaleVideo, scaleVideoX, scaleVideoY;
//	CGFloat videoSizeX, videoSizeY;
//	CGRect transformedVideoRect = self.scanRectView.frame;
//	if([self.capture.sessionPreset isEqualToString:AVCaptureSessionPreset1920x1080]) {
//		videoSizeX = 1080;
//		videoSizeY = 1920;
//	} else {
//		videoSizeX = 720;
//		videoSizeY = 1280;
//	}
//	if(UIInterfaceOrientationIsPortrait(orientation)) {
//		scaleVideoX = self.view.frame.size.width / videoSizeX;
//		scaleVideoY = self.view.frame.size.height / videoSizeY;
//		scaleVideo = MAX(scaleVideoX, scaleVideoY);
//		if(scaleVideoX > scaleVideoY) {
//			transformedVideoRect.origin.y += (scaleVideo * videoSizeY - self.view.frame.size.height) / 2;
//		} else {
//			transformedVideoRect.origin.x += (scaleVideo * videoSizeX - self.view.frame.size.width) / 2;
//		}
//	} else {
//		scaleVideoX = self.view.frame.size.width / videoSizeY;
//		scaleVideoY = self.view.frame.size.height / videoSizeX;
//		scaleVideo = MAX(scaleVideoX, scaleVideoY);
//		if(scaleVideoX > scaleVideoY) {
//			transformedVideoRect.origin.y += (scaleVideo * videoSizeX - self.view.frame.size.height) / 2;
//		} else {
//			transformedVideoRect.origin.x += (scaleVideo * videoSizeY - self.view.frame.size.width) / 2;
//		}
//	}
//	_captureSizeTransform = CGAffineTransformMakeScale(1/scaleVideo, 1/scaleVideo);
//    
//    NSLog(@"transform video rect x: %f y: %f",transformedVideoRect.origin.x,transformedVideoRect.origin.y);
//
//    
//	self.capture.scanRect = CGRectApplyAffineTransform(transformedVideoRect, _captureSizeTransform);
//   // self.capture.scanRect = self.scanRectView.frame;
//
//}

#pragma mark - Private Methods

- (NSString *)barcodeFormatToString:(ZXBarcodeFormat)format {
  switch (format) {
    case kBarcodeFormatAztec:
      return @"Aztec";

    case kBarcodeFormatCodabar:
      return @"CODABAR";

    case kBarcodeFormatCode39:
      return @"Code 39";

    case kBarcodeFormatCode93:
      return @"Code 93";

    case kBarcodeFormatCode128:
      return @"Code 128";

    case kBarcodeFormatDataMatrix:
      return @"Data Matrix";

    case kBarcodeFormatEan8:
      return @"EAN-8";

    case kBarcodeFormatEan13:
      return @"EAN-13";

    case kBarcodeFormatITF:
      return @"ITF";

    case kBarcodeFormatPDF417:
      return @"PDF417";

    case kBarcodeFormatQRCode:
      return @"QR Code";

    case kBarcodeFormatRSS14:
      return @"RSS 14";

    case kBarcodeFormatRSSExpanded:
      return @"RSS Expanded";

    case kBarcodeFormatUPCA:
      return @"UPCA";

    case kBarcodeFormatUPCE:
      return @"UPCE";

    case kBarcodeFormatUPCEANExtension:
      return @"UPC/EAN extension";

    default:
      return @"Unknown";
  }
}

#pragma mark - ZXCaptureDelegate Methods

- (void)captureResult:(ZXCapture *)capture result:(ZXResult *)result {
  if (!result) return;

	CGAffineTransform inverse = CGAffineTransformInvert(_captureSizeTransform);
	NSMutableArray *points = [[NSMutableArray alloc] init];
	NSString *location = @"";
	for (ZXResultPoint *resultPoint in result.resultPoints) {
		CGPoint cgPoint = CGPointMake(resultPoint.x, resultPoint.y);
		CGPoint transformedPoint = CGPointApplyAffineTransform(cgPoint, inverse);
		transformedPoint = [self.scanRectView convertPoint:transformedPoint toView:self.scanRectView.window];
		NSValue* windowPointValue = [NSValue valueWithCGPoint:transformedPoint];
		location = [NSString stringWithFormat:@"%@ (%f, %f)", location, transformedPoint.x, transformedPoint.y];
		[points addObject:windowPointValue];
	}


  // We got a result. Display information about the result onscreen.
  NSString *formatString = [self barcodeFormatToString:result.barcodeFormat];
  NSString *display = [NSString stringWithFormat:@"Scanned!\n\nFormat: %@\n\nContents:\n%@\nLocation: %@", formatString, result.text, location];
    
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Succesfully Scan"
                                 message:[NSString stringWithFormat:@"Tracking Number: %@",result.text]
                                 preferredStyle:UIAlertControllerStyleAlert];
    

    
    
    
    
    
    
  [self.decodedLabel performSelectorOnMainThread:@selector(setText:) withObject:display waitUntilDone:YES];
  [self.decodedLabel setHidden:true];

    if(_isTransfer)
    {
        Boolean isExist = false;
        
        [self presentViewController:alert animated:YES completion:nil];

        
        for(int i=0;i<_orderScanList.count;i++)
        {
            
            
            
            if([[_orderScanList objectAtIndex:i] isEqualToString:result.text])
            {
                isExist=true;
                break;
            }
            
        }
        if(isExist==false)
        {
            [_orderScanList addObject:result.text];
            [_tableView reloadData];
        }
        
        // Vibrate
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        
        [self.capture stop];
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:^{
            }];
        });

        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.capture start];
        });



    }
    else{
        
        Boolean correctOrderBool = false;
        
        for(int i=0;i<_orderScanList.count;i++)
        {
            
    
            
            if([[_orderScanList objectAtIndex:i] isEqualToString:result.text])
            {
                correctOrderBool=true;
                [_orderScanList removeObjectAtIndex:i];
                [_tableView reloadData];
                break;
            }
            
        }
        
        
        if(correctOrderBool)
        {
            [self presentViewController:alert animated:YES completion:nil];

        }
        else{
            if(_isInProgress)
            {
                alert.title = @"Error";
                alert.message = @"Wrong Barcode Scan";
            }
            else{
                alert.title = @"Error";
                alert.message = @"No Job Assigned";
            }
            
            [self presentViewController:alert animated:YES completion:nil];

        }
        
        // Vibrate
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        
        [self.capture stop];
        
        
        if(_orderScanList.count==0)
        {
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self.capture start];
                
                [alert dismissViewControllerAnimated:YES completion:^{
                }];
                [self dismissViewControllerAnimated:true completion:nil];
                
                [self.delegate scanDelegateMethod:self.orderScanList isTransfer:_isTransfer]; //this will call the method implemented in your other class
                
                // code here

            });
            
                 }
        else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [alert dismissViewControllerAnimated:YES completion:^{
                }];
            });
            
 
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self.capture start];
            });
        }
        
        
    }
    
  

    
 
}

#pragma uitableview delegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_orderScanList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"OrderItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [_orderScanList objectAtIndex:indexPath.row];
    return cell;
}



@end
