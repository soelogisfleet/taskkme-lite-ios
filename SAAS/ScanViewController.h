/*
 * Copyright 2012 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import <UIKit/UIKit.h>
#import <ZXingObjC/ZXingObjC.h>

@protocol ScanDelegate <NSObject>   //define delegate protocol
- (void) scanDelegateMethod: (NSMutableArray *) unscannedItems isTransfer: (Boolean) isTransfer;  //define delegate method to be implemented within another class
@end //end protocol

@interface ScanViewController : UIViewController <ZXCaptureDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,retain) NSMutableArray *orderScanList;
@property (nonatomic,assign) Boolean isTransfer;
@property (nonatomic,assign) Boolean isInProgress;
@property (nonatomic, weak) id <ScanDelegate> delegate; //define MyClassDelegate as delegate

@end
