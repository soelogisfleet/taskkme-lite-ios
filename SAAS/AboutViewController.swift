//
//  AboutViewController.swift
//  WMG
//
//  Created by Coolasia on 16/1/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    

    @IBOutlet weak var lbl_version: UILabel!
  
    @IBOutlet weak var deviceID: UILabel!
    
    var deviceId: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // deviceID Click
        let clickDeviceID = UILongPressGestureRecognizer(target: self, action: #selector(AboutViewController.clickDeviceIDFunc(_:)))
        self.deviceID.addGestureRecognizer(clickDeviceID)
        self.deviceID.isUserInteractionEnabled = true
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        self.navigationController?.isNavigationBarHidden = false
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;

        displayVersion()
        
        let keyChainDeviceId = KeychainManager()
        self.deviceId = keyChainDeviceId.getDeviceIdentifierFromKeychain()
        
        self.deviceID.text = self.deviceId
        
    }
    
    /* Open List Order Attempt */
    func clickDeviceIDFunc(_ gr: UITapGestureRecognizer) {
        //Copy a string to the pasteboard.
        UIPasteboard.general.string = self.deviceId
        
        //Alert
        let alertController = UIAlertController(title: "", message: "Device ID is copied!", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }

    @IBAction func leftBarButtonTap(_ sender: Any) {
       // self.sideBarController.showMenuViewController(in: LMSideBarControllerDirection(rawValue: 0))
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.menuViewSize = CGSize(width: view.frame.size.width / 4 * 3, height: view.frame.size.height)
        
        // Present the view controller
        self.frostedViewController.presentMenuViewController()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func displayVersion(){
        let appVersionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        
//        let appVersionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        
        
        lbl_version.text = "Version \(appVersionString)"
        print(appVersionString)
    }
    



}
