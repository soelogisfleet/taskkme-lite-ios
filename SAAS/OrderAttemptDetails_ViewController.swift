//
//  OrderAttemptDetails_ViewController.swift
//  WMG
//
//  Created by Coolasia on 27/3/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher
import MarqueeLabel
import SwiftyJSON
import Alamofire
import SwiftSpinner
import CoreData
import CoreLocation
import Photos
import MediaBrowser
import SDWebImage
import SCLAlertView
import Toast_Swift

class OrderAttemptDetails_ViewController: UIViewController {
    
    @IBOutlet weak var receivedTitle: UILabel!
    //declaration
    
    @IBOutlet weak var failedDateLabel: UILabel!
    @IBOutlet weak var failedAttemptView: UIView!
    
    @IBOutlet weak var receivedBy: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var failedReasonLabel: UILabel!
    
    @IBOutlet weak var txtNotes: UILabel!
    
    @IBOutlet weak var viewPhoto: UIView!
    @IBOutlet weak var totalPhoto: UILabel!
    @IBOutlet weak var imageSlide: ImageSlideshow!
 //   @IBOutlet weak var lbl_nric: UITextView!
 //   @IBOutlet weak var lbl_srno: UITextView!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var imageSlideHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var photoTextHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addPhotoButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewPhotoHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var addPhotos: UIButton!
    
    @IBOutlet weak var photoText: UILabel!
    
    
    open var tintColor = UIColor.defaultTintColor()
    //    var nameReceivedBy = String()
    //    var dateAttempt = String()
    //    var reasonAttempt = String()
    var orderID : Int?
    var orderStatusID: Int?
    var inProgressStatusIDFromOrderStatusDB: Int?
    var jobStepID : Int?
    var jobStepStatusID : Int?
    var orderAttemptArray: [OrderAttempt] = []
    var orderAttemptID = Int()
    
    var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate?
    var items = [KingfisherSource]()
    var url: URL!
    var arrayIdAllImage: [Int] = []
    var arrayEditDescriptionIndex: [Int] = []
    var haveImageSignature: Bool = false
    
    var arrayImageURL: [String] = []
    var arrayImageDecription: [String] = []
    
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var request: URLRequest!
    let moc = DataController().managedObjectContext
    var totalPhotoWithoutSignature = Int()
    
    /// For Image Gallery
    var selections = [Bool]()
    
    // Set your Image first when you need pre-caching
    //    browser = MediaBrowser(delegate: self)
    //    var mediaArray = DemoData.webPhotos()
    var mediaArray = [Media]()
    var thumbs = [Media]()
    
    var displayActionButton = false
    var displaySelectionButtons = false
    var displayMediaNavigationArrows = true
    var enableGrid = true
    var startOnGrid = true
    var autoPlayOnAppear = false
    var browser = MediaBrowser()
    /// For Image Gallery
    
    var photo: MBPhotoPicker?
    var imagesString64Base: [String] = []
    var reachInternetConnection = Reachability()
    
    
    func buildUIMediaBrowser(){
        /// For Image Gallery
       // navigationController?.navigationBar.tintColor = UIColor.black
        
        mediaArray = self.webPhotos()
        thumbs = self.webPhotos()
        
        browser = MediaBrowser(delegate: self)
        browser.displayActionButton = displayActionButton
        browser.displayMediaNavigationArrows = displayMediaNavigationArrows
        browser.displaySelectionButtons = displaySelectionButtons
        browser.alwaysShowControls = displaySelectionButtons
        browser.zoomPhotosToFill = true
        browser.enableGrid = enableGrid
        browser.startOnGrid = startOnGrid
        browser.enableSwipeToDismiss = true
        browser.autoPlayOnAppear = autoPlayOnAppear
        browser.cachingImageCount = 2
        browser.setCurrentIndex(at: 2)
        
        
        if displaySelectionButtons {
            selections.removeAll()
            
            for _ in 0..<mediaArray.count {
                selections.append(false)
            }
        }
        
        /// For Image Gallery
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.reloadInfo()
    }
    
    func UTCToLocal(date:String) -> String {
        //2017-07-24 02:16:25
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-mm-dd h:mm a"
        
        return dateFormatter.string(from: dt!)
    }

    
    func reloadInfo(){
        
        self.addPhotoButtonHeightConstraint.constant = 0
        self.addPhotos.layoutIfNeeded()
        self.addPhotos.isHidden = true
        
        self.title = "Order Attempt Details"
        
     //   self.receivedBy.textContainer.maximumNumberOfLines = 1
      //  self.lbl_nric.textContainer.maximumNumberOfLines = 1
      //  self.lbl_srno.textContainer.maximumNumberOfLines = 1
    //    self.txtNotes.textContainer.maximumNumberOfLines = 5
     //   self.receivedBy.scrollsToTop = false
    //    self.lbl_nric.scrollsToTop = false
   //     self.lbl_srno.scrollsToTop = false
        
        self.haveImageSignature = false
        
        //self.buildUIMediaBrowser()
        
        totalPhotoWithoutSignature = 0
        
    //    self.receivedBy.isEditable = false
     //   self.lbl_nric.isEditable = false
     //   self.lbl_srno.isEditable = false
     //   self.txtNotes.isEditable = false
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(OrderAttemptDetails_ViewController.onTouchCancelButton))
       // cancelButton.tintColor = tintColor
        self.navigationItem.leftBarButtonItem = cancelButton
        
        
        
        /*Only in progress can edit*/
        
        inProgressStatusIDFromOrderStatusDB = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.INPROGRESS_TEXT)
        
        if(orderStatusID == inProgressStatusIDFromOrderStatusDB){
            if(jobStepStatusID == Constants.JOB_STEP_INPROGRESS){
            //    let editButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.edit, target: self, action: #selector(OrderAttemptDetails_ViewController.onTouchEditButton))
            //    self.navigationItem.rightBarButtonItem = editButton

            }
        }
        
        if(orderAttemptArray[0].reason == nil)
        {
            failedAttemptView.isHidden = true
        }
        else{
            failedAttemptView.isHidden = false
            failedDateLabel.text = self.UTCToLocal(date: orderAttemptArray[0].submittedTime!)
            let reasonString = String(format: "\n%@\n", orderAttemptArray[0].reason!)

            failedReasonLabel.text = reasonString

        }
        txtDate.text = self.UTCToLocal(date: orderAttemptArray[0].submittedTime!)
        txtNotes.text =  orderAttemptArray[0].note
        
        
        
        if(orderAttemptArray[0].receivedBy == nil || orderAttemptArray[0].receivedBy == ""){
            receivedBy.text =  " - "
        }else{
            receivedBy.text =  orderAttemptArray[0].receivedBy
        }
        
//        if(orderAttemptArray[0].nric == nil || orderAttemptArray[0].nric == ""){
//            lbl_nric.text =  " - "
//        }else{
//            lbl_nric.text =  orderAttemptArray[0].nric
//        }
        
//        if(orderAttemptArray[0].srno == nil || orderAttemptArray[0].srno == ""){
//            lbl_srno.text =  " - "
//        }else{
//            lbl_srno.text =  orderAttemptArray[0].srno
//        }
        
        if(orderAttemptArray[0].note == nil || orderAttemptArray[0].note == ""){
            txtNotes.text =  " - "
        }else{
            txtNotes.text =  orderAttemptArray[0].note
        }
        
        print("received is \(String(describing: orderAttemptArray[0].receivedBy))")
        print("reason is \(String(describing: orderAttemptArray[0].reason))")
        
        //        if((orderAttemptArray[0].receivedBy?.characters.count)!<0 || (orderAttemptArray[0].reason?.characters.count)!>0){
        //            receivedTitle.text = "Reason"
        //            receivedBy.text = orderAttemptArray[0].reason
        //        } else{
        //            receivedTitle.text = "Received By"
        //            receivedBy.text = orderAttemptArray[0].receivedBy
        //        }
        
        if(self.orderAttemptArray[0].orderAttemptImageList.count<0){
            imageSlide.isHidden = true
        }
            
        else{
            
            imageSlide.isHidden = false
            
            print("List Image \(self.orderAttemptArray[0].orderAttemptImageList)")
            
            
            self.imageSlide.backgroundColor = UIColor.white
            self.imageSlide.pageControlPosition = PageControlPosition.underScrollView
            self.imageSlide.pageControl.currentPageIndicatorTintColor = UIColor.black;
            self.imageSlide.pageControl.pageIndicatorTintColor = UIColor.lightGray;
            self.imageSlide.contentScaleMode = UIViewContentMode.scaleAspectFit
            
            arrayIdAllImage.removeAll()
            arrayImageURL.removeAll()
            arrayImageDecription.removeAll()
            
            if self.orderAttemptArray[0].orderAttemptImageList.count != 0{
                
                items.removeAll()
                self.imageSlide.setImageInputs(items)
                self.totalPhotoWithoutSignature = 0
                self.totalPhotoWithoutSignature = self.orderAttemptArray[0].orderAttemptImageList.count
                
                for index in 0 ..< self.orderAttemptArray[0].orderAttemptImageList.count
                {
                    print("have image \(self.orderAttemptArray[0].orderAttemptImageList.count)")
                    print("have index \(index)")
                    
                    if(self.orderAttemptArray[0].orderAttemptImageList[index].isSignature)!{
                        url = URL(string: self.orderAttemptArray[0].orderAttemptImageList[index].mUrl!)
                        
                        let kingfisherSource = KingfisherSource(urlString: "\(self.orderAttemptArray[0].orderAttemptImageList[index].mUrl!)")!
                        items.append(kingfisherSource)
                        
                        self.totalPhotoWithoutSignature = self.totalPhotoWithoutSignature - 1
                        haveImageSignature = true
                        
                    }else{
                        
                        if(self.orderAttemptArray[0].orderAttemptImageList[index].mUrl != nil){
                        arrayImageURL.append(self.orderAttemptArray[0].orderAttemptImageList[index].mUrl!)
                            
                        arrayIdAllImage.append(self.orderAttemptArray[0].orderAttemptImageList[index].id!)
                            
                            if(self.orderAttemptArray[0].orderAttemptImageList[index].mNote != nil){
                                arrayImageDecription.append(self.orderAttemptArray[0].orderAttemptImageList[index].mNote!)
                            }else{
                                arrayImageDecription.append("")
                            }
                            
                            previewImage.sd_setImage(with: URL(string: self.orderAttemptArray[0].orderAttemptImageList[index].mUrl!))
                            
                        }else{
                          //  arrayImageURL.append("")
                            self.totalPhotoWithoutSignature = self.totalPhotoWithoutSignature - 1
                        }
                        
                        
                        
                        
                    }
                }
                self.imageSlide.setImageInputs(items)
            }
            
            print("have totalPhotoWithoutSignature \(self.totalPhotoWithoutSignature)")
            print("haveImageSignature \(haveImageSignature)")
            
            if(self.totalPhotoWithoutSignature <= 0){
                previewImage.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
            }else{
//                if(haveImageSignature){
//                    previewImage.sd_setImage(with: URL(string: self.orderAttemptArray[0].orderAttemptImageList[self.totalPhotoWithoutSignature-1].mUrl!))
//                }else{
//
////                    self.imageSlide.isHidden = true
////                    imageSlideHeightConstraint.constant = 0
////                    self.imageSlide.layoutIfNeeded()
//
//                    previewImage.sd_setImage(with: URL(string: self.orderAttemptArray[0].orderAttemptImageList[self.totalPhotoWithoutSignature].mUrl!))
//                }
                
             //   previewImage.sd_setImage(with: URL(string: self.orderAttemptArray[0].orderAttemptImageList[self.totalPhotoWithoutSignature].mUrl!))
                
            }
            
            self.imageSlide.backgroundColor = UIColor.white
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(OrderAttemptDetails_ViewController.tapped(_:)))
            
            self.imageSlide.addGestureRecognizer(recognizer)
            
            if(totalPhotoWithoutSignature == 0){
                self.viewPhotoHeightConstraint.constant = 0
                self.viewPhoto.layoutIfNeeded()
                self.viewPhoto.isHidden = true
                
                self.photoTextHeightConstraint.constant = 0
                self.photoText.layoutIfNeeded()
                self.photoText.isHidden = true
                
                
            }else{
                self.viewPhotoHeightConstraint.constant = 113
                self.viewPhoto.layoutIfNeeded()
                self.viewPhoto.isHidden = false
                
                self.photoTextHeightConstraint.constant = 28
                self.photoText.layoutIfNeeded()
                self.photoText.isHidden = false
                
                let disclosure = UITableViewCell()
                disclosure.frame = viewPhoto.bounds
                disclosure.accessoryType = .disclosureIndicator
                disclosure.isUserInteractionEnabled = false
                
                viewPhoto.addSubview(disclosure)
            }
            
            totalPhoto.text = String(self.totalPhotoWithoutSignature)
            
            
            let viewPhotoTapped = UITapGestureRecognizer(target: self, action: #selector(OrderAttemptDetails_ViewController.viewPhotoTap(_:)))
            
            self.viewPhoto.addGestureRecognizer(viewPhotoTapped)
            
        }
    }
    
    
    @IBAction func clickAddPhotos(_ sender: Any) {
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("OrderAttempt clickAddPhotos Internet connection OK")
            photo = MBPhotoPicker()
            photo?.disableEntitlements = false // If you don't want use iCloud entitlement just set this value True
            photo?.alertTitle = nil
            photo?.alertMessage = nil
            photo?.disableEntitlements = true
            photo?.resizeImage = CGSize(width: 250, height: 150)
            photo?.allowDestructive = false
            photo?.allowEditing = false
            photo?.cameraDevice = .rear
            photo?.cameraFlashMode = .auto
            photo?.photoCompletionHandler = { (image: UIImage?) -> Void in
                
                
                let imageData:NSData = UIImagePNGRepresentation(image!)! as NSData
                let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                
                let string1 = "data:image/png;base64,\(imageStr)"
                self.imagesString64Base.removeAll()
                self.imagesString64Base.append(string1)
                
                let imageStringParams : Dictionary<String,AnyObject> = ["folder_name" : "TestingUploadBase64" as AnyObject,
                                                                        "base64_images" : self.imagesString64Base as AnyObject]
                
                /* ======= UPLOAD BASE64 IMAGE TO GET URL IMAGE ========= */
                
                self.view.makeToast("Photo will upload in background")
                
                /* ====== UPLOAD BASE64 IMAGE ======= */
                AlamofireRequest.uploadImage(parameters: imageStringParams as NSDictionary) { responseObject, statusCode, error in
                    
                    if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                        if(responseObject == nil)
                        {
                            
                        }else{
                            let swiftyJsonVar = JSON(responseObject)
                            if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                                if(swiftyJsonVar["status"].bool == true){
                                    var resultImageNormalList = swiftyJsonVar["result"].array
                                    for subJson in resultImageNormalList! {
                                        let resultImageNormal = subJson["image_normal"].string
                                        // self.arrayImageURL.append(resultImageNormal!)
                                        print("resultImageNormal \(resultImageNormal)")
                                        
                                        self.view.makeToast("Photo successfully updated.")
                                        
                                        self.openDescriptionImage(resultImageNormal: resultImageNormal!)
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                    }else{
                        //error code
                        if(responseObject == nil)
                        {
                            self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                        }else{
                            let swiftyJsonVar = JSON(responseObject)
                            let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                            self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                        }
                    }
                }
                /* ====== UPLOAD BASE64 IMAGE ======= */
                
                /* ======= UPLOAD BASE64 IMAGE TO GET URL IMAGE ========= */
                
                
                
            }
            photo?.cancelCompletionHandler = {
                print("Cancel Pressed")
            }
            photo?.errorCompletionHandler = { (error: MBPhotoPicker.ErrorPhotoPicker!) -> Void in
                //print("Error: \(error.rawValue)")
            }
            photo?.present(self)
        } else {
            print("OrderAttempt clickAddPhotos Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
       
    }
    
    func openDescriptionImage(resultImageNormal: String){
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Image Description", message: "Enter a text", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            
        }
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \(textField?.text)")
            if(textField?.text == ""){
                textField?.text = " - "
            }
           // self.arrayImageDecription.append((textField?.text)!)
            
            self.orderAttemptArray[0].orderAttemptImageList.removeAll()
            
            let orderAttemptImage = OrderAttemptImage(mUrl:resultImageNormal,mNote:textField?.text,isSignature: false)
            self.orderAttemptArray[0].orderAttemptImageList.append(orderAttemptImage)
            
            self.updateNewImageToServer()
            
        }))
        
        // 4. Present the alert.
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        self.navigationController?.navigationBar.topItem?.title="Order Attempt Details"
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        self.navigationController?.isNavigationBarHidden = false
        
      //  self.receivedBy.textContainer.maximumNumberOfLines = 1
     //   self.lbl_nric.textContainer.maximumNumberOfLines = 1
     //   self.lbl_srno.textContainer.maximumNumberOfLines = 1
     //   self.txtNotes.textContainer.maximumNumberOfLines = 5
    //    self.receivedBy.scrollsToTop = false
    //    self.lbl_nric.scrollsToTop = false
    //    self.lbl_srno.scrollsToTop = false
        
      //  navigationController?.navigationBar.tintColor = UIColor.black
        
        print("array select at index \(selections)")
        
        let indexSelected = selections.enumerated().filter { $1 }.map { $0.0 }
        print(indexSelected)
        
        
        
       // arrayIdAllImage = [000,111,222,333,4444,55555,66666]
        if(arrayIdAllImage.count > 0){
            
            //remove image selected
            
            if(indexSelected.count > 0){
                
                self.orderAttemptArray[0].orderAttemptImageList.removeAll()
                
                print("arrayIDImageCount \(arrayIdAllImage.count)")
                
                for removeImageAtindex in indexSelected{
                    print("removeImageAtindex \(removeImageAtindex)")
                    let idImageRemove = self.arrayIdAllImage[removeImageAtindex]
                    
                    print("idImageRemove \(idImageRemove)")
                    
                    let orderAttemptImage = OrderAttemptImage(id:idImageRemove,isRemove:true,isSignature: false)
                    self.orderAttemptArray[0].orderAttemptImageList.append(orderAttemptImage)
                    
                }
            }
            
            //edit description image
            if(arrayEditDescriptionIndex.count > 0){
                self.orderAttemptArray[0].orderAttemptImageList.removeAll()
                
                for editDescriptionAtIndex in arrayEditDescriptionIndex{
                    print("editDescriptionAtIndex \(editDescriptionAtIndex)")
                    let idEditDescription = self.arrayIdAllImage[editDescriptionAtIndex]
                    
                    print("idEditDescription \(idEditDescription)")
                    
                    let orderAttemptImage = OrderAttemptImage(id:idEditDescription,isRemove:false,mNote: arrayImageDecription[editDescriptionAtIndex])
                    self.orderAttemptArray[0].orderAttemptImageList.append(orderAttemptImage)
                    
                }
                
            }
            
            if(indexSelected.count > 0 || arrayEditDescriptionIndex.count > 0){
                print("update remove image or edit desc")
                if reachInternetConnection.isConnectedToNetwork() == true {
                    print("Internet connection OK")
                    removeImagePhotoFromGalleryAndEditDescription()
                } else {
                    print("Internet connection FAILED")
                    var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
                
            }
            
        }
        
     //   self.buildUIMediaBrowser()
        
    }
    
    // Update Edit/Remove image Process
    
    func removeImagePhotoFromGalleryAndEditDescription() {
        
        let batchOrder: BatchOrder = BatchOrder()
        
        let para:NSMutableDictionary = NSMutableDictionary()
        let prodArray:NSMutableArray = NSMutableArray()
        
        batchOrder.orderIds.append(String(self.orderID!))
        batchOrder.orderStatus = String(describing: self.orderStatusID!)
        
        
        para.setValue(batchOrder.orderIds, forKey: "id")
        para.setValue(batchOrder.orderStatus, forKey: "order_status_id")
        
        let paraObjectStep:NSMutableDictionary = NSMutableDictionary()
        let prodArrayStep:NSMutableArray = NSMutableArray()
        
        paraObjectStep.setValue(self.jobStepID, forKey: "job_step_id")
        paraObjectStep.setValue(self.jobStepStatusID, forKey: "job_step_status_id")
        
        prodArray.add(self.orderAttemptArray[0].toJsonEditDescriptionAndRemoveImage())
        
        paraObjectStep.setObject(prodArray, forKey: "step_attempts" as NSCopying)
        
        prodArrayStep.add(paraObjectStep)
        
        para.setObject(prodArrayStep, forKey: "job_steps" as NSCopying)
        
        let dataPara:NSMutableDictionary = NSMutableDictionary()
        let dataArray:NSMutableArray = NSMutableArray()
        
        dataArray.add(para)
        dataPara.setValue(dataArray, forKey: "data")
        
        SwiftSpinner.show("Update Image Order Attempt ...")
        
        self.updateStepAttemptsForJobStep(dataPara: dataPara)
        
    }
    
   
    
    /* ======== UPDATE NEW IMAGE TO SERVER ========== */
    func updateNewImageToServer() {
        
        let batchOrder: BatchOrder = BatchOrder()
        
        let para:NSMutableDictionary = NSMutableDictionary()
        let prodArray:NSMutableArray = NSMutableArray()
        
        batchOrder.orderIds.append(String(self.orderID!))
        batchOrder.orderStatus = String(describing: self.orderStatusID!)
        
        
        para.setValue(batchOrder.orderIds, forKey: "id")
        para.setValue(batchOrder.orderStatus, forKey: "order_status_id")
        
        let paraObjectStep:NSMutableDictionary = NSMutableDictionary()
        let prodArrayStep:NSMutableArray = NSMutableArray()
        
        paraObjectStep.setValue(self.jobStepID, forKey: "job_step_id")
        paraObjectStep.setValue(self.jobStepStatusID, forKey: "job_step_status_id")
        
        prodArray.add(self.orderAttemptArray[0].toJsonUploadPhotoOnlyWithURL())
        
        paraObjectStep.setObject(prodArray, forKey: "step_attempts" as NSCopying)
        
        prodArrayStep.add(paraObjectStep)
        
        para.setObject(prodArrayStep, forKey: "job_steps" as NSCopying)
        
        let dataPara:NSMutableDictionary = NSMutableDictionary()
        let dataArray:NSMutableArray = NSMutableArray()
        
        dataArray.add(para)
        dataPara.setValue(dataArray, forKey: "data")
        
        SwiftSpinner.show("Update new Photo Order Attempt ...")
        
        self.updateStepAttemptsForJobStep(dataPara: dataPara)
        
    }
    /* ======== UPDATE NEW IMAGE TO SERVER ========== */
    
    /* ======== RELOAD JOB STEP DETAIL BASED ON JOB STEP ID ======== */
    func getJobStepDetails(){
        
        SwiftSpinner.show("Getting Job Step Details...")
        
        let message   = String(format: Constants.api_get_job_step_details, arguments: [String(describing: jobStepID!)])
        
        print("meesageGetOrderDetail \(message)")
        
        /* ====== FETCH JOB STEP ======= */
        AlamofireRequest.fetchJobStepDetails(url: message) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    let resultJson = swiftyJsonVar["result"]
                    if let jobStep = JobSteps(json: resultJson) {
                        for i in 0..<jobStep.orderAttemptList.count{
                            var attemptID = jobStep.orderAttemptList[i].id
                            print("attemptID \(attemptID!)")
                            print("self.orderAttemptID \(self.orderAttemptID)")
                            
                            if(attemptID == self.orderAttemptID){
                                self.orderAttemptArray = jobStep.orderAttemptList
                                self.reloadInfo()
                            }
                        }
                        
                        
                        
                        
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== FETCH JOB STEP ======= */
    }
    
    /* ======== RELOAD JOB STEP DETAIL BASED ON JOB STEP ID ======== */
    
    
    func viewPhotoTap(_ gr: UITapGestureRecognizer) {
        
        if(totalPhotoWithoutSignature != 0){
            self.buildUIMediaBrowser()
            self.navigationController?.pushViewController(browser, animated: true)
        }
    }
    
    func tapped(_ gr: UITapGestureRecognizer) {
        
        let imageSlide:ImageSlideshow = gr.view as! ImageSlideshow
        imageSlide.presentFullScreenController(from: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onTouchCancelButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func onTouchEditButton() {
        
     //   self.receivedBy.isEditable = true
      //  self.lbl_nric.isEditable = true
     //   self.lbl_srno.isEditable = true
     //   self.txtNotes.isEditable = true
        
        self.txtNotes.textAlignment = .left
        self.receivedBy.textAlignment = .left
     //   self.lbl_nric.textAlignment = .left
     //   self.lbl_srno.textAlignment = .left
        
        self.receivedBy.textColor = UIColor.black
    //    self.lbl_nric.textColor = UIColor.black
    //    self.lbl_srno.textColor = UIColor.black
        self.txtNotes.textColor = UIColor.black
        
        self.receivedBy.becomeFirstResponder()
        
        self.photoTextHeightConstraint.constant = 28
        self.photoText.layoutIfNeeded()
        self.photoText.isHidden = false
        
        self.addPhotoButtonHeightConstraint.constant = 22
        self.addPhotos.layoutIfNeeded()
        self.addPhotos.isHidden = false
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(OrderAttemptDetails_ViewController.onTouchDoneButton))
        self.navigationItem.rightBarButtonItem = doneButton
    }
    
    func onTouchDoneButton() {
        let a = self.receivedBy.text
        print("receivedby \(a)")
        
        if(self.orderAttemptArray[0].receivedBy != self.receivedBy.text || self.orderAttemptArray[0].note != self.txtNotes.text){
            
            self.orderAttemptArray[0].receivedBy = self.receivedBy.text
          //  self.orderAttemptArray[0].nric = self.lbl_nric.text
          //  self.orderAttemptArray[0].srno = self.lbl_srno.text
            self.orderAttemptArray[0].note = self.txtNotes.text
            
            if reachInternetConnection.isConnectedToNetwork() == true {
                print("onTouchDoneButton Internet connection OK")
                if(self.orderID != nil){
                    saveEditDetails()
                }
                
            } else {
                print("onTouchDoneButton Internet connection FAILED")
                var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
            
         //   dismiss(animated: true, completion: nil)
            
        }else{
            let alert = UIAlertController(title: "Error", message: "You did not edit anything", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
          //  dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    // Process
    func saveEditDetails() {
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            self.orderAttemptArray[0].latitude = currentLocation.coordinate.latitude
            self.orderAttemptArray[0].longitude = currentLocation.coordinate.longitude
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
        }
        
        
        let batchOrder: BatchOrder = BatchOrder()
        
        let para:NSMutableDictionary = NSMutableDictionary()
        let prodArray:NSMutableArray = NSMutableArray()
        
        batchOrder.orderIds.removeAll()
        
        batchOrder.orderIds.append(String(describing: self.orderID!))
        batchOrder.orderStatus = String(describing: self.orderStatusID)
        
        
        para.setValue(batchOrder.orderIds, forKey: "id")
        para.setValue(batchOrder.orderStatus, forKey: "order_status_id")
        
        let paraObjectStep:NSMutableDictionary = NSMutableDictionary()
        let prodArrayStep:NSMutableArray = NSMutableArray()
        
        paraObjectStep.setValue(self.jobStepID, forKey: "job_step_id")
        paraObjectStep.setValue(self.jobStepStatusID, forKey: "job_step_status_id")
        
        
        prodArray.add(self.orderAttemptArray[0].toJsonEditDetailOnly())
        paraObjectStep.setObject(prodArray, forKey: "step_attempts" as NSCopying)
        
        prodArrayStep.add(paraObjectStep)
        
        para.setObject(prodArrayStep, forKey: "job_steps" as NSCopying)
        
        let dataPara:NSMutableDictionary = NSMutableDictionary()
        let dataArray:NSMutableArray = NSMutableArray()
        
        dataArray.add(para)
        dataPara.setValue(dataArray, forKey: "data")
        
        
        SwiftSpinner.show("Update Step Attempt Details...")
        
        self.updateStepAttemptsForJobStep(dataPara: dataPara)
        
    }
    
    /* ======= UPDATE STEP ATTEMPT FOR JOB STEP ========= */
    func updateStepAttemptsForJobStep(dataPara: NSMutableDictionary){
        
    
            /* ====== UPDATE STEP ATTEMPTS ======= */
            AlamofireRequest.batchUpdate(parameters: dataPara as NSDictionary) { responseObject, statusCode, error in
                
                SwiftSpinner.hide()
                
                if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                    if(responseObject == nil)
                    {
                        
                    }else{
                        
                        let swiftyJsonVar = JSON(responseObject)
                        if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                            if(swiftyJsonVar["status"].bool == true){
                                print("success and goto previous page")
                                DispatchQueue.main.async {
                                    print("success page")
                                    self.mediaArray.removeAll()
                                    self.thumbs.removeAll()
                                    self.arrayIdAllImage.removeAll()
                                    self.arrayEditDescriptionIndex.removeAll()
                                    self.arrayImageURL.removeAll()
                                    self.arrayImageDecription.removeAll()
                                    self.imagesString64Base.removeAll()
                                    self.orderAttemptArray.removeAll()
                    
                                    self.showUIAlertDialog()
                                }

                                
                            }
                        }
                    }
                    
                }else{
                    //error code
                    SwiftSpinner.hide()
                    if(responseObject == nil)
                    {
                        self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                    }else{
                        let swiftyJsonVar = JSON(responseObject)
                        let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                        self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                    }
                }
            }
            /* ====== UPDATE STEP ATTEMPTS ======= */
        
    }
    
    /* ====== FUNC SHOW ALERT DIALOG SUCCESS UPDATE ======= */
    func showUIAlertDialog(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") {
            print("YES button tapped")
            //self.dismiss(animated: true, completion: nil)
            if self.reachInternetConnection.isConnectedToNetwork() == true {
                print("showUIAlertDialog Internet connection OK")
                self.getJobStepDetails()
            } else {
                print("showUIAlertDialog Internet connection FAILED")
                var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
            
        }
       
        alertView.showSuccess("SUCCESS", subTitle: "Updated successfully.")
    }
    
    
    
    //// media browser get image browser
    func webPhotos() -> [Media] {
        var photos = [Media]()
        
        for (index, url) in self.arrayImageURL.enumerated() {
            print("Item get in \(index): \(url)")
            
            let photo = webMediaPhoto(url: url, caption: arrayImageDecription[index])
            photos.append(photo)
            
            
        }
        
        return photos
        
    }
    
    func webMediaPhoto(url: String, caption: String?) -> Media {
        guard let validUrl = URL(string: url) else {
            fatalError("Image is nil")
        }
        
        var photo = Media()
        if let _caption = caption {
            photo = Media(url: validUrl, caption: _caption)
        } else {
            photo = Media(url: validUrl)
        }
        return photo
    }
    
}

//MARK: MediaBrowserDelegate
extension OrderAttemptDetails_ViewController: MediaBrowserDelegate {
    func thumbnail(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < thumbs.count {
            return thumbs[index]
        }
        
        
        return self.webMediaPhoto(url: "", caption: nil)
    }
    
    
    
    func media(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < mediaArray.count {
            return mediaArray[index]
        }
        
        return self.webMediaPhoto(url: "", caption: nil)
    }
    
    func numberOfMedia(in mediaBrowser: MediaBrowser) -> Int {
        return mediaArray.count
    }
    
    func isMediaSelected(at index: Int, in mediaBrowser: MediaBrowser) -> Bool {
        return selections[index]
        
    }
    
    func actionButtonPressed(at photoIndex: Int, in mediaBrowser: MediaBrowser) {
        //put ur code here to edit the description. have a pop up
        print("edit klickkkk")
        //self.openDescriptionImage(photoIndex: photoIndex)
        
        let alert = UIAlertController(title: "Image Description", message: "Enter a text", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
        }
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            print("Text field: \(textField?.text)")
            self.arrayImageDecription[photoIndex] = (textField?.text)!
            self.arrayEditDescriptionIndex.append(photoIndex)
            self.mediaArray[photoIndex].caption = (textField?.text)!
            self.browser.viewDidLoad()
          
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func didDisplayMedia(at index: Int, in mediaBrowser: MediaBrowser) {
        // print("Did start viewing photo at index \(index)")
        
    }
    
    func mediaDid(selected: Bool, at index: Int, in mediaBrowser: MediaBrowser) {
        selections[index] = selected
        print("Select at index \(index)")
    }
    
    //    func titleForPhotoAtIndex(index: Int, MediaBrowser: MediaBrowser) -> String {
    //    }
    
}

extension OrderAttemptDetails_ViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}
