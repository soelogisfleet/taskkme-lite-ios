//
//  ViewController.swift
//  SampleIOSNew
//
//  Created by Coolasia on 19/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SkyFloatingLabelTextField
import Firebase
import FirebaseMessaging
import CoreData
import SwiftSpinner
import PasswordTextField
import DeviceKit

class LoginViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: PasswordTextField!
    @IBOutlet weak var usernameForgetPassword: UITextField!
    @IBOutlet weak var usernamePasswordView: UIView!
    @IBOutlet weak var adminView: UIView!
    
    @IBOutlet weak var forgetPasswordTxtTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var btn_signin: UIButton!
    
    @IBOutlet weak var viewBoxEmailPassword: UIView!
    
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernamePasswordTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernamePasswordViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var adminViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var signInButtonTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewContentView: UIView!
    
    @IBOutlet weak var adminLoginLabel: UILabel!
    
    @IBOutlet weak var secondBox: UIView!
    
    
    @IBOutlet weak var messageForgetPassword: UILabel!
    @IBOutlet weak var forgetPasswordSignInWord: UILabel!
    
    
    @IBOutlet weak var versionAppLabel: UILabel!
    
    
    let moc = DataController().managedObjectContext
    var request: URLRequest!
    
    var backGround = LocationTracker_ViewController()
    var reachInternetConnection = Reachability()
    var isForgetPasswordClick: Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.password.delegate = self
        
        
        isForgetPasswordClick = false
        
        self.email.underlined()
        //self.password.underlined()
        //self.hideKeyboard()
        
        
        self.secondBox.isHidden = true
        self.secondBox.layoutIfNeeded()
        self.messageForgetPassword.isHidden = true
        
        
        design()
        //fetch()
        
        UserDefaults.standard.setValue(true, forKey: "firstTimeUserOnBoarding")
        
        UIApplication.shared.statusBarStyle = .default
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.forgetPasswordSignInClick(_:)))
        self.forgetPasswordSignInWord.isUserInteractionEnabled = true
        self.forgetPasswordSignInWord.addGestureRecognizer(tap)
        let underlineAttriString = NSAttributedString(string:"Forgot Password?", attributes:
            [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        self.forgetPasswordSignInWord.attributedText = underlineAttriString
        
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.adminLoginClick(_:)))
        self.adminLoginLabel.isUserInteractionEnabled = true
        self.adminLoginLabel.addGestureRecognizer(tap2)
        let underlineAttriStringAdmin = NSAttributedString(string:"Login as admin / operations", attributes:
            [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        self.adminLoginLabel.attributedText = underlineAttriStringAdmin
        
        
        //        let validationRule = RegexRule(regex:"^(?=.*?[0-9])(?=.*?[a-z]).{6,}$", errorMessage: "Password must contain 6 characters")
        //
        //        password.validationRule = validationRule
        //
        let infoDictionary = Bundle.main.infoDictionary
        let currentVersion = infoDictionary!["CFBundleShortVersionString"] as? String
        
        self.versionAppLabel.text = ("Version \(currentVersion!)")
        
    }
    
    
    func changeUiToSignIn(){
        UserDefaults.standard.removeObject(forKey: "usernameTemporary")
        adminView.isHidden=false
        
        let underlineAttriString = NSAttributedString(string:"Forgot Password?", attributes:
            [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        
        self.forgetPasswordSignInWord.attributedText = underlineAttriString
        
        btn_signin.setTitle("SIGN IN", for: .normal)
        
        
        self.secondBox.isHidden = true
        self.secondBox.layoutIfNeeded()
        self.messageForgetPassword.isHidden = true
        
        self.viewBoxEmailPassword.isHidden = false
        self.usernamePasswordViewHeightConstraint.constant = 93
        self.usernamePasswordView.layoutIfNeeded()
        
        self.isForgetPasswordClick = false
        
        self.email.placeholder = "Username"
        self.email.underlined()
        
        let device = Device()
        
        if device.isPad {
            // Do something
            if device != .simulator(.iPadPro12Inch) {
                // Do something
                
                logoTopConstraint.constant = 60
                usernamePasswordTopConstraint.constant = 70
                usernamePasswordViewHeightConstraint.constant = 75
                signInButtonTopConstraint.constant = 280
                self.btn_signin.layoutIfNeeded()
                self.logoImageView.layoutIfNeeded()
                self.usernamePasswordView.layoutIfNeeded()
                self.scrollViewContentView.layoutIfNeeded()
            }
            
            self.scrollView.contentSize.height = 1.0 // disable vertical scroll
            
            
            
            
            
        } else {
            
            if device == .simulator(.iPhone5) || device == .simulator(.iPhone5c) || device == .simulator(.iPhone5s){
                // Do something
                
                logoTopConstraint.constant = 80
                usernamePasswordTopConstraint.constant = 60
                usernamePasswordViewHeightConstraint.constant = 75
                signInButtonTopConstraint.constant = 300
                self.btn_signin.layoutIfNeeded()
                
                self.logoImageView.layoutIfNeeded()
                self.usernamePasswordView.layoutIfNeeded()
                self.scrollViewContentView.layoutIfNeeded()
            }
            
            self.scrollView.contentSize.height = 1.0 // disable vertical scroll
            
            // Do something else
        }
        
        
    }
    
    func adminLoginClick(_ recognizer: UITapGestureRecognizer){
        
        UIApplication.shared.open(NSURL(string:"http://auth.taskk.me/login")! as URL)
        
    }
    
    func forgetPasswordSignInClick(_ recognizer: UITapGestureRecognizer){
        if(self.isForgetPasswordClick){
            
            self.changeUiToSignIn()
            
        }else{
            adminView.isHidden=true
            btn_signin.setTitle("RESET PASSWORD", for: .normal)
            
            let device = Device()
            if device.isPad {
                // Do something
                if device != .simulator(.iPadPro12Inch) {
                    // Do something
                    
                    self.forgetPasswordTxtTopConstraint.constant = 30
                    self.signInButtonTopConstraint.constant = 320
                    self.adminViewBottomConstraint.constant = 35
                    
                }
                
                
                
                
                
                
            }
            else{
                if device == .simulator(.iPhone5) || device == .simulator(.iPhone5c) || device == .simulator(.iPhone5s){
                    // Do something
                    
                    // self.forgetPasswordTxtTopConstraint.constant = 30
                    self.signInButtonTopConstraint.constant = 380
                    self.adminViewBottomConstraint.constant = 35
                    
                }
            }
            
            //check if is ipad
            
            
            
            self.forgetPasswordSignInWord.layoutIfNeeded()
            self.viewBoxEmailPassword.isHidden = true
            self.usernamePasswordViewHeightConstraint.constant = 0
            self.usernamePasswordView.layoutIfNeeded()
            
            self.secondBox.isHidden = false
            self.secondBox.layoutIfNeeded()
            self.messageForgetPassword.isHidden = false
            self.scrollViewContentView.layoutIfNeeded()
            
            
            forgetPasswordSignInWord.text = "Back to sign in"
            self.isForgetPasswordClick = true
        }
        
    }
    
    func changeUiToConfirmationCode(){
        btn_signin.setTitle("SET NEW PASSWORD", for: .normal)
        
        
        self.secondBox.isHidden = true
        self.secondBox.layoutIfNeeded()
        self.messageForgetPassword.isHidden = true
        
        self.viewBoxEmailPassword.isHidden = false
        self.usernamePasswordViewHeightConstraint.constant = 93
        self.viewBoxEmailPassword.layoutIfNeeded()
        
        self.email.placeholder = "Verification code (from SMS)"
        
        self.isForgetPasswordClick = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        var contentRect = CGRect.zero
        
        
        let device = Device()
        if device.isPad {
            // Do something
            if device != .simulator(.iPadPro12Inch) {
                // Do something
                
                logoTopConstraint.constant = 60
                usernamePasswordTopConstraint.constant = 70
                usernamePasswordViewHeightConstraint.constant = 75
                signInButtonTopConstraint.constant = 280
                self.btn_signin.layoutIfNeeded()
                self.logoImageView.layoutIfNeeded()
                self.usernamePasswordView.layoutIfNeeded()
                self.scrollViewContentView.layoutIfNeeded()
            }
            
            self.scrollView.contentSize.height = 1.0 // disable vertical scroll
            
            
            
            
            
        } else {
            
            if device == .simulator(.iPhone5) || device == .simulator(.iPhone5c) || device == .simulator(.iPhone5s){
                // Do something
                
                logoTopConstraint.constant = 80
                usernamePasswordTopConstraint.constant = 60
                usernamePasswordViewHeightConstraint.constant = 75
                signInButtonTopConstraint.constant = 300
                self.btn_signin.layoutIfNeeded()
                
                self.logoImageView.layoutIfNeeded()
                self.usernamePasswordView.layoutIfNeeded()
                self.scrollViewContentView.layoutIfNeeded()
            }
            
            self.scrollView.contentSize.height = 1.0 // disable vertical scroll
            
            // Do something else
        }
        
        
        
        
        
        
        
        //        for view in self.scrollView.subviews {
        //            contentRect = contentRect.union(view.frame)
        //        }
        //        self.scrollView.contentSize = contentRect.size
        
        
        
        
        
        
        
        
        
        
        
    }
    
    func calculateContentSize(scrollView: UIScrollView) -> CGSize {
        
        
        var topPoint = CGFloat()
        var height = CGFloat()
        
        for subview in scrollView.subviews {
            if subview.frame.origin.y > topPoint {
                topPoint = subview.frame.origin.y
                height = subview.frame.size.height
            }
        }
        return CGSize(width: scrollView.frame.size.width, height: height + topPoint + 50)
    }
    
    @IBAction func btn_signin(_ sender: UIButton) {
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("btn_signin Internet connection OK")
            
            
            if(btn_signin.titleLabel?.text == "SIGN IN" && isForgetPasswordClick == false){
                
                
                if((email.text?.characters.count)! > 0 && (password.text?.characters.count)! > 0){
                    
                    if (password.text?.count)! < 6{
                        self.alert_popup(title: "Password too short", message: "Password must contain 6 characters.")
                        
                        // print(password.errorMessage)
                    }else{
                        print(email.text!)
                        print(password.text!)
                        print("login")
                        
                        sign_in(email: email.text!,password: password.text!)
                    }
                    
                    
                    
                }
                else{
                    self.alert_popup(title: "Sign in error", message: "Please fill in your username & password")
                }
                
            }else if(btn_signin.titleLabel?.text == "RESET PASSWORD" && isForgetPasswordClick == true){
                if((usernameForgetPassword.text?.characters.count)! > 0 ){
                    print(usernameForgetPassword.text!)
                    print("rest password")
                    //reset password
                    
                    self.checkValidateEmail(username: usernameForgetPassword.text!)
                    
                }
                else{
                    self.alert_popup(title: "Error", message: "Please fill in your username")
                }
                
            }else if (btn_signin.titleLabel?.text == "SET NEW PASSWORD"){
                if((email.text?.characters.count)! > 0 && (password.text?.characters.count)! > 0){
                    
                    if (password.text?.count)! < 6{
                        self.alert_popup(title: "Password too short", message: "Password must contain 6 characters.")
                        
                        // print(password.errorMessage)
                    }else{
                        print("new password")
                        // verifcation code
                        self.confirmCode(email: email.text!,password: password.text!)
                    }
                    
                    
                }
                else{
                    self.alert_popup(title: "Error", message: "Please fill in your verification code & new password")
                }
            }
            
            
            
            
            
            
        } else {
            print("btn_signin Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    
    @IBAction func showpassword(_ sender: Any) {
    }
    
    func turnOffLocationTracker(){
        backGround.stopTimer()
        print("stopTimer")
    }
    
    func turnOnLocationTracker(){
        backGround.viewDidLoad()
    }
}

extension LoginViewController{
    public func sign_in(email: String!, password: String!) {
        // let deviceUUID: String = (UIDevice.current.identifierForVendor?.uuidString)!
        // let deviceUUID: String = "4B235AD9-E245-4DF1-9BCE-CC19136E8040"
        
        // print("Device Id \(deviceUUID)")
        var swiftyJsonVars: String!
        var status: Bool! = false
        var token = InstanceID.instanceID().token()!
        print("fbs token:\(token)")
        
        
        if let iphoneTokenExist = UserDefaults.standard.object(forKey: "iphone_token") {
            if(token.isEmpty){
                token = UserDefaults.standard.value(forKey: "iphone_token") as! String
            }
        }
        
        
        
        /*device id iphone akmal
         4B235AD9-E245-4DF1-9BCE-CC19136E8040
         This because maybe other fon cant login because company allow 3 device for basic plan
         */
        
        let keyChainDeviceId = KeychainManager()
        let deviceId: String = keyChainDeviceId.getDeviceIdentifierFromKeychain()
        //let deviceId: String = "DB684CD2-EF0F-4A79-A1DA-AA1C5F036947"
        
        UserDefaults.standard.setValue(token, forKey: "fbsToken")
        
        let device_model = UIDevice.current.modelName
        // let device_manufacturer = UIDevice.current.name
        print("device_model: \(device_model)")
        //  print("device_manufacturer: \(device_manufacturer)")
        
        
        
        let loginInfo : Dictionary<String,AnyObject> = ["email" : email! as AnyObject,
                                                        "password" : password! as AnyObject,
                                                        "scope" : "admin" as AnyObject,
                                                        "grant_type" : "password" as AnyObject
        ]
        
        print("loginInfo: \(loginInfo)")
        SwiftSpinner.show("Login...")
        
        AlamofireRequest.loginUser(parameters: loginInfo as NSDictionary) { responseObject, statusCode, error in
            
            let swiftyJsonVar = JSON(responseObject)
            print("swiftyJsonVarLogin: \(swiftyJsonVar)")
            print("statusCodeLogin: \(statusCode)")
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                
                let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                print("URLPathSqlite \(urls[urls.count-1] as URL)") // look for doc. directory
                
                // Create Fetch Request
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AppCompanySetting")
                // Create Batch Delete Request
                let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                do {
                    try self.moc.execute(batchDeleteRequest)
                } catch {
                    // Error Handling
                }
                
                // Create Fetch Request
                let fetchRequestOrderStatus = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderStatus")
                // Create Batch Delete Request
                let batchDeleteRequestOrderStatus = NSBatchDeleteRequest(fetchRequest: fetchRequestOrderStatus)
                do {
                    try self.moc.execute(batchDeleteRequestOrderStatus)
                } catch {
                    // Error Handling
                }
                
                if(swiftyJsonVar["token"]["access_token"].exists()){
                    let access_token = swiftyJsonVar["token"]["access_token"].string
                    
                    UserDefaults.standard.setValue(access_token, forKey: "Token")
                    UserDefaults.standard.setValue(email!, forKey: "Email")
                    UserDefaults.standard.setValue(password!, forKey: "Password")
                    
                    if(!token.isEmpty){
                        UserDefaults.standard.setValue(token, forKey: "iphone_token")
                    }
                    
                    
                    var appCompanyStatusId = NSInteger()
 
                    let resultJson = swiftyJsonVar["result"]
                    if(resultJson["application_company_status_id"].exists() && resultJson["application_company_status_id"] != JSON.null){
                        
                        appCompanyStatusId = resultJson["application_company_status_id"].intValue

                    }
                    else{
                        appCompanyStatusId = 0;
                    }
                    
                    
                    var isTrialBoolean = Bool()
                    let companyLicenseJson = swiftyJsonVar["result"]["application_company_license"]
                
                    print("companyLicenseJson: \(String(describing: companyLicenseJson))")
                    isTrialBoolean = companyLicenseJson["is_trial"].boolValue
                    
                    var canLoginBoolean = false
                    
                    switch appCompanyStatusId {
                    case 0:
                        if(!isTrialBoolean){
                            // tak subsribce so show popup not subscribe
                            canLoginBoolean = false;
                        }else{
                            // trial
                            canLoginBoolean = true;
                        }

                    case 1:
                        canLoginBoolean = false;

                    case 2:
                        canLoginBoolean = true;

                    default:
                        canLoginBoolean = false;

                    }

                    
                    if(canLoginBoolean)
                    {
                        
                        /* ======= ADD APP COMPANY SETTING INTO COREDATA ======= */
                        /*This because to check save data which is this company have customer or not*/
                        let appCompanySetttingList = resultJson["application_company_settings"].array!
                        for subJson in appCompanySetttingList {
                            let appCompanySettings = AppCompanySettings(json: subJson)
                            print("rule: \(String(describing: appCompanySettings?.rule))")
                            
                            let entity = NSEntityDescription.insertNewObject(forEntityName: "AppCompanySetting", into: self.moc) as! AppCompanySetting
                            entity.setValue(appCompanySettings?.id, forKey: "id")
                            entity.setValue(appCompanySettings?.rule, forKey: "rule")
                            entity.setValue(appCompanySettings?.jsonData, forKey: "jsonData")
                            entity.setValue(appCompanySettings?.enable, forKey: "enable")
                            entity.setValue(appCompanySettings?.current_status_general_id, forKey: "currentStatus")
                            entity.setValue(appCompanySettings?.to_status_general_id, forKey: "toStatus")
                            entity.setValue(appCompanySettings?.is_scan_required, forKey: "isScanRequired")
                            entity.setValue(appCompanySettings?.is_man_power_worker, forKey: "isManPowerCanUpdate")
                            
                            do{
                                try self.moc.save()
                            }catch{
                                fatalError("errorInsert: \(error)")
                            }
                            
                        }
                        
                        
                        
                        AlamofireRequest.getOrderStatuses() { responseObject, statusCode, error in
                            
                            SwiftSpinner.hide()
                            let swiftyJsonVar = JSON(responseObject)
                            print("swiftyJsonVarGetOrderStatuses: \(swiftyJsonVar)")
                            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                                
                                
                                /* ======= ADD ORDER STATUS LISTING INTO COREDATA ======= */
                                /*This because each company have thier own order id status. This id is will be use for fetch job listing and update job using batch order */
                                let listOrderStatus = swiftyJsonVar.array!
                                for subJson in listOrderStatus {
                                    let orderStatus = OrderStatusDB(json: subJson)
                                    print("orderStatus: \(String(describing: orderStatus?.orderStatusName))")
                                    
                                    let entity = NSEntityDescription.insertNewObject(forEntityName: "OrderStatus", into: self.moc) as! OrderStatus
                                    entity.setValue(orderStatus?.orderStatusId, forKey: "orderStatusId")
                                    entity.setValue(orderStatus?.orderStatusName, forKey: "orderStatusName")
                                    entity.setValue(orderStatus?.orderIsActive, forKey: "orderIsActive")
                                    entity.setValue(orderStatus?.orderGeneralId, forKey: "orderGeneralId")
                                    entity.setValue(orderStatus?.orderViewName, forKey: "orderViewName")
                                    
                                    do{
                                        try self.moc.save()
                                    }catch{
                                        fatalError("errorInsert: \(error)")
                                    }
                                    
                                }
                                
                                
                                
                                var mainView: UIStoryboard!
                                mainView = UIStoryboard(name: "Main", bundle: nil)
                                let viewcontroller : UIViewController = mainView.instantiateViewController(withIdentifier: "vcRoot") as UIViewController
                                self.view.window!.rootViewController = viewcontroller
                                
                            }else{
                                // status code either than 200
                                SwiftSpinner.hide()
                                if(responseObject == nil)
                                {
                                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me")
                                }else{
                                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message)
                                }
                                
                            }
                        }
                    }
                    else{
                        if(appCompanyStatusId == 0)
                        {
                            self.alert_popup(title: "Unable to Sign In", message: "Please login to the website & set device quota to confirm your subscription")

                        }
                        else if(appCompanyStatusId == 1)
                        {
                            self.alert_popup(title: "Unable to Sign In", message: "Your account was removed. Contact our support team to assist you on this matter.")

                        }
                    }
                    
                    
                 

                }else{
                    SwiftSpinner.hide()
                    self.alert_popup(title: "Sign in error", message: "Incorrect username or password")
                }
            }else{
                // status code either than 200
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me")
                }else{
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message)
                }
                
                
            }
            
        }
        
    }
}

extension LoginViewController{
    public func alert_popup(title: String!, message: String!) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
}


extension LoginViewController{
    
    /* ====== Driver checkValidateUsername ======= */
    
    func checkValidateEmail(username: String!){
        
        SwiftSpinner.show("Checking your username...")
        
        
        let usernameInfo : Dictionary<String,AnyObject> = ["username" : username as AnyObject]
        
        AlamofireRequest.validateUsername(parameters: usernameInfo as NSDictionary) { responseObject, statusCode, error in
            
            print("statusCode \(statusCode)")
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me")
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    if(swiftyJsonVar["status"].exists()){
                        if(swiftyJsonVar["status"].bool == true){
                            self.resetPassword(username: username)
                            
                        }
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me")
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    
                    if(statusCode == Constants.STATUS_RESPONSE_BAD_REQUEST){
                        self.alert_popup(title: "Error", message: "Username does not exist")
                    }else{
                        self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message)
                    }
                    
                }
            }
        }
    }
    
    
    /* ====== Driver forgotPassword ======= */
    func resetPassword(username: String!){
        
        
        let usernameInfo : Dictionary<String,AnyObject> = ["username" : username as AnyObject]
        
        
        AlamofireRequest.forgotPassword(parameters: usernameInfo as NSDictionary) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            print("statusCode \(statusCode)")
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me")
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    if(swiftyJsonVar["status"].exists()){
                        if(swiftyJsonVar["status"].bool == true){
                            self.isForgetPasswordClick = true
                            UserDefaults.standard.setValue(username, forKey: "usernameTemporary")
                            self.changeUiToConfirmationCode()
                        }
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me")
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message)
                }
            }
        }
        /* ====== Driver forgotPassword ======= */
    }
    
    
    /* ====== Driver verificationCode ======= */
    func confirmCode(email: String!, password: String!){
        
        
        SwiftSpinner.show("Verify Code...")
        
        self.isForgetPasswordClick = true
        
        let verifyCodeInfo : Dictionary<String,AnyObject> =
            ["verification_code" : email as AnyObject,
             "password" : password as AnyObject,
             "password_confirmation" : password as AnyObject]
        
        
        AlamofireRequest.verificationCodeNewPassword(parameters: verifyCodeInfo as NSDictionary) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            print("statusCode \(statusCode)")
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me")
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    if(swiftyJsonVar["status"].exists()){
                        if(swiftyJsonVar["status"].bool == true){
                            
                            if let usernameTemporaryExist = UserDefaults.standard.object(forKey: "usernameTemporary") {
                                
                                let usernameTemporary = UserDefaults.standard.value(forKey: "usernameTemporary") as! String
                                
                                if(usernameTemporary == nil){
                                    
                                    UserDefaults.standard.removeObject(forKey: "usernameTemporary")
                                    
                                    self.changeUiToSignIn()
                                    
                                }else{
                                    // direct login
                                    self.sign_in(email: usernameTemporary,password: password)
                                }
                                
                            }else{
                                //usernameTemporaryExist not exist
                                self.changeUiToSignIn()
                            }
                            
                        }else{
                            //status false
                            self.alert_popup(title: "Error", message: "Your verification code is invalid")
                        }
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me")
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    if(statusCode == Constants.STATUS_RESPONSE_BAD_REQUEST){
                        self.alert_popup(title: "Error", message: "Your verification code is invalid")
                    }else{
                        self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message)
                    }
                    
                }
            }
        }
        /* ====== Driver verificationCode ======= */
    }
    
    
}


extension LoginViewController{
    public func design(){
        
        btn_signin.layer.cornerRadius = 6
        
        
        viewBoxEmailPassword.backgroundColor = Constants.hexStringToUIColor(hex:"f2f0f2")
        viewBoxEmailPassword.layer.cornerRadius = 6
        
        
        secondBox.backgroundColor = Constants.hexStringToUIColor(hex:"f2f0f2")
        secondBox.layer.cornerRadius = 6
    }
    
    
    func textFieldDidBeginEditing(_ password: UITextField) {
        animateViewMoving(up: true, moveValue: 42)
    }
    
    func textFieldDidEndEditing(_ password: UITextField) {
        animateViewMoving(up: false, moveValue: 42)
    }
    
    func textFieldShouldReturn(_ password: UITextField) -> Bool {
        self.view.endEditing(true)
        password.resignFirstResponder()
        return false
    }
    
    
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        var movementDuration:TimeInterval = 0.3
        var movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
}

extension UITextField {
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}



// params code
//let parameters: [String: Any] = [
//    "email" : email,
//    "password" : password,
//    "scope" : "worker",
//    "List": [
//        [
//            "IdQuestion" : 5,
//            "IdProposition": 2,
//            "Time" : 32
//        ],
//        [
//            "IdQuestion" : 4,
//            "IdProposition": 3,
//            "Time" : 9
//        ]
//    ]
//]


