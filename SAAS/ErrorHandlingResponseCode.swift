//
//  ErrorHandlingResponseCode.swift
//  SAAS
//
//  Created by Coolasia on 3/4/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ErrorHandlingResponseCode {
    static func getErrorTitleAndErrorContext(statusCode: Int, swiftyJsonVar: JSON) -> (errorTitle: String, message: String, unauthorizedNeedToLoginPage: Bool) {
        
        var message = "Please check with your administrator"
        var errorTitle = "Data error"
        var deviceNotFound: Bool
        var deviceUnPaid: Bool
        var quotaReached: Bool
        var companyBlacklist: Bool
        var unauthorizedNeedToLoginPage: Bool
        
        unauthorizedNeedToLoginPage = false
        
        if(statusCode == Constants.STATUS_RESPONSE_BAD_REQUEST){
            if(swiftyJsonVar["error"].exists()){
                do {
                    message = swiftyJsonVar["error"].string!
                    print("error 11")
                } catch{
                    message = swiftyJsonVar["error"]["error"].string!
                    print("error 12")
                }
                
                errorTitle = "Data error"
            }
        }else if(statusCode == Constants.STATUS_RESPONSE_UNAUTHORIZED){
            
            unauthorizedNeedToLoginPage = true
            
            if(swiftyJsonVar["device_not_found"].exists()){
                deviceNotFound = swiftyJsonVar["device_not_found"].bool!
                
                if(deviceNotFound){
                    errorTitle = "Device ID not found"
                    message = "Please check with your administrator"
                }
            }else if (swiftyJsonVar["device_is_banned"].exists()){
                errorTitle = "Your device is banned"
                message = "Please check with your administrator"
            }else{
                errorTitle = "Your data is unauthorized"
                message = "Please check with your administrator"
            }
        }else if(statusCode == Constants.STATUS_RESPONSE_FORBIDDEN){
            
            unauthorizedNeedToLoginPage = true
            
            if(swiftyJsonVar["unpaid_subscription"].exists()){
                deviceUnPaid = swiftyJsonVar["unpaid_subscription"].bool!
                
                if(deviceUnPaid){
                    errorTitle = "Company subscription expired"
                    message = "Please check with your administrator"
                }
            }
            else if(swiftyJsonVar["quota_reached"].exists()){
                quotaReached = swiftyJsonVar["quota_reached"].bool!
                
                if(quotaReached){
                    errorTitle = "Quota limit reached"
                    message = "Please check with your administrator"
                }
            }
                
            else if(swiftyJsonVar["blacklist"].exists()){
                companyBlacklist = swiftyJsonVar["blacklist"].bool!
                
                if(companyBlacklist){
                    errorTitle = "Your company is blacklisted"
                    message = "Please check with your administrator"
                }
            }else{
                errorTitle = "Your data is forbidden"
                message = "Please check with your administrator"
            }
        }else if(statusCode == Constants.STATUS_RESPONSE_NOT_FOUND){
            errorTitle = "Sorry!"
            message = "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me"
        }else if(statusCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR){
            errorTitle = "Sorry!"
            message = "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me"
        }else if(statusCode == Constants.STATUS_RESPONSE_SERVER_DOWN){
            errorTitle = "Sorry!"
            message = "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me"
        }else if(statusCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT){
            errorTitle = "Sorry!"
            message = "Request Timeout. Please try again later or contact us at hello@taskk.me"
        }
        
        return (errorTitle, message, unauthorizedNeedToLoginPage)
    }
}

// Sorry
// We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me

