//
//  SearchOrderViewController.swift
//  WMG
//
//  Created by Coolasia on 25/1/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Gloss
import SwiftSpinner
import MarqueeLabel

class SearchOrderViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,UISearchBarDelegate{
    
    var jobList: [Job] = []
    var assignedJobList : [Job] = []
    var acknowledgedJobList : [Job] = []
    var inProgressJobList : [Job] = []
    var completedJobList : [Job] = []
    var cancelledJobList : [Job] = []
    var failedJobList : [Job] = []
    var selfCollectJobList : [Job] = []
    var sectionArray : [JobSection] = []
    var assignedStatusIDFromOrderStatusDB: Int? = 0
    var acknowledgedStatusIDFromOrderStatusDB: Int? = 0
    var inProgressStatusIDFromOrderStatusDB: Int? = 0
    var completedStatusIDFromOrderStatusDB: Int? = 0
    var selfCollectStatusIDFromOrderStatusDB: Int? = 0
    var cancelledStatusIDFromOrderStatusDB: Int? = 0
    var failedStatusIDFromOrderStatusDB: Int? = 0
    
    
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .custom)
    var keyboardShown = false
    
    var orderStatusName: String?
    var orderStatusTextColor: String?

    
    var currentDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    var testCalendar = Calendar(identifier: .gregorian)
    var itemTracking = String()
    open var tintColor = UIColor.defaultTintColor()
    var reachInternetConnection = Reachability()
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Please enter another item tracking number"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "No Order for your tracking number"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func onTouchCancelButton() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func keyboardWillAppear() {
        keyboardShown = true
        //Do something here
    }
    
    @objc func keyboardWillDisappear() {
        //Do something here
        keyboardShown = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.tableView.estimatedRowHeight = 164
//        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        //self.tableView.backgroundColor = UIColor(red:0.81, green:0.81, blue:0.82, alpha:1)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       // searchButton.setImage(UIImage(named: "search"), for: .normal)
       // searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
       // searchButton.addTarget(self, action:#selector(self.createSearchBar), for: .touchUpInside)
       // let searchBarButton = UIBarButtonItem(customView: searchButton)
        
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Enter tracking number"
        searchBar.delegate = self
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "back_button"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.addTarget(self, action: #selector(SearchOrderViewController.onTouchCancelButton), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: backButton)
        
        searchBar.subviews.flatMap({$0.subviews}).forEach({ ($0 as? UIButton)?.isEnabled = true })


        searchBar.becomeFirstResponder()

//        let backButton = UIBarButtonItem(image: UIImage(named: "back_button"), style: .plain, target: self, action: #selector(SearchOrderViewController.onTouchCancelButton)) // action:#selector(Class.MethodName) for swift 3

        //cancelButton.tintColor = tintColor
     //   self.navigationItem.leftBarButtonItem = item1
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.assignedStatusIDFromOrderStatusDB = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ASSIGNED_TEXT)
        self.acknowledgedStatusIDFromOrderStatusDB = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ACKNOWLEDGED_TEXT)
        self.inProgressStatusIDFromOrderStatusDB = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.INPROGRESS_TEXT)
        self.completedStatusIDFromOrderStatusDB = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.COMPLETE_TEXT)
        self.selfCollectStatusIDFromOrderStatusDB = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.SELF_COLLECT_TEXT)
        self.cancelledStatusIDFromOrderStatusDB  = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.CANCELLED_TEXT)
        self.failedStatusIDFromOrderStatusDB  = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.FAILED_TEXT)
        
        
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        
        // self.tableView.register(MainTableViewCell.self, forCellReuseIdentifier: "MainTableViewCell")
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        // self.tableView.backgroundColor = UIColor.lightGray
        self.tableView.backgroundView = nil;
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        //searchOrder(itemTracking: itemTracking)
        
    }
    
    func stringFormatDate(dropOffDate: String, dateformat: String, dateString: String) -> String {
        if(dropOffDate == nil || dropOffDate == ""){
            return " - "
        }else{
            let formatter = DateFormatter()
            formatter.dateFormat = dateformat
            let dateNew = formatter.date(from:dropOffDate)
            let returnDateString = dateNew?.string(format: dateString)
            if(returnDateString == nil){
                return ""
            }else{
                return returnDateString!
            }
            
        }
    }
    
    //button call dropOff
    func dropOffcallButtonClicked(button: UIButton) {
        
        
        var dropOffCall = String()
        dropOffCall = self.jobList[button.tag].dropOffContact!
        let alert = UIAlertController(title: dropOffCall, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: { action in
            print("call \(dropOffCall)")
            
            if let url = URL(string: "tel://\(dropOffCall)") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            
            //   UIApplication.shared.openURL(NSURL(string: "tel://\(self.dropOffcall)") as! URL)
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    //button call pickup
    func pickupcallButtonClicked(button: UIButton) {
        
        
        var pickupCall = String()
        pickupCall = self.jobList[button.tag].pickUpContact!
        let alert = UIAlertController(title: pickupCall, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: { action in
            print("call \(pickupCall)")
            
            if let url = URL(string: "tel://\(pickupCall)") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            
            //   UIApplication.shared.openURL(NSURL(string: "tel://\(self.dropOffcall)") as! URL)
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    // show popup if number call empty
    func popupErrorNumberCall(button: UIButton) {
        
        let alertController = UIAlertController(title: "Error Number", message: "This number is not available", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(sectionArray.count>0)
        {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 26))
            // 2. Set a custom background color and a border
//            headerView.layer.borderColor = UIColor(white: 0.5, alpha: 1.0).cgColor
//            headerView.layer.borderWidth = 1.0
            // 3. Add a label
            let headerLabel = UILabel()
            headerLabel.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 26)
            headerLabel.backgroundColor = UIColor.clear
            headerLabel.textColor = UIColor.white
            headerLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
            
        
            
            if(sectionArray[section].jobSectionStatusId == self.assignedStatusIDFromOrderStatusDB)
            {
                headerView.backgroundColor = Constants.hexStringToUIColor(hex: Constants.ASSIGNED_COLOR)
                headerLabel.text = Constants.ASSIGNED_TEXT
                
            }
            else if(sectionArray[section].jobSectionStatusId == self.acknowledgedStatusIDFromOrderStatusDB)
            {
                headerView.backgroundColor = Constants.hexStringToUIColor(hex: Constants.ACKNOWLEDGED_COLOR)
                headerLabel.text = Constants.ACKNOWLEDGED_TEXT
                
            }
            else if(sectionArray[section].jobSectionStatusId == self.inProgressStatusIDFromOrderStatusDB)
            {
                headerView.backgroundColor = Constants.hexStringToUIColor(hex: Constants.INPROGRESS_COLOR)
                headerLabel.text = Constants.INPROGRESS_TEXT
                
            }
            else if(sectionArray[section].jobSectionStatusId == self.completedStatusIDFromOrderStatusDB)
            {
                headerView.backgroundColor = Constants.hexStringToUIColor(hex: Constants.COMPLETE_COLOR)
                headerLabel.text = Constants.COMPLETE_TEXT
                
            }
            else if(sectionArray[section].jobSectionStatusId == self.selfCollectStatusIDFromOrderStatusDB)
            {
                headerView.backgroundColor = Constants.hexStringToUIColor(hex: Constants.SELF_COLLECT_COLOR)
                headerLabel.text = Constants.SELF_COLLECT_TEXT
                
            }else if(sectionArray[section].jobSectionStatusId == self.failedStatusIDFromOrderStatusDB)
            {
                headerView.backgroundColor = Constants.hexStringToUIColor(hex: Constants.FAILED_COLOR)
                headerLabel.text = Constants.FAILED_TEXT
                
            }else if(sectionArray[section].jobSectionStatusId == self.cancelledStatusIDFromOrderStatusDB)
            {
                headerView.backgroundColor = Constants.hexStringToUIColor(hex: Constants.CANCELLED_COLOR)
                headerLabel.text = Constants.CANCELLED_TEXT
                
            }
            
            headerLabel.textAlignment = .center
            // 4. Add the label to the header view
            headerView.addSubview(headerLabel)
            // 5. Finally return
            return headerView
        }
        else{
            return nil
        }
       
        
    }
    
    
    //MARK: - Tableview Delegate & Datasource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        if(sectionArray.count>0)
        {
            if(sectionArray[section].jobSectionStatusId == self.assignedStatusIDFromOrderStatusDB)
            {
                return self.assignedJobList.count;
                
            }
            else if(sectionArray[section].jobSectionStatusId == self.acknowledgedStatusIDFromOrderStatusDB)
            {
                return self.acknowledgedJobList.count;
                
            }
            else if(sectionArray[section].jobSectionStatusId == self.inProgressStatusIDFromOrderStatusDB)
            {
                return self.inProgressJobList.count;
                
            }
            else if(sectionArray[section].jobSectionStatusId == self.completedStatusIDFromOrderStatusDB)
            {
                return self.completedJobList.count;
                
            }
            else if(sectionArray[section].jobSectionStatusId == self.selfCollectStatusIDFromOrderStatusDB)
            {
                return self.selfCollectJobList.count;
                
            }else if(sectionArray[section].jobSectionStatusId == self.failedStatusIDFromOrderStatusDB)
            {
                return self.failedJobList.count;
                
            }
            else
            {
                return self.cancelledJobList.count;
                
            }
        }
        else{
            return 0
        }
       
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.sectionArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        print("lbl_orderStatus \(self.jobList[indexPath.row].orderStatusId)")
        
        if(self.jobList[indexPath.row].orderStatusId == self.assignedStatusIDFromOrderStatusDB){
            self.orderStatusName = Constants.ASSIGNED_TEXT
            self.orderStatusTextColor = Constants.ASSIGNED_COLOR
        }else if(self.jobList[indexPath.row].orderStatusId == self.acknowledgedStatusIDFromOrderStatusDB){
            self.orderStatusName = Constants.ACKNOWLEDGED_TEXT
            self.orderStatusTextColor = Constants.ACKNOWLEDGED_COLOR
        }else if(self.jobList[indexPath.row].orderStatusId == self.inProgressStatusIDFromOrderStatusDB){
            self.orderStatusName = Constants.INPROGRESS_TEXT
            self.orderStatusTextColor = Constants.INPROGRESS_COLOR
        }else if(self.jobList[indexPath.row].orderStatusId == self.completedStatusIDFromOrderStatusDB){
            self.orderStatusName = Constants.COMPLETE_TEXT
            self.orderStatusTextColor = Constants.COMPLETE_COLOR
        }else if(self.jobList[indexPath.row].orderStatusId == self.cancelledStatusIDFromOrderStatusDB){
            self.orderStatusName = Constants.CANCELLED_TEXT
            self.orderStatusTextColor = Constants.CANCELLED_COLOR
        }else if(self.jobList[indexPath.row].orderStatusId == self.failedStatusIDFromOrderStatusDB){
            self.orderStatusName = Constants.FAILED_TEXT
            self.orderStatusTextColor = Constants.FAILED_COLOR
        }else if(self.jobList[indexPath.row].orderStatusId == self.selfCollectStatusIDFromOrderStatusDB){
            self.orderStatusName = Constants.SELF_COLLECT_TEXT
            self.orderStatusTextColor = Constants.SELF_COLLECT_COLOR
        }
        
            let cell:JobCardTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "JobCardTableViewCell") as! JobCardTableViewCell
            

            cell.selectionStyle = .none
            
            
            
            var job = Job()
            
            if(sectionArray[indexPath.section].jobSectionStatusId == self.assignedStatusIDFromOrderStatusDB)
            {
                job = assignedJobList[indexPath.row]
            }
            else if(sectionArray[indexPath.section].jobSectionStatusId == self.acknowledgedStatusIDFromOrderStatusDB)
            {
                job = acknowledgedJobList[indexPath.row]

            }
            else if(sectionArray[indexPath.section].jobSectionStatusId == self.inProgressStatusIDFromOrderStatusDB)
            {
                job = inProgressJobList[indexPath.row]

            }
            else if(sectionArray[indexPath.section].jobSectionStatusId == self.completedStatusIDFromOrderStatusDB)
            {
                job = completedJobList[indexPath.row]

            }
            else if(sectionArray[indexPath.section].jobSectionStatusId == self.selfCollectStatusIDFromOrderStatusDB)
            {
                job = selfCollectJobList[indexPath.row]

            }else if(sectionArray[indexPath.section].jobSectionStatusId == self.failedStatusIDFromOrderStatusDB)
            {
                job = failedJobList[indexPath.row]
                
            }
            else{
                job = cancelledJobList[indexPath.row]

            }
            
            
            cell.lbl_customerName.text = job.companyName
            cell.lbl_doReferenceNumber.text = job.orderNumber
            
            var stepSequence = 0
            var stepCompletion = 0
            var isFailed = false
            var isCancelled = false
            var firstPending = true;

            for (index, jobstep) in  job.job_stepsList.enumerated() {
               
                
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_PENDING || jobstep.job_step_status_id == Constants.JOB_STEP_INPROGRESS)
                {
                    
                    if(firstPending == true)
                    {
                        stepSequence = jobstep.order_sequence!
                        firstPending = false
                    }
                    
                    if(stepSequence >= jobstep.order_sequence!)
                    {
                        print("job step location is \(String(describing: jobstep.location))")

                        if(jobstep.location == "" || jobstep.location == nil){
                            cell.lbl_dropOffAdrress.text = " - "
                        }else{
                            
                            cell.lbl_dropOffAdrress.text = NSString(format:"Step %d: %@", jobstep.order_sequence!,jobstep.location!) as String
                        }
                    }
                    
                    
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_COMPLETED)
                {
                    stepCompletion = stepCompletion + 1
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_CANCELLED)
                {
                    isCancelled = true
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_FAILED)
                {
                    isFailed = true
                }
            }
            
            if(job.job_stepsList.count==1)
            {
                if(job.job_stepsList[0].job_step_status_id == Constants.JOB_STEP_PENDING)
                {
                    cell.lbl_step_status.text = "Pending"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.PENDING_COLOR)
                    
                }
                else  if(job.job_stepsList[0].job_step_status_id == Constants.JOB_STEP_INPROGRESS)
                {
                    cell.lbl_step_status.text = "In Progress"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.INPROGRESS_COLOR)
                    
                }
                else  if(job.job_stepsList[0].job_step_status_id == Constants.JOB_STEP_COMPLETED)
                {
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Completed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.COMPLETE_COLOR)
                    
                }
                else  if(job.job_stepsList[0].job_step_status_id == Constants.JOB_STEP_FAILED)
                {
                    
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Failed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.FAILED_COLOR)
                    
                }
                else  if(job.job_stepsList[0].job_step_status_id == Constants.JOB_STEP_CANCELLED)
                {
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Cancelled"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.CANCELLED_COLOR)
                    
                }
            }
            else{
                
                if(stepCompletion==0)
                {
                    
                    cell.lbl_step_status.text =  NSString(format:"0 of %d steps completed", self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.PENDING_COLOR)
                    
                }
                else if(stepCompletion == self.jobList[indexPath.row].job_stepsList.count)
                {
                    cell.lbl_step_status.text =  NSString(format:"%d of %d steps completed", self.jobList[indexPath.row].job_stepsList.count,self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.COMPLETE_COLOR)
                }
                else if(isCancelled)
                {
                    cell.lbl_step_status.text =  "Cancelled"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.CANCELLED_COLOR)
                }
                else if(isFailed)
                {
                    cell.lbl_step_status.text =  "Failed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.FAILED_COLOR)
                }
                else {
                    cell.lbl_step_status.text =  NSString(format:"%d of %d steps completed", stepCompletion,self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.INPROGRESS_COLOR)
                }
                
                
                
            }
            
            
            if(job.dropOffDate == nil || job.dropOffDate == ""){
                
                cell.lbl_dropOffDate.isHidden = true
            }
            else{
                let dropOffDateString = job.dropOffDate!
                
                print("date is \(dropOffDateString)")
                
                
            let dateString = self.stringFormatDate(dropOffDate: dropOffDateString, dateformat: "yyyy-MM-dd hh:mm:ss", dateString: "dd MMM yyyy")
                
               // print("new time is \(dropOffTimeString)")
                
                
                
                cell.lbl_dropOffDate.text = dateString
            }
            
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 148.0;//Choose your custom row height
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("Select Row")
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Self Collect didSelectRowAt Internet connection OK")
            let storyboard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
            let levelController = storyboard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as! OrderDetailsManyStepViewController
            
            // levelController.jobdetail = self.jobList[(indexPath.row)]
            
            if(self.jobList[indexPath.row].orderId != 0){
                levelController.orderID = self.jobList[indexPath.row].orderId
                levelController.orderNumber = self.jobList[indexPath.row].orderNumber
                
                self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
                
                
                self.navigationController?.pushViewController(levelController, animated: true)
            }else{
                // order id is 0 .. so cannot open other page
            }
        } else {
            print("Self Collect didSelectRowAt Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    /* ======= Search Process ========= */
    func searchOrder(itemTracking: String){
        self.jobList.removeAll()
        self.assignedJobList.removeAll()
        self.acknowledgedJobList.removeAll()
        self.inProgressJobList.removeAll()
        self.completedJobList.removeAll()
        self.cancelledJobList.removeAll()
        self.failedJobList.removeAll()
        self.selfCollectJobList.removeAll()
        self.sectionArray.removeAll()
        
        
        let statusString = NSString(format:"%d,%d,%d,%d,%d,%d,%d", self.assignedStatusIDFromOrderStatusDB!, self.acknowledgedStatusIDFromOrderStatusDB!, self.inProgressStatusIDFromOrderStatusDB!, self.completedStatusIDFromOrderStatusDB!,self.selfCollectStatusIDFromOrderStatusDB!,self.cancelledStatusIDFromOrderStatusDB!,self.failedStatusIDFromOrderStatusDB!)
        
        let message   = String(format: Constants.api_get_search_order, arguments: [currentDate.string(format: "yyyy-MM-dd"),statusString,itemTracking])
        
        let urlString = Constants.web_api + message
        
        
        SwiftSpinner.show("Search Order Job List...")
        
        AlamofireRequest.searchJobListing(urlString: urlString) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            print("statusCode \(statusCode)")
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    self.jobList.removeAll()
                    
                    let swiftyJsonVar = JSON(responseObject)
                    print(swiftyJsonVar)
                    
                    
                    
                    for subJson in swiftyJsonVar.array! {
                        for jobsJson  in subJson["jobs"].array! {
                            
                            for jobJson in jobsJson["job"].array! {
                                if let job = Job(json: jobJson) {
                                    
                                    if(job.orderStatusId == self.assignedStatusIDFromOrderStatusDB){
                                        self.assignedJobList.append(job)
                                        
                                        
                                    }else if(job.orderStatusId == self.acknowledgedStatusIDFromOrderStatusDB){
                                        self.acknowledgedJobList.append(job)
                                        
                                    }else if(job.orderStatusId == self.inProgressStatusIDFromOrderStatusDB){
                                        self.inProgressJobList.append(job)
                                        
                                    }else if(job.orderStatusId == self.completedStatusIDFromOrderStatusDB){
                                        self.completedJobList.append(job)
                                        
                                    }else if(job.orderStatusId == self.selfCollectStatusIDFromOrderStatusDB){
                                        self.selfCollectJobList.append(job)
                                        
                                    }else if(job.orderStatusId == self.failedStatusIDFromOrderStatusDB){
                                        self.failedJobList.append(job)
                                        
                                    }else if(job.orderStatusId == self.cancelledStatusIDFromOrderStatusDB){
                                        self.cancelledJobList.append(job)
                                    }
                                    
                                    
                                    self.jobList.append(job)
                                }
                                
                            }
                        }
                    }
                    self.sectionArray.removeAll()
                    if(self.assignedJobList.count>0)
                    {
                        let jobSection  =  JobSection()
                        jobSection.jobSectionStatusId = self.assignedStatusIDFromOrderStatusDB
                        jobSection.jobSectionStatusName = "Assigned"
                        self.sectionArray.append(jobSection);
                    }
                    if(self.acknowledgedJobList.count>0)
                    {
                        let jobSection  =  JobSection()
                        jobSection.jobSectionStatusId = self.acknowledgedStatusIDFromOrderStatusDB
                        jobSection.jobSectionStatusName = "Acknowledged"
                        self.sectionArray.append(jobSection);
                        
                    }
                    if(self.inProgressJobList.count>0)
                    {
                        let jobSection  =  JobSection()
                        jobSection.jobSectionStatusId = self.inProgressStatusIDFromOrderStatusDB
                        jobSection.jobSectionStatusName = "In Progress"
                        self.sectionArray.append(jobSection);                }
                    if(self.completedJobList.count>0)
                    {
                        let jobSection  =  JobSection()
                        jobSection.jobSectionStatusId = self.completedStatusIDFromOrderStatusDB
                        jobSection.jobSectionStatusName = "Completed"
                        self.sectionArray.append(jobSection);
                        
                    }
                    if(self.selfCollectJobList.count>0)
                    {
                        let jobSection  =  JobSection()
                        jobSection.jobSectionStatusId = self.selfCollectStatusIDFromOrderStatusDB
                        jobSection.jobSectionStatusName = "Self Collect"
                        self.sectionArray.append(jobSection);
                        
                    }
                    if(self.failedJobList.count>0)
                    {
                        var jobSection  =  JobSection()
                        jobSection.jobSectionStatusId = self.failedStatusIDFromOrderStatusDB
                        jobSection.jobSectionStatusName = "Failed"
                        self.sectionArray.append(jobSection);
                        
                    }
                    if(self.cancelledJobList.count>0)
                    {
                        var jobSection  =  JobSection()
                        jobSection.jobSectionStatusId = self.cancelledStatusIDFromOrderStatusDB
                        jobSection.jobSectionStatusName = "Cancelled"
                        self.sectionArray.append(jobSection);
                        
                    }
                    
                    self.tableView.reloadData()
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== Search Process ======= */
        
        
        
    }
    

    
    func setDate() {
        let month = testCalendar.dateComponents([.month], from: currentDate).month!
        let weekday = testCalendar.component(.weekday, from: currentDate)
        
        let monthName = DateFormatter().monthSymbols[(month-1) % 12] //GetHumanDate(month: month)//
        let week = DateFormatter().shortWeekdaySymbols[weekday-1]
        
        let day = testCalendar.component(.day, from: currentDate)
        
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
       // print("searchBarTextDidBeginEditing \(itemTrackingSearch)")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
       // print("searchBarTextDidEndEditing \(itemTrackingSearch)")
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
        self.searchBar.subviews.flatMap({$0.subviews}).forEach({ ($0 as? UIButton)?.isEnabled = true })

        // getSearchJobList(itemTrackingSearch: itemTrackingSearch)
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        //print("searchBarSearchButtonClicked \(itemTrackingSearch)")
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
        searchBar.subviews.flatMap({$0.subviews}).forEach({ ($0 as? UIButton)?.isEnabled = true })

        searchOrder(itemTracking: self.searchBar.text!)

      
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       // itemTrackingSearch = searchText
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        //searchBar.text = ""
        // Hide the cancel button
       // searchBar.showsCancelButton = false
        // You could also change the position, frame etc of the searchBar
        if(keyboardShown)
        {
            searchBar.resignFirstResponder()
            searchBar.subviews.flatMap({$0.subviews}).forEach({ ($0 as? UIButton)?.isEnabled = true })

        }
        else{
            dismiss(animated: true, completion: nil)

        }
    }
    
    
    
}

extension SearchOrderViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}


