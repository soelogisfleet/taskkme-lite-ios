//
//  OrderDetailsManyStepViewController.swift
//  SAAS
//
//  Created by Coolasia on 5/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON
import SCLAlertView

class OrderDetailsManyStepViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var itemOrderView: UIView!
    
    @IBOutlet weak var startJobView: UIView!
    
    @IBOutlet weak var jobFailed: UIView!
    
    @IBOutlet weak var totalitemOrderLabel: UILabel!
    
    @IBOutlet weak var descOrderDetail: UILabel!
    
    @IBOutlet weak var orderStatusLabel: UILabel!
    
    @IBOutlet weak var trackingNumberLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var heightConstraintTableView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintStartJobView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintJobFailedView: NSLayoutConstraint!
    
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var indexRow: Int?
    var countRow: Int?
    var heightEachRow: Int?
    var orderID : Int?
    var jobdetail = Job()
    var orderStatusNameFromDB: String = ""
    var orderNumber: String?
    var reachInternetConnection = Reachability()
    var lastContentOffset: CGFloat = 0
    var needToReloadJobListing: Bool?
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return .portrait
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func clickStartJobView(_ gr: UITapGestureRecognizer) {
        if(self.jobdetail.orderId != 0){
            //Check internet connection
            if reachInternetConnection.isConnectedToNetwork() == true {
                print("OrderDetailsMany 99 Internet connection OK")
                changedStatusToAcknowledge()
                
            } else {
                print("OrderDetailsMany 99 Internet connection FAILED")
                var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
            
            
        }
        
    }
    
    
    /* ====== FUNC SHOW ALERT DIALOG SUCCESS UPDATE ======= */
    func showUIAlertDialog(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") {
            print("YES button tapped")
            self.navigationController!.popViewController(animated:true)
        }
        
        alertView.showSuccess(StringMessage.assignedToacknowledgedTitle, subTitle: StringMessage.assignedToacknowledgedMessage)
    }
    
    
    func changedStatusToAcknowledge(){
        
        var orderStatusIDFromOrderStatusDBToAcknowledged: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ACKNOWLEDGED_TEXT)
        
        let batchOrder: BatchOrder = BatchOrder()
        batchOrder.orderIds.removeAll()
        batchOrder.orderStatus = String(describing: orderStatusIDFromOrderStatusDBToAcknowledged)
        
        batchOrder.orderIds.append(String(self.jobdetail.orderId))
        
        let para:NSMutableDictionary = NSMutableDictionary()
        
        
        para.setValue(batchOrder.orderIds, forKey: "id")
        para.setValue(batchOrder.orderStatus, forKey: "order_status_id")
        
        
        let dataPara:NSMutableDictionary = NSMutableDictionary()
        let dataArray:NSMutableArray = NSMutableArray()
        
        dataArray.add(para)
        
        dataPara.setValue(dataArray, forKey: "data")
        
        SwiftSpinner.show("Submitting")
        
        /* ====== UPDATE JOB STATUS ======= */
        AlamofireRequest.batchUpdate(parameters: dataPara as NSDictionary) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                        if(swiftyJsonVar["status"].bool == true){
                            //succesfully update order, show an success message
                            DispatchQueue.main.async {
                                var tabBarController: MainVC
                                tabBarController = self.parent?.parent as!MainVC
                                tabBarController.refreshTabBarItemCount()
                                
                                self.showUIAlertDialog()
                            }
                            
                        }
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil || statusCode == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== UPDATE JOB STATUS ======= */
    }
    
    
    
    /* ===== FirstLoad ======== */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundView = nil;
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = 67
        self.tableView.layoutMargins = UIEdgeInsets.zero
        self.tableView.separatorInset = UIEdgeInsets.zero
        
        self.scrollView.delegate = self
        
        /* Make arrow for item order view*/
//        let disclosure = UITableViewCell()
//        disclosure.frame = itemOrderView.bounds
//        disclosure.accessoryType = .disclosureIndicator
//        disclosure.isUserInteractionEnabled = true
//        itemOrderView.addSubview(disclosure)
        
        /* Setup func for view to make sure user can interact with it */
        let clickItemOrder = UITapGestureRecognizer(target: self, action: #selector(OrderDetailsManyStepViewController.clickItemOrderView(_:)))
        self.itemOrderView.addGestureRecognizer(clickItemOrder)
        self.itemOrderView.isUserInteractionEnabled = true
        
        
        let clickAcceptJob = UITapGestureRecognizer(target: self, action: #selector(OrderDetailsManyStepViewController.clickStartJobView(_:)))
        self.startJobView.addGestureRecognizer(clickAcceptJob)
        self.startJobView.isUserInteractionEnabled = true
        
        
        self.needToReloadJobListing = true
        


        
    }
    /* ===== FirstLoad ======== */
    
    /* ===== Load Everytime ======== */
    override func viewWillAppear(_ animated: Bool) {
        
//        self.navigationController?.navigationBar.topItem?.title = self.orderNumber
//        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
//        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
//        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
//        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
//        let backButton = UIButton(type: .system)
//        backButton.setImage(#imageLiteral(resourceName: "scan-enabled"), for: .normal)
//        let backBarButton = UIBarButtonItem.init(customView: backButton)
//        navigationItem.setLeftBarButton(UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil), animated: true)
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = self.orderNumber
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        //self.navigationController?.navigationBar.backItem?.backBarButtonItem?.title = "Back"

        //self.parent?.navigationController?.isNavigationBarHidden = true

       // self.navigationItem.titleView=nil
        
        
        if(self.needToReloadJobListing)!{
            if reachInternetConnection.isConnectedToNetwork() == true {
                print("OrderDetailMany will appear Internet connection OK")
                self.getOrderDetails()
                self.needToReloadJobListing = false
            } else {
                print("OrderDetailMany will appear Internet connection FAILED")
                var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        }
        
        
        
      
    }
    /* ===== Load Everytime ======== */
    
    /* ======== RELOAD JOB DETAIL BASED ON ORDER ID ======== */
    func getOrderDetails(){
        
        SwiftSpinner.show("Getting Order Details...")
        
        let message   = String(format: Constants.api_get_order_detail, arguments: [String(describing: orderID!)])
        
        print("meesageGetOrderDetail \(message)")
        
        /* ====== FETCH JOB ======= */
        AlamofireRequest.fetchDetailsOfJob(url: message) { responseObject, statusCode, error in
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    if let job = Job(json: swiftyJsonVar) {
                        self.jobdetail = job
                        print("array job step \(self.jobdetail.job_stepsList.count)")
                        
                        //                            if(self.jobdetail.job_stepsList.count > 0){
                        //                                for var i in 0..<self.jobdetail.job_stepsList.count {
                        //                                    var jobStep = JobSteps()
                        //                                    jobStep = self.jobdetail.job_stepsList[i]
                        //                                    print("job step \(jobStep.jobStepStatus.name)")
                        //
                        //                                    }
                        //                                }
                        
                        
                        self.reloadJobInfo()
                        self.sortOrderSequenceList()
                    }
                    
                    
                }
                
                
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== FETCH JOB ======= */
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON ORDER ID ======== */
    
    /* ======= RELOAD / SETUP VIEW DATA =========== */
    public func reloadJobInfo(){
        
        // Tracking Number
        if(self.jobdetail.itemTrackingNumber == "" || self.jobdetail.itemTrackingNumber == nil){
            self.trackingNumberLabel.text = " - "
        }else{
            self.trackingNumberLabel.text = self.jobdetail.itemTrackingNumber
        }
        
        print("job11step \(String(describing: self.jobdetail.itemTrackingNumber))")
        
        // Description
        if(self.jobdetail.dropOffDesc == "" || self.jobdetail.dropOffDesc == nil){
            self.descOrderDetail.text = "\n - \n"
        }else{
            self.descOrderDetail.text = "\n" + self.jobdetail.dropOffDesc! + "\n"
        }
        print("job22step \(String(describing: self.jobdetail.dropOffDesc))")
        
        // Items Order Number
        if(self.jobdetail.orderDetailList.count == 0){
            self.totalitemOrderLabel.text = "None"
            self.arrowImage.isHidden = true
        }else{
            self.totalitemOrderLabel.text = String(self.jobdetail.orderDetailList.count)
            self.arrowImage.isHidden = false
        }
        
        
        /*This code is always hide startjob button and failed job button*/
        self.startJobView.isHidden = true
        self.heightConstraintStartJobView.constant = 0
        self.startJobView.layoutIfNeeded()
        
        self.jobFailed.isHidden = true
        self.heightConstraintJobFailedView.constant = 0
        self.jobFailed.layoutIfNeeded()
        
        // Status for Order status
        orderStatusNameFromDB = QueryDatabase.getOrderStatusNameFromOrderStatusDB(orderStatusId: self.jobdetail.orderStatusId)
        
        switch (orderStatusNameFromDB)
        {
        case Constants.ASSIGNED_TEXT:
            
            self.startJobView.isHidden = false
            self.heightConstraintStartJobView.constant = 45
            self.startJobView.layoutIfNeeded()
            
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.ASSIGNED_COLOR)
            self.orderStatusLabel.text = Constants.ASSIGNED_TEXT.uppercased()
            
        case Constants.ACKNOWLEDGED_TEXT:
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.ACKNOWLEDGED_COLOR)
            self.orderStatusLabel.text = Constants.ACKNOWLEDGED_TEXT.uppercased()
            
        case Constants.INPROGRESS_TEXT:
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.INPROGRESS_COLOR)
            self.orderStatusLabel.text = Constants.INPROGRESS_TEXT.uppercased()
            
        case Constants.COMPLETE_TEXT:
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.COMPLETE_COLOR)
            self.orderStatusLabel.text = Constants.COMPLETE_TEXT.uppercased()
            
        case Constants.FAILED_TEXT:
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.FAILED_COLOR)
            self.orderStatusLabel.text = Constants.FAILED_TEXT.uppercased()
            
        case Constants.CANCELLED_TEXT:
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.CANCELLED_COLOR)
            self.orderStatusLabel.text = Constants.CANCELLED_TEXT.uppercased()
            
        case Constants.SELF_COLLECT_TEXT:
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.SELF_COLLECT_COLOR)
            self.orderStatusLabel.text = Constants.SELF_COLLECT_TEXT.uppercased()
            
        case "Unknown status":
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.SELF_COLLECT_COLOR)
            self.orderStatusLabel.text = "UNKNOWN STATUS"
            
        default:
            self.orderStatusLabel.backgroundColor = Constants.hexStringToUIColor(hex: Constants.SELF_COLLECT_COLOR)
            self.orderStatusLabel.text = "UNKNOWN STATUS"
        }
        
    }
    /* ======= RELOAD / SETUP VIEW DATA =========== */
    
    /* ======== TABLE VIEW STEP LIST ====== */
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraintTableView.constant = tableView.contentSize.height
        tableView.layoutIfNeeded()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        //tableView.layoutIfNeeded()
        return 67;
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension
    }
    
    
    // TableView
    //MARK: - Tableview Delegate & Datasource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        
        countRow =  self.jobdetail.job_stepsList.count
        return countRow!
        
        
    }
    private func numberOfSectionsInTableView(tableViewOrderItem: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:StepsListTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "StepsListTableViewCell") as! StepsListTableViewCell
        
        cell.stepNumber.setTitle("\(indexPath.row + 1)", for: .normal)
        cell.layoutMargins = UIEdgeInsets.zero

        if((indexPath.row + 1) == countRow){
            cell.bottomLine.isHidden = true
        }
        
        cell.stepName.text = self.jobdetail.job_stepsList[indexPath.row].job_step_name
        if(self.jobdetail.job_stepsList[indexPath.row].location == "" || self.jobdetail.job_stepsList[indexPath.row].location == nil){
            cell.stepAddress.text = " - "
        }else{
            cell.stepAddress.text = self.jobdetail.job_stepsList[indexPath.row].location
        }
        
        
        /*This code want to show start job and failed button in order detail. But now we not used this code anymore*/
//        if(self.jobdetail.job_stepsList[indexPath.row].order_sequence == 1 && self.jobdetail.job_stepsList[indexPath.row].job_step_status_id != Constants.JOB_STEP_PENDING){
//
//            self.startJobView.isHidden = true
//            self.heightConstraintStartJobView.constant = 0
//            self.startJobView.layoutIfNeeded()
//
//            self.jobFailed.isHidden = true
//            self.heightConstraintJobFailedView.constant = 0
//            self.jobFailed.layoutIfNeeded()
//        }
        
        /* This code is to check if previous step is completed but current step still pending. This should not be happen because when current step will be completed then next step will be auto to in progress. This just to make sure button start and failed step not hidden */
//        if(self.jobdetail.job_stepsList.count > 1){
//            if(self.jobdetail.job_stepsList[self.jobdetail.job_stepsList.count - 1].job_step_status_id == Constants.JOB_STEP_COMPLETED
//                && self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_PENDING){
//                self.startJobView.isHidden = false
//                self.heightConstraintStartJobView.constant = 45
//                self.startJobView.layoutIfNeeded()
//
//                self.jobFailed.isHidden = false
//                self.heightConstraintJobFailedView.constant = 45
//                self.jobFailed.layoutIfNeeded()
//            }
//        }
        /*This code want to show start job and failed button in order detail. But now we not used this code anymore*/
        
        
        
        
        //cell.stepAddress.restart()
        
        if(self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_PENDING){
            cell.topLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_PENDING)
            cell.bottomLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_PENDING)
            cell.stepNumber.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_PENDING)
            cell.stepNumber.layer.borderColor = Constants.colorOrderStatusStepBorder(orderStatusStep: Constants.JOB_STEP_PENDING).cgColor
            cell.stepNumber.layer.borderWidth = 1.0

            
        }else if(self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_FAILED){
            cell.topLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_FAILED)
            cell.bottomLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_FAILED)
            cell.stepNumber.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_FAILED)
            cell.stepNumber.layer.borderColor = Constants.colorOrderStatusStepBorder(orderStatusStep: Constants.JOB_STEP_FAILED).cgColor
            cell.stepNumber.layer.borderWidth = 1.0
            
            
        }
        
        else if(self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_INPROGRESS){
        
            cell.topLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_INPROGRESS)

            cell.bottomLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_PENDING)
            cell.stepNumber.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_INPROGRESS)
            cell.stepNumber.layer.borderColor = Constants.colorOrderStatusStepBorder(orderStatusStep: Constants.JOB_STEP_INPROGRESS).cgColor
            cell.stepNumber.layer.borderWidth = 1.0
            
            cell.stepNumber.alpha = 0.2
            cell.bottomLine.alpha = 0.2
            cell.topLine.alpha = 0.2

            UIView.animate(withDuration: 1, delay: 0.0, options: [.curveEaseInOut, .repeat, .autoreverse], animations: {cell.stepNumber.alpha = 1.0
                 cell.bottomLine.alpha=1.0
                 cell.topLine.alpha = 1.0
            }, completion: nil)
            
            
//            UIView.animate(withDuration: 1.0, delay: 0.0, options:[.repeat, .autoreverse], animations: {
//                cell.topLine.backgroundColor = Constants.hexStringToUIColor(hex: Constants.ANIMATE_INPROGRESS_COLOR)
//                cell.bottomLine.backgroundColor = Constants.hexStringToUIColor(hex: Constants.ANIMATE_INPROGRESS_COLOR)
//                cell.stepNumber.backgroundColor = Constants.hexStringToUIColor(hex: Constants.ANIMATE_INPROGRESS_COLOR)
//                cell.stepNumber.layer.borderColor = Constants.hexStringToUIColor(hex: Constants.ANIMATE_INPROGRESS_LAYER_COLOR).cgColor
//
//            }, completion:nil)
            
        }else if(self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_COMPLETED){
            
          
            if(indexPath.row != self.jobdetail.job_stepsList.count && indexPath.row + 1 < self.jobdetail.job_stepsList.count)
            {
                if(self.jobdetail.job_stepsList[indexPath.row + 1].job_step_status_id == Constants.JOB_STEP_PENDING)
                {
                    cell.bottomLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_PENDING)
                  

                }else if(self.jobdetail.job_stepsList[indexPath.row + 1].job_step_status_id == Constants.JOB_STEP_FAILED)
                {
                    cell.bottomLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_FAILED)
                    
                    
                }
                else if(self.jobdetail.job_stepsList[indexPath.row + 1].job_step_status_id == Constants.JOB_STEP_INPROGRESS){
                    cell.bottomLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_INPROGRESS)
                    cell.bottomLine.alpha = 0.2
                    UIView.animate(withDuration: 1, delay: 0.0, options: [.curveEaseInOut, .repeat, .autoreverse], animations: {
                        cell.bottomLine.alpha=1.0
                    }, completion: nil)
                    

                }
                else{
                    cell.bottomLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_COMPLETED)

                }
            }
       
            
            cell.topLine.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_COMPLETED)
            cell.stepNumber.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_COMPLETED)
            cell.stepNumber.layer.borderColor = Constants.colorOrderStatusStepBorder(orderStatusStep: Constants.JOB_STEP_COMPLETED).cgColor
            cell.stepNumber.layer.borderWidth = 1.0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("select")
        var showCompletedAndJobFailedButton: Bool?
        var showStartJobButton: Bool?
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Internet connection OK")
            /* This code to check current step status and previous step status to show completed or start job or hide */
            if(self.jobdetail.job_stepsList.count != 0){
                if(self.jobdetail.job_stepsList.count > 1){
                    /*This code below mean it is first row, because first row is 0*/
                    if((indexPath.row - 1) < 0){
                        if(self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_INPROGRESS){
                            // need to completed job
                            //show completed and failed button
                            showStartJobButton = false
                            showCompletedAndJobFailedButton = true
                        }else if(self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_PENDING){
                            // need to start Job
                            //show completed and failed button
                            showStartJobButton = true
                            showCompletedAndJobFailedButton = true
                        }else{
                            //dont show completed and failed button
                            showCompletedAndJobFailedButton = false
                            showStartJobButton = false
                        }
                        
                    }else{
                        /*This statement mean it not first row, so need to check previous row either that status is completed or not*/
                        if(self.jobdetail.job_stepsList[indexPath.row - 1].job_step_status_id == Constants.JOB_STEP_COMPLETED && self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_PENDING){
                            // need to Start job
                            //show completed and failed button
                            showStartJobButton = true
                            showCompletedAndJobFailedButton = true
                        }else if(self.jobdetail.job_stepsList[indexPath.row - 1].job_step_status_id == Constants.JOB_STEP_COMPLETED && self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_INPROGRESS){
                            // need to Completed job
                            //show completed and failed button
                            showStartJobButton = false
                            showCompletedAndJobFailedButton = true
                        }else if(self.jobdetail.job_stepsList[indexPath.row - 1].job_step_status_id != Constants.JOB_STEP_COMPLETED){
                            
                            //dont show completed and failed button
                            showStartJobButton = false
                            showCompletedAndJobFailedButton = false
                        }
                    }
                    
                }else{
                    // Just have one step only
                    
                    /* Need to check is current step is pending ? If pending, need hide button completed job. If it inprogress, need show completed, otherwise dont need to show */
                    if(self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_INPROGRESS){
                        //show completed and failed button
                        // show completed job
                        showStartJobButton = false
                        showCompletedAndJobFailedButton = true
                        
                    }else if(self.jobdetail.job_stepsList[indexPath.row].job_step_status_id == Constants.JOB_STEP_PENDING){
                        // show completed and failed button
                        // show start job
                        showStartJobButton = true
                        showCompletedAndJobFailedButton = true
                    }else{
                        // not need to show completed and failed button
                        showStartJobButton = false
                        showCompletedAndJobFailedButton = false
                    }
                }
                
                print("counnttt11 \(self.jobdetail.job_stepsList.count)")
                print("counnttt11 \(self.jobdetail.job_stepsList[indexPath.row].order_sequence)")
                
                var isLastJobStep = Bool()
                if(self.jobdetail.job_stepsList.count > 1 && self.jobdetail.job_stepsList[indexPath.row].order_sequence == self.jobdetail.job_stepsList.count){
                    isLastJobStep = true
                    //last job is true / for popup message last job step
                }else if(self.jobdetail.job_stepsList.count == 1){
                    //last job is true / for popup message last job step
                    isLastJobStep = true
                }else{
                    // not last job step
                    isLastJobStep = false
                }
                
                print("counnttt11 \(isLastJobStep)")
                
                let storyboard = UIStoryboard(name: "OrderDetailsOneStep", bundle: nil)
                let levelController = storyboard.instantiateViewController(withIdentifier: "OrderDetailsOneStep") as! OrderDetailsOneStepViewController
                
                levelController.orderID = self.jobdetail.orderId
                levelController.orderStatusID = self.jobdetail.orderStatusId
                levelController.itemTrackingNumber =  self.jobdetail.itemTrackingNumber
                levelController.jobStepID = self.jobdetail.job_stepsList[indexPath.row].id
                levelController.jobStepName = self.jobdetail.job_stepsList[indexPath.row].job_step_name
                levelController.showCompletedAndJobFailedButton = showCompletedAndJobFailedButton
                levelController.showStartJobButton = showStartJobButton
                levelController.companyName = self.jobdetail.companyName
                levelController.isLastJobStep = isLastJobStep
                
                self.needToReloadJobListing = true
                
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
                
                self.navigationController?.pushViewController(levelController, animated: true)
                
                
            }else{
                // no job step
                // it should not be happen because at least must have one step
            }
        } else {
            print("Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    
    /* ======== TABLE VIEW STEP LIST ====== */
    
    
    /* === FUNCTION TO SORT ORDER SEQUENCE JOB STEP ======= */
    func sortOrderSequenceList() {
        self.jobdetail.job_stepsList.sort() { $0.self.order_sequence! < $1.self.order_sequence! }
        self.tableView.reloadData()
    }
    /* === FUNCTION TO SORT ORDER SEQUENCE JOB STEP ======= */
    
    
    /* ======= CLICK / ACTION BUTTON ========== */
    
    /* Open List Item Order */
    func clickItemOrderView(_ gr: UITapGestureRecognizer) {
        if(self.jobdetail.orderDetailList.count > 0){
            //Check internet connection
            if reachInternetConnection.isConnectedToNetwork() == true {
                print("OrderDetailsMany 11 Internet connection OK")
                let storyboard = UIStoryboard(name: "ListItemOrder", bundle: nil)
                let levelController = storyboard.instantiateViewController(withIdentifier: "ListItemOrder") as! ListItemOrderViewController
                levelController.orderStatusNameFromDB = self.orderStatusNameFromDB
                levelController.orderID = self.jobdetail.orderId
                self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                
                self.navigationController?.pushViewController(levelController, animated: true)
            } else {
                print("OrderDetailsMany 11 Internet connection FAILED")
                var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
            
            
        }
        
    }
    
    /* ======= CLICK / ACTION BUTTON ========== */
    
}


extension OrderDetailsManyStepViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}
