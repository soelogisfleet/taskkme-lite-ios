//
//  DriverListViewController.swift
//  SAAS
//
//  Created by jing jie wong on 4/6/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import Foundation
import SwiftSpinner
import SwiftyJSON
import SCLAlertView

class DriverListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightConstraintTableView: NSLayoutConstraint!

    
    var countRow: Int?
    var orderID : Int?
    var orderStatusID : Int?
    var jobStepID : Int?
    var jobStepDetail = JobSteps()
    var workerList: [Worker] = []
    var selectedJobIdArray: [Int] = []

    var reachInternetConnection = Reachability()
    let textCellIdentifier = "TextCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundView = nil;
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("getDriverList Internet connection OK")
            getDriverList()
        } else {
            print("getDriverList Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Assign job(s) to"
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        
        
    }
    
    func showUIAlertDialog(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") {
            print("YES button tapped")
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)

            //print("date after accept \(self.selectedDate)")
           // self.reloadJobList(date: self.selectedDate.string(format: "yyyy-MM-dd"))
        }
        
        alertView.showSuccess(StringMessage.unassignedToAssignedTitle, subTitle: StringMessage.unassignedToAssignedMessage)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON JOB STEP ID ======== */
    func getDriverList(){
        
        SwiftSpinner.show("Getting Driver List...")
        
        
        
        /* ====== FETCH JOB STEP ======= */
        AlamofireRequest.fetchDriverList(url: Constants.api_get_driver_list) { responseObject, statusCode, error in
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    print ("swiftyJsonVar = \(swiftyJsonVar)")

                    let resultJson = swiftyJsonVar["result"]
                    for subJson in resultJson.array! {
                        if let worker = Worker(json: subJson) {
                            self.workerList.append(worker)
                        }
                        
                    }
                
                   
                    print ("worker list = \(self.workerList.count)")
                    

                    self.tableView.reloadData()
                }
                
                
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== FETCH JOB STEP ======= */
    }
    
    /* ======== RELOAD JOB DETAIL BASED ON JOB STEP ID ======== */
    
    /* ======== TABLE VIEW ORDER ATTEMPT ====== */

    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        heightConstraintTableView.constant = tableView.contentSize.height
        tableView.layoutIfNeeded()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension;
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        tableView.layoutIfNeeded()
        return UITableViewAutomaticDimension
    }
    
    
    // TableView
    //MARK: - Tableview Delegate & Datasource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        
        countRow =  self.workerList.count
        return countRow!
        
        
    }
    private func numberOfSectionsInTableView(tableViewOrderItem: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath)
        
        let row = indexPath.row
        if(workerList[row].firstName != nil && workerList[row].lastName != nil)
        {
            let workerName = workerList[row].firstName! + " " + workerList[row].lastName!
            
            cell.textLabel?.text = workerName

        }
        else{
            if(workerList[row].firstName == nil && workerList[row].lastName == nil)
            {
                cell.textLabel?.text = "N.A"
            }
            else if(workerList[row].firstName == nil)
            {
                let workerName = workerList[row].lastName!
                
                cell.textLabel?.text = workerName
            }
            else if(workerList[row].lastName == nil)
            {
                let workerName = workerList[row].firstName!
                
                cell.textLabel?.text = workerName
            }
            
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if tableView.cellForRow(at: indexPath)?.accessoryType == UITableViewCellAccessoryType.checkmark
        {
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.none
        }
        else{
            tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.checkmark

        }
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("DriverList Internet connection OK")
            
            changedStatusToAssigned(selectedWorker: workerList[indexPath.row])
          
        } else {
            print("Driver List Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    func changedStatusToAssigned(selectedWorker: Worker){
        print("Hello1!")
        
        var orderStatusIDFromOrderStatusDBToAcknowledged: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ASSIGNED_TEXT)
        
        let batchOrder: BatchOrder = BatchOrder()
        batchOrder.orderIds.removeAll()
        batchOrder.orderStatus = String(describing: orderStatusIDFromOrderStatusDBToAcknowledged)
        batchOrder.workerId = selectedWorker.id
        for jobCanAccept in selectedJobIdArray{
            print("orderId, \(jobCanAccept)")
            batchOrder.orderIds.append(String(jobCanAccept))
            
        }
        
        let para:NSMutableDictionary = NSMutableDictionary()
        
        
        para.setValue(batchOrder.orderIds, forKey: "id")
        para.setValue(batchOrder.orderStatus, forKey: "order_status_id")
        para.setValue(batchOrder.workerId, forKey: "drop_off_worker_id")


        
        let dataPara:NSMutableDictionary = NSMutableDictionary()
        let dataArray:NSMutableArray = NSMutableArray()
        
        dataArray.add(para)
        
        dataPara.setValue(dataArray, forKey: "data")
        
        SwiftSpinner.show("Submitting")
        
        /* ====== UPDATE JOB STATUS ======= */
        AlamofireRequest.batchUpdate(parameters: dataPara as NSDictionary) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                        if(swiftyJsonVar["status"].bool == true){
                            //succesfully update order, show an success message
                            DispatchQueue.main.async {
                                var tabBarController: MainVC
                                
                                tabBarController = self.parent?.parent as!MainVC
                                tabBarController.refreshTabBarItemCount()
                                
                                self.showUIAlertDialog()
                            }
                            
                        }
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil || statusCode == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== UPDATE JOB STATUS ======= */
    }
    
    /* ======== TABLE VIEW ORDER ATTEMPT ====== */
    
    /* === FUNCTION TO SORT LATEST ORDER ATTEMPT IN TABLEVIEW ======= */
    func sortTableViewList() {
        self.jobStepDetail.orderAttemptList.sort() { $0.self.submittedTime! < $1.self.submittedTime! }
        self.tableView.reloadData()
    }
    /* === FUNCTION TO SORT LATEST ORDER ATTEMPT IN TABLEVIEW ======= */
}


extension DriverListViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}

