//
//  ListOfCustomer_TableViewCell.swift
//  SAAS
//
//  Created by Coolasia on 27/6/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit

class ListOfCustomer_TableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameOfCustomer: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
