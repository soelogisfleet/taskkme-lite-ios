//
//  OfflineOrder_TableViewCell.swift
//  WMG
//
//  Created by Coolasia on 28/3/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import MarqueeLabel

class OfflineOrder_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var txt_postal_offline: UILabel!
    @IBOutlet weak var bg_box: UIImageView!
    @IBOutlet weak var noInternetText: MarqueeLabel!
    
    @IBOutlet weak var spinGif: UIImageView!
    @IBOutlet weak var cell: UIView!
    
    @IBOutlet weak var address_offline_dropoff: MarqueeLabel!
    
    @IBOutlet weak var trackingNumber_offline: UILabel!
    
    @IBOutlet weak var name_offline_dropoff: UILabel!
    
    
    @IBOutlet weak var btn_call: UIButton!
    @IBOutlet weak var btn_remark: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        address_offline_dropoff.animationCurve = .curveLinear
        address_offline_dropoff.marqueeType = .MLContinuous
        address_offline_dropoff.scrollDuration = 16.0
        
        noInternetText.animationCurve = .curveLinear
        noInternetText.marqueeType = .MLContinuous
        noInternetText.scrollDuration = 16.0
        
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
