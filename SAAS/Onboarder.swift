////
////  Onboarder.swift
////  SAAS
////
////  Created by Coolasia on 18/3/18.
////  Copyright © 2018 Coolasia. All rights reserved.
////
//
//import UIKit
//import Foundation
//
//
//class Onboarder: OnboardingViewController {
//    var firstBackground: UIImageView!
//    var firstImage:UIImage!
//    var secondBackground:UIImageView!
//    var secondImage:UIImage!
//    var thirdBackground:UIImageView!
//    var thirdImage:UIImage!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        createOtherBackgrounds()
//    }
//    
//    func createOtherBackgrounds() {
//        firstBackground = UIImageView(frame: self.view.bounds)
//        firstBackground.clipsToBounds = true
//        firstBackground.contentMode = .scaleAspectFill
//        firstBackground.image = UIImage(named: "splash-iphone6plus")
//        firstBackground.alpha = 1
//        self.view.addSubview(firstBackground)
//        
//        
//        secondBackground = UIImageView(frame: self.view.bounds)
//        secondBackground.clipsToBounds = true
//        secondBackground.contentMode = .scaleAspectFill
//        secondBackground.image = UIImage(named: "bg-blue")
//        secondBackground.alpha = 0
//        self.view.addSubview(secondBackground)
//        
//        thirdBackground = UIImageView(frame: self.view.bounds)
//        thirdBackground.clipsToBounds = true
//        thirdBackground.contentMode = .scaleAspectFill
//        thirdBackground.image = UIImage(named: "splash-iphone6plus")
//        thirdBackground.alpha = 0
//        self.view.addSubview(thirdBackground)
//        
//        self.view.sendSubview(toBack: firstBackground)
//        self.view.sendSubview(toBack: secondBackground)
//        self.view.sendSubview(toBack: thirdBackground)
//    }
//    
//    class func getOnboard() -> Onboarder {
//        var onboardingVC = Onboarder()
//        
//        let oneT = "TitleOne"
//        let oneB = ""
//        let oneI = UIImage()
//        let oneBT = ""
//        let oneA = {}
//        let fP = OnboardingContentViewController.content(withTitle: oneT, body: oneB, image: oneI, buttonText: oneBT, action: oneA)
//        
//        let twoT = "TitleTwo"
//        let twoB = ""
//        let twoI = UIImage()
//        let twoBT = ""
//        let twoA = {}
//        let sP = OnboardingContentViewController.content(withTitle: twoT, body: twoB, image: twoI, buttonText: twoBT, action: twoA)
//        
//        let threeT = "TitleThree"
//        let threeB = ""
//        let threeI = UIImage()
//        let threeBT = ""
//        let threeA = {}
//        let tP = OnboardingContentViewController.content(withTitle: threeT, body: threeB, image: threeI, buttonText: threeBT, action: threeA)
//        
//        onboardingVC = Onboarder.onboard(withBackgroundImage: nil, contents: [fP, sP, tP])
//        onboardingVC.shouldFadeTransitions = true
//        onboardingVC.shouldMaskBackground = true
//        onboardingVC.shouldBlurBackground = false
//        onboardingVC.fadePageControlOnLastPage = true
//        onboardingVC.pageControl.pageIndicatorTintColor = UIColor.darkGray
//        onboardingVC.pageControl.currentPageIndicatorTintColor = UIColor.white
//        onboardingVC.skipButton.setTitleColor(UIColor.white, for: .normal)
//        onboardingVC.allowSkipping = true
//        onboardingVC.fadeSkipButtonOnLastPage = true
//        
//        onboardingVC.skipHandler = {
//            onboardingVC.skip()
//        }
//        
//        return onboardingVC
//    }
//    
//    func skip() {
//        self.setupNormalRoot()
//    }
//    
//    
//    
//    func setupNormalRoot() {
//       
//    }
//    
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        super.scrollViewDidScroll(scrollView)
//        let percentComplete = fabs(scrollView.contentOffset.x - self.view.frame.size.width) / self.view.frame.size.width
//        if percentComplete == 0 {return}
//        switch pageControl.currentPage {
//        case 0:
//            if scrollView.contentOffset.x < self.view.bounds.width {
//                return
//            } else {
//                secondBackground.alpha = percentComplete
//                firstBackground.alpha = 1 - percentComplete
//            }
//        case 1:
//            if scrollView.contentOffset.x < (self.view.bounds.width) {
//                secondBackground.alpha = 1 - percentComplete
//                firstBackground.alpha = percentComplete
//            } else if scrollView.contentOffset.x >= (self.view.bounds.width) {
//                secondBackground.alpha = 1 - percentComplete
//                thirdBackground.alpha = percentComplete
//            }
//        case 2:
//            if scrollView.contentOffset.x < (self.view.bounds.width) {
//                thirdBackground.alpha = 1 - percentComplete
//                secondBackground.alpha = percentComplete
//            } else if scrollView.contentOffset.x >= (self.view.bounds.width) {
//                return
//            }
//        default: break
//        }
//    }
//    
//    override func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
//        super.pageViewController(pageViewController, didFinishAnimating: finished, previousViewControllers: previousViewControllers, transitionCompleted: completed)
//        switch self.pageControl.currentPage {
//        case 0:
//            firstBackground.alpha = 1
//            secondBackground.alpha = 0
//            thirdBackground.alpha = 0
//        case 1:
//            firstBackground.alpha = 0
//            secondBackground.alpha = 1
//            thirdBackground.alpha = 0
//        case 2:
//            firstBackground.alpha = 0
//            secondBackground.alpha = 0
//            thirdBackground.alpha = 1
//        default: break
//        }
//    }
//    
//}

