//
//  OrderStatusDB.swift
//  SAAS
//
//  Created by Coolasia on 8/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit


class OrderStatusDB: NSObject {
    
    
    var orderStatusId: Int?
    var orderStatusName: String?
    var orderIsActive: Bool?
    var orderGeneralId: Int?
    var orderViewName: String?
    
    override init() {
        
        self.orderStatusId = 0
        self.orderStatusName = ""
        self.orderIsActive = false
        self.orderGeneralId = 0
        self.orderViewName = ""
        
        super.init()
        
    }
    
    init(orderStatusId: Int?, orderStatusName: String?, orderIsActive: Bool?, orderGeneralId: Int?, orderViewName: String?) {
        
        self.orderStatusId = orderStatusId
        self.orderStatusName = orderStatusName
        self.orderIsActive = orderIsActive
        self.orderGeneralId = orderGeneralId
        self.orderViewName = orderViewName
        
        super.init()
    }
    
}


extension OrderStatusDB {
    convenience init?(json: JSON) {
        
        let orderStatusId: Int? = json["id"].int
        let orderStatusName: String? = json["status"].string
        let orderIsActive: Bool? = json["is_active"].bool
        let orderGeneralId: Int? = json["general_id"].int
        let orderViewName: String? = json["view_name"].string
        
        self.init(orderStatusId: orderStatusId, orderStatusName:orderStatusName, orderIsActive:orderIsActive, orderGeneralId:orderGeneralId, orderViewName:orderViewName)
        
    }
}
