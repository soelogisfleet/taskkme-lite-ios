//
//  ListOrderAttemptTableViewCell.swift
//  SAAS
//
//  Created by Coolasia on 7/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit

class ListOrderAttemptTableViewCell: UITableViewCell {

    
    @IBOutlet weak var noOrderAttempt: UILabel!
    
    @IBOutlet weak var dateOrderAttempt: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
