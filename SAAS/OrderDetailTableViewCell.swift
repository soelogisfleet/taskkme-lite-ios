//
//  OrderDetailTableViewCell.swift
//  WMG
//
//  Created by Coolasia on 29/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//

import UIKit

class OrderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_contentCR: UILabel!
   
    @IBOutlet weak var lbl_qty: UILabel!
    @IBOutlet weak var lbl_serialNumber: UILabel!
    @IBOutlet weak var lbl_partNo: UILabel!
    @IBOutlet weak var cell: UIView!
    @IBOutlet weak var LabelCRHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelPartNoHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_description: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        lbl_description.numberOfLines = 0
        lbl_description.lineBreakMode = NSLineBreakMode.byWordWrapping
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
