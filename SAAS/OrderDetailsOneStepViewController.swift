//
//  OrderDetailsOneStepViewController.swift
//  SAAS
//
//  Created by Coolasia on 29/1/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SwiftSpinner
import SwiftyJSON
import SCLAlertView

class OrderDetailsOneStepViewController: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate,NewScanDelegate,EPSignatureDelegate,MyProtocol,UIScrollViewDelegate {
    
    
    @IBOutlet weak var heightConstraintCompanyName: NSLayoutConstraint!
    @IBOutlet weak var jobStepStatus: UILabel!
    @IBOutlet weak var heightConstraintFailedJobView: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintCompleteJobView: NSLayoutConstraint!
    
    @IBOutlet weak var failedJobView: UIView!
    @IBOutlet weak var completeJobView: UIView!
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var heightConstraintMapView: NSLayoutConstraint!
    @IBOutlet weak var orderAttemptView: UIView!
    @IBOutlet weak var callRecipientView: UIView!
    @IBOutlet weak var navigateLocationView: UIView!
    @IBOutlet weak var completedJobLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var remarkStepLbl: UILabel!
    @IBOutlet weak var totalOrderAttempt: UILabel!
    @IBOutlet weak var customerNameHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var addressLabelHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    var jobStepDetail = JobSteps()
    var orderID : Int?
    var orderStatusID : Int?
    var jobStepID : Int?
    var showStartJobButton:  Bool?
    var isLastJobStep:  Bool?
    var showCompletedAndJobFailedButton:  Bool?
    var itemTrackingNumber: String?
    var jobStepName: String?
    var companyName: String?
    
    let locationManager = CLLocationManager()
    var reachInternetConnection = Reachability()
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        
//        let disclosure = UITableViewCell()
//        disclosure.frame = orderAttemptView.bounds
//        disclosure.accessoryType = .disclosureIndicator
//        disclosure.isUserInteractionEnabled = true
//        orderAttemptView.addSubview(disclosure)
        
        mapView.delegate = self
        mapView.showsScale =  true
        mapView.showsPointsOfInterest = true
       // mapView.showsUserLocation = true
        
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
       // locationManager.startUpdatingLocation()
        
        /* Setup func for view to make sure user can interact with it */
        
        //Order Attempt View
        let clickOrderAttempt = UITapGestureRecognizer(target: self, action: #selector(OrderDetailsOneStepViewController.clickOrderAttemptView(_:)))
        self.orderAttemptView.addGestureRecognizer(clickOrderAttempt)
        self.orderAttemptView.isUserInteractionEnabled = true
        
        // Completed Job View
        let clickCompleteJob = UITapGestureRecognizer(target: self, action: #selector(OrderDetailsOneStepViewController.clickCompleteJobView(_:)))
        self.completeJobView.addGestureRecognizer(clickCompleteJob)
        self.completeJobView.isUserInteractionEnabled = true
        
        // Failed Job View
        let clickFailedJob = UITapGestureRecognizer(target: self, action: #selector(OrderDetailsOneStepViewController.clickFailedJobView(_:)))
        self.failedJobView.addGestureRecognizer(clickFailedJob)
        self.failedJobView.isUserInteractionEnabled = true
        
        // callRecipient View
        let clickCallRecipient = UITapGestureRecognizer(target: self, action: #selector(OrderDetailsOneStepViewController.clickCallRecipientView(_:)))
        self.callRecipientView.addGestureRecognizer(clickCallRecipient)
        self.callRecipientView.isUserInteractionEnabled = true
        
        // navigateLocation View
        let clickNavigateLocation = UITapGestureRecognizer(target: self, action: #selector(OrderDetailsOneStepViewController.clickNavigateLocationView(_:)))
        self.navigateLocationView.addGestureRecognizer(clickNavigateLocation)
        self.navigateLocationView.isUserInteractionEnabled = true
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Internet connection OK")
            getJobStepDetails()
        } else {
            print("Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        print("jobStepName \(jobStepName)")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.parent?.navigationController?.isNavigationBarHidden = true
        
       // self.navigationItem.titleView=nil
        
        self.completeJobView.isUserInteractionEnabled = true
        self.failedJobView.isUserInteractionEnabled = true
        
        self.navigationController?.navigationBar.topItem?.title = self.jobStepName
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.scrollView.contentSize.height)
        
       

    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "map_pin")
        }
        
        return annotationView
    }
    
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 5.0
        
        return renderer
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let location = locations[0]
//
//        let center = location.coordinate
//        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.06)
//        let region = MKCoordinateRegion(center: center, span: span)
//
//        mapView.setRegion(region, animated: true)
//        mapView.showsUserLocation = true
//    }
    
    /* ======== SETUP MAP LOCATION ========== */
    func setupMapLocation(){
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        let sourceCoordinate = locationManager.location?.coordinate
        let destCoordinate = CLLocationCoordinate2DMake(self.jobStepDetail.latitude!, self.jobStepDetail.longitude!)
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceCoordinate!)
        let destPlacemark = MKPlacemark(coordinate: destCoordinate)
        
//        let sourceItem = MKMapItem(placemark: sourcePlacemark)
//        let destItem = MKMapItem(placemark: destPlacemark)
//
        let annotation = MKPointAnnotation()
        annotation.coordinate = destCoordinate
        mapView.addAnnotation(annotation)

        let annotationPoint: MKMapPoint = MKMapPointForCoordinate(annotation.coordinate)
        let zoomRect: MKMapRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1)
        //mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsetsMake(20, 20, 20, 20), animated: true)


        let latDelta:CLLocationDegrees = 0.005
        let longDelta:CLLocationDegrees = 0.005
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(annotation.coordinate, span)
        mapView.setRegion(region, animated: true)
        
        //mapView.setVisibleMapRect(zoomRect, animated: true)

//        let directionRequest = MKDirectionsRequest()
//        directionRequest.source = sourceItem
//        directionRequest.destination = destItem
//        directionRequest.transportType = .automobile
//
//
//        let directions = MKDirections(request: directionRequest)
//        directions.calculate(completionHandler: {
//            response, error in
//            guard let response = response else {
//                if error != nil {
//                    print("Something went wrong")
//                }
//                return
//            }
//            let route = response.routes[0]
//            self.mapView.add(route.polyline, level: .aboveRoads)
//
//            let rekt = route.polyline.boundingMapRect
//            self.mapView.setRegion(MKCoordinateRegionForMapRect(rekt), animated: true)
//
//            let pin = MKPointAnnotation()
//            pin.title = self.jobStepDetail.location
//           // pin.subtitle = "Sp"
//            pin.coordinate = destCoordinate
//            self.mapView.addAnnotation(pin)
//
//            var myLocation = CLLocation(latitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!)
//            var myDestination = CLLocation(latitude: destCoordinate.latitude, longitude: destCoordinate.longitude)
//            var distance = myLocation.distance(from: myDestination) / 1000
//            self.distanceLabel.text = (String(format: "The distance is %.01fKM", distance))
//
//        })
    }
    /* ======== SETUP MAP LOCATION ========== */
    
    /* ======== RELOAD / SETUP JOB STEP DETAIL INFO ======= */
    func reloadJobStepInfo(){
        if((self.jobStepDetail.latitude == nil || self.jobStepDetail.latitude == 0.0) && (self.jobStepDetail.longitude == nil || self.jobStepDetail.longitude == 0.0)){
            self.mapView.isHidden = true
            heightConstraintMapView.constant = 0
            self.mapView.layoutIfNeeded()
        }else{
            setupMapLocation()
        }
        
        print("showStartJobButton \(showStartJobButton)")
        print("showCompletedAndJobFailedButton \(showCompletedAndJobFailedButton)")
        
        self.hideCompletedAndFailedButtonAndStartJobOrCompletedJob()
        
    
        /* Setup based on job step status */
        if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_PENDING){
            /* Job step status , Complete button changed to Start Job */
            self.completedJobLabel.text = "Start Job"
            self.jobStepStatus.text = Constants.JOB_STEP_PENDING_TEXT
            self.jobStepStatus.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_PENDING)
        }else if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_INPROGRESS){
            /* Job step status , Complete button keep remain */
            self.completedJobLabel.text = "Complete this Job"
            self.jobStepStatus.text = Constants.JOB_STEP_INPROGRESS_TEXT
            self.jobStepStatus.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_INPROGRESS)
        }else if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_COMPLETED){
            /* Job step status , Complete button and failed button must be hide */
            self.completeJobView.isHidden = true
            self.heightConstraintFailedJobView.constant = 0
            self.completeJobView.layoutIfNeeded()
            
            self.failedJobView.isHidden = true
            self.heightConstraintCompleteJobView.constant = 0
            self.failedJobView.layoutIfNeeded()
            
            self.jobStepStatus.text = Constants.JOB_STEP_COMPLETED_TEXT
            self.jobStepStatus.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_COMPLETED)
        }else if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_FAILED){
            /* Job step status , Complete button and failed button must be hide */
            self.jobStepStatus.text = Constants.JOB_STEP_FAILED_TEXT
             self.jobStepStatus.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_FAILED)
        }else if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_CANCELLED){
            /* Job step status , Complete button and failed button must be hide */
            self.jobStepStatus.text = Constants.JOB_STEP_CANCELLED_TEXT
            self.jobStepStatus.backgroundColor = Constants.colorOrderStatusStep(orderStatusStep: Constants.JOB_STEP_CANCELLED)
        }
        
        
        
        if(self.companyName == "" || self.companyName == nil ){
            self.companyNameLbl.isHidden = true
            self.heightConstraintCompanyName.constant = 0
            self.companyNameLbl.layoutIfNeeded()
        }else{
            self.companyNameLbl.text = self.companyName
        }
        
        
        if(self.jobStepDetail.job_step_pic == "" || self.jobStepDetail.job_step_pic == nil ){
            self.customerNameLbl.isHidden = true
            customerNameHeightConstraint.constant = 0
            self.customerNameLbl.layoutIfNeeded()
        }else{
            self.customerNameLbl.text = self.jobStepDetail.job_step_pic
        }
        
        print("self.jobStepDetail.location \(self.jobStepDetail.location)")

        
        if(self.jobStepDetail.location == "" || self.jobStepDetail.location == nil ){
            self.addressLbl.isHidden = true
            addressLabelHeightConstraint.constant = 0
            self.addressLbl.layoutIfNeeded()
            
        }
        else{
           self.addressLbl.text = self.jobStepDetail.location
        }
        
        print("self.jobStepDetail.descriptions \(self.jobStepDetail.descriptions)")
        
        if(self.jobStepDetail.descriptions == "" || self.jobStepDetail.descriptions == nil ){
            self.remarkStepLbl.text = " - "
            
        }else{
            self.remarkStepLbl.text = self.jobStepDetail.descriptions
        }
        
        self.totalOrderAttempt.text = String(self.jobStepDetail.orderAttemptList.count)
        
        if(self.jobStepDetail.orderAttemptList.count == 0){
            self.totalOrderAttempt.text = "None"
            self.arrowImage.isHidden = true
        }else{
            self.totalOrderAttempt.text = String(self.jobStepDetail.orderAttemptList.count)
            self.arrowImage.isHidden = false
        }
        
        
        
        var orderStatusIDFromOrderStatusDBAssigned: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ASSIGNED_TEXT)
        var orderStatusIDFromOrderStatusDBCompleted: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.COMPLETE_TEXT)
        var orderStatusIDFromOrderStatusDBCancelled: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.CANCELLED_TEXT)
        var orderStatusIDFromOrderStatusDBFailed: Int? = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.FAILED_TEXT)
        
        
        if(self.orderStatusID == orderStatusIDFromOrderStatusDBAssigned || self.orderStatusID == orderStatusIDFromOrderStatusDBCompleted || self.orderStatusID == orderStatusIDFromOrderStatusDBCancelled || self.orderStatusID == orderStatusIDFromOrderStatusDBFailed){
            self.completeJobView.isHidden = true
            self.heightConstraintFailedJobView.constant = 0
            self.completeJobView.layoutIfNeeded()
            
            self.failedJobView.isHidden = true
            self.heightConstraintCompleteJobView.constant = 0
            self.failedJobView.layoutIfNeeded()
        }
        
    }
    /* ======== RELOAD / SETUP JOB STEP DETAIL INFO ======= */
    
    /* ========= HIDE COMPLETED BUTTON AND FAILED BUTTON AND START JOB OR COMPLETED JOB ======== */
    func hideCompletedAndFailedButtonAndStartJobOrCompletedJob(){
        if(showCompletedAndJobFailedButton == true){
            if(showStartJobButton == true){
                self.completedJobLabel.text = "Start Job"
            }else{
                self.completedJobLabel.text = "Complete this Job"
            }
        }else{
            /*Dont show button failed completed*/
            self.completeJobView.isHidden = true
            self.heightConstraintFailedJobView.constant = 0
            self.completeJobView.layoutIfNeeded()
            
            self.failedJobView.isHidden = true
            self.heightConstraintCompleteJobView.constant = 0
            self.failedJobView.layoutIfNeeded()
        }
        
    }
    /* ========= HIDE COMPLETED BUTTON AND FAILED BUTTON AND START JOB OR COMPLETED JOB ======== */
    
    /* ======== RELOAD JOB STEP DETAIL BASED ON JOB STEP ID ======== */
    func getJobStepDetails(){
        
        SwiftSpinner.show("Getting Job Step Details...")
        
        let message   = String(format: Constants.api_get_job_step_details, arguments: [String(describing: jobStepID!)])
        
        print("meesageGetOrderDetail \(message)")
        
        /* ====== FETCH JOB STEP ======= */
        AlamofireRequest.fetchJobStepDetails(url: message) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    let resultJson = swiftyJsonVar["result"]
                    if let jobStep = JobSteps(json: resultJson) {
                        self.jobStepDetail = jobStep
                        
                        self.reloadJobStepInfo()
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil || statusCode == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== FETCH JOB STEP ======= */
    }
    
    /* ======== RELOAD JOB STEP DETAIL BASED ON JOB STEP ID ======== */

    func getFormatDateAMorPM() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        return formatter.string(from: Date())
    }
    
    /* ====== FUNC SHOW ALERT DIALOG SUCCESS UPDATE ======= */
    func showUIAlertDialog(nextJobStepStatus: Int?){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") {
            print("YES button tapped")
            if self.reachInternetConnection.isConnectedToNetwork() == true {
                print("Internet connection OK")
                self.getJobStepDetails()
            } else {
                print("Internet connection FAILED")
                var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        }
        
        print("YEnextJobStepStatus \(nextJobStepStatus)")
         print("YEnextJobStepStatus \(self.isLastJobStep)")
        
        var title: String = "Success"
        var message: String = "Update Successfully"
        if(nextJobStepStatus == Constants.JOB_STEP_INPROGRESS){
            title = StringMessage.jobStepStartedTitle
            message = StringMessage.jobStepStartedMessage
        }else if(nextJobStepStatus == Constants.JOB_STEP_FAILED){
            title = StringMessage.jobFailedTitle
            message = StringMessage.jobFailedMessage
        }else if(nextJobStepStatus == Constants.JOB_STEP_COMPLETED){
            if(self.isLastJobStep == true){
                title = StringMessage.jobLastStepCompletedTitle
                message = StringMessage.jobLastStepCompletedMessage
            }else{
                title = StringMessage.jobStepCompletedTitle
                message = StringMessage.jobStepCompletedMessage
            }
        }
        
        alertView.showSuccess(title, subTitle: message)
    }

    /* ======= CHANGE JOB STEP STATUS TO NEXT STATUS ========= */
    func changeJobStepStatusToNextStatus(nextJobStepStatus: Int?, orderAttempt: OrderAttempt?){
        
    
        if(self.orderID == 0 || self.orderID == nil ){
            //cannot update , order id zero/nil so need to check again
            print("orderID \(self.orderID)")
        }else{
            
            SwiftSpinner.show("Update Job Step Status...")
            
            let batchOrder: BatchOrder = BatchOrder()
            
            let para:NSMutableDictionary = NSMutableDictionary()
            let prodArray:NSMutableArray = NSMutableArray()
            
            // set order id in array
            batchOrder.orderIds.append(String(describing: self.orderID!))
            para.setValue(batchOrder.orderIds, forKey: "id")
            para.setValue("", forKey: "order_status_id")
            
            if(nextJobStepStatus == Constants.JOB_STEP_COMPLETED || nextJobStepStatus == Constants.JOB_STEP_FAILED){
                let dateString = self.getFormatDateAMorPM()
                print("current date \(dateString)")
                para.setValue(dateString, forKey: "drop_off_time_end")
            }else if(nextJobStepStatus == Constants.JOB_STEP_INPROGRESS){
                let dateString = self.getFormatDateAMorPM()
                print("current date \(dateString)")
                para.setValue(dateString, forKey: "drop_off_time")
            }
        
            self.jobStepDetail.job_step_status_id = nextJobStepStatus
            self.jobStepDetail.latitude = self.locationManager.location?.coordinate.latitude
            self.jobStepDetail.longitude = self.locationManager.location?.coordinate.longitude
            
            if orderAttempt != nil{
                
                let paraObjectStep:NSMutableDictionary = NSMutableDictionary()
                let prodArrayStep:NSMutableArray = NSMutableArray()
                
                paraObjectStep.setValue(self.jobStepDetail.id, forKey: "job_step_id")
                
                paraObjectStep.setValue(nextJobStepStatus, forKey: "job_step_status_id")
                prodArray.add(orderAttempt?.toJson())
                paraObjectStep.setObject(prodArray, forKey: "step_attempts" as NSCopying)
                prodArrayStep.add(paraObjectStep)
                para.setObject(prodArrayStep, forKey: "job_steps" as NSCopying)

            }
            else{
                prodArray.add(self.jobStepDetail.toJsonUpdateJobStepStatus())
                para.setObject(prodArray, forKey: "job_steps" as NSCopying)

            }
                
          
  
            
            let dataPara:NSMutableDictionary = NSMutableDictionary()
            let dataArray:NSMutableArray = NSMutableArray()
            
            dataArray.add(para)
            dataPara.setValue(dataArray, forKey: "data")
           
            /* ====== UPDATE JOB STEP ======= */
            AlamofireRequest.batchUpdate(parameters: dataPara as NSDictionary) { responseObject, statusCode, error in
                
                SwiftSpinner.hide()
                
                print("statusCode \(statusCode)")
                
                if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                    if(responseObject == nil)
                    {
                        
                    }else{
                        
                        let swiftyJsonVar = JSON(responseObject)
                        if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                            if(swiftyJsonVar["status"].bool == true){
                                self.completeJobView.isUserInteractionEnabled = true
                                self.failedJobView.isUserInteractionEnabled = true
                                //self.getJobStepDetails()
                                
                                DispatchQueue.main.async {
                                    self.showUIAlertDialog(nextJobStepStatus: nextJobStepStatus)
                                }
                                
                                print("success getJobStepDetails")
                            }
                        }
                    }
                    
                }else{
                    //error code
                    SwiftSpinner.hide()
                    if(responseObject == nil)
                    {
                        self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                    }else{
                        let swiftyJsonVar = JSON(responseObject)
                        let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                       self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                    }
                }
            }
            /* ====== UPDATE JOB STEP ======= */
        }
    
        
    }
    
    /* ========= FUNC SCAN ========= */
    
    func scanProcess(){
        
        let initialOrderTrackingList: NSMutableArray = []
        if(self.itemTrackingNumber == nil){
            //for testing purpose
            self.itemTrackingNumber = "1234567890"
        }
        let orderScanList: NSMutableArray = []
        orderScanList.removeAllObjects()
        orderScanList.add(self.itemTrackingNumber)
        
        var style = LBXScanViewStyle()
        style.centerUpOffset = 44
        style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer
        style.photoframeLineW = 6
        style.photoframeAngleW = 24
        style.photoframeAngleH = 24
        style.anmiationStyle = LBXScanViewAnimationStyle_LineMove
        style.animationImage = UIImage(named: "CodeScan.bundle/qrcode_scan_light_green")
        let vc = SubLBXScanViewController()
        vc.style = style
        vc.isQQSimulator = true
        vc.isVideoZoom = true
        vc.delegate = self
        vc.isInProgress = true
        vc.isTransfer = false
        vc.orderScanList = orderScanList
        vc.initialOrderScanList = initialOrderTrackingList
        
        self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
   
    /* Get detail after scan */
    func scanDelegateMethod(_ unscannedItems: NSMutableArray!, isTransfer isDoneScanned: Bool) {
        print("unscannedItems \(unscannedItems)")
        print("isDoneScanned \(isDoneScanned)")
        print("scanDelegateMethod")
        
        /*After scan, will get flag either scan is success or failed. If success then need to check either this job step need to sign or not. If failed, job steps cannot proceed to complete and need to scan again*/
        
        if(isDoneScanned && unscannedItems.count == 0){
            /*Check is have internet then check if user online*/
            let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
            if reachInternetConnection.isConnectedToNetwork() == true {
                print("clickCompleteJobView Internet connection OK")
                if(isOnline){
                    //scan first
                    if(self.jobStepDetail.is_signature_required)!{
                        // need to sign before completed this job
                        self.sign()
                    }else{
                        //directly can proceed without sign
                        self.changeJobStepStatusToNextStatus(nextJobStepStatus: Constants.JOB_STEP_COMPLETED,orderAttempt: nil)
                    }
                }else{
                    // tak online
                    var alert = UIAlertView(title: "Cannot Update", message: "Make sure you availibality is online before update job", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
            } else {
                print("clickCompleteJobView Internet connection FAILED")
                var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        }else{
            var alert = UIAlertView(title: "Cannot Update Job", message: "You need scan barcode for this step if want to proceed.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
        
        
    }
    /* ========= FUNC SCAN ========= */
    
    
    /* ======== FUNC SIGNATURE ======= */
    func sign(){
        let orderAttempt = OrderAttempt()
        print("in 33")
        let signatureVC = EPSignatureViewController(signatureDelegate: self, showsDate: true, showsSaveSignatureOption: true)
        signatureVC.subtitleText = "I agree to the terms and conditions"
        signatureVC.title = "Signature"
        signatureVC.orderAttempt = orderAttempt
        signatureVC.mDelegate = self
        signatureVC.jobStepDetail = self.jobStepDetail
        signatureVC.orderID = self.orderID
        signatureVC.orderStatusID = self.orderStatusID
        signatureVC.isLastJobStep = self.isLastJobStep
        
        
        if(self.jobStepDetail.job_step_pic_contact != nil)
        {
           // signatureVC.recipientName = jobdetail.dropOffContactName!
        }
        
        let nav = UINavigationController(rootViewController: signatureVC)
        present(nav, animated: true, completion: nil)
        
    }
    
    // Signature Process
    func sendArrayToPreviousVC(myObject:OrderAttempt, signature: UIImage, imagesArray: [UIImage],imagesDescription: [String]) {
   
       // SwiftSpinner.show("Submitting")
        self.completeJobView.isUserInteractionEnabled = true
        self.failedJobView.isUserInteractionEnabled = true

        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Internet connection OK")
            getJobStepDetails()
        } else {
            print("Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
    }
    
    func epSignature(_: EPSignatureViewController, didCancel error : NSError){
        print("User Canceled")
    }
    
    func epSignature(_: EPSignatureViewController, didSign signatureImage : UIImage, boundingRect: CGRect){
        print(signatureImage)
    }
    /* ======== FUNC SIGNATURE ======= */
    
    /* ======= CLICK / ACTION BUTTON ========== */
    
    /* Open List Order Attempt */
    func clickOrderAttemptView(_ gr: UITapGestureRecognizer) {
        if(self.jobStepDetail.orderAttemptList.count > 0){
            
            if reachInternetConnection.isConnectedToNetwork() == true {
                print("clickOrderAttemptView Internet connection OK")
                let storyboard = UIStoryboard(name: "ListOrderAttempt", bundle: nil)
                let levelController = storyboard.instantiateViewController(withIdentifier: "ListOrderAttempt") as! ListOrderAttemptViewController
                
                levelController.orderID = self.orderID
                levelController.orderStatusID = self.orderStatusID
                levelController.jobStepID = self.jobStepDetail.id
                print("jobStepDetailID \(self.jobStepDetail.id)")
                self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                
                self.navigationController?.pushViewController(levelController, animated: true)
            } else {
                print("clickOrderAttemptView Internet connection FAILED")
                var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
            
            
        }
        
    }
    
     /* Click Completed Job View */
    func clickCompleteJobView(_ gr: UITapGestureRecognizer) {
        /*Check is have internet then check if user online*/
        let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("clickCompleteJobView Internet connection OK")
            if(isOnline){
                self.completeJobView.isUserInteractionEnabled = false
                /* check if job step status id is in progress so it will be complete*/
                if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_INPROGRESS){
                    /*Need to check either this step need to scan before proceed*/
                    if(self.jobStepDetail.is_scan_required)!{
                        print("in 11")
                        self.scanProcess()
                        
                    }else{
                        print("in 22")
                        if(self.jobStepDetail.is_signature_required)!{
                            // need to sign before completed this job
                            self.sign()
                        }else{
                            //directly can proceed without sign
                            self.changeJobStepStatusToNextStatus(nextJobStepStatus: Constants.JOB_STEP_COMPLETED, orderAttempt: nil )
                        }
                    }
                    /*Need to check either this step need to signature before proceed*/
                }else if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_PENDING){
                    /*if job step status is pending, then will be update to in progress*/
                    self.changeJobStepStatusToNextStatus(nextJobStepStatus: Constants.JOB_STEP_INPROGRESS, orderAttempt: nil)
                }
            }else{
                // tak online
                var alert = UIAlertView(title: "Cannot Update", message: "Make sure you availibality is online before update job", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        } else {
            print("clickCompleteJobView Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
        
    }
    
    /* Click Failed Job View */
    func clickFailedJobView(_ gr: UITapGestureRecognizer) {
        
        let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("clickFailedJobView Internet connection OK")
            if(isOnline){
                
                let alert = UIAlertController(title: "Reason", message: "Enter a reason to fail this job", preferredStyle: .alert)
                
                //2. Add the text field. You can configure it however you need.
                alert.addTextField { (textField) in
                    
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
                    alert?.dismiss(animated: false, completion: nil)
                    
                }))
                
                alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
                    let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                    print("Text field: \(textField?.text)")
                    if(textField?.text == ""){
                        textField?.text = " - "
                    }
                    
                    var orderAttempt: OrderAttempt = OrderAttempt()
                    orderAttempt.reason = textField?.text

                    
                    
                    self.failedJobView.isUserInteractionEnabled = false
                    self.changeJobStepStatusToNextStatus(nextJobStepStatus: Constants.JOB_STEP_FAILED, orderAttempt: orderAttempt)
                 
                }))
                
             
                
                // 4. Present the alert.
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
                
                
             
            }else{
                // tak online
                var alert = UIAlertView(title: "Cannot Update", message: "Make sure you availibality is online before update job", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        } else {
            print("clickFailedJobView Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    }
    
    /* Click callRecipient View */
    func clickCallRecipientView(_ gr: UITapGestureRecognizer) {
        var dropOffcall: String?
        
        print("call in out")
        
        if(self.jobStepDetail.job_step_pic_contact == nil || self.jobStepDetail.job_step_pic_contact == ""){
            dropOffcall = ""
        }else{
            dropOffcall = self.jobStepDetail.job_step_pic_contact
        }
        
        if(dropOffcall == nil || dropOffcall == ""){
            let alert = UIAlertController(title: "Cannot call recipient", message: "Invalid phone number.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: dropOffcall, message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            
            alert.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: { action in
                print("call \(dropOffcall)")
                
                if let url = URL(string: "tel://\(dropOffcall!)") {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                
                //   UIApplication.shared.openURL(NSURL(string: "tel://\(self.dropOffcall)") as! URL)
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    /* Click navigateLocation View */
    func clickNavigateLocationView(_ gr: UITapGestureRecognizer) {
        var location: String?
        
        if(self.jobStepDetail.location == nil || self.jobStepDetail.location == ""){
            location = ""
        }else{
            location = self.jobStepDetail.location
        }
        
        
        if(location == nil || location == ""){
            let alert = UIAlertController(title: "Error", message: "Invalid address", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let baseUrl: String = "http://maps.apple.com/?q="
            let urlwithPercentEscapes = location?.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            
            
            let scheme = baseUrl + urlwithPercentEscapes!
            if let url = NSURL(string: scheme) {
                
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url as URL, options: [:],
                                              completionHandler: {
                                                (success) in
                                                print("Open \(scheme): \(success)")
                    })
                } else {
                    let success = UIApplication.shared.openURL(url as URL)
                    print("Open \(scheme): \(success)")
                }
            }
        }
    }
    /* ======= CLICK / ACTION BUTTON ========== */
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}



extension OrderDetailsOneStepViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}
