//
//  ListItemOrderTableViewCell.swift
//  SAAS
//
//  Created by Coolasia on 7/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit

class ListItemOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var itemName: UILabel!
    
    @IBOutlet weak var totalUnit: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
