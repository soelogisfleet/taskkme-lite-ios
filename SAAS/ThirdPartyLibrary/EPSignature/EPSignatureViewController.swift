//
//  EPSignatureViewController.swift
//  Pods
//
//  Created by Prabaharan Elangovan on 13/01/16.
//
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import SwiftSpinner
import Toast_Swift
import SCLAlertView
import MediaBrowser

// MARK: - EPSignatureDelegate
@objc public protocol EPSignatureDelegate {
    @objc optional    func epSignature(_: EPSignatureViewController, didCancel error : NSError)
    @objc optional    func epSignature(_: EPSignatureViewController, didSign signatureImage : UIImage, boundingRect: CGRect)
}

protocol MyProtocol: class
{
    func sendArrayToPreviousVC(myObject:OrderAttempt, signature: UIImage,imagesArray:[UIImage],imagesDescription:[String])
}

open class EPSignatureViewController: UIViewController,KUIActionSheetItemViewProtocol,UITextFieldDelegate,EPSignatureViewDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var recipientTextField: UITextField!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var switchSaveSignature: UISwitch!
    @IBOutlet weak var lblSignatureSubtitle: UILabel!
    @IBOutlet weak var lblDefaultSignature: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewMargin: UIView!
    @IBOutlet weak var lblX: UILabel!
    @IBOutlet weak var signatureView: EPSignatureView!
    
    
    @IBOutlet weak var noteTextField: UITextField!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var removeSignatureButton: UIButton!
    
    @IBOutlet weak var logoWatermarkSignature: UIImageView!
    @IBOutlet weak var overallScrollView: UIScrollView!
    
    // MARK: - Public Vars
    
    open var showsDate: Bool = true
    open var showsSaveSignatureOption: Bool = true
    open weak var signatureDelegate: EPSignatureDelegate?
    open var subtitleText = "Sign Here"
    open var tintColor = UIColor.defaultTintColor()
    
    var orderAttempt: OrderAttempt = OrderAttempt()
    var recipientName: String = ""
    //var orderAttempt: OrderAttempt = OrderAttempt()
    
    // MARK: - Life cycle methods
    var photo: MBPhotoPicker?
    weak var mDelegate:MyProtocol?
    var noteArray : [String] = []
    var recipientsArray : [String] = []
    var imagesArray : [UIImage] = []
    var imagesString64Base: [String] = []
    var imagesDescription: [String] = []
    var signatureImage: UIImage?
    
    var urlImageNormalArray: [String] = []
    var jobStepDetail = JobSteps()
    var orderID : Int?
    var orderStatusID : Int?
    var signatureDescription: String?
    var reachInternetConnection = Reachability()
    
    /// For Image Gallery
    var selections = [Bool]()
    var mediaArray = [Media]()
    var thumbs = [Media]()
    
    var displayActionButton = true
    var displaySelectionButtons = true
    var displayMediaNavigationArrows = true
    var enableGrid = true
    var startOnGrid = true
    var autoPlayOnAppear = false
    var browser = MediaBrowser()
    /// For Image Gallery
    
    var isLastJobStep:  Bool?
    
//    // we set a variable to hold the contentOffSet before scroll view scrolls
//    var lastContentOffset: CGFloat = 0
//
//    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
//    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        self.lastContentOffset = scrollView.contentOffset.y
//    }
//
//    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
//    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if (self.lastContentOffset < scrollView.contentOffset.y) {
//            // moved to top
//            print("movvvveeeeeee upppppppp")
//            self.heightConstraintSignatureView.constant = 0
//            self.signatureView.isHidden = true
//        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
//            // moved to bottom
//            print("movvvveeeeeee downnnnnnnnn")
//            self.heightConstraintSignatureView.constant = 263
//            self.signatureView.isHidden = false
//        } else {
//            // didn't move
//        }
//    }
    
    func buildUIMediaBrowser(){
        mediaArray = self.webPhotos()
        thumbs = self.webPhotos()
        
        browser = MediaBrowser(delegate: self)
        browser.displayActionButton = displayActionButton
        browser.displayMediaNavigationArrows = displayMediaNavigationArrows
        browser.displaySelectionButtons = displaySelectionButtons
        browser.alwaysShowControls = displaySelectionButtons
        browser.zoomPhotosToFill = true
        browser.enableGrid = enableGrid
        browser.startOnGrid = startOnGrid
        browser.enableSwipeToDismiss = true
        browser.autoPlayOnAppear = autoPlayOnAppear
        browser.cachingImageCount = 2
        browser.setCurrentIndex(at: 2)
        
        
        if displaySelectionButtons {
            selections.removeAll()
            
            for _ in 0..<mediaArray.count {
                selections.append(false)
            }
        }
        
        /// For Image Gallery
    }
    
    //// media browser get image browser
    func webPhotos() -> [Media] {
        var photos = [Media]()
        
        for (index, url) in self.urlImageNormalArray.enumerated() {
            print("Item get in \(index): \(url)")
            
            let photo = webMediaPhoto(url: url, caption: imagesDescription[index])
            photos.append(photo)
            
            
        }
        
        return photos
        
    }
    
    func webMediaPhoto(url: String, caption: String?) -> Media {
        guard let validUrl = URL(string: url) else {
            fatalError("Image is nil")
        }
        
        var photo = Media()
        if let _caption = caption {
            photo = Media(url: validUrl, caption: _caption)
        } else {
            photo = Media(url: validUrl)
        }
        return photo
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func recipientButtonClicked(_ sender: Any) {
        
        var httpHeader = [
            "Authorization": "bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        
        Alamofire.request(Constants.web_api+Constants.api_get_recipient_types, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: httpHeader ).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil)
            {
                
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                
                
                
                let alert = UIAlertController(title: "Recipients", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                
                
                let closure = { (action: UIAlertAction!) -> Void in
                    let index = alert.actions.index(of: action)
                    if index != nil {
                        NSLog("Index: \(index!)")
                        if(index==0)
                        {
                            self.recipientTextField.text = self.recipientsArray[index!]
                            
                            self.orderAttempt.receivedBy = self.recipientName
                        }
                        else{
                            self.noteTextField.text = self.recipientsArray[index!]
                            
                            self.orderAttempt.note = self.recipientsArray[index!]
                        }
                    }
                }
                let recipientName = "Recipient: " + self.recipientName
                
                self.recipientsArray.append(recipientName)
                alert.addAction(UIAlertAction(title: recipientName, style: .default, handler: closure))
                
                
                for subJson in swiftyJsonVar["result"].array!{
                    print(subJson["name"])
                    self.recipientsArray.append(subJson["name"].rawString()!)
                    alert.addAction(UIAlertAction(title: subJson["name"].rawString()!, style: .default, handler: closure))
                    
                    
                    
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
                alert.addAction(cancelAction)
                
                self.present(alert, animated: false, completion: nil)
                
                
            }
            else{
                print("NULLL")
                // self.image(forEmptyDataSet: <#T##UIScrollView#>)
            }
        }
        
        
        
    }
    
    
    @IBAction func NoteButtonClicked(_ sender: Any) {
        
        var httpHeader = [
            "Authorization": "bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        Alamofire.request(Constants.web_api+Constants.api_get_notes, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: httpHeader ).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil)
            {
                
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                
                
                
                let alert = UIAlertController(title: "Notes", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                
                
                let closure = { (action: UIAlertAction!) -> Void in
                    let index = alert.actions.index(of: action)
                    if index != nil {
                        NSLog("Index: \(index!)")
                        self.noteTextField.text = self.noteArray[index!]
                        self.orderAttempt.note = self.noteArray[index!]
                    }
                }
                
                
                
                
                for subJson in swiftyJsonVar["result"].array!{
                    print(subJson["note"])
                    self.noteArray.append(subJson["note"].rawString()!)
                    alert.addAction(UIAlertAction(title: subJson["note"].rawString()!, style: .default, handler: closure))
                    
                    
                    
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
                alert.addAction(cancelAction)
                
                self.present(alert, animated: false, completion: nil)
                
                
            }
            else{
                print("NULLL")
                // self.image(forEmptyDataSet: <#T##UIScrollView#>)
            }
        }
        
        
    }
    
    
    
    
    @IBAction func AddPhotoClicked(_ sender: Any) {
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("AddPhotoClicked Internet connection OK")
            
            let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
            if(isOnline){
                photo = MBPhotoPicker()
                photo?.disableEntitlements = false // If you don't want use iCloud entitlement just set this value True
                photo?.alertTitle = nil
                photo?.alertMessage = nil
                photo?.disableEntitlements = true
                photo?.resizeImage = CGSize(width: 250, height: 150)
                photo?.allowDestructive = false
                photo?.allowEditing = false
                photo?.cameraDevice = .rear
                photo?.cameraFlashMode = .auto
                photo?.photoCompletionHandler = { (image: UIImage?) -> Void in
                    
                    
                    self.imagesArray.append(image!)
                    let imageData:NSData = UIImagePNGRepresentation(image!)! as NSData
                    let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                    
                    let string1 = "data:image/png;base64,\(imageStr)"
                    self.imagesString64Base.removeAll()
                    self.imagesString64Base.append(string1)
                    
                    let imageStringParams : Dictionary<String,AnyObject> = ["folder_name" : "TestingUploadBase64" as AnyObject,
                                                                            "base64_images" : self.imagesString64Base as AnyObject]
                    
                    /* ======= UPLOAD BASE64 IMAGE TO GET URL IMAGE ========= */
                    
                    self.addPhotoButton.isEnabled = false
                    
                    self.view.makeToast("Photo will upload in background")
                    
                    /* ====== UPLOAD BASE64 IMAGE ======= */
                    AlamofireRequest.uploadImage(parameters: imageStringParams as NSDictionary) { responseObject, statusCode, error in
                        
                        if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                            if(responseObject == nil)
                            {
                                
                            }else{
                                let swiftyJsonVar = JSON(responseObject)
                                if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                                    if(swiftyJsonVar["status"].bool == true){
                                        var resultImageNormalList = swiftyJsonVar["result"].array
                                        for subJson in resultImageNormalList! {
                                            let resultImageNormal = subJson["image_normal"].string
                                            self.urlImageNormalArray.append(resultImageNormal!)
                                            print("resultImageNormal \(resultImageNormal)")
                                            
                                            self.view.makeToast("Photo successfully updated.")
                                        }
                                    }
                                }
                            }
                            
                        }else{
                            self.addPhotoButton.isEnabled = true
                            //error code
                        }
                    }
                    /* ====== UPLOAD BASE64 IMAGE ======= */
                    
                    /* ======= UPLOAD BASE64 IMAGE TO GET URL IMAGE ========= */
                    
                    self.openDescriptionImage()
                    /////////
                    
                    
                    
                    //  let orderAttemptImage = OrderAttemptImage(mUrl: "",mNote: "",mBase64:string1)
                    
                    
                    let imageWidth:CGFloat = 100
                    let imageHeight:CGFloat = 100
                    var xPosition:CGFloat = 10
                    var scrollViewContentSize:CGFloat=0;
                    
                    for index in 0 ..< self.imagesArray.count
                    {
                        let tempImage = self.imagesArray[index]
                        let myImageView:UIImageView = UIImageView()
                        myImageView.image = tempImage
                        
                        //myImageView.image = myImage
                        myImageView.contentMode = UIViewContentMode.scaleAspectFit
                        myImageView.frame.size.width = imageWidth
                        myImageView.frame.size.height = imageHeight
                        myImageView.frame.origin.x = xPosition
                        myImageView.frame.origin.y = 5
                        
                        self.imageScrollView.addSubview(myImageView)
                        let spacer:CGFloat = 20
                        xPosition+=imageWidth + spacer
                        scrollViewContentSize+=imageWidth + spacer
                        self.imageScrollView.contentSize = CGSize(width: scrollViewContentSize, height: imageHeight)
                        
                        
                    }
                    
                    
                }
                photo?.cancelCompletionHandler = {
                    print("Cancel Pressed")
                }
                photo?.errorCompletionHandler = { (error: MBPhotoPicker.ErrorPhotoPicker!) -> Void in
                    //print("Error: \(error.rawValue)")
                }
                photo?.present(self)
            }else{
                var alert = UIAlertView(title: "Cannot Update", message: "Make sure you availibality is online before update job", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
            
            
        } else {
            print("AddPhotoClicked Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
        
        
    }
    func openDescriptionImage(){
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Photo Description", message: "Describe what this photo is all about", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            
        }
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
        
            self.imagesDescription.append("No description")
            alert?.dismiss(animated: false, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \(textField?.text)")
            if(textField?.text == ""){
                textField?.text = "No description"
            }
            self.imagesDescription.append((textField?.text)!)
            
        }))
        
        // 4. Present the alert.
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
            self.addPhotoButton.isEnabled = true
           // self.buildUIMediaBrowser()
        }
    }
    
    /* ====== FUNC SHOW ALERT DIALOG SUCCESS UPDATE ======= */
    func showUIAlertDialog(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") {
            print("YES button tapped")
            //self.navigationController!.popViewController(animated:true)
        
            self.mDelegate?.sendArrayToPreviousVC(myObject: self.orderAttempt, signature: self.signatureImage!,imagesArray: self.imagesArray,imagesDescription: self.imagesDescription)
            self.dismiss(animated: true, completion: nil)
        }
        
        var title: String = "Success"
        var message: String = "Update Successfully"
        
        if(self.isLastJobStep == true){
            title = StringMessage.jobLastStepCompletedTitle
            message = StringMessage.jobLastStepCompletedMessage
        }else{
            title = StringMessage.jobStepCompletedTitle
            message = StringMessage.jobStepCompletedMessage
        }
        
        alertView.showSuccess(title, subTitle: message)
    }
    
    func getUrlFromBase64Image(myObject:OrderAttempt, signature: UIImage){
        
        
        //convert signature to base 64
        let imageData:NSData = UIImagePNGRepresentation(signature)! as NSData
        let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let string1 = "data:image/png;base64,\(imageStr)"
        
        self.imagesString64Base.removeAll()
        self.imagesString64Base.append(string1)
        
        let imageStringParams : Dictionary<String,AnyObject> = ["folder_name" : "TestingUploadBase64" as AnyObject,
                                                                "base64_images" : self.imagesString64Base as AnyObject]
        
        /* ======= UPLOAD BASE64 IMAGE TO GET URL IMAGE ========= */
        
        
        /* ====== UPLOAD BASE64 IMAGE ======= */
        AlamofireRequest.uploadImage(parameters: imageStringParams as NSDictionary) { responseObject, statusCode, error in
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                        if(swiftyJsonVar["status"].bool == true){
                            var resultImageNormalList = swiftyJsonVar["result"].array
                            for subJson in resultImageNormalList! {
                                let resultImageNormal = subJson["image_normal"].string
                                self.urlImageNormalArray.append(resultImageNormal!)
                                self.signatureDescription = "Recipient’s Signature"
                                self.imagesDescription.append(self.signatureDescription!)
                                print("resultImageNormal \(resultImageNormal)")
                                self.sendConfirmationJobStep(myObject: myObject)
                            }
                        }
                    }
                }
                
            }else{
                //error code
                if(responseObject == nil || statusCode == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== UPLOAD BASE64 IMAGE ======= */
        
        /* ======= UPLOAD BASE64 IMAGE TO GET URL IMAGE ========= */
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        overallScrollView.delaysContentTouches = false;
        overallScrollView.canCancelContentTouches = false;
        imagesArray.removeAll()
        imagesDescription.removeAll()
        urlImageNormalArray.removeAll()
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(EPSignatureViewController.onTouchCancelButton))
       // cancelButton.tintColor = tintColor
        self.navigationItem.leftBarButtonItem = cancelButton
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(EPSignatureViewController.onTouchDoneButton))
       // doneButton.tintColor = tintColor
        let clearButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(EPSignatureViewController.onTouchClearButton))
        //clearButton.tintColor = tintColor
        
     //   self.navigationItem.rightBarButtonItems = [doneButton, clearButton]
        self.navigationItem.rightBarButtonItems = [doneButton]
        
       // self.signatureView.layer.borderWidth = 0
       // self.signatureView.layer.borderColor =  Constants.hexStringToUIColor(hex:"FF8000").cgColor
        
        self.noteTextField.delegate=self
        self.recipientTextField.delegate=self
        
      //  self.scrollView.delegate=self
        
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        self.navigationController?.navigationBar.topItem?.title="Complete this job"
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        
        let removeSignature = UITapGestureRecognizer(target: self, action: #selector(self.removeSignatureView(_:)))
        self.removeSignatureButton.addGestureRecognizer(removeSignature)
        self.removeSignatureButton.isUserInteractionEnabled = true
        
        // lblSignatureSubtitle.text = subtitleText
        //switchSaveSignature.setOn(false, animated: true)
        
        
        let viewPhotoTapped = UITapGestureRecognizer(target: self, action: #selector(EPSignatureViewController.viewPhotoTap(_:)))
        self.imageScrollView.addGestureRecognizer(viewPhotoTapped)
        
        UserDefaults.standard.setValue(false, forKey: "deletePhotoSelected")
        
        
    }
    
    func viewPhotoTap(_ gr: UITapGestureRecognizer) {
        
        if(self.urlImageNormalArray.count != 0){
            UserDefaults.standard.setValue(false, forKey: "deletePhotoSelected")
            self.buildUIMediaBrowser()
            self.navigationItem.title = nil
            self.navigationController?.pushViewController(browser, animated: true)
        }
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        removeSignatureButton.isHidden = true
        
        self.signatureView.delegate = self
        
        print("array select at index \(selections)")
        
        if let deletePhotoSelected = UserDefaults.standard.object(forKey: "deletePhotoSelected"){
            
            let deletePhotoSelectedBool = UserDefaults.standard.value(forKey: "deletePhotoSelected") as! Bool
            
            if(deletePhotoSelectedBool){
                let indexSelected = selections.enumerated().filter { $1 }.map { $0.0 }
                print(indexSelected)
                
                print("urlImageNormalArray \(urlImageNormalArray)")
                print("imagesDescription \(imagesDescription)")
                print("imagesArray \(imagesArray)")
                
                if(indexSelected.count > 0){
                    
                    var shiftRemove: Int = 0
                    
                    for removeImageAtindex in indexSelected{
                        
                        print("removeImageAtindex \(removeImageAtindex)")
                        self.urlImageNormalArray.remove(at: removeImageAtindex - shiftRemove)
                        self.imagesDescription.remove(at: removeImageAtindex - shiftRemove)
                        self.imagesArray.remove(at: removeImageAtindex - shiftRemove)
                        
                        shiftRemove = shiftRemove + 1
                        // self.imagesArray.remove(at: removeImageAtindex)
                        
                        //  print("urlImageNormalArray \(urlImageNormalArray)")
                        //  print("imagesDescription \(imagesDescription)")
                        
                    }
                    
                    self.imageScrollView.subviews.map { $0.removeFromSuperview() }
                    
                    let imageWidth:CGFloat = 100
                    let imageHeight:CGFloat = 100
                    var xPosition:CGFloat = 10
                    var scrollViewContentSize:CGFloat=0;
                    
                    for index in 0 ..< self.imagesArray.count
                    {
                        let tempImage = self.imagesArray[index]
                        let myImageView:UIImageView = UIImageView()
                        myImageView.image = tempImage
                        
                        //myImageView.image = myImage
                        myImageView.contentMode = UIViewContentMode.scaleAspectFit
                        myImageView.frame.size.width = imageWidth
                        myImageView.frame.size.height = imageHeight
                        myImageView.frame.origin.x = xPosition
                        myImageView.frame.origin.y = 5
                        
                        self.imageScrollView.addSubview(myImageView)
                        let spacer:CGFloat = 20
                        xPosition+=imageWidth + spacer
                        scrollViewContentSize+=imageWidth + spacer
                        self.imageScrollView.contentSize = CGSize(width: scrollViewContentSize, height: imageHeight)
                        
                        
                    }
                }
                
                UserDefaults.standard.setValue(false, forKey: "deletePhotoSelected")
            }
            
        }
        
        
        
        self.buildUIMediaBrowser()
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title="Complete this job"

    }
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Initializers
    
    public convenience init(signatureDelegate: EPSignatureDelegate) {
        self.init(signatureDelegate: signatureDelegate, showsDate: true, showsSaveSignatureOption: true)
    }
    
    public convenience init(signatureDelegate: EPSignatureDelegate, showsDate: Bool) {
        self.init(signatureDelegate: signatureDelegate, showsDate: showsDate, showsSaveSignatureOption: true)
    }
    
    public init(signatureDelegate: EPSignatureDelegate, showsDate: Bool, showsSaveSignatureOption: Bool ) {
        self.showsDate = showsDate
        self.showsSaveSignatureOption = showsSaveSignatureOption
        self.signatureDelegate = signatureDelegate
        let bundle = Bundle(for: EPSignatureViewController.self)
        super.init(nibName: "EPSignatureViewController", bundle: bundle)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Button Actions
    
    func onTouchCancelButton() {
        signatureDelegate?.epSignature!(self, didCancel: NSError(domain: "EPSignatureDomain", code: 1, userInfo: [NSLocalizedDescriptionKey:"User not signed"]))
        dismiss(animated: true, completion: nil)
    }
    
    func onTouchDoneButton() {
        if (self.signatureView.getSignatureAsImage() == nil) {
            showAlert("Please include recipient's signature on Taskk.Me's logo.", andTitle: "Cannot complete job")
        } else {
            //            if switchSaveSignature.isOn {
            //                let docPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            //                let filePath = (docPath! as NSString).appendingPathComponent("sig.data")
            //                signatureView.saveSignature(filePath)
            //            }
            //                        signatureDelegate?.epSignature!(self, didSign: signature, boundingRect: signatureView.getSignatureBoundsInCanvas())
            if recipientTextField.text?.characters.count==0
            {
                showAlert("Please fill in recipient's name.", andTitle: "Cannot complete job")
                
            }
            else{
                
                self.removeSignatureButton.isHidden = true
                self.logoWatermarkSignature.isHidden = true
                signatureImage = self.signatureView.getSignatureAsImage()
                
                
                orderAttempt.receivedBy = recipientTextField.text
                orderAttempt.note = noteTextField.text
                
                print("orderImageAttempt \(orderAttempt.receivedBy)" )
       
                if reachInternetConnection.isConnectedToNetwork() == true {
                    print("onTouchDoneButton Internet connection OK")
                    let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
                    if(isOnline){
                        self.getUrlFromBase64Image(myObject: orderAttempt, signature: signatureImage!)
                    }else{
                        var alert = UIAlertView(title: "Cannot Update", message: "Make sure you availibality is online before update job.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                    }
                } else {
                    print("onTouchDoneButton Internet connection FAILED")
                    var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
                
            
                
            }
            
            
        }
    }
    
    /* ======= UPLOAD BASE64 IMAGE TO GET URL IMAGE ========= */
    func sendConfirmationJobStep(myObject:OrderAttempt){
        var locManager = CLLocationManager()
        var currentLocation: CLLocation!
        
        orderAttempt = myObject
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            orderAttempt.latitude = currentLocation.coordinate.latitude
            orderAttempt.longitude = currentLocation.coordinate.longitude
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
        }
        
        
        let batchOrder: BatchOrder = BatchOrder()
        
        let para:NSMutableDictionary = NSMutableDictionary()
        let prodArray:NSMutableArray = NSMutableArray()
        
        print("orderAttemptImageList22 \(orderAttempt.orderAttemptImageList.count)" )
        
        let dateString = Constants.getFormatDateAMorPM()
        print("current date \(dateString)")
        
        para.setValue(dateString, forKey: "drop_off_time_end")
        
       
        if(urlImageNormalArray.count > 0){
            for (index, imageString) in self.urlImageNormalArray.enumerated() {
                print("ItemImage \(index)")
                print("imageString \(imageString)")
                print("ItemDescription \(index): \(imagesDescription[index])")
                
                if(urlImageNormalArray.count == index + 1){
                    let orderAttemptImage = OrderAttemptImage(mUrl:imageString,mNote:imagesDescription[index],isSignature: true)
                    self.orderAttempt.orderAttemptImageList.append(orderAttemptImage)
                }else{
                    let orderAttemptImage = OrderAttemptImage(mUrl:imageString,mNote:imagesDescription[index],isSignature: false)
                    self.orderAttempt.orderAttemptImageList.append(orderAttemptImage)
                }
                
                
            }
        }
        
        print("orderAttemptImageList33 \(orderAttempt.orderAttemptImageList.count)" )
        
        
        batchOrder.orderIds.append(String(self.orderID!))
        batchOrder.orderStatus = String(describing: self.orderStatusID!)
        
        
        print(self.orderAttempt.orderAttemptImageList)
        
        para.setValue(batchOrder.orderIds, forKey: "id")
        para.setValue(batchOrder.orderStatus, forKey: "order_status_id")
        
        let paraObjectStep:NSMutableDictionary = NSMutableDictionary()
        let prodArrayStep:NSMutableArray = NSMutableArray()
        
        paraObjectStep.setValue(self.jobStepDetail.id, forKey: "job_step_id")
        
        var jobStepStatusID: Int?
        if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_PENDING){
            jobStepStatusID = Constants.JOB_STEP_INPROGRESS
        }else if(self.jobStepDetail.job_step_status_id == Constants.JOB_STEP_INPROGRESS){
            jobStepStatusID = Constants.JOB_STEP_COMPLETED
        }
        
        paraObjectStep.setValue(jobStepStatusID, forKey: "job_step_status_id")
        
        prodArray.add(self.orderAttempt.toJson())
        paraObjectStep.setObject(prodArray, forKey: "step_attempts" as NSCopying)
        
        prodArrayStep.add(paraObjectStep)
        
        para.setObject(prodArrayStep, forKey: "job_steps" as NSCopying)
        
        let dataPara:NSMutableDictionary = NSMutableDictionary()
        let dataArray:NSMutableArray = NSMutableArray()
        
        dataArray.add(para)
        dataPara.setValue(dataArray, forKey: "data")
        
        SwiftSpinner.show("Submitting")
        
        AlamofireRequest.batchUpdate(parameters: dataPara as NSDictionary) { responseObject, statusCode, error in
            
             SwiftSpinner.hide()
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                        if(swiftyJsonVar["status"].bool == true){
                            self.showUIAlertDialog()
                        }
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil || statusCode == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        
        
    }
    /* ======= UPLOAD BASE64 IMAGE TO GET URL IMAGE ========= */
    
    
    
    func onTouchActionButton(_ barButton: UIBarButtonItem) {
        let action = UIAlertController(title: "Action", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        action.view.tintColor = tintColor
        
        action.addAction(UIAlertAction(title: "Load default signature", style: UIAlertActionStyle.default, handler: { action in
            let docPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let filePath = (docPath! as NSString).appendingPathComponent("sig.data")
            self.signatureView.loadSignature(filePath)
        }))
        
        action.addAction(UIAlertAction(title: "Delete default signature", style: UIAlertActionStyle.destructive, handler: { action in
            self.signatureView.removeSignature()
        }))
        
        action.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        if let popOver = action.popoverPresentationController {
            popOver.barButtonItem = barButton
        }
        present(action, animated: true, completion: nil)
    }
    
    func onTouchClearButton() {
        signatureView.clear()
        self.removeSignatureButton.isHidden = true
    }
    
    
    
    func removeSignatureView(_ gr: UITapGestureRecognizer) {
        self.signatureView.clear()
        self.removeSignatureButton.isHidden = true
    }
    
    //MARK: - EPSignatureViewDelegate methods
    public func startedDrawing() {
        print("drawingggggg")
        self.removeSignatureButton.isHidden = false
        
    }
    
    
}

//MARK: MediaBrowserDelegate
extension EPSignatureViewController: MediaBrowserDelegate {
    public func thumbnail(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < thumbs.count {
            return thumbs[index]
        }
        
        
        return self.webMediaPhoto(url: "", caption: nil)
    }
    
    
    
    public func media(for mediaBrowser: MediaBrowser, at index: Int) -> Media {
        if index < mediaArray.count {
            return mediaArray[index]
        }
        
        return self.webMediaPhoto(url: "", caption: nil)
    }
    
    public func numberOfMedia(in mediaBrowser: MediaBrowser) -> Int {
        return mediaArray.count
    }
    
    public func isMediaSelected(at index: Int, in mediaBrowser: MediaBrowser) -> Bool {
        
        print("Select at index isMediaSelected \(index)")
        return selections[index]
        
    }
    
    public func actionButtonPressed(at photoIndex: Int, in mediaBrowser: MediaBrowser) {
        //put ur code here to edit the description. have a pop up
        print("edit klickkkk")
        //self.openDescriptionImage(photoIndex: photoIndex)
        
        let alert = UIAlertController(title: "Photo Description", message: "Describe what this photo is all about", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            if(self.imagesDescription[photoIndex] != "No description"){
                textField.text = self.imagesDescription[photoIndex]
            }
            
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
            
            self.imagesDescription.append("No description")
            alert?.dismiss(animated: false, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            print("Text field: \(textField?.text)")
            
            if(textField?.text == ""){
                textField?.text = "No description"
            }
            
          //  self.arrayImageDecription[photoIndex] = (textField?.text)!
          //  self.arrayEditDescriptionIndex.append(photoIndex)
            self.mediaArray[photoIndex].caption = (textField?.text)!
            self.imagesDescription[photoIndex] = (textField?.text)!
            self.browser.viewDidLoad()
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    public func didDisplayMedia(at index: Int, in mediaBrowser: MediaBrowser) {
        // print("Did start viewing photo at index \(index)")
        
    }
    
    public func mediaDid(selected: Bool, at index: Int, in mediaBrowser: MediaBrowser) {
        selections[index] = selected
        print("Select at index mediaDid \(index) \(selected)")
    }
    
    //    func titleForPhotoAtIndex(index: Int, MediaBrowser: MediaBrowser) -> String {
    //    }
    
}

extension EPSignatureViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}




