//
//  OrderDetail.swift
//  WMG
//
//  Created by Coolasia on 29/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//


import SwiftyJSON
import UIKit


class OrderDetail: NSObject {
    
    var order_id: Int?
    var id: Int?
    var quantity: Int?
    var tracking_number: String?
    var descriptions: String?
    var weight: Double?
    var item: String?
    
// add new param
    var part_no: String?
    var lot_no: String?
    var serial_no: String?
    var width: Double?
    var height: Double?
    var length: Double?
    var balance: Double?
    var container_receipt: String?
    var remarks: String?
// add new param
    
    override init() {
        
        self.order_id = 0
        self.id = 0
        self.quantity = 0
        self.tracking_number = ""
        self.descriptions = ""
        self.weight = 0.00
        self.item = ""
        
        self.part_no = ""
        self.lot_no = ""
        self.serial_no = ""
        self.width = 0.00
        self.height = 0.00
        self.length = 0.00
        self.balance = 0.00
        self.container_receipt = ""
        self.remarks = ""
        
        
        super.init()
        
    }
    
    init(order_id: Int?, id: Int?,quantity:Int?,tracking_number: String?,descriptions: String?,weight:Double?,item:String?,
         part_no:String?,lot_no:String?,serial_no:String?,width:Double?,height:Double?,length:Double?,balance:Double?,container_receipt:String?,remarks:String?) {
        
        self.order_id = order_id
        self.id = id
        self.quantity = quantity
        self.tracking_number = tracking_number
        self.descriptions = descriptions
        self.weight = weight
        self.item = item
        
        self.part_no = part_no
        self.lot_no = lot_no
        self.serial_no = serial_no
        self.width = width
        self.height = height
        self.length = length
        self.balance = balance
        self.container_receipt = container_receipt
        self.remarks = remarks

        super.init()
    }
    
    
    func toJsonAddOrderItems()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        orderAttemptDic.setValue(self.descriptions, forKey: "description")
        orderAttemptDic.setValue(self.quantity, forKey: "quantity")
        orderAttemptDic.setValue(self.weight, forKey: "weight")
        orderAttemptDic.setValue(self.remarks, forKey: "remarks")
        
        return orderAttemptDic
        
    }
    
    func toJsonEditOrderItems()->NSMutableDictionary{
        let orderAttemptDic:NSMutableDictionary = NSMutableDictionary()
        orderAttemptDic.setValue(self.id, forKey: "order_detail_id")
        orderAttemptDic.setValue(self.descriptions, forKey: "description")
        orderAttemptDic.setValue(self.quantity, forKey: "quantity")
        orderAttemptDic.setValue(self.weight, forKey: "weight")
        orderAttemptDic.setValue(self.remarks, forKey: "remarks")
        
        return orderAttemptDic
        
    }
    
    
}


extension OrderDetail {
    convenience init?(json: JSON) {
        
        let order_id: Int? = json["order_id"].int
        let id: Int? = json["id"].int
        let quantity: Int? = json["quantity"].int
        let tracking_number: String? = json["tracking_number"].string
        let descriptions: String? = json["description"].string
        let weight: Double? = json["weight"].double
        var item: String? = json["item"].string
        if(item == nil){
            item = ""
        }
        
        let part_no: String? = json["part_no"].string
        let lot_no: String? = json["lot_no"].string
        let serial_no: String? = json["serial_no"].string
        let width: Double? = json["width"].double
        let height: Double? = json["height"].double
        let length: Double? = json["length"].double
        let balance: Double? = json["balance"].double
        let container_receipt: String? = json["container_receipt"].string
        let remarks: String? = json["remarks"].string
        
        
        
        self.init(order_id: order_id, id: id,quantity:quantity,tracking_number: tracking_number,descriptions: descriptions,weight:weight,item:item,part_no:part_no,lot_no:lot_no,serial_no:serial_no,width:width,height:height,length:length,balance:balance,container_receipt:container_receipt,remarks:remarks)
        
    }
}





