//
//  OrderStatus+CoreDataProperties.swift
//  SAAS
//
//  Created by Coolasia on 8/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import Foundation
import CoreData


extension OrderStatus {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<OrderStatus> {
        return NSFetchRequest<OrderStatus>(entityName: "OrderStatus");
    }
    
    @NSManaged public var orderStatusId: Int64
    @NSManaged public var orderStatusName: String
    @NSManaged public var orderIsActive: Bool
    @NSManaged public var orderGeneralId: Int64
    @NSManaged public var orderViewName: String
    
}
