//
//  OrderAttemptImage.swift
//  WMG
//
//  Created by jing jie wong on 23/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit


class OrderAttemptImage: NSObject {
    
    var mUrl: String?
    var mNote: String?
    var isSignature: Bool?
    var isRemove: Bool?
    var orderAttemptId: Int?
    var id: Int?
    
    //add for base64
    var mBase64: String?
   
    init(mUrl: String?, mNote: String?, mBase64: String?,orderAttemptId: Int?, id: Int?, isSignature: Bool?,isRemove: Bool?) {
        self.mUrl = mUrl
        self.mNote = mNote
        self.mBase64 = mBase64
        self.orderAttemptId = orderAttemptId
        self.id = id
        self.isSignature = isSignature
        self.isRemove = isRemove
        self.id = id
        
        super.init()
    }
    
    init(mUrl: String?, mNote: String?, mBase64: String?) {
        self.mUrl = mUrl
        self.mNote = mNote
        self.mBase64 = mBase64
        
        super.init()
    }
    
    init(mBase64: String?, mNote: String?, isSignature: Bool?) {
        self.mBase64 = mBase64
        self.mNote = mNote
        self.isSignature = isSignature
        
        super.init()
    }
    
    init(id:Int?, isRemove: Bool?, isSignature: Bool? ) {
        self.id = id
        self.isRemove = isRemove
        self.isSignature = isSignature
        
        super.init()
    }
    
    init(id:Int?, isRemove: Bool?, mNote: String? ) {
        self.id = id
        self.isRemove = isRemove
        self.mNote = mNote
        
        super.init()
    }
    
    init(mUrl: String?, mNote: String?, isSignature: Bool?) {
        self.mUrl = mUrl
        self.mNote = mNote
        self.isSignature = isSignature
        
        super.init()
    }
    
}

extension OrderAttemptImage {
    convenience init?(json: JSON) {

        let orderAttemptId: Int? = json["order_attempt_id"].intValue
        let id: Int? = json["id"].intValue
        let mUrl: String? = json["image_url"].string
        let mNote: String? = json["description"].string
        let isSignature: Bool? = json["is_signature"].boolValue
        let isRemove: Bool? = json["remove"].boolValue
        
        //let mBase64: String? = ""

        self.init(mUrl: mUrl, mNote: mNote,mBase64:"",orderAttemptId:orderAttemptId,id:id,isSignature: isSignature,isRemove: isRemove)
        
        
        //add for base64
      //  self.init(mBase64:"")
        
    }
}




