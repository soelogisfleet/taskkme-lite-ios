//
//  AppCompanySetting+CoreDataProperties.swift
//  SAAS
//
//  Created by Coolasia on 23/1/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import Foundation
import CoreData


extension AppCompanySetting {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<AppCompanySetting> {
        return NSFetchRequest<AppCompanySetting>(entityName: "AppCompanySetting");
    }
    
    @NSManaged public var id: Int64
    @NSManaged public var rule: String
    @NSManaged public var jsonData: String
    @NSManaged public var enable: Bool
    @NSManaged public var currentStatus: Int64
    @NSManaged public var toStatus: Int64
    @NSManaged public var isScanRequired: Bool
    @NSManaged public var isManPowerCanUpdate: Bool
    
}
