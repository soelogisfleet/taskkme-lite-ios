//
//  MainVC.swift
//  1106-douyu
//
//  Created by targetcloud on 2016/11/6.
//  Copyright © 2016年 targetcloud. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PermissionScope
import CoreData
import SwiftSpinner
import Firebase
import FirebaseMessaging

class MainVC: UITabBarController,UISearchBarDelegate {
    
    let pscope = PermissionScope()
    var itemTrackingSearch = String()
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .custom)
    let moc = DataController().managedObjectContext
    var dateDict: Dictionary<String, Date> = [:]
    
    var aPopupContainer: PopupContainer?
    var testCalendar = Calendar(identifier: .gregorian)
    var currentDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    var unassignedCount: Int?
    var assignedCount: Int?
    var acknowledgedCount: Int?
    var inProgressCount: Int?
    var completedCount: Int?
    var cancelledCount: Int?
    var failedCount: Int?
    var selfCollectCount: Int?
    
    var window: UIWindow?
    var navController = UINavigationController()
    var navController2: [UINavigationController] = []
    var navController1 = UINavigationController()
    var totalStatusTesting: Int?
    
    var dateChangeSelect = String()
    
    var backGround = LocationTracker_ViewController()
    var reachInternetConnection = Reachability()
    
    
    func turnOffLocationTracker(){
        backGround.stopTimer()
        print("stopTimer turnOffLocationTracker mainvc")
    }
    
    func turnOnLocationTracker(){
        print("start turnOnLocationTracker mainvc")
        backGround.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //  self.window = UIWindow(frame: UIScreen.main.bounds)
        
    }
    
    func reloadJobAfterNotification(){
        
        NotificationCenter.default.post(name: NSNotification.Name("RELOAD_JOB_AFTER_NOTIFICATION_ASSIGNED"), object: self)
        
    }

    func showSelectJobLayout(){
       // self.navigationController?.navigationBar.topItem?.setLeftBarButton(nil, animated: true)
        
//        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = nil
//        let cancelButton = UIButton(type: .custom)
//        cancelButton.setImage(UIImage(named: "close"), for: .normal)
//        cancelButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        cancelButton.addTarget(self, action:#selector(MainVC.cancelSelectJobLayout), for: .touchUpInside)
//        let cancelBarButton = UIBarButtonItem(customView: cancelButton)
//        self.navigationController?.navigationBar.topItem?.setRightBarButton(cancelBarButton, animated: true)
//        
        

    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizableViewControllers = nil;
        UIApplication.shared.statusBarStyle = .lightContent
        //        var nav1 = UINavigationController()
        //        var first = TestingViewController(nibName: nil, bundle: nil)
        //        nav1.viewControllers = [first]
        //        nav1.title = "111"
        
        
//        let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
//        let firstTrackingLocation = UserDefaults.standard.value(forKey: "firstTracking") as! Bool
//        print("printisOnline \(isOnline)")
//        
//        //if(firstTrackingLocation){
//        if(isOnline){
//            self.turnOnLocationTracker()
//        }else{
//            self.turnOffLocationTracker()
//        }
        
        //    UserDefaults.standard.setValue(false, forKey: "firstTracking")
        //  }
        
        
        
        
        //        self.totalStatusTesting = 2
        //        var tabs = UITabBarController()
        //
        //        for var i in 0..<self.statusName.count {
        //            let storyboard = UIStoryboard(name: "HomeStatusTab", bundle: nil)
        //            navController = storyboard.instantiateViewController(withIdentifier: "HomeStatusTab") as! UINavigationController
        //            let workflow2TabItem = UITabBarItem(title: "Item \(i)", image: nil, selectedImage: nil)
        //            navController.tabBarItem = workflow2TabItem
        //            let levelController = navController.viewControllers[0] as! HomeStatusTabViewController
        //            levelController.titleStatus = "Scanned"
        //            self.navController2.append(navController)
        //
        //        }
        //        tabs.viewControllers = navController2
        
        
        //        let storyboard1 = UIStoryboard(name: "Testing", bundle: nil)
        //        navController1 = storyboard.instantiateViewController(withIdentifier: "Testing") as! UINavigationController
        //        let workflow2TabItem1 = UITabBarItem(title: "Item 1", image: nil, selectedImage: nil)
        //        navController1.tabBarItem = workflow2TabItem1
        //
        //        var tabs = UITabBarController()
        //        tabs.viewControllers = [navController, navController1,navController2,navController3,navController4,navController5,navController6]
        
        //self.window!.rootViewController = tabs;
        //self.window?.makeKeyAndVisible();
   

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.showSelectJobLayout),
            name: NSNotification.Name(rawValue: "showSelectJobLayout"),
            object: nil)
        // self.hideKeyboardWhenTappedAround()
        
        self.dateChangeSelect = self.currentDate.string(format: "yyyy-MM-dd")
        
        UserDefaults.standard.setValue(self.dateChangeSelect, forKey: "dateChangeSelect")
        
        print("self.dateChangeSelect \(self.dateChangeSelect)")
        
        // Set up permissions
        pscope.addPermission(CameraPermission(),
                             message: "Taskkme will need camera access for scanning barcode and capture photos")
        pscope.addPermission(PhotosPermission(),
                             message: "Taskkme will only upload photos you choose.")
        pscope.addPermission(LocationWhileInUsePermission(),
                             message: "Taskkme need to update your location to the controller admin panel")
        
        
        // Show dialog with callbacks
        pscope.show({ finished, results in
            print("got results \(results)")
        }, cancelled: { (results) -> Void in
            print("thing was cancelled")
        })
        
        
        let menuButton = UIButton(type: .custom)
        menuButton.setImage(UIImage(named: "btn_left_menu"), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        menuButton.addTarget(self, action:#selector(MainVC.leftBarButtonMethod), for: .touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        
        //button search
        
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchButton.addTarget(self, action:#selector(MainVC.createSearchBar), for: .touchUpInside)
        let searchBarButton = UIBarButtonItem(customView: searchButton)
        //button search
        
        
        let refreshButton = UIButton(type: .custom)
        refreshButton.setImage(UIImage(named: "refresh"), for: .normal)
        refreshButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        refreshButton.addTarget(self, action:#selector(MainVC.refreshJob), for: .touchUpInside)
        let refreshBarButton = UIBarButtonItem(customView: refreshButton)
        
//        self.navigationController?.navigationBar.topItem?.setLeftBarButton(menuBarButton, animated: true)
//        self.navigationController?.navigationBar.topItem?.setRightBarButtonItems([refreshBarButton,searchBarButton], animated: true)
        
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        self.navigationController?.navigationBar.topItem?.title="Job List"
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        self.tabBar.tintColor = Constants.hexStringToUIColor(hex:"4E85BD")
        
        self.navigationController?.isNavigationBarHidden = true
        // leeway color b20000
        self.moreNavigationController.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:"4E85BD")
        self.moreNavigationController.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.moreNavigationController.navigationBar.tintColor = UIColor.white
        self.moreNavigationController.navigationBar.topItem?.rightBarButtonItem = nil
        self.moreNavigationController.view.tintColor = UIColor.darkGray
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.leftBarButtonMethod),
            name: NSNotification.Name(rawValue: "LEFT_MENU_TAPPED"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.refreshJob),
            name: NSNotification.Name(rawValue: "REFRESH_JOB"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.createSearchBar),
            name: NSNotification.Name(rawValue: "SEARCH_JOB"),
            object: nil)
        
        if (UserDefaults.standard.value(forKey: "setupForCalendarFirst") != nil) {
            let setupForCalendar = UserDefaults.standard.value(forKey: "setupForCalendarFirst") as! Bool
            if(setupForCalendar){
                NotificationCenter.default.addObserver(
                    self,
                    selector: #selector(self.showCalendar),
                    name: NSNotification.Name(rawValue: "SHOW_CALENDAR"),
                    object: nil)
                
                UserDefaults.standard.setValue(false, forKey: "setupForCalendarFirst")
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainVC.turnOnLocationTracker), name: NSNotification.Name(rawValue: "TurnOnLocation"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainVC.turnOffLocationTracker), name: NSNotification.Name(rawValue: "TurnOffLocation"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainVC.driverLogout), name: NSNotification.Name(rawValue: "driverLogout"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainVC.reloadJobAfterNotification), name: NSNotification.Name(rawValue: "RELOAD_JOB_AFTER_NOTIFICATION_MAIN"), object: nil)
        
        
        //checkDatabase()
        refreshTabBarItemCount()
        
        let infoDictionary = Bundle.main.infoDictionary
        let appID = infoDictionary!["CFBundleIdentifier"] as? String
        print("appID store \(appID)")
        
        DispatchQueue.global().async {
            do {
                let update = try self.needsUpdate()
                
                print("update",update)
                DispatchQueue.main.async {
                    if update{
                        self.popupUpdateDialogue();
                    }
                    
                }
            } catch {
                print(error)
            }
        }
        
    }
    
    
    /* ======= Driver Click Logout ========= */
    func driverLogout(){
        
        
        
        SwiftSpinner.show("Logout from TaskkMe...")
        
        
        var token = ""
        
        
        if let iphoneTokenExist = UserDefaults.standard.object(forKey: "iphone_token") {
            token = UserDefaults.standard.value(forKey: "iphone_token") as! String
            if(token == ""){
                token = InstanceID.instanceID().token()!
            }else{
                token = UserDefaults.standard.value(forKey: "iphone_token") as! String
            }
        }else{
            token = InstanceID.instanceID().token()!
        }
        
        print("fbs token:\(token)")
        
        let logoutInfo : Dictionary<String,AnyObject> = ["iphone_token" : token as AnyObject]
        
        /* ====== Driver Logout ======= */
        AlamofireRequest.driverLogout(parameters: logoutInfo as NSDictionary) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            
            print("statusCode \(statusCode)")
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    UserDefaults.standard.removeObject(forKey: "Token")
                    var backGround = LocationTracker_ViewController()
                    backGround.stopTimer()
                    UserDefaults.standard.setValue(false, forKey: "isOnline")
                    var window: UIWindow? = UIApplication.shared.keyWindow
                    var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                    window?.rootViewController = storyBoard.instantiateInitialViewController()
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                 UserDefaults.standard.removeObject(forKey: "Token")
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== Driver Logout ======= */
        
        
        
    }
    
    func popupUpdateDialogue(){
        var versionInfo = ""
        do {
            versionInfo = try self.getAppStoreVersion()
        }catch {
            print(error)
        }
        
        
        let alertMessage = "Please update TaskkMe to version "+versionInfo;
        let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let url = URL(string: "itms-apps://itunes.apple.com/us/app/taskkme/id1364900786"),
                UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
        let noBtn = UIAlertAction(title:"Skip this Version" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(okBtn)
        //  alert.addAction(noBtn)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getAppStoreVersion() -> String {
        let infoDictionary = Bundle.main.infoDictionary
        let appID = infoDictionary!["CFBundleIdentifier"] as! String
        let url = URL(string: "http://itunes.apple.com/us/lookup?bundleId=\(appID)")
        let data = try? Data(contentsOf: url!)
        let lookup = (try? JSONSerialization.jsonObject(with: data! , options: [])) as? [String: Any]
        if let resultCount = lookup!["resultCount"] as? Int, resultCount == 1 {
            if let results = lookup!["results"] as? [[String:Any]] {
                if let appStoreVersion = results[0]["version"] as? String{
                    let currentVersion = infoDictionary!["CFBundleShortVersionString"] as? String
                    if !(appStoreVersion == currentVersion) {
                        print("Need to update [\(appStoreVersion) != \(currentVersion)]")
                        return appStoreVersion
                    }
                }
            }
        }
        return ""
    }
    
    func needsUpdate() -> Bool {
        let infoDictionary = Bundle.main.infoDictionary
        let appID = infoDictionary!["CFBundleIdentifier"] as! String
        let url = URL(string: "http://itunes.apple.com/us/lookup?bundleId=\(appID)")
        let data = try? Data(contentsOf: url!)
        let lookup = (try? JSONSerialization.jsonObject(with: data! , options: [])) as? [String: Any]
        if let resultCount = lookup!["resultCount"] as? Int, resultCount == 1 {
            if let results = lookup!["results"] as? [[String:Any]] {
                if let appStoreVersion = results[0]["version"] as? String{
                    let currentVersion = infoDictionary!["CFBundleShortVersionString"] as? String
                    if !(appStoreVersion == currentVersion) {
                        print("Need to update [\(appStoreVersion) != \(currentVersion)]")
                        return true
                    }
                }
            }
        }
        return false
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let morenavbar = navigationController.navigationBar
        if let morenavitem = morenavbar.topItem {
            morenavitem.rightBarButtonItem = nil
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addChildVc(storyName:String){
        let childVc = UIStoryboard(name: storyName, bundle: nil).instantiateInitialViewController()!
        addChildViewController(childVc)
    }
    
    
    // fetch data from core data
    func checkDatabase(){
        let joborderFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "JobOrder")
        do{
            let fetchOrder = try moc.fetch(joborderFetch) as! [JobOrder]
            
            if fetchOrder.count > 0
            {
                for result in fetchOrder {
                    
                    print("result\(result)")
                    
                    if let body = result.value(forKey: "body") as? NSMutableDictionary{
                        print("bodyOrder \(body)")
                        if let orderID = result.value(forKey: "orderID") as? [String]{
                            print("orderIDCoreData \(orderID)")
                            
                            if let orderStatus = result.value(forKey: "orderStatus") as? String{
                                print("orderStatus \(orderStatus)")
                                
                                
                                
                                let para:NSMutableDictionary = NSMutableDictionary()
                                let prodArray:NSMutableArray = NSMutableArray()
                                
                                para.setValue(orderID, forKey: "id")
                                para.setValue(orderStatus, forKey: "order_status_id")
                                
                                
                                
                                prodArray.add(body)
                                
                                
                                para.setObject(prodArray, forKey: "order_attempts" as NSCopying)
                                print(para)
                                
                                let dataPara:NSMutableDictionary = NSMutableDictionary()
                                let dataArray:NSMutableArray = NSMutableArray()
                                
                                dataArray.add(para)
                                
                                dataPara.setValue(dataArray, forKey: "data")
                                
                                print("DataPara \(dataPara)")
                                
                                do {
                                    let jsonData = try JSONSerialization.data(withJSONObject: dataPara, options: JSONSerialization.WritingOptions.prettyPrinted)
                                    
                                    
                                    if let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) {
                                        
                                        print("jsonString \(jsonString)")
                                        
                                        
                                        //declare parameter as a dictionary which contains string as key and value combination.
                                        
                                        //create the url with NSURL
                                        let url = NSURL(string: Constants.web_api+Constants.api_batch_update_order)
                                        
                                        //create the session object
                                        let urlconfig = URLSessionConfiguration.default
                                        urlconfig.timeoutIntervalForRequest = 20
                                        urlconfig.timeoutIntervalForResource = 20
                                        let session = URLSession(configuration: urlconfig)
                                        
                                        
                                        //now create the NSMutableRequest object using the url object
                                        var request = URLRequest(url: url! as URL)
                                        request.httpMethod = "POST" //set http method as POST
                                        
                                        do {
                                            request.httpBody = try JSONSerialization.data(withJSONObject: dataPara, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                                            
                                        } catch let error {
                                            print(error.localizedDescription)
                                        }
                                        
                                        
                                        //HTTP Headers
                                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                                        request.addValue("application/json", forHTTPHeaderField: "Accept")
                                        request.addValue("bearer \(UserDefaults.standard.value(forKey: "Token")!)", forHTTPHeaderField:"Authorization")
                                        
                                        
                                        print(request.allHTTPHeaderFields!)
                                        
                                        //create dataTask using the session object to send data to the server
                                        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                                            
                                            
                                            guard error == nil else {
                                                
                                                // request time out store in database.
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CALENDAR_DAY_SELECTED"), object: nil, userInfo: self.dateDict)
                                                
                                                self.refreshTabBarItemCount()
                                                
                                                return
                                            }
                                            
                                            guard let data = data else {
                                                return
                                            }
                                            
                                            do {
                                                //create json object from data
                                                print(response!)
                                                print("Data is \(data)")
                                                
                                                if let httpResponse = response as? HTTPURLResponse {
                                                    print("error \(httpResponse.statusCode)")
                                                    if(httpResponse.statusCode==200)
                                                    {
                                                        //succesfully update order, show an success message
                                                        DispatchQueue.main.async {
                                                            
                                                            self.moc.delete(result)
                                                            // self.showing()
                                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CALENDAR_DAY_SELECTED"), object: nil, userInfo: self.dateDict)
                                                            
                                                            self.refreshTabBarItemCount()
                                                            
                                                        }
                                                        
                                                    }
                                                    else{
                                                        //error. show an error message
                                                    }
                                                    
                                                }
                                                
                                            } catch let error {
                                                print(error.localizedDescription)
                                            }
                                            
                                        })
                                        
                                        task.resume()
                                        
                                    }
                                    
                                }
                                catch {
                                    print("Error first DO \(error.localizedDescription)")
                                }
                            }
                        }
                    }
                    
                }
            }
            else{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CALENDAR_DAY_SELECTED"), object: nil, userInfo: self.dateDict)
                
                refreshTabBarItemCount()
                
            }
            
        }
        catch {
            fatalError("errorFetch: \(error)")
        }
    }
    
    func refreshTabBarItemCount(){
        
        var orderStatusIDUnassigned: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.UNASSIGNED_TEXT)
        
        
        var orderStatusIDAssigned: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ASSIGNED_TEXT)
        
        var orderStatusIDAcknowledged: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ACKNOWLEDGED_TEXT)
        
        var orderStatusIDInProgress: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.INPROGRESS_TEXT)
        
        var orderStatusIDCompleted: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.COMPLETE_TEXT)
        
        var orderStatusIDFailed: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.FAILED_TEXT)
        
        var orderStatusIDCancelled: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.CANCELLED_TEXT)
        
        var orderStatusIDSelfCollect: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.SELF_COLLECT_TEXT)
        
        var orderStatusIDForALL: String = "\(orderStatusIDAssigned),\(orderStatusIDAcknowledged),\(orderStatusIDInProgress),\(orderStatusIDCompleted),\(orderStatusIDFailed),\(orderStatusIDCancelled),\(orderStatusIDSelfCollect)"
        
        
        //        let message   = String(format: Constants.api_get_order_count, arguments: [currentDate.string(format: "yyyy-MM-dd"),currentDate.string(format: "yyyy-MM-dd")])
        
        let message   = String(format: Constants.api_get_orders_total, arguments: [currentDate.string(format: "yyyy-MM-dd"),currentDate.string(format: "yyyy-MM-dd"),orderStatusIDForALL])
        
        var httpHeader = [
            "Authorization": "bearer \(UserDefaults.standard.value(forKey: "Token")!)"
        ]
        
        Alamofire.request(Constants.web_api+message, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: httpHeader ).responseJSON { (responseData) -> Void in
            
            if(responseData.response?.statusCode == 401){
                
            }else{
                if((responseData.result.value) == nil){
                    print("Null")
                }
                    
                else{
                    let swiftyJsonVar = JSON(responseData.result.value!)
                    self.unassignedCount = 0
                    self.assignedCount = 0
                    self.acknowledgedCount = 0
                    self.inProgressCount = 0
                    self.completedCount = 0
                    self.cancelledCount = 0
                    self.failedCount = 0
                    self.selfCollectCount = 0
                    
                    if(swiftyJsonVar.array == nil){}else{
                        
                        for subJson in swiftyJsonVar.array! {
                            
                            
                            
                            if(subJson["order_status_id"].int == orderStatusIDUnassigned){
                                self.unassignedCount = subJson["total_all_jobs"].int
                            }
                            
                            // this is Assigned
                            if(subJson["order_status_id"].int == orderStatusIDAssigned){
                                self.assignedCount = subJson["total_all_jobs"].int
                            }
                            
                            // this is Acknowledged
                            if(subJson["order_status_id"].int == orderStatusIDAcknowledged){
                                self.acknowledgedCount = subJson["total_all_jobs"].int
                            }
                            
                            // this is InProgress
                            if(subJson["order_status_id"].int == orderStatusIDInProgress){
                                self.inProgressCount = subJson["total_all_jobs"].int
                            }
                            
                            // this is Completed
                            if(subJson["order_status_id"].int == orderStatusIDCompleted){
                                self.completedCount = subJson["total_all_jobs"].int
                            }
                            
                            // this is Cancelled
                            if(subJson["order_status_id"].int == orderStatusIDCancelled){
                                self.cancelledCount = subJson["total_all_jobs"].int
                            }
                            
                            // this is Failed
                            if(subJson["order_status_id"].int == orderStatusIDFailed){
                                self.failedCount = subJson["total_all_jobs"].int
                            }
                            
                            // this is Completed
                            if(subJson["order_status_id"].int == orderStatusIDSelfCollect){
                                self.selfCollectCount = subJson["total_all_jobs"].int
                            }
                            
                            
                            
                            print("assignedCount \(self.assignedCount)")
                            
                            if self.unassignedCount != 0
                            {
                                self.tabBar.items?[0].badgeValue = String(self.unassignedCount!)
                            }
                            else{
                                self.tabBar.items?[0].badgeValue  = nil
                            }
                            
                            
                            
                            
                            if self.assignedCount != 0
                            {
                                self.tabBar.items?[1].badgeValue = String(self.assignedCount!)
                            }
                            else{
                                self.tabBar.items?[1].badgeValue  = nil
                            }
                            
                            print("acknowledgedCount \(self.acknowledgedCount)")
                            if self.acknowledgedCount != 0
                            {
                                self.tabBar.items?[2].badgeValue = String(self.acknowledgedCount!)
                            }
                            else{
                                self.tabBar.items?[2].badgeValue  = nil
                            }
                            
                            print("inProgressCount \(self.inProgressCount)")
                            if self.inProgressCount != 0
                            {
                                self.tabBar.items?[3].badgeValue = String(self.inProgressCount!)
                            }
                            else{
                                self.tabBar.items?[3].badgeValue  = nil
                            }
                            
                            print("completedCount \(String(describing: self.completedCount))")
                            if self.completedCount != 0 {
                                self.tabBar.items?[4].badgeValue = String(self.completedCount!)
                            }
                            else{
                                self.tabBar.items?[4].badgeValue = nil
                            }
                            
                            
                            print("cancelledCount \(String(describing: self.cancelledCount))")
                            if self.cancelledCount != 0 {
                                //self.viewControllers?[4].tabBarItem.badgeValue = String(describing: self.cancelledCount!)
                            }
                            else{
                                //self.viewControllers?[4].tabBarItem.badgeValue  = nil
                            }
                            
                            print("failedCount \(String(describing: self.failedCount))")
                            if self.failedCount != 0 {
                                self.viewControllers?[5].tabBarItem.badgeValue = String(describing: self.failedCount!)
                            }
                            else{
                                self.viewControllers?[5].tabBarItem.badgeValue  = nil
                            }
                            
                            print("selfCollectCount \(String(describing: self.selfCollectCount))")
                            if self.selfCollectCount != 0 {
                                self.viewControllers?[6].tabBarItem.badgeValue = String(describing: self.selfCollectCount!)
                            }
                            else{
                                self.viewControllers?[6].tabBarItem.badgeValue  = nil
                            }
                            
                            
                            
                            if(self.moreNavigationController.topViewController?.view is UITableView)
                            {
                                let moreTableView: UITableView = self.moreNavigationController.topViewController?.view as!UITableView
                                
                                
                                moreTableView.reloadData()
                            }
                            
                            
                            
                            print("failedCount 11 \(String(describing: self.failedCount))")
                            print("selfCollectCount 11 \(String(describing: self.selfCollectCount))")
                            print("cancelledCount 11 \(String(describing: self.cancelledCount))")
                            
                            
                            let totalMoreCount = self.cancelledCount! + self.selfCollectCount! + self.failedCount! + self.completedCount!
                            
                            print("totalMoreCount \(String(describing: totalMoreCount))")
                            
                            if totalMoreCount != 0 {
                                self.tabBar.items?[4].badgeValue = String(totalMoreCount)
                                
                            }
                            else{
                                self.tabBar.items?[4].badgeValue  = nil
                                
                            }
                            
                        }
                    }
                    
                }
            }
            
            
        }
        
        
    }
    
    func leftBarButtonMethod() {
        
      //  self.sideBarController.showMenuViewController(in: LMSideBarControllerDirection(rawValue: 0))
        //self.sideBarController.showMenuViewController(in:LMSideBarControllerDirection.right)
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.menuViewSize = CGSize(width: view.frame.size.width / 4 * 3, height: view.frame.size.height)

        // Present the view controller
        self.frostedViewController.presentMenuViewController()
        
        print("Perform action leftBarButtonMethod")
    }
    
    func rightBarButtonMethod() {
        print("Perform action rightBarButtonMethod")
    }
    
    
    
    func showCalendar() {
        let xibView = Bundle.main.loadNibNamed("CalendarPopUp", owner: nil, options: nil)?[0] as! CalendarPopUp
        xibView.calendarDelegate = self
        xibView.selected = currentDate
        PopupContainer.generatePopupWithView(xibView).show()
        
        print("showCalendar11")
    }
    
    func setDate() {
        let month = testCalendar.dateComponents([.month], from: currentDate).month!
        let weekday = testCalendar.component(.weekday, from: currentDate)
        
        let monthName = DateFormatter().monthSymbols[(month-1) % 12] //GetHumanDate(month: month)//
        let week = DateFormatter().shortWeekdaySymbols[weekday-1]
        
        let day = testCalendar.component(.day, from: currentDate)
        
    }
    
    func refreshJob(){
//        if reachInternetConnection.isConnectedToNetwork() == true {
//            print("MainVC 11 Internet connection OK")
//            print("refresh")
//            dateDict["date"] = currentDate
//            checkDatabase()
//        } else {
//            print("MainVC 11 Internet connection FAILED")
//            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
//            alert.show()
//        }
        
        
        let storyboard = UIStoryboard(name: "CreateJobActivity", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "CreateJobActivity") as! UINavigationController
        let levelController = navController.viewControllers[0] as! CreateJobActivity_ViewController
        self.present(navController, animated: true, completion: nil)
    }
    
    
    //func create search bar
    func createSearchBar(){
        
        //        if(self.navigationItem.titleView == searchBar)
        //        {
        //
        //            searchButton.setImage(UIImage(named: "search"), for: .normal)
        //            self.navigationItem.titleView=nil
        //            self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        //            self.navigationController?.navigationBar.topItem?.title="Job List"
        //            let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        //            self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        //
        //            self.navigationController?.navigationBar.tintColor = UIColor.white;
        //
        //        }
        //        else{
        //            searchBar.showsCancelButton = false
        //            searchBar.placeholder = "Enter tracking number"
        //            searchBar.delegate = self
        //            searchBar.text = ""
        //            self.navigationItem.titleView = searchBar
        //            searchButton.setImage(UIImage(named: "cancel-search"), for: .normal)
        //        }
        
        let storyboard = UIStoryboard(name: "SearchOrder", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "SearchOrderViewController") as! UINavigationController
        let levelController = navController.viewControllers[0] as! SearchOrderViewController
        levelController.itemTracking = itemTrackingSearch
        self.present(navController, animated: true, completion: nil)
        
        
        
    }
    //func create search bar
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing \(itemTrackingSearch)")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing \(itemTrackingSearch)")
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
        // getSearchJobList(itemTrackingSearch: itemTrackingSearch)
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        print("searchBarSearchButtonClicked \(itemTrackingSearch)")
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
        
        
        let storyboard = UIStoryboard(name: "SearchOrder", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "SearchOrderViewController") as! UINavigationController
        let levelController = navController.viewControllers[0] as! SearchOrderViewController
        levelController.itemTracking = itemTrackingSearch
        self.present(navController, animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        itemTrackingSearch = searchText
    }
    
}


extension MainVC: CalendarPopUpDelegate {
    func dateChaged(date: Date) {
        currentDate = date
        Constants.current_date = date;
        
        let dateDict:[String: Date] = ["date": date]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CALENDAR_DAY_SELECTED"), object: nil, userInfo: dateDict)
        refreshTabBarItemCount()
        
    }
}

extension MainVC{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}





