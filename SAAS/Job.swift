//
//  Job.swift
//  WMG
//
//  Created by jing jie wong on 23/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit


class Job: NSObject {
    
    var orderId: Int
    var orderStatusId: Int
    var pickUpWorkerId: Int?
    var masterTrackingNumber: String?
    var senderName: String?
    var mawb: String?
    var itemTrackingNumber: String?
    var dropOffWorkerId: Int?
    var pickUpContactName: String?
    var dropOffContactName: String?
    var jobDescription: String?
    var dropOffDesc: String?
    var pickUpContact: String?
    var dropOffContact: String?
    var totalPrice: Double?
    var pickUpAddress: String?
    var pickUpPostalCode: String?
    var dropOffAddress: String?
    var dropOffPostalCode: String?
    var pickUpTime: String?
    var dropOffDate: String?
    var dropOffTime: String?
    var isSelected: Bool?
    var review: String?
    var orderAttemptList: [OrderAttempt] = []
    var orderDetailList: [OrderDetail] = []
    
    
    //add new field
    var wmsOrderId: Int?
    var dropOffTo: String?
    var dropOffPic: String?
    var pickupPic: String?
    var companyName: String?
    var referenceNumber: String?
    var totalWeight: Double?
    var totalPackage: Int?
    var job_stepsList: [JobSteps] = []
    var orderNumber: String?
    
    
    override init() {
        
        self.orderId = 0
        self.orderStatusId = 0
        self.pickUpWorkerId = 0
        self.masterTrackingNumber = ""
        self.senderName = ""
        self.mawb = ""
        self.itemTrackingNumber = ""
        self.dropOffWorkerId = 0
        self.pickUpContactName = ""
        self.dropOffContactName = ""
        self.jobDescription = ""
        self.dropOffDesc = ""
        self.pickUpContact=""
        self.dropOffContact = ""
        self.totalPrice = 0.00
        self.pickUpAddress = ""
        self.pickUpPostalCode = ""
        self.dropOffAddress = ""
        self.dropOffPostalCode = ""
        self.pickUpTime = ""
        self.dropOffDate = ""
        self.dropOffTime = ""
        self.review = ""
  
        self.isSelected=false
        
        //add new field
        self.wmsOrderId = 0
        self.dropOffTo = ""
        self.dropOffPic = ""
        self.pickupPic = ""
        self.companyName = ""
        self.referenceNumber = ""
        self.totalWeight = 0.0
        self.totalPackage = 0
        self.orderNumber = ""
        
        super.init()
        
    }

    init(orderId: Int, orderStatusId: Int,pickUpWorkerId:Int?,masterTrackingNumber: String?,senderName: String?,mawb:String?,itemTrackingNumber: String?, dropOffWorkerId: Int?,pickUpContactName:String?,dropOffContactName: String?,jobDescription:String?,dropOffDesc:String?,pickUpContact:String?,dropOffContact:String?,totalPrice:Double?,pickUpAddress:String?,pickUpPostalCode:String?,dropOffAddress:String?,dropOffPostalCode:String?,pickUpTime:String?,dropOffDate:String?,dropOffTime:String?,review:String?,
        orderAttemptList:[OrderAttempt],orderDetailList:[OrderDetail],wmsOrderId:Int?,dropOffTo:String?,dropOffPic:String?,pickupPic:String?,companyName:String?,referenceNumber:String?,totalWeight:Double?,totalPackage:Int?,job_stepsList:[JobSteps], orderNumber: String?
        ) {
        
        self.orderId = orderId
        self.orderStatusId = orderStatusId
        self.pickUpWorkerId = pickUpWorkerId
        self.masterTrackingNumber = masterTrackingNumber
        self.senderName = senderName
        self.mawb = mawb
        self.itemTrackingNumber = itemTrackingNumber
        self.dropOffWorkerId = dropOffWorkerId
        self.pickUpContactName = pickUpContactName
        self.dropOffContactName = dropOffContactName
        self.jobDescription = jobDescription
        self.dropOffDesc = dropOffDesc
        self.pickUpContact=pickUpContact
        self.dropOffContact = dropOffContact
        self.totalPrice=totalPrice
        self.pickUpAddress = pickUpAddress
        self.pickUpPostalCode = pickUpPostalCode
        self.dropOffAddress = dropOffAddress
        self.dropOffPostalCode = dropOffPostalCode
        self.pickUpTime = pickUpTime
        self.dropOffDate = dropOffDate
        self.dropOffTime = dropOffTime
        self.review = review
        self.orderAttemptList = orderAttemptList
        self.orderDetailList = orderDetailList
        self.isSelected=false
        
        //add new
        self.wmsOrderId = wmsOrderId
        self.dropOffTo = dropOffTo
        self.dropOffPic = dropOffPic
        self.pickupPic = pickupPic
        self.companyName = companyName
        self.referenceNumber = referenceNumber
        self.totalWeight = totalWeight
        self.totalPackage = totalPackage
        self.job_stepsList = job_stepsList
        self.orderNumber = orderNumber
        
        super.init()
    }
    
    
  }


extension Job {
    convenience init?(json: JSON) {
        
        print(json)
        let orderId: Int = json["id"].int!
        let orderStatusId: Int = json["order_status_id"].int!
        let pickUpWorkerId: Int? = json["pickup_worker_id"].int
        let masterTrackingNumber: String? = json["master_tracking_number"].string
        let senderName: String? = json["sender_name"].string
        let mawb: String? = json["mawb"].string
        let itemTrackingNumber: String? = json["item_tracking_number"].string
        let dropOffWorkerId: Int? = json["drop_off_worker_id"].int
        let pickUpContactName: String? = json["pickup_contact_name"].string
        let dropOffContactName: String? = json["drop_off_contact_name"].string
        let jobDescription: String? = json["description"].string
        let dropOffDesc: String? = json["drop_off_description"].string
        let pickUpContact: String? = json["pickup_contact_no"].string
        let dropOffContact: String? = json["drop_off_contact_no"].string
        let totalPrice: Double? = json["total_price"].double
        let pickUpAddress: String? = json["pickup_address"].string
        let pickUpPostalCode: String? = json["pickup_postal_code"].string
        let dropOffAddress: String? = json["drop_off_address"].string
        let dropOffPostalCode: String? = json["drop_off_postal_code"].string
        let pickUpTime: String? = json["pickup_time"].string
        let dropOffDate: String? = json["drop_off_date"].string
        let dropOffTime: String? = json["drop_off_time"].string
        let review: String? = json["review"].string
        
        //add new field
        let wmsOrderId: Int? = json["wms_order_id"].int
        let dropOffTo: String? = json["drop_off_to"].string
        let dropOffPic: String? = json["drop_off_pic"].string
        let pickupPic: String? = json["pickup_pic"].string
        let companyName: String? = json["company_name"].string
        let referenceNumber: String? = json["reference_no"].string
        let totalWeight: Double? = json["total_kg"].double
        let totalPackage: Int? = json["total_package"].int
        let orderNumber: String? = json["order_number"].string

        
        
        var orderAttemptList: [OrderAttempt] = []
        
        if let items = json["order_attempts"].array {
            for item in items {
                if let orderAttempt = OrderAttempt(json: item) {
                    print("order attempt 11\(String(describing: orderAttempt.id))")
                    orderAttemptList.append(orderAttempt)
                }
                
            }
        }
        
        var orderDetailList: [OrderDetail] = []
        
        if let items = json["order_details"].array {
            for item in items {
                if let orderDetail = OrderDetail(json: item) {
                    
                    orderDetailList.append(orderDetail)
                }
                
            }
        }
        
//        var overtimeList: [Overtime] = []
//
//        if let items = json["commissions_and_overtimes"].array {
//            for item in items {
//                if let overTime = Overtime(json: item) {
//                    overtimeList.append(overTime)
//                    print("arrayOvertime11 \(item["worker_id"].int)")
//                }
//            }
//        }
        
        
        var job_stepsList: [JobSteps] = []
        
        if let items = json["job_steps"].array {
            for item in items {
                if let jobSteps = JobSteps(json: item) {
                    job_stepsList.append(jobSteps)
                }
            }
        }
        
        print("job_stepsListCount \(job_stepsList.count)")
        
        
      
        self.init(orderId: orderId, orderStatusId: orderStatusId,pickUpWorkerId:pickUpWorkerId,masterTrackingNumber: masterTrackingNumber,senderName: senderName,mawb:mawb,itemTrackingNumber: itemTrackingNumber, dropOffWorkerId: dropOffWorkerId,pickUpContactName:pickUpContactName,dropOffContactName: dropOffContactName,jobDescription:jobDescription,dropOffDesc:dropOffDesc,pickUpContact:pickUpContact,dropOffContact:dropOffContact,totalPrice:totalPrice,pickUpAddress:pickUpAddress,pickUpPostalCode:pickUpPostalCode,dropOffAddress:dropOffAddress,dropOffPostalCode:dropOffPostalCode,pickUpTime:pickUpTime,dropOffDate:dropOffDate,dropOffTime:dropOffTime,review:review,orderAttemptList:orderAttemptList,orderDetailList:orderDetailList,wmsOrderId:wmsOrderId,dropOffTo:dropOffTo,dropOffPic:dropOffPic,pickupPic:pickupPic,companyName:companyName,referenceNumber:referenceNumber,totalWeight:totalWeight,totalPackage:totalPackage,job_stepsList:job_stepsList,orderNumber:orderNumber)
        
        }
}




