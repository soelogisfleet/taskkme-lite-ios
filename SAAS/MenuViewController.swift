//
//  MenuViewController.swift
//  REFrostedViewControllerSwiftExample
//
//  Created by Benny Singer on 5/20/17.
//  Copyright © 2017 Benny Singer. All rights reserved.
//

import UIKit

class MenuViewController: UITableViewController {

    @IBOutlet weak var onlineOffline_lbl: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var username_lbl: UILabel!

    
    //  Converted to Swift 4 by Swiftify v4.1.6715 - https://objectivec2swift.com/
    @IBAction func isOnline(_ sender: UISwitch) {
        if sender.isOn {
            onlineOffline_lbl.text = "Online"
            NotificationCenter.default.post(name: NSNotification.Name("TurnOnLocation"), object: self)
            UserDefaults.standard.set(true, forKey: "isOnline")
        } else {
            onlineOffline_lbl.text = "Offline"
            NotificationCenter.default.post(name: NSNotification.Name("TurnOffLocation"), object: self)
            UserDefaults.standard.set(false, forKey: "isOnline")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let onlineStatus: Bool = UserDefaults.standard.bool(forKey: "isOnline")
        if onlineStatus {
            switchBtn.isOn = true
            onlineOffline_lbl.text = "Online"
        } else {
            switchBtn.isOn = false
            onlineOffline_lbl.text = "Offline"
        }
    }


    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor(colorLiteralRed: 150/255.0, green: 161/255.0, blue: 177/255.0, alpha: 1.0)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isOpaque = false
        self.tableView.backgroundColor = .clear
        
        username_lbl.text = UserDefaults.standard.string(forKey: "Email")

    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = UIColor(colorLiteralRed: 62/255.0, green: 68/255.0, blue: 75/255.0, alpha: 1.0)
        cell.textLabel?.font = UIFont(name: "HelveticaNeue", size: 17.0)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (section == 0) {
            return nil
        }
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 24))
        view.backgroundColor = UIColor(colorLiteralRed: 167/255.0, green: 167/255.0, blue: 167/255.0, alpha: 0.6)
        
        let label = UILabel(frame: CGRect(x: 10, y: 8, width: 0, height: 0))
        label.text = "Friends Online"
        label.font = UIFont.systemFont(ofSize: 15.0)
        label.textColor = .white
        label.sizeToFit()
        
        view.addSubview(label)
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return 0
        }
        
        return 34
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "contentController") as! CustomNC
        
        if (indexPath.section == 0 && indexPath.row == 0) {
            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "mainViewController")
            navigationController.viewControllers = [homeViewController!]
        } else if (indexPath.section == 0 && indexPath.row == 1) {
            let aboutViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController")
            navigationController.viewControllers = [aboutViewController!]
        }
        else{
            NotificationCenter.default.post(name: NSNotification.Name("driverLogout"), object: self)

            
        }
        
        self.frostedViewController.contentViewController = navigationController
        self.frostedViewController.hideMenuViewController()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        
        if indexPath.section == 0 {
            let titles = ["Jobs", "About", "Logout"]
            cell?.textLabel?.text = titles[indexPath.row]
        } else {
//            let titles = ["John Appleseed", "John Doe", "Test User"]
//            cell?.textLabel?.text = titles[indexPath.row]
        }
        
        return cell!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
