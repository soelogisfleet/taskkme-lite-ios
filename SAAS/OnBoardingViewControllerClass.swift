////
////  OnBoardingViewController.swift
////  SAAS
////
////  Created by Coolasia on 6/3/18.
////  Copyright © 2018 Coolasia. All rights reserved.
////
//
//import UIKit
////import Onboard
//
//class OnBoardingViewControllerClass: UIViewController  {
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        //self.alertView.show()
//        // Initialize onboarding view controller
//        var onboardingVC = OnboardingViewController()
//
//        // Create slides
//        let firstPage = OnboardingContentViewController.content(withTitle: "Welcome!", body: "Here's a bried intro before you get started.", image: UIImage(named: "onboarding-logo"), buttonText: nil, action: nil)
//
//        let secondPage = OnboardingContentViewController.content(withTitle: "Daily Jobs", body: "Jobs are given out by your administrator. If the job does not appear on that day, click the refresh button or contact your administrator.", image: UIImage(named: "search"), buttonText: nil, action: nil)
//
//        let thirdPage = OnboardingContentViewController.content(withTitle: "Job with steps", body: "If the job have stops or detours in between, it will break down to several steps before you could finish that job.", image: UIImage(named: "splash-iphone6plus"), buttonText: nil, action: nil)
//
//        let fourthPage = OnboardingContentViewController.content(withTitle: "Proof Of Delivery", body: "Upon completing this job, you are required to get the recipient's signature, if required. Barcode scanning or photos are optional", image: UIImage(named: "search"), buttonText: nil, action: nil)
//
//         let fitfhPage = OnboardingContentViewController.content(withTitle: "Availability Status", body: "You can yutn off your availability after work from the menu. This will stop the location tracking until you activate it back next time and you cannot update process when in offline. ", image: UIImage(named: "search"), buttonText: nil, action: self.handleOnboardingCompletion)
//
//        // Define onboarding view controller properties
//        onboardingVC = OnboardingViewController.onboard(withBackgroundImage: UIImage(named: "bg-blue"), contents: [firstPage,secondPage,thirdPage,fourthPage,fitfhPage])
//        onboardingVC.shouldFadeTransitions = true
//        onboardingVC.shouldMaskBackground = false
//        onboardingVC.shouldBlurBackground = false
//        onboardingVC.fadePageControlOnLastPage = true
//        onboardingVC.pageControl.pageIndicatorTintColor = UIColor.darkGray
//        onboardingVC.pageControl.currentPageIndicatorTintColor = UIColor.white
//        onboardingVC.swipingEnabled = true
//        onboardingVC.allowSkipping = true
//       // onboardingVC.fadeSkipButtonOnLastPage = true
//
//
//        onboardingVC.skipHandler = {
//            self.skip()
//        }
//
//        firstPage.viewWillAppearBlock = {
//            onboardingVC.skipButton.setTitle("Skip", for: .normal)
//        }
//        secondPage.viewWillAppearBlock = {
//            onboardingVC.skipButton.setTitle("Skip", for: .normal)
//        }
//        thirdPage.viewWillAppearBlock = {
//            onboardingVC.skipButton.setTitle("Skip", for: .normal)
//        }
//
//        fourthPage.viewWillAppearBlock = {
//            onboardingVC.skipButton.setTitle("Skip", for: .normal)
//        }
//
//        fitfhPage.viewWillAppearBlock = {
//            onboardingVC.skipButton.setTitle("Get Started", for: .normal)
//        }
//
//        onboardingVC.skipButton.setTitleColor(UIColor.black, for: .normal)
//
//
//        self.present(onboardingVC, animated: true, completion: nil)
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//
//
//
//    //// onBoard
//
//    func handleOnboardingCompletion (){
//        print("handleOnboardingCompletion")
//        var window: UIWindow? = UIApplication.shared.keyWindow
//        var stryBoard = UIStoryboard(name: "Login", bundle: nil)
//        window?.rootViewController = stryBoard.instantiateInitialViewController()
//    }
//
//    func skip (){
//        print("skip")
//        var window: UIWindow? = UIApplication.shared.keyWindow
//        var stryBoard = UIStoryboard(name: "Login", bundle: nil)
//        window?.rootViewController = stryBoard.instantiateInitialViewController()
//
//    }
//
//}

