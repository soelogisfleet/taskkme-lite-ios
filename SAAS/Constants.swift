//
//  Constants.swift
//  SampleIOSNew
//
//  Created by Coolasia on 20/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import UIKit
import CoreData

struct Constants {
    
    /* ======== LIST OF STATUS ========= */
    static let ASSIGNED = 2
    static let ACKNOWLEDGED = 6
    static let INPROGRESS = 3
    static let COMPLETE = 5
    static let SELF_COLLECT = 14
    static let FAILED = 10
    static let CANCELLED = 9
    static let INCOMPLETED = 17  // This status not use anymore
    /* ======== LIST OF STATUS ========= */
    
    /* ======== LIST OF STATUS COLOR ========= */
    static let ASSIGNED_COLOR = "998BE4"
    static let ACKNOWLEDGED_COLOR = "F6A61C"
    static let INPROGRESS_COLOR = "55B2E5"
    static let COMPLETE_COLOR = "2FAE42"
    static let SELF_COLLECT_COLOR = "48C3CA"
    static let PENDING_COLOR = "999999"
    static let FAILED_COLOR = "D52C36"
    static let CANCELLED_COLOR = "FF7779"
    static let ANIMATE_INPROGRESS_COLOR = "C9E8F8"
    static let ANIMATE_INPROGRESS_LAYER_COLOR = "074c83"
    static let NAV_BAR_COLOR = "4C84BF"

    static let DARKEY_GREY_COLOR = "666666"
    /* ======== LIST OF STATUS COLOR ========= */
    
    /* ======== LIST OF STATUS TEXT ========= */
    
    static let UNASSIGNED_TEXT = "Not Assigned"
    static let ASSIGNED_TEXT = "Assigned"
    static let ACKNOWLEDGED_TEXT = "Acknowledged"
    static let INPROGRESS_TEXT = "In Progress"
    static let COMPLETE_TEXT = "Completed"
    static let CANCELLED_TEXT = "Cancelled"
    static let FAILED_TEXT = "Failed"
    static let SELF_COLLECT_TEXT = "Self Collect"
    /* ======== LIST OF STATUS TEXT ========= */
    
    /* ======== LIST OF JOB STEP STATUS ========= */
    static let JOB_STEP_PENDING = 1
    static let JOB_STEP_INPROGRESS = 2
    static let JOB_STEP_COMPLETED = 3
    static let JOB_STEP_FAILED = 5
    static let JOB_STEP_CANCELLED = 6
    /* ======== LIST OF JOB STEP STATUS ========= */
    
    /* ======== LIST OF JOB STEP STATUS NAME ========= */
    static let JOB_STEP_PENDING_TEXT = "PENDING"
    static let JOB_STEP_INPROGRESS_TEXT = "IN PROGRESS"
    static let JOB_STEP_COMPLETED_TEXT = "COMPLETED"
    static let JOB_STEP_FAILED_TEXT = "FAILED"
    static let JOB_STEP_CANCELLED_TEXT = "CANCELLED"
    /* ======== LIST OF JOB STEP STATUS NAME ========= */
    
    
    
    /* ======== LIST OF SELECTED TABBAR CONTROLLER ========= */
    static let SELECTED_TAB_UNASSIGNED = 0
    static let SELECTED_TAB_ASSIGNED = 1
    static let SELECTED_TAB_ACKNOWLEDGED = 2
    static let SELECTED_TAB_INPROGRESS = 3
    static let SELECTED_TAB_COMPLETE = 4
    //static let SELECTED_TAB_CANCELLED = 4
    static let SELECTED_TAB_FAILED = 5
    static let SELECTED_TAB_SELF_COLLECT = 6
    /* ======== LIST OF SELECTED TABBAR CONTROLLER ========= */
    
    /* ======== STATUS RESPONSE ========= */
    static let STATUS_RESPONSE_SUCCESS = 200
    static let STATUS_RESPONSE_BAD_REQUEST = 400
    static let STATUS_RESPONSE_UNAUTHORIZED = 401
    static let STATUS_RESPONSE_FORBIDDEN = 403
    static let STATUS_RESPONSE_NOT_FOUND = 404
    static let STATUS_RESPONSE_INTERNAL_SERVER_ERROR = 501
    static let STATUS_RESPONSE_SERVER_DOWN = 503
    static let STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT = 504
    /* ======== STATUS RESPONSE ========= */


    /* ======== HOST DOMAIN ========= */
    
    /* ====== STAGING ========= */
//    static let web_api = "http://saas.staging.lds.apptimice.com" //lds
//    static let web_api_auth = "http://saas.staging.auth.apptimice.com" //auth
//    static let web_api_wms = "http://saas.staging.wms.apptimice.com" //wms
//    static let web_api_location = "http://13.250.100.114:3000" //update location only
    /* ====== STAGING ========= */
    
    
    /* ====== PRODUCTION ========= */
    static let web_api = "http://lds-api.taskk.me" //lds
    static let web_api_auth = "http://auth-api.taskk.me" //auth
    static let web_api_wms = "http://wms-api.taskk.me" //wms
    static let web_api_location = "http://52.221.19.244:3000" //update location only
    /* ====== PRODUCTION ========= */
    
    /* ======== HOST DOMAIN ========= */
    
    
    /* ====== LIST OF API ========= */
    static let api_get_orders = "/admin/api/orders?sort=date&start_date=%@&end_date=%@&page=0&take=9999&pagination_by_date=true&order_status=%d"
    static let api_upload_image = "/admin/api/mobile/upload"
    static let api_get_order_status = "/admin/api/order_statuses"
    static let api_get_order_count = "admin/api/orders?count_job=true&start_date=%@&end_date=%@&order_status"
    static let api_get_order_detail = "/api/orders/%@"
    static let api_batch_update_order = "/admin/api/orders/assign/order"
    static let api_get_notes = "/api/notes"
    static let api_get_recipient_types = "/api/recipient_types"
    static let api_get_workers = "/api/workers"
    static let api_transfer = "/api/orders/transfer"
    static let api_get_reason = "/api/reject_reasons"
    static let api_get_search_order = "/admin/api/orders?sort=date&start_date=2017-06-01&end_date=%@&page=0&take=9999&pagination_by_date=true&order_status=%@&with_user=false&search=%@"
    
    static let api_get_profile_worker = "/worker/api/profile/workers"
    
    static let api_get_orders_total = "/admin/api/workers/me/orders?sort=date&start_date=%@&end_date=%@&page=0&take=9999&pagination_by_date=true&order_status=%@"
    static let api_get_driver_list = "/admin/api/workers"

    
    static let api_update_location = "/worker/api/location/update"
    static let api_app_company_settings = "/api/application_company_settings"
    static let api_get_job_step_details = "/api/job_steps/%@"
    
    static let api_upload_image_base64 = "/api/upload_base64"
    static let api_driver_update_location_minute = "/api/locations"
    
    static let api_driver_logout = "/api/logout"
    static let api_driver_validate_username = "/worker/api/validate/username"
    static let api_driver_forget_password = "/worker/api/forgot/password"
    static let api_driver_reset_password = "/worker/api/reset/password"
    
    static let api_get_customer_listing = "/admin/api/customers"
    /* ====== LIST OF API ========= */
    
    
    static var current_date = Date()
    
    /* ===== Function Color for Step ====== */
    static func colorOrderStatusStep (orderStatusStep:Int) -> UIColor {
        var red: CGFloat?
        var green: CGFloat?
        var blue: CGFloat?
        var alpha: CGFloat?
        if(orderStatusStep == Constants.JOB_STEP_PENDING){
            red = 216/255; green = 216/255; blue = 216/255; alpha = 1
        }else if(orderStatusStep == Constants.JOB_STEP_INPROGRESS){
            red = 85/255; green = 178/255; blue = 229/255; alpha = 1
        }else if(orderStatusStep == Constants.JOB_STEP_COMPLETED){
            red = 47/255; green = 174/255; blue = 66/255; alpha = 1
        }else if(orderStatusStep == Constants.JOB_STEP_FAILED){
            red = 213/255; green = 44/255; blue = 54/255; alpha = 1
        }else if(orderStatusStep == Constants.JOB_STEP_CANCELLED){
            red = 255/255; green = 119/255; blue = 121/255; alpha = 1
        }
        
        return UIColor(red: red!, green: green!, blue: blue!, alpha: alpha!)
    }
    /* ===== Function Color for Step ====== */
    
    
    /* ===== Function Color for Step Border ====== */
    static func colorOrderStatusStepBorder (orderStatusStep:Int) -> UIColor {
        var red: CGFloat?
        var green: CGFloat?
        var blue: CGFloat?
        var alpha: CGFloat?
        if(orderStatusStep == Constants.JOB_STEP_PENDING){
            red = 151/255; green = 151/255; blue = 151/255; alpha = 1
        }else if(orderStatusStep == Constants.JOB_STEP_INPROGRESS){
            red = 7/255; green = 76/255; blue = 131/255; alpha = 1
        }else if(orderStatusStep == Constants.JOB_STEP_COMPLETED){
            red = 9/255; green = 101/255; blue = 23/255; alpha = 1
        }else if(orderStatusStep == Constants.JOB_STEP_FAILED){
            red = 133/255; green = 18/255; blue = 18/255; alpha = 1
        }else if(orderStatusStep == Constants.JOB_STEP_CANCELLED){
            red = 206/255; green = 54/255; blue = 54/255; alpha = 1
        }
        
        return UIColor(red: red!, green: green!, blue: blue!, alpha: alpha!)
    }
    /* ===== Function Color for Step ====== */
    
    /* ====== CONVERT HEX CODE TO COLOR ===== */
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    /* ====== CONVERT HEX CODE TO COLOR ===== */
    
    static func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        
        let cal: Calendar = Calendar(identifier: .gregorian)
        
        let newStartDate: Date = cal.date(bySettingHour: 0, minute: 0, second: 0, of: startDate)!
        let newEndDate: Date = cal.date(bySettingHour: 0, minute: 0, second: 0, of: endDate)!

        
        let components = calendar.dateComponents([Calendar.Component.day], from: newStartDate, to: newEndDate)
        return components.day!
    }
    
    /* ====== FORMAT DATE ======= */
    
    /*This func is convert string to date then convert back to string */
    static func formatDateWithTime(date: String) -> String {
        let dateFormatterDate = DateFormatter()
        dateFormatterDate.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterDate.timeZone = TimeZone(abbreviation: "UTC")
        let serverDate: Date = dateFormatterDate.date(from: date)!
        
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "dd MMM yyyy, hh:mm a"
        let newDate: String = dateFormatterString.string(from: serverDate)
        print(newDate)
        
        return newDate
    }
    
    /*This func to get format date either it am or pm */
    static func getFormatDateAMorPM() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        return formatter.string(from: Date())
    }
    
    /* ====== FORMAT DATE ======= */
    
    
    
    
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}



