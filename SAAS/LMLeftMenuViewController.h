//
//  LMMenuViewController.h
//  LMSideBarControllerDemo
//
//  Created by LMinh on 10/11/15.
//  Copyright © 2015 LMinh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LMLeftMenuViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *onlineOffline_lbl;
@property (weak, nonatomic) IBOutlet UISwitch *switchBtn;
@property (weak, nonatomic) IBOutlet UILabel *username_lbl;
- (IBAction)isOnline:(UISwitch*)sender;

@end
