//
//  CreateJobActivity_ViewController.swift
//  SAAS
//
//  Created by Coolasia on 25/6/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit

class CreateJobActivity_ViewController: UIViewController {
    
    var isHaveCustomer: Bool?
    
    @IBOutlet weak var customer_label: UILabel!
    @IBOutlet weak var customer_disclosure: UIImageView!
    
    @IBOutlet weak var customerDetails_label: UILabel!
    
    @IBOutlet weak var customerViewCard: UIView!
    
    @IBOutlet weak var customerViewCardHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var recipientViewCard: UIView!
    
    @IBOutlet weak var recipientViewCardHeightConstraint: NSLayoutConstraint!
    
    /* Open Customer Listing */
    func clickCustomerListing(_ gr: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "ListOfCustomer", bundle: nil)
        let levelController = storyboard.instantiateViewController(withIdentifier: "ListOfCustomer") as! ListOfCustomer_ViewController
        self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.navigationController?.pushViewController(levelController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Setup func for view to make sure user can interact with it */
        let clickCustomerListing = UITapGestureRecognizer(target: self, action: #selector(CreateJobActivity_ViewController.clickCustomerListing(_:)))
        self.customer_label.addGestureRecognizer(clickCustomerListing)
        self.customer_label.isUserInteractionEnabled = true
        self.customer_disclosure.addGestureRecognizer(clickCustomerListing)
        self.customer_disclosure.isUserInteractionEnabled = true
        
        isHaveCustomer = QueryDatabase.checkIsHaveCustomer()
        
        print("isHaveCustomer is \(isHaveCustomer)")
        
        if(isHaveCustomer)!{
            recipientViewCard.isHidden = true
            recipientViewCardHeightConstraint.constant = 0
            recipientViewCard.layoutIfNeeded()
        }else{
            
            customerDetails_label.text = "RECIPIENT DETAILS"
            
            customerViewCard.isHidden = true
            customerViewCardHeightConstraint.constant = 0
            customerViewCard.layoutIfNeeded()
        }
        
        
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "New Job"
        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(CreateJobActivity_ViewController.onTouchCreateButton))
            
            let button = UIButton(type: .system)
            //  button.setImage(UIImage(named: "plus"), for: .normal)
            //  button.sizeToFit()
            button.setTitle("Create", for: .normal)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            button.addTarget(self, action:#selector(CreateJobActivity_ViewController.onTouchCancelButton), for: .touchUpInside)
            // button.tintColor = tintColorDisable
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
           // self.navigationItem.rightBarButtonItem?.isEnabled = false
        
            self.navigationItem.leftBarButtonItem = cancelButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onTouchCancelButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func onTouchCreateButton() {
        dismiss(animated: true, completion: nil)
    }
    

    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    /* ===== Load Everytime ======== */

}
