//
//  QueryDatabase.swift
//  SAAS
//
//  Created by Coolasia on 8/2/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//
//

import Foundation
import SwiftyJSON
import Alamofire
import UIKit
import CoreData

struct QueryDatabase {
    

    /* == GET APP COMPANY SETTINGS DATABASE ====
     This func to know either from assigned to acknowledged need to scan and man power can update job
     */
    
    static func checkNeedToScanAndManPowerCanUpdateAppCompanyDB(currentStatus: Int, toStatus: Int, isNeedScan: Bool) -> Bool {
        let moc = DataController().managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AppCompanySetting")
        var isNeedScanOrManPowerCanUpdate = Bool()
        
        // Add Predicate
        let predicate2 = NSPredicate(format: "enable = 1")
        let predicate3 = NSPredicate(format: "currentStatus = \(currentStatus)")
        let predicate4 = NSPredicate(format: "toStatus = \(toStatus)")
        
        if(isNeedScan){
            let predicate1 = NSPredicate(format: "isScanRequired = 1")
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3, predicate4])
        }else{
            let predicate1 = NSPredicate(format: "isManPowerCanUpdate = 1")
            fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2, predicate3, predicate4])
        }
        
        do {
            let records = try moc.fetch(fetchRequest) as! [NSManagedObject]
            
            if records.count > 0
            {
                for record in records {
                    isNeedScanOrManPowerCanUpdate = true
                }
            }else{
                // Not exist so it mean not need to scan
                isNeedScanOrManPowerCanUpdate = false
                
            }
            
        } catch {
            fatalError("errorFetch: \(error)")
        }
        
        return isNeedScanOrManPowerCanUpdate
        
    }
    
    
    
    /* == GET LIST ORDER STATUS DATABASE ====
     This func to get listing order status. To set for listing and batch update
     */
    
    static func getOrderStatusIDFromOrderStatusDB(orderStatusName: String) -> Int {
        let moc = DataController().managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderStatus")
        var orderStatusId: Int?
        
        // Add Predicate
        let predicate1 = NSPredicate(format: "orderStatusName == %@", orderStatusName)
        
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
        
        do {
            let records = try moc.fetch(fetchRequest) as! [NSManagedObject]
            
            if records.count > 0
            {
                for record in records {
                    orderStatusId = record.value(forKey: "orderStatusId") as? Int
                    print("orderStatusIdCoreData \(orderStatusId)")
                }
            }else {
                orderStatusId = 0
            }
            
        } catch {
            fatalError("errorFetch: \(error)")
        }
        
        return orderStatusId!
        
    }
    
    
    /*Based on orderStatusId , get the orderStatusName */
    static func getOrderStatusNameFromOrderStatusDB(orderStatusId: Int) -> String {
        let moc = DataController().managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderStatus")
        var orderStatusName: String?
        
        // Add Predicate
        let predicate1 = NSPredicate(format: "orderStatusId = \(orderStatusId)")
        
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1])
        
        do {
            let records = try moc.fetch(fetchRequest) as! [NSManagedObject]
            
            if records.count > 0
            {
                for record in records {
                    orderStatusName = record.value(forKey: "orderStatusName") as? String
                    print("orderStatusNameDB \(orderStatusName)")
                }
            }else {
                orderStatusName = "Unknown status"
            }
            
        } catch {
            fatalError("errorFetch: \(error)")
        }
        
        return orderStatusName!
        
    }
    
    
   /* To check either this company have customer or not. If have customer , need show customer page but it dont have , hide it and show reciepnt page */
    static func checkIsHaveCustomer() -> Bool {
        let moc = DataController().managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AppCompanySetting")
        var isHaveCustomer = Bool()
        var is_customer_required = String()
            is_customer_required = "is_customer_required"
        
        // Add Predicate
        let predicate1 = NSPredicate(format: "rule == %@", is_customer_required)
        let predicate2 = NSPredicate(format: "enable = 1")
        
        
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
        
        do {
            let records = try moc.fetch(fetchRequest) as! [NSManagedObject]
            
            if records.count > 0
            {
                for record in records {
                    isHaveCustomer = true
                }
            }else{
                // Not exist so it mean dont have customer
                isHaveCustomer = false
                
            }
            
        } catch {
            fatalError("errorFetch: \(error)")
        }
        
        return isHaveCustomer
        
    }
    
    
    
}



