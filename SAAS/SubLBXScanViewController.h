//
//  SubLBXScanViewController.h
//
//  github:https://github.com/MxABC/LBXScan
//  Created by lbxia on 15/10/21.
//  Copyright © 2015年 lbxia. All rights reserved.
//

#import "LBXAlertAction.h"
#import "LBXScanViewController.h"

@protocol NewScanDelegate <NSObject>   //define delegate protocol
- (void) scanDelegateMethod: (NSMutableArray *) unscannedItems isTransfer: (Boolean) isTransfer;  //define delegate method to be implemented within another class
@end //end protocol
//继承LBXScanViewController,在界面上绘制想要的按钮，提示语等
@interface SubLBXScanViewController : LBXScanViewController<UITableViewDelegate,UITableViewDataSource>



#pragma mark -模仿qq界面

@property (nonatomic, assign) BOOL isQQSimulator;

/**
 @brief  扫码区域上方提示文字
 */
@property (nonatomic, strong) UILabel *topTitle;

#pragma mark --增加拉近/远视频界面
@property (nonatomic, assign) BOOL isVideoZoom;

#pragma mark - 底部几个功能：开启闪光灯、相册、我的二维码
//底部显示的功能项
@property (nonatomic, strong) UIView *bottomItemsView;


@property (nonatomic, strong) UITableView *tableView;

//相册
@property (nonatomic, strong) UIButton *btnPhoto;
//闪光灯
@property (nonatomic, strong) UIButton *btnFlash;
@property (nonatomic,retain) UILabel *remainingLabel;
//我的二维码
@property (nonatomic, strong) UIButton *btnMyQR;
@property (nonatomic,retain) NSMutableArray *orderScanList;
@property (nonatomic,retain) NSMutableArray *initialOrderScanList;

@property (nonatomic,assign) Boolean isTransfer;
@property (nonatomic,assign) Boolean isInProgress;
@property (nonatomic, weak) id <NewScanDelegate> delegate; //define MyClassDelegate as delegate
    
@property (nonatomic, strong) UIBarButtonItem *button;
@property (nonatomic, strong) UIBarButtonItem *buttonFlash;
@property (nonatomic,assign) Boolean flashTurnOn;
    
    @property (nonatomic, strong) UIImage *imageFlash;




@end
