//
//  JobCardTableViewCell3.swift
//  LEEWAY
//
//  Created by Coolasia on 15/9/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import MarqueeLabel

class JobCardTableViewCell3: UITableViewCell {
    
    
    @IBOutlet weak var cardJob: UIView!
    @IBOutlet weak var lbl_customerName: MarqueeLabel!
    
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_orderStatus: UILabel!
    @IBOutlet weak var lbl_dropOffDate: UILabel!
    @IBOutlet weak var lbl_doReferenceNumber: UILabel!
    @IBOutlet weak var layoutCustomerName: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(roundedRect:layoutCustomerName.bounds,
                                byRoundingCorners:[.topLeft, .topRight],
                                cornerRadii: CGSize(width: 10, height:  10))
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        layoutCustomerName.layer.mask = maskLayer
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    

}



