//
//  OrderAttemptTableViewCell.swift
//  LEEWAY
//
//  Created by Coolasia on 25/9/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit

class OrderAttemptTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateOrderAttempt: UILabel!
    
    @IBOutlet weak var titleOrderAttempt: UILabel!
    @IBOutlet weak var cell: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    


}
