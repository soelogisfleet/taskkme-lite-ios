//
//  PermissionOnBoardingViewController.swift
//  SAAS
//
//  Created by Coolasia on 30/4/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import CoreLocation
import SCLAlertView
import DeviceKit

class PermissionOnBoardingViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var cameraPermissionLbl: UILabel!
    
    @IBOutlet weak var permissionTitleLabel: UILabel!
    @IBOutlet weak var galleryPermissionLbl: UILabel!
    
    @IBOutlet weak var locationPermissionLbl: UILabel!
    
    @IBOutlet weak var permissionsWordTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var locationAccessTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var galleryAccessTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cameraAcessTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cameraIconTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonGrantPermission: UIButton!
    
    @IBOutlet weak var locationIconTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var grantPermissionButtonBottomConstraint: NSLayoutConstraint!
    var locationManager: CLLocationManager?
    
    var locationPermission: Int!
    
    lazy var defaults:UserDefaults = {
        return .standard
    }()
    
    
//    // MARK: - Various lazy managers
//    lazy var locationManager:CLLocationManager = {
//        let lm = CLLocationManager()
//        lm.delegate = self
//        return lm
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let device = Device()
        
        if device.isPad {
            // Do something
            if device != .simulator(.iPadPro12Inch) {
                // Do something
                
                permissionsWordTopConstraint.constant = 25
                cameraIconTopConstraint.constant = 30
                cameraAcessTopConstraint.constant = 30
                galleryAccessTopConstraint.constant = 30
                locationAccessTopConstraint.constant = 30
                locationIconTopConstraint.constant = 30
                grantPermissionButtonBottomConstraint.constant = 20
                
                self.permissionTitleLabel.layoutIfNeeded()
                self.cameraPermissionLbl.layoutIfNeeded()
                self.galleryPermissionLbl.layoutIfNeeded()
                self.locationPermissionLbl.layoutIfNeeded()
                self.buttonGrantPermission.layoutIfNeeded()
                self.view.layoutIfNeeded()
            }
            
            
            
            
            
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//
//    func checkPhotoLibraryPermission() -> Int {
//        let status = PHPhotoLibrary.authorizationStatus()
//        switch status {
//        case .authorized:
//            photoPermission = 1
//            return photoPermission
//        case .denied, .restricted :
//            photoPermission = -1
//            return photoPermission
//        case .notDetermined:
//            photoPermission = 0
//            return photoPermission
//        }
//    }
//
//
//    public func statusCamera() -> Int {
//        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
//        switch status {
//        case .authorized:
//            cameraPermission = 1
//            return cameraPermission
//        case .restricted, .denied:
//            cameraPermission = -1
//            return cameraPermission
//        case .notDetermined:
//            cameraPermission = 0
//            return cameraPermission
//        }
//    }
    
    public func statusLocationAlways() -> Int {
        guard CLLocationManager.locationServicesEnabled() else {
            return 0
        }
        
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways:
            locationPermission = 1
            return locationPermission
        case .restricted, .denied:
            locationPermission = -1
            return locationPermission
        case .authorizedWhenInUse:
            //ocationPermission = 1
           // return locationPermission
            
            // Curious why this happens? Details on upgrading from WhenInUse to Always:
            // [Check this issue](https://github.com/nickoneill/PermissionScope/issues/24)
            if defaults.bool(forKey: ConstantBoard.NSUserDefaultsKeys.requestedInUseToAlwaysUpgrade) {
                locationPermission = 1
                return locationPermission
            } else {
                locationPermission = 1
                return locationPermission
            }
        case .notDetermined:
            locationPermission = 0
            return locationPermission
        }
    }
    
    
    @IBAction func grantPermission(_ sender: Any) {
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo,
                                      completionHandler: { granted in
                                        if granted {
                                            print("Granted access to")
                                            self.photoGallery()
                                            
                                        } else {
                                            print("Denied access to")
                                            self.photoGallery()
                                            DispatchQueue.main.async {
                                               // self.showUIAlertDialog(permissionCameraPhotoLocation: 1)
                                            }
                                            
        
                                        }
        })
        
        
    }
    
    func location(){
        
        if (CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .denied){
            defaults.set(true, forKey: ConstantBoard.NSUserDefaultsKeys.requestedInUseToAlwaysUpgrade)
            defaults.synchronize()
            self.locationManager = CLLocationManager()
            self.locationManager?.requestAlwaysAuthorization()
         }
        
        let status = statusLocationAlways()
        switch status {
        case 1:
            print("page loggin")
            DispatchQueue.main.async {
                var window: UIWindow? = UIApplication.shared.keyWindow
                var stryBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = stryBoard.instantiateInitialViewController()
            }
            
        case -1:
            print("popup allow")
            DispatchQueue.main.async {
                var window: UIWindow? = UIApplication.shared.keyWindow
                var stryBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = stryBoard.instantiateInitialViewController()
            }
        case 0:
            print("call location again")
            self.location()
            //                DispatchQueue.main.async {
            //                    var window: UIWindow? = UIApplication.shared.keyWindow
            //                    var stryBoard = UIStoryboard(name: "Login", bundle: nil)
            //                    window?.rootViewController = stryBoard.instantiateInitialViewController()
        //                }
        default:
            break
            
        }
        
        
    }
    
    func photoGallery(){
        PHPhotoLibrary.requestAuthorization({ status in
            switch status {
            case .authorized:
                print("cameraAction tapped1")
                self.location()
            case .denied, .restricted:
                print("cameraAction tapped2")
                self.location()
                DispatchQueue.main.async {
                   // self.showUIAlertDialog(permissionCameraPhotoLocation: 2)
                }
                
            case .notDetermined:
                print("cameraAction tapped3")
               // self.location()
               // self.photoGallery()
            }
        })
    }
    
    func showUIAlertDialog(permissionCameraPhotoLocation: Int){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("Settings") {
            print("YES button tapped")
            DispatchQueue.main.async {
                if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(settingsURL)
                }
            }
        }
        alertView.addButton("Cancel") {
            print("NO button tapped")
        }
        
        var subTitle = ""
        var titleString = ""
        
        switch permissionCameraPhotoLocation {
        case 1:
            subTitle = "Enable camera access will allow you to snap photos and use scanner barcode"
            titleString = "Camera Access"
        case 2:
            subTitle = "Enable photo access will allow you to use photos gallery as proof of delivery"
            titleString = "Photo Access"
        case 3:
            subTitle = "Enable location services will improve location accurancy"
            titleString = "Location Services"
        default:
            break
        }
        
        alertView.showEdit(titleString, subTitle: subTitle)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
