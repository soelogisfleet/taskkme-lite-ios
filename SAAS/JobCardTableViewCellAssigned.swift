//
//  JobCardTableViewCellAssigned.swift
//  SAAS
//
//  Created by Coolasia on 2/5/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import UIKit
import MarqueeLabel
import M13Checkbox

class JobCardTableViewCellAssigned: UITableViewCell {
    
    @IBOutlet weak var checkboxHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkboxWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardJob: UIView!
    
    @IBOutlet weak var cell: UIView!
    @IBOutlet weak var lbl_customerName: MarqueeLabel!
    @IBOutlet weak var lbl_dropOffAdrress: UILabel!
    @IBOutlet weak var lbl_doReferenceNumber: UILabel!
    @IBOutlet weak var lbl_dropOffDate: UILabel!
    
    @IBOutlet weak var lbl_step_status: UILabel!
    
    @IBOutlet weak var lbl_dropOffTime: UILabel!
    @IBOutlet weak var checkbox: M13Checkbox!
    
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var checkBoxView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cardJob.layer.borderWidth = 1
        self.cardJob.layer.cornerRadius = 10
        self.cardJob.layer.borderColor = UIColor.clear.cgColor
        self.cardJob.layer.masksToBounds = true
        
        self.layer.shadowOpacity = 0.20
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 8
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    override func prepareForReuse() {
//        
//        if checkbox != nil
//        {
//            checkbox.setCheckState(M13Checkbox.CheckState.unchecked, animated: false)
//            
//        }
//        super.prepareForReuse()
//    }

}
