//
//  AssignedViewController.swift
//  LEEWAY
//
//  Created by Coolasia on 18/9/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Gloss
import SwiftSpinner
import MarqueeLabel
import CoreData
import AnimatableReload
import M13Checkbox
import SCLAlertView

class AssignedViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,UISearchBarDelegate {
    
    var jobList: [Job] = []
    @IBOutlet weak var calendarOuterviewHeight: NSLayoutConstraint!
    var dropOfWorkerIdArray: [Int] = []
    var selectedDate: Date = Date()
    var searchBar = UISearchBar()
    var isEditMode = Bool()
    let searchButton = UIButton(type: .custom)
    var itemTrackingSearch = String()
    let moc = DataController().managedObjectContext
    var request: URLRequest!
    var isManPowerCanUpdateProcess = Bool()
    var isManPower = Bool()
    var orderStatusIDFromOrderStatusDB: Int?
    var selectedIndexTabStatus = Int()
    @IBOutlet weak var assignBottomPanel: UIView!
    
    @IBOutlet weak var lblBtnAccept: UIButton!
    
    @IBOutlet weak var btnSelectJob: UIButton!
    @IBOutlet weak var calendar_outerview: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbl_selectedDate: UILabel!
    
    @IBOutlet weak var btnAcceptHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomLayoutHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var reachInternetConnection = Reachability()
    var jobListAfterSelected: [Int] = []
    var isJobHaveSelected = Bool()

    @IBOutlet weak var assignedButton: UIButton!
    
    @IBAction func btnAssignClick(_ sender: Any) {
        
        
        self.jobListAfterSelected.removeAll()
        for i in 0..<jobList.count{
            
            if(self.jobList[i].isSelected == true)
            {
                self.isJobHaveSelected = true
                self.jobListAfterSelected.append(jobList[i].orderId)
            }
        }
        
        
        
        let storyboard = UIStoryboard(name: "DriverList", bundle: nil)
        let levelController = storyboard.instantiateViewController(withIdentifier: "DriverList") as! DriverListViewController
        levelController.selectedJobIdArray = self.jobListAfterSelected
        
        
//        self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)

        
        self.navigationController?.pushViewController(levelController, animated: true)
    }
    
    @IBAction func btnSelectJobClick(_ sender: Any) {
        self.navigationController?.navigationBar.topItem?.title="Select Job(s)"
        isEditMode = true
        
        var tabBarController: UITabBarController
        tabBarController = self.parent?.parent as!UITabBarController
       // tabBarController.tabBar.isHidden = true
        
        
        
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = nil
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "close"), for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cancelButton.addTarget(self, action:#selector(self.cancelSelectJobLayout), for: .touchUpInside)
        let cancelBarButton = UIBarButtonItem(customView: cancelButton)
        self.navigationController?.navigationBar.topItem?.setRightBarButtonItems(nil, animated: true)
        self.navigationController?.navigationBar.topItem?.setRightBarButton(cancelBarButton, animated: true)
        
        
        self.navigationController?.navigationBar.topItem?.setLeftBarButton(nil, animated: true)

        let frame = tabBarController.tabBar.frame
        let height = frame.size.height
        var offsetY = (tabBarController.tabBar.frame.origin.y < UIScreen.main.bounds.height ? -height : height)
        print ("offsetY = \(offsetY)")
        
        // zero duration means no animation
        let duration:TimeInterval = (true ? 0.3 : 0.0)
        
        // animate tabBar
        if frame != nil {
            UIView.animate(withDuration: duration) {
                tabBarController.tabBar.frame = tabBarController.tabBar.frame.offsetBy(dx: 0, dy: -offsetY)
                self.view.frame = self.CGRectMake(0, 0, self.view.frame.width, self.view.frame.height - offsetY)
                self.view.setNeedsDisplay()
                self.view.layoutIfNeeded()
                
                self.tableViewHeightConstraint.constant = self.view.frame.height + offsetY
                self.tableView.layoutIfNeeded()
                
                self.calendarOuterviewHeight.constant = 0
                self.calendar_outerview.layoutIfNeeded()
                self.calendar_outerview.isHidden = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSelectJobLayout"), object: nil, userInfo: nil)

                self.tableView.reloadData()
                
               self.assignedButton.isHidden = false
               self.assignBottomPanel.isHidden = true

                return
            }
        }
    }
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
   
    
    @IBAction func btnAcceptJob(_ sender: Any) {
        
        
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Assigned Accept Job Internet connection OK")
            if(self.jobList.count > 0){
                let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
                if(isOnline){
                    
                    self.isJobHaveSelected = false
                    self.jobListAfterSelected.removeAll()
                    
                    for i in 0..<jobList.count{
                        
                        if(self.jobList[i].isSelected == true)
                        {
                            self.isJobHaveSelected = true
                            self.jobListAfterSelected.append(jobList[i].orderId)
                        }
                    }
                    
                    if(self.isJobHaveSelected == true){
                        if(isManPower){
                            if(isManPowerCanUpdateProcess){
                                self.lblBtnAccept.isEnabled = false
                                changedStatusToAcknowledge()
                            }else{
                                // man power cannot update
                                var alert = UIAlertView(title: "Cannot Update", message: "Man power cannot update", delegate: nil, cancelButtonTitle: "OK")
                                alert.show()
                            }
                        }else{
                            // its Driver
                            self.lblBtnAccept.isEnabled = false
                            changedStatusToAcknowledge()
                        }
                    }else{
                        let alert = UIAlertController(title: "Cannot accept jobs", message: "Please select at least one job.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }else{
                    // tak online
                    var alert = UIAlertView(title: "Cannot Update", message: "Make sure you availibality is online before update job", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
                
                

            }else{
                
            }
        } else {
            print("Assigned Accept Job Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
//        if reachInternetConnection.isConnectedToNetwork() == true {
//            print("Assigned Accept Job Internet connection OK")
//            if(dropOfWorkerIdArray.count > 0){
//                let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
//                if(isOnline){
//                    if(isManPower){
//                        if(isManPowerCanUpdateProcess){
//                            self.lblBtnAccept.isEnabled = false
//                            changedStatusToAcknowledge()
//                        }else{
//                            // man power cannot update
//                            var alert = UIAlertView(title: "Cannot Update", message: "Man power cannot update", delegate: nil, cancelButtonTitle: "OK")
//                            alert.show()
//                        }
//                    }else{
//                        // its Driver
//                        self.lblBtnAccept.isEnabled = false
//                        changedStatusToAcknowledge()
//                    }
//                }else{
//                    // tak online
//                    var alert = UIAlertView(title: "Cannot Update", message: "Make sure you availibality is online before update job", delegate: nil, cancelButtonTitle: "OK")
//                    alert.show()
//                }
//
//            }else{
//                let alert = UIAlertController(title: "Error", message: "No order to accept", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
//        } else {
//            print("Assigned Accept Job Internet connection FAILED")
//            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
//            alert.show()
//        }
    }
    
    
    
    @IBAction func leftMenuButtonTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LEFT_MENU_TAPPED"), object: nil, userInfo: nil)
        
    }
    
    /* ====== FUNC SHOW ALERT DIALOG SUCCESS UPDATE ======= */
    func showUIAlertDialog(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") {
            print("YES button tapped")
            print("date after accept \(self.selectedDate)")
            self.reloadJobList(date: self.selectedDate.string(format: "yyyy-MM-dd"))
        }
        
        alertView.showSuccess(StringMessage.assignedToacknowledgedTitle, subTitle: StringMessage.assignedToacknowledgedMessage)
    }
    
    func changedStatusToAcknowledge(){
        print("Hello1!")
        
        var orderStatusIDFromOrderStatusDBToAcknowledged: Int = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ACKNOWLEDGED_TEXT)
        
        let batchOrder: BatchOrder = BatchOrder()
        batchOrder.orderIds.removeAll()
        batchOrder.orderStatus = String(describing: orderStatusIDFromOrderStatusDBToAcknowledged)
        
        for jobCanAccept in jobListAfterSelected{
             print("orderId, \(jobCanAccept)")
             batchOrder.orderIds.append(String(jobCanAccept))
            
        }
        
        let para:NSMutableDictionary = NSMutableDictionary()
        
    
        para.setValue(batchOrder.orderIds, forKey: "id")
        para.setValue(batchOrder.orderStatus, forKey: "order_status_id")
        
       
        let dataPara:NSMutableDictionary = NSMutableDictionary()
        let dataArray:NSMutableArray = NSMutableArray()
        
        dataArray.add(para)
        
        dataPara.setValue(dataArray, forKey: "data")
        
        SwiftSpinner.show("Submitting")
        
        /* ====== UPDATE JOB STATUS ======= */
        AlamofireRequest.batchUpdate(parameters: dataPara as NSDictionary) { responseObject, statusCode, error in
            
            SwiftSpinner.hide()
            self.lblBtnAccept.isEnabled = true
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    
                    let swiftyJsonVar = JSON(responseObject)
                    if(swiftyJsonVar["status"].exists() && swiftyJsonVar["result"].exists()){
                        if(swiftyJsonVar["status"].bool == true){
                            //succesfully update order, show an success message
                            DispatchQueue.main.async {
                                var tabBarController: MainVC
                                
                                tabBarController = self.parent?.parent as!MainVC
                                tabBarController.refreshTabBarItemCount()
                                
                                self.showUIAlertDialog()
                            }

                        }
                    }
                }
                
            }else{
                //error code
                SwiftSpinner.hide()
                if(responseObject == nil || statusCode == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== UPDATE JOB STATUS ======= */
    }
    
    func calendarDaySelected(notification: NSNotification){
        
        var tabBarController: UITabBarController
        tabBarController = self.parent?.parent as!UITabBarController
        selectedIndexTabStatus = tabBarController.selectedIndex
        print("selectedIndexTabStatus \(selectedIndexTabStatus)")
        
        if(selectedIndexTabStatus == Constants.SELECTED_TAB_ASSIGNED || selectedIndexTabStatus == Constants.SELECTED_TAB_UNASSIGNED ){
            if let selectedDate = notification.userInfo?["date"] as? Date {
                print(selectedDate)
                
                if reachInternetConnection.isConnectedToNetwork() == true {
                    print("Assinged 22 connection OK")
                    reloadJobList(date: selectedDate.string(format: "yyyy-MM-dd"))
                } else {
                    print("Assinged 22 Internet connection FAILED")
                    var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
                
            }
        }
  
    }
    
    
    func stringFormatDate(dropOffDate: String,dateformat: String, dateString: String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = dateformat
        let dateNew = formatter.date(from:dropOffDate)
        let returnDateString = dateNew?.string(format: dateString)
        return returnDateString!
    }
    
    func reloadJobList(date: String){
        
//        if(isEditMode)
//        {
//            self.cancelSelectJobLayout()
//
//        }
        var tabBarController: UITabBarController
        tabBarController = self.parent?.parent as!UITabBarController
        selectedIndexTabStatus = tabBarController.selectedIndex
        
        
        let dateString = self.stringFormatDate(dropOffDate: date, dateformat: "yyyy-MM-dd", dateString: "dd MMMM yyyy")
        
        print("dateString \(dateString)")
        
        lbl_selectedDate.text = dateString
        var message = String()

        if(selectedIndexTabStatus == Constants.SELECTED_TAB_UNASSIGNED){
             orderStatusIDFromOrderStatusDB = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.UNASSIGNED_TEXT)
            
            message =  String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
            SwiftSpinner.show("Fetching Unassigned Job List...")
        }
        else{
            orderStatusIDFromOrderStatusDB = QueryDatabase.getOrderStatusIDFromOrderStatusDB(orderStatusName: Constants.ASSIGNED_TEXT)
            
             message   = String(format: Constants.api_get_orders, arguments: [date,date,orderStatusIDFromOrderStatusDB!])
            
            SwiftSpinner.show("Fetching Assigned Job List...")
            
        }
        

        
     
        // SwiftSpinner.show("Failed to connect, waiting...", animated: false)
        
        
        /* ====== FETCH JOB ======= */
        AlamofireRequest.fetchJobList(url: message) { responseObject, statusCode, error in
            SwiftSpinner.hide()
            
            if(statusCode == Constants.STATUS_RESPONSE_SUCCESS){
                if(responseObject == nil)
                {
                    
                }else{
                    self.jobList.removeAll()
                    self.dropOfWorkerIdArray.removeAll()
                    
                    let swiftyJsonVar = JSON(responseObject)
                    let resultJson = swiftyJsonVar["result"]
                    
                    for subJson in resultJson.array! {
                        for jobsJson  in subJson["jobs"].array! {

                        for jobJson  in jobsJson["job"].array! {
                            
//                                print("resourceD \(UserDefaults.standard.value(forKey: "resourseID")!)")
                            
                                let drop_off_worker_id = jobJson["drop_off_worker_id"].intValue
//                                let resourceID = UserDefaults.standard.value(forKey: "resourseID") as! Int
//
//                                if(drop_off_worker_id == resourceID){
//                                    self.dropOfWorkerIdArray.append(jobJson["id"].intValue)
//                                }
                                self.dropOfWorkerIdArray.append(jobJson["id"].intValue)

                                print("dropOfWorkerIdArray \(self.dropOfWorkerIdArray)")
                                
                                if let job = Job(json: jobJson) {
                                    self.jobList.append(job)
                                    
                                }
                                
                            
                        }
                        }
                    }
                    
                    AnimatableReload.reload(tableView: self.tableView, animationDirection: "down")
                    
                    // SwiftSpinner.hide()
                    var tabBarController: UITabBarController
                    
                    tabBarController = self.parent?.parent as!UITabBarController
                    if(self.jobList.count != 0)
                    {
                        tabBarController.tabBar.items?[0].badgeValue = String(self.jobList.count)   // this will add "1" badge to
                        
                    }
                    else{
                        tabBarController.tabBar.items?[0].badgeValue = nil
                    }
                    
                    if(self.jobList.count == 0){
                        // self.lblBtnAccept.titleLabel?.textColor = UIColor.lightGray
                        print("innnn111")



                        if(self.selectedIndexTabStatus == Constants.SELECTED_TAB_UNASSIGNED)
                        {
                            self.tableViewHeightConstraint.constant = self.tableViewHeightConstraint.constant + self.bottomLayoutHeightConstraint.constant

                        }
                        else{
                            self.tableViewHeightConstraint.constant = self.tableViewHeightConstraint.constant + self.btnAcceptHeightConstraint.constant
                        }




                        self.tableView.layoutIfNeeded()

//                        self.btnAcceptHeightConstraint.constant = 0
//                        self.lblBtnAccept.layoutIfNeeded()
                        self.lblBtnAccept.isHidden = true

//                        self.bottomLayoutHeightConstraint.constant = 0
//                        self.assignBottomPanel.layoutIfNeeded()
                        self.assignBottomPanel.isHidden = true

                    }else{
                        print("innnn")

                        if(self.selectedIndexTabStatus == Constants.SELECTED_TAB_UNASSIGNED)
                        {
//                            self.bottomLayoutHeightConstraint.constant = 40
//                            self.assignBottomPanel.layoutIfNeeded()
                            self.assignBottomPanel.isHidden = false

                            self.lblBtnAccept.isHidden = true

                        }
                        else{
//                            self.btnAcceptHeightConstraint.constant = 40
//                            self.lblBtnAccept.layoutIfNeeded()
                            self.lblBtnAccept.isHidden = false


                            self.assignBottomPanel.isHidden = true
                        }







                                           self.tableViewHeightConstraint.constant = self.tableView.contentSize.height - self.bottomLayoutHeightConstraint.constant
                                            self.tableView.layoutIfNeeded()

                    }
                    
                    //self.tableView.reloadData()
                    
                }
                
            }else{
                SwiftSpinner.hide()
                if(responseObject == nil)
                {
                    self.alert_popup(title: "Server Error", message: "We are currently unable to connect to server. Please try again later or contact us at hello@taskk.me", unauthorizedNeedToLoginPage: false)
                }else{
                    let swiftyJsonVar = JSON(responseObject)
                    let errorMessage = ErrorHandlingResponseCode.getErrorTitleAndErrorContext(statusCode: statusCode!, swiftyJsonVar: swiftyJsonVar)
                    self.alert_popup(title: errorMessage.errorTitle, message: errorMessage.message, unauthorizedNeedToLoginPage: errorMessage.unauthorizedNeedToLoginPage)
                }
            }
        }
        /* ====== FETCH JOB ======= */
        
    }
    
    // Show when no data exist
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Click Refresh button or Select another date."
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "No Order on this date."
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.navigationController?.isNavigationBarHidden = true
        
        self.lblBtnAccept.isEnabled = true

//        let height: CGFloat = 100 //whatever height you want
//        let bounds = self.navigationController!.navigationBar.bounds
//        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + height)
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        self.navigationItem.titleView=nil
        
        var tabBarController: UITabBarController
        tabBarController = self.parent?.parent as!UITabBarController
        var selectedIndexTabStatus = tabBarController.selectedIndex
        print("selectedIndexTabStatus \(selectedIndexTabStatus)")
        
        if(selectedIndexTabStatus == 0){
            self.navigationController?.navigationBar.topItem?.title="Unassigned"
        }else{
            self.navigationController?.navigationBar.topItem?.title="Assigned"

        }
        
        

        self.navigationController?.navigationBar.barTintColor = Constants.hexStringToUIColor(hex:Constants.NAV_BAR_COLOR)
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        //let isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
       // print("printisOnline \(isOnline)")
        isManPowerCanUpdateProcess = QueryDatabase.checkNeedToScanAndManPowerCanUpdateAppCompanyDB(currentStatus: 2, toStatus: 6, isNeedScan: false)
        print("isManPowerCanUpdateProcess \(isManPowerCanUpdateProcess)")
        
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Assigned 11 Internet connection OK")
            
            if let firstTimeUserOnBoarding = UserDefaults.standard.object(forKey: "fromPushNotification") {
                let fromPushNotification = UserDefaults.standard.value(forKey: "fromPushNotification") as! Bool
                
                
                
                if(fromPushNotification){
                    
                    UserDefaults.standard.setValue(false, forKey: "fromPushNotification")
                    
                    if let orderIDPushNotification1 = UserDefaults.standard.string(forKey: "orderIDPushNotification"){
                        let orderIDPushNotification = UserDefaults.standard.value(forKey: "orderIDPushNotification") as! Int
                        
                        if let orderIDPushNotification1 = UserDefaults.standard.string(forKey: "orderNumberPushNotification"){
                            let orderNumberPushNotification = UserDefaults.standard.value(forKey: "orderNumberPushNotification") as! String
                            
                            let storyboard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
                            let levelController = storyboard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as! OrderDetailsManyStepViewController
                            levelController.orderID = orderIDPushNotification
                            levelController.orderNumber = orderNumberPushNotification
                            
                            self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                            self.navigationController?.pushViewController(levelController, animated: true)
                        }else{
                            var alert = UIAlertView(title: "The Order dont have Reference Number", message: "Cannot open order detail page.", delegate: nil, cancelButtonTitle: "OK")
                            alert.show()
                        }
                        
                        
                    }else{
                        var alert = UIAlertView(title: "The Order dont have Order ID", message: "Cannot open order detail page.", delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                    }
                    
                    
                    
                }else{
                    reloadJobList(date: Constants.current_date.string(format: "yyyy-MM-dd"))
                }
                
                
            }else{
                UserDefaults.standard.setValue(false, forKey: "fromPushNotification")
                reloadJobList(date: Constants.current_date.string(format: "yyyy-MM-dd"))
            }
            
        } else {
            print("Assigned 11 Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.tableView.estimatedRowHeight = 164
//        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        
        // check when job empty or not empty
       
        
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SHOW_CALENDAR"), object: nil, userInfo: nil)
    }
    
    func reloadJobAfterNotification(notification: NSNotification){
        
        //  Converted to Swift 4 by Swiftify v4.1.6680 - https://objectivec2swift.com/
        var currentViewController: UIViewController? = navigationController?.visibleViewController
        if currentViewController == self {
            print("reloadJobAfterNotification assigned")
            var tabBarController: UITabBarController
            tabBarController = self.parent?.parent as!UITabBarController
            var selectedIndexTabStatus = tabBarController.selectedIndex
            print("selectedIndexTabStatus \(selectedIndexTabStatus)")
            
            if(selectedIndexTabStatus == 0){
                reloadJobList(date: Constants.current_date.string(format: "yyyy-MM-dd"))
            }else{
                NotificationCenter.default.post(name: NSNotification.Name("RELOAD_JOB_AFTER_NOTIFICATION_OTHER"), object: self)
            }
        } else {
            print("not inside this page asssigned")
        }
        
    }
    
    
    func cancelSelectJobLayout(){
        
        isEditMode = false
        let menuButton = UIButton(type: .custom)
        menuButton.setImage(UIImage(named: "btn_left_menu"), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        menuButton.addTarget(self, action:#selector(MainVC.leftBarButtonMethod), for: .touchUpInside)
        let menuBarButton = UIBarButtonItem(customView: menuButton)
        
        //button search
        
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchButton.addTarget(self, action:#selector(MainVC.createSearchBar), for: .touchUpInside)
        let searchBarButton = UIBarButtonItem(customView: searchButton)
        //button search
        
        
        let refreshButton = UIButton(type: .custom)
        refreshButton.setImage(UIImage(named: "refresh"), for: .normal)
        refreshButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        refreshButton.addTarget(self, action:#selector(MainVC.refreshJob), for: .touchUpInside)
        let refreshBarButton = UIBarButtonItem(customView: refreshButton)
        
                self.navigationController?.navigationBar.topItem?.setLeftBarButton(menuBarButton, animated: true)
            self.navigationController?.navigationBar.topItem?.setRightBarButtonItems([refreshBarButton,searchBarButton], animated: true)
        
        var tabBarController: UITabBarController
        tabBarController = self.parent?.parent as!UITabBarController

        
        let frame = tabBarController.tabBar.frame
        let height = frame.size.height
        var offsetY = (tabBarController.tabBar.frame.origin.y < UIScreen.main.bounds.height ? -height : height)
        print ("offsetY = \(offsetY)")
        
        // zero duration means no animation
        let duration:TimeInterval = (true ? 0.3 : 0.0)
        
        // animate tabBar
        if frame != nil {
            UIView.animate(withDuration: duration) {
                tabBarController.tabBar.frame = tabBarController.tabBar.frame.offsetBy(dx: 0, dy: -offsetY)
                self.view.frame = self.CGRectMake(0, 0, self.view.frame.width, self.view.frame.height - offsetY)
                self.view.setNeedsDisplay()
                self.view.layoutIfNeeded()
                
                self.tableViewHeightConstraint.constant = self.view.frame.height - offsetY
                self.tableView.layoutIfNeeded()
                
                self.calendarOuterviewHeight.constant = 37
                self.calendar_outerview.layoutIfNeeded()
                self.calendar_outerview.isHidden = false
                self.assignBottomPanel.isHidden = false
                self.assignedButton.isHidden = true
                self.tableView.reloadData()

//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showSelectJobLayout"), object: nil, userInfo: nil)
                
                return
            }
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignedButton.isHidden = true
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        self.view.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        self.tableView.backgroundView = nil;
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        print("tap \(tap)")
        print("calendar outer view \(self.calendar_outerview)")

        self.calendar_outerview.isUserInteractionEnabled = true
        self.calendar_outerview.addGestureRecognizer(tap)
  
        self.lblBtnAccept.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightSemibold)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.calendarDaySelected),
            name: NSNotification.Name(rawValue: "CALENDAR_DAY_SELECTED"),
            object: nil)
        
        //button search

        searchButton.setImage(UIImage(named: "search"), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchButton.addTarget(self, action:#selector(self.createSearchBar), for: .touchUpInside)
        let searchBarButton = UIBarButtonItem(customView: searchButton)
  
        
        let refreshButton = UIButton(type: .custom)
        refreshButton.setImage(UIImage(named: "refresh"), for: .normal)
        refreshButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        refreshButton.addTarget(self, action:#selector(self.refreshJob), for: .touchUpInside)
        let refreshBarButton = UIBarButtonItem(customView: refreshButton)
        
        self.navigationController?.navigationBar.topItem?.setRightBarButtonItems([refreshBarButton,searchBarButton], animated: true)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadJobAfterNotification),
            name: NSNotification.Name(rawValue: "RELOAD_JOB_AFTER_NOTIFICATION_ASSIGNED"),
            object: nil)
        
       
        
    }
    
    func refreshJob(){
        
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "REFRESH_JOB"), object: nil, userInfo: nil)
    }
    
    
    //func create search bar
    func createSearchBar(){
        
//        if(self.navigationItem.titleView == searchBar)
//        {
//
//            searchButton.setImage(UIImage(named: "search"), for: .normal)
//            self.navigationItem.titleView=nil
//            self.navigationController?.navigationBar.topItem?.title="Assigned"
//            let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
//            self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
//
//            self.navigationController?.navigationBar.tintColor = UIColor.white;
//
//        }
//        else{
//            searchBar.showsCancelButton = false
//            searchBar.placeholder = "Enter tracking number"
//            searchBar.delegate = self
//            searchBar.text = ""
//            self.navigationItem.titleView = searchBar
//            searchButton.setImage(UIImage(named: "cancel-search"), for: .normal)
//        }

        let storyboard = UIStoryboard(name: "SearchOrder", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "SearchOrderViewController") as! UINavigationController
        let levelController = navController.viewControllers[0] as! SearchOrderViewController
        levelController.itemTracking = itemTrackingSearch
        self.present(navController, animated: true, completion: nil)
        
        
        
    }
    //func create search bar
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing \(itemTrackingSearch)")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing \(itemTrackingSearch)")
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
        // getSearchJobList(itemTrackingSearch: itemTrackingSearch)
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        print("searchBarSearchButtonClicked \(itemTrackingSearch)")
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
        
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        self.navigationItem.titleView=nil
        self.navigationController?.navigationBar.topItem?.title="Assigned"
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = (titleDict as! [String : Any])
        
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        let storyboard = UIStoryboard(name: "SearchOrder", bundle: nil)
        let navController = storyboard.instantiateViewController(withIdentifier: "SearchOrderViewController") as! UINavigationController
        let levelController = navController.viewControllers[0] as! SearchOrderViewController
        levelController.itemTracking = itemTrackingSearch
        
        var tabBarController: MainVC
        tabBarController = self.parent?.parent as!MainVC
       // tabBarController.navigationController?.pushViewController(navController, animated: true)

       tabBarController.present(navController, animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        itemTrackingSearch = searchText
    }
    
    //button call dropOff
    func dropOffcallButtonClicked(button: UIButton) {
        
        
        var dropOffCall = String()
        dropOffCall = self.jobList[button.tag].dropOffContact!
        let alert = UIAlertController(title: dropOffCall, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: { action in
            print("call \(dropOffCall)")
            
            if let url = URL(string: "tel://\(dropOffCall)") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            
            //   UIApplication.shared.openURL(NSURL(string: "tel://\(self.dropOffcall)") as! URL)
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    //button call pickup
    func pickupcallButtonClicked(button: UIButton) {
        
        
        var pickupCall = String()
        pickupCall = self.jobList[button.tag].pickUpContact!
        let alert = UIAlertController(title: pickupCall, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Call", style: UIAlertActionStyle.default, handler: { action in
            print("call \(pickupCall)")
            
            if let url = URL(string: "tel://\(pickupCall)") {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            
            //   UIApplication.shared.openURL(NSURL(string: "tel://\(self.dropOffcall)") as! URL)
            
        }))
        
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    // show popup if number call empty
    func popupErrorNumberCall(button: UIButton) {
        
        let alertController = UIAlertController(title: "Error Number", message: "This number is not available", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    func checkBoxButtonClick(_ sender:UIButton!)
    {
        
        let clickedCell = (sender.superview!.superview!.superview!.superview! as! JobCardTableViewCellAssigned)
        if(clickedCell.checkbox.checkState == M13Checkbox.CheckState.checked)
        {
            self.jobList[sender.tag].isSelected = false
            clickedCell.checkbox.setCheckState(M13Checkbox.CheckState.unchecked, animated: true)
            print("UNCheck")
            
//            totalReattempt = totalReattempt - 1
//
//            if(totalReattempt >= 1){
//                enableBtnandLabelReattemt(totalReattempt: totalReattempt)
//            } else if(totalReattempt <= 0){
//                disableBtnandLabelReattemt()
//            }
            
            
            
        }
        else if(clickedCell.checkbox.checkState == M13Checkbox.CheckState.unchecked)
        {
            self.jobList[sender.tag].isSelected = true
            
            clickedCell.checkbox.setCheckState(M13Checkbox.CheckState.checked, animated: true)
            print("Check")
            
//            totalReattempt = totalReattempt + 1
//            if(totalReattempt >= 1){
//                enableBtnandLabelReattemt(totalReattempt: totalReattempt)
//            } else if(totalReattempt<=0){
//                disableBtnandLabelReattemt()
//            }
            
        }
        
        
    }
    
    func checkBoxStateDidChange(_ sender:M13Checkbox!)
    {
        print("checkbox value: \(sender.checkState)")
        
        
        
        let orderId = sender.tag
        
        if(sender.checkState == M13Checkbox.CheckState.checked)
        {
            
            for i in 0..<self.jobList.count {
                
                if(orderId == self.jobList[i].orderId)
                {
                    self.jobList[i].isSelected = true
                    break
                }
                
            }
        }
        else if(sender.checkState == M13Checkbox.CheckState.unchecked)
        {
            
            for i in 0..<self.jobList.count {
                
                if(orderId == self.jobList[i].orderId)
                {
                    self.jobList[i].isSelected = false
                    break
                }
                
            }
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TableView
    //MARK: - Tableview Delegate & Datasource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return self.jobList.count;
    }
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        print("tableView Assigned cellForRowAt")
            let cell:JobCardTableViewCellAssigned = self.tableView.dequeueReusableCell(withIdentifier: "JobCardTableViewCellAssigned") as! JobCardTableViewCellAssigned
            
            // ui for cardJob
           // cell.cardJob.layer.cornerRadius = 10
            cell.selectionStyle = .none
        
       
        
        
        
            //cell.backgroundColor = UIColor(red:0.81, green:0.81, blue:0.82, alpha:1)
            
//            cell.cardJob.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:1).cgColor
//            cell.cardJob.layer.shadowOpacity = 1
//            cell.cardJob.layer.shadowRadius = 8
        
            if(self.jobList[indexPath.row].companyName == "" || self.jobList[indexPath.row].companyName == nil){
                cell.lbl_customerName.text = " - "
                
            }else{
                cell.lbl_customerName.text = self.jobList[indexPath.row].companyName
            }
        
            cell.lbl_doReferenceNumber.text = self.jobList[indexPath.row].orderNumber
            var stepSequence = 0
            var stepCompletion = 0
            var isFailed = false
            var isCancelled = false
            var firstPending = true;

            for (index, jobstep) in  self.jobList[indexPath.row].job_stepsList.enumerated() {
               
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_PENDING || jobstep.job_step_status_id == Constants.JOB_STEP_INPROGRESS)
                {
                    if(firstPending == true)
                    {
                        stepSequence = jobstep.order_sequence!
                        firstPending = false
                    }
                    
                    
                    if(stepSequence >= jobstep.order_sequence!)
                    {
                        print("job step location is \(String(describing: jobstep.location))")

                        if(jobstep.location == "" || jobstep.location == nil){
                            cell.lbl_dropOffAdrress.text = " - "
                        }else{
                            cell.lbl_dropOffAdrress.text = NSString(format:"Step %d: %@", jobstep.order_sequence!,jobstep.location!) as String
                        }
                    }
                    
                    
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_COMPLETED)
                {
                    stepCompletion = stepCompletion + 1
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_CANCELLED)
                {
                    isCancelled = true
                }
                
                if(jobstep.job_step_status_id == Constants.JOB_STEP_FAILED)
                {
                    isFailed = true
                }
            }
            
            if(self.jobList[indexPath.row].job_stepsList.count==1)
            {
                if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_PENDING)
                {
                    cell.lbl_step_status.text = "Pending"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.PENDING_COLOR)
                    
                }
                else  if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_INPROGRESS)
                {
                    cell.lbl_step_status.text = "In Progress"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.INPROGRESS_COLOR)
                    
                }
                else  if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_COMPLETED)
                {
                    
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Completed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.COMPLETE_COLOR)
                    
                }
                else  if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_FAILED)
                {
                    
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Failed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.FAILED_COLOR)
                    
                }
                else  if(self.jobList[indexPath.row].job_stepsList[0].job_step_status_id == Constants.JOB_STEP_CANCELLED)
                {
                    
                    if(self.jobList[indexPath.row].job_stepsList[0].location == "" || self.jobList[indexPath.row].job_stepsList[0].location == nil){
                        cell.lbl_dropOffAdrress.text = " - "
                    }else{
                        
                        cell.lbl_dropOffAdrress.text = self.jobList[indexPath.row].job_stepsList[0].location
                    }
                    
                    cell.lbl_step_status.text = "Cancelled"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.CANCELLED_COLOR)
                    
                }
            }
            else{
                
                if(stepCompletion==0)
                {
                    
                    cell.lbl_step_status.text =  NSString(format:"0 of %d steps completed", self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.PENDING_COLOR)
                    
                }
                else if(stepCompletion == self.jobList[indexPath.row].job_stepsList.count)
                {
                    cell.lbl_step_status.text =  NSString(format:"%d of %d steps completed", self.jobList[indexPath.row].job_stepsList.count,self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.COMPLETE_COLOR)
                }
                else if(isCancelled)
                {
                    cell.lbl_step_status.text =  "Cancelled"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.CANCELLED_COLOR)
                }
                else if(isFailed)
                {
                    cell.lbl_step_status.text =  "Failed"
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.FAILED_COLOR)
                }
                else {
                    cell.lbl_step_status.text =  NSString(format:"%d of %d steps completed", stepCompletion,self.jobList[indexPath.row].job_stepsList.count) as String
                    cell.lbl_step_status.textColor = Constants.hexStringToUIColor(hex:Constants.INPROGRESS_COLOR)
                }
                
                
                
            }
            
            
            if(self.jobList[indexPath.row].dropOffTime == nil || self.jobList[indexPath.row].dropOffTime == ""){
                
                cell.lbl_dropOffDate.isHidden = true
            }
            else{
                let dropOffTimeString = self.jobList[indexPath.row].dropOffTime!
                
                print("time is \(dropOffTimeString)")
                
                
                let timeString = self.stringFormatDate(dropOffDate: dropOffTimeString, dateformat: "HH:mm", dateString: "hh:mm a")
                
                print("new time is \(timeString)")
                
                
                
                cell.lbl_dropOffDate.text = timeString
            }
        
        
        
            print("orderr idddddd \(self.jobList[indexPath.row].orderId)")
            cell.checkbox.tag = self.jobList[indexPath.row].orderId
        
            cell.checkBoxButton.tag = indexPath.row
            cell.checkBoxButton.addTarget(self, action: #selector(AssignedViewController.checkBoxButtonClick(_:)), for: .touchUpInside)
        
        
        
            cell.checkbox.addTarget(self, action:#selector(AssignedViewController.checkBoxStateDidChange(_:)),for: .valueChanged)
        
            if(self.jobList[indexPath.row].isSelected)!
            {
                cell.checkbox.setCheckState(M13Checkbox.CheckState.checked, animated: false)
            }
            else{
                cell.checkbox.setCheckState(M13Checkbox.CheckState.unchecked, animated: false)
                
            }
        
        if(selectedIndexTabStatus == Constants.SELECTED_TAB_UNASSIGNED && isEditMode == false)
        {
            cell.checkboxWidthConstraint.constant = 0
            cell.checkboxHeightConstraint.constant = 0
            cell.setNeedsUpdateConstraints()
            cell.layoutIfNeeded()
            
            
        }
        else{
            cell.checkboxWidthConstraint.constant = 45
            cell.checkboxHeightConstraint.constant = 91
            cell.setNeedsUpdateConstraints()
            cell.layoutIfNeeded()
        }
            
            return cell
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 148.0;//Choose your custom row height
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if reachInternetConnection.isConnectedToNetwork() == true {
            print("Assigned didSelectRowAt Internet connection OK")
            let storyboard = UIStoryboard(name: "OrderDetailsManyStep", bundle: nil)
            let levelController = storyboard.instantiateViewController(withIdentifier: "OrderDetailsManyStep") as! OrderDetailsManyStepViewController
            
            // levelController.jobdetail = self.jobList[(indexPath.row)]
            
            if(self.jobList[indexPath.row].orderId != 0){
                levelController.orderID = self.jobList[indexPath.row].orderId
                levelController.orderNumber = self.jobList[indexPath.row].orderNumber
                
                self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
                
                self.navigationController?.pushViewController(levelController, animated: true)
                
            }else{
                // order id is 0 .. so cannot open other page
            }
        } else {
            print("Assigned didSelectRowAt Internet connection FAILED")
            var alert = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
        
        
        
        

    }
    
    
}

//extension AssignedViewController{
//    public func alert_popup(title: String!, message: String!) {
//        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
//        let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
//            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
//            UserDefaults.standard.synchronize()
//            let storyBoard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
//            let viewcontroller : UIViewController = storyBoard.instantiateViewController(withIdentifier: "vcLogin") as UIViewController
//            self.present(viewcontroller, animated: true, completion: nil)
//
//        })
//        alertController.addAction(defaultAction)
//        present(alertController, animated: true, completion: nil)
//    }
//}


extension AssignedViewController{
    public func alert_popup(title: String!, message: String!, unauthorizedNeedToLoginPage: Bool) {
        let alertController = UIAlertController(title: "\(title!)", message: "\(message!)", preferredStyle: .alert)
        
        if(unauthorizedNeedToLoginPage == true){
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: { action in
                UserDefaults.standard.removeObject(forKey: "Token")
                var backGround = LocationTracker_ViewController()
                backGround.stopTimer()
                UserDefaults.standard.setValue(false, forKey: "isOnline")
                var window: UIWindow? = UIApplication.shared.keyWindow
                var storyBoard = UIStoryboard(name: "Login", bundle: nil)
                window?.rootViewController = storyBoard.instantiateInitialViewController()
            })
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }else{
            let defaultAction = UIAlertAction(title: "Close ", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
    }
}




