//
//  StringMessage.swift
//  SAAS
//
//  Created by Coolasia on 2/5/18.
//  Copyright © 2018 Coolasia. All rights reserved.
//

import Foundation
import UIKit

struct StringMessage {
    
    /* ====== STRING MESSAGE ============ */
    
    static let unassignedToAssignedTitle = "Job Assigned"
    static let unassignedToAssignedMessage = "The selected worker has been assigned to these job(s)"

    
    static let assignedToacknowledgedTitle = "Job Acknowledged"
    static let assignedToacknowledgedMessage = "You have accepted these job(s)."
    
    
    static let jobStepStartedTitle = "Job Started"
    static let jobStepStartedMessage = "Your progress for this step starts now."
    
    static let jobStepCompletedTitle = "Step Completed!"
    static let jobStepCompletedMessage = "Your progress has been updated."
    
    static let jobLastStepCompletedTitle = "Job Completed!"
    static let jobLastStepCompletedMessage = "You have completed this job."
    
    static let jobFailedTitle = "Job Failed"
    static let jobFailedMessage = "This job has been marked as Failed."
    
    
    static let offlineTitle = "Cannot accept job(s)"
    static let offlineMessage = "Change your availability to online so that you can acccept jobs."
    
}




