//
//  Worker.swift
//  WMG
//
//  Created by jing jie wong on 10/1/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit

class Worker: NSObject {
    
    var id: Int?
    var email: String?
    var firstName: String?
    var lastName: String?
    
    override init() {
        
        self.id = 0
        self.email = ""
        self.firstName = ""
        self.lastName = ""
        super.init()
        
    }
    
    init(id: Int?,email: String?, firstName: String?,lastName:String?) {
        self.id = id
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        super.init()
    }

    
}


extension Worker {
    convenience init?(json: JSON) {
        
        
        
        let id: Int? = json["id"].int
        let email: String? = json["email"].string
        let firstName: String? = json["first_name"].string
        let lastName: String? = json["last_name"].string
        
        self.init(id: id,email: email, firstName: firstName,lastName:lastName)
        
    }
}
