//
//  OrderAttemptSectionHeader.swift
//  WMG
//
//  Created by Coolasia on 5/1/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit

final class OrderAttemptSectionHeader: LUExpandableTableViewSectionHeader {
    // MARK: - Properties
    

    @IBOutlet weak var order_attempt: UILabel!
    
    @IBOutlet weak var datetime: UILabel!
    @IBOutlet weak var updownButton: UIButton!
    
    override var isExpanded: Bool {
        didSet {
         //  updownButton.setTitle(isExpanded ? "Collapse" : "Expand", for: .normal)
            updownButton.setImage(isExpanded ? UIImage(named: "arrow-up.png") : UIImage(named: "arrow-down.png"), for: .normal)
        }
    }
    
    // MARK: - Base Class Overrides
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        order_attempt.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnLabel)))
        order_attempt.isUserInteractionEnabled = true
        
       // contentView.backgroundColor = UIColor.darkGray
    }
    
    // MARK: - IBActions
    
    @IBAction func expandCollapse(_ sender: UIButton) {
        delegate?.expandableSectionHeader(self, shouldExpandOrCollapseAtSection: section)
}
    
    // MARK: - Private Functions
    
    @objc private func didTapOnLabel(_ sender: UIGestureRecognizer) {
        // Send the message to his delegate that was selected
       // delegate?.expandableSectionHeader(self, wasSelectedAtSection: section)
        delegate?.expandableSectionHeader(self, shouldExpandOrCollapseAtSection: section)

    }
    
    
}

