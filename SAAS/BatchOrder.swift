//
//  BatchOrder.swift
//  WMG
//
//  Created by jing jie wong on 2/1/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import SwiftyJSON
import UIKit

class BatchOrder: NSObject {
    
    var orderStatus: String?
    var workerId: Int?

    var orderIds: [String] = []
    var orderAttemptList: [OrderAttempt] = []
    
    override init() {
        
        self.orderStatus = ""
        super.init()
        
    }
    
    func toJSON() -> Dictionary<String, AnyObject> {
        
        var orderIdString:String = ""
        var orderAttemptListString:String = ""

        
        do {
//
            //Convert to Data
            let orderIdjsonData = try JSONSerialization.data(withJSONObject: self.orderIds, options:.prettyPrinted)

         
//            let json = JSON(self.orderIds)
//
//            let orderIdjsonData =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data

            //Convert back to string. Usually only do this for debugging
            if let orderIdJsonString = String(data: orderIdjsonData, encoding: String.Encoding.utf8) {
                

                orderIdString = orderIdJsonString
                
                orderIdString = orderIdString.replacingOccurrences(of: "\n", with: "")
                orderIdString = orderIdString.trimmingCharacters(in: CharacterSet.newlines)
                orderIdString = orderIdString.replacingOccurrences(of: " ", with: "")
                
                
                print(orderIdString)

            }
            
            let orderAttemptJsonString = try JSONSerialization.data(withJSONObject: self.orderAttemptList, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            
            if let orderAttemptListJsonString = String(data: orderAttemptJsonString, encoding: String.Encoding.utf8) {
             
                orderAttemptListString = orderAttemptListJsonString

                orderAttemptListString = orderAttemptListString.replacingOccurrences(of: "\n", with: "")
                orderAttemptListString = orderAttemptListString.trimmingCharacters(in: CharacterSet.newlines)
                orderAttemptListString = orderAttemptListString.replacingOccurrences(of: " ", with: "")
                
                print(orderAttemptListString)


            }

    
            
            
            
        } catch {
            //print(error.description)
        }
        
 
        return [
            "id": orderIdString as AnyObject,
            "order_status": self.orderStatus as AnyObject,
            "order_attempts": orderAttemptListString as AnyObject
        ]
    }
    
   
    
}


//extension BatchOrder {
//    convenience init?(json: JSON) {
//        
//        
//        
//        
//        let receivedBy: String? = json["received_by"].string
//        let reason: String? = json["reason"].string
//        let note: String? = json["note"].string
//        let submittedTime: String? = json["submitted_time"].string
//        let latitude: Double? = json["latitude"].double
//        let longitude: Double? = json["longitude"].double
//        
//        var orderAttemptImageList: [OrderAttemptImage] = []
//        
//        if let items = json["order_attempt_image"].array {
//            for item in items {
//                if let orderAttemptImage = OrderAttemptImage(json: item) {
//                    
//                    orderAttemptImageList.append(orderAttemptImage)
//                }
//                
//            }
//        }
//        
//        
//        
//        
//        
//        self.init(receivedBy: receivedBy, reason: reason,note:note,submittedTime: submittedTime,latitude: latitude,longitude:longitude,orderAttemptImageList:orderAttemptImageList)
//        
//    }
//}
