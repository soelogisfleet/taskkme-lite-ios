//
//  LocationTracker_ViewController.swift
//  WMG
//
//  Created by Coolasia on 28/6/17.
//  Copyright © 2017 Coolasia. All rights reserved.
//

import UIKit
import CoreLocation
import AVFoundation
import Alamofire
import SwiftyJSON
import Toast_Swift

class LocationTracker_ViewController: UIViewController,CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var latitude: Double!
    var longitude: Double!
    var timer: Timer?
    var resourceID: Int?
    var isOnline: Bool?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // self.locationManager.requestAlwaysAuthorization()
        self.locationManager.pausesLocationUpdatesAutomatically = false

//        if CLLocationManager.locationServicesEnabled(){
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.startUpdatingLocation()
//        }
        
        if CLLocationManager.locationServicesEnabled(){
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            
            
            switch(CLLocationManager.authorizationStatus())
            {
                
            case .authorizedAlways:
                
                print("Authorize.")
                self.locationManager.requestAlwaysAuthorization()

                break
                
            case .authorizedWhenInUse:
                
                print("Authorize.")
                self.locationManager.requestWhenInUseAuthorization()

                break

            case .notDetermined:
                
                print("Not determined.")
                
                break
                
            case .restricted:
                
                print("Restricted.")
                
                break
                
            case .denied:
                
                print("Denied.")
            }
            
            
            
         //   locationManager.allowsBackgroundLocationUpdates = true
            self.locationManager.startUpdatingLocation()

            self.locationManager.startMonitoringSignificantLocationChanges()
        }
        
        
        
        self.latitude = self.locationManager.location?.coordinate.latitude
        self.longitude = self.locationManager.location?.coordinate.longitude
        
   // startTimer()
        
//        DispatchQueue.global(qos: .background).async {
//            while (shouldCallMethod) {
//                self.startTimer()
//                sleep(1)
//            }
//        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // stopTimer()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
          //  print("current latitude \(location.coordinate.latitude)")
          //  print("current longitude \(location.coordinate.longitude)")
            
           // var resourceID: Int?
            var isOnline: Bool?
            
            self.latitude = location.coordinate.latitude
            self.longitude = location.coordinate.longitude
            
            print("didUpdateLocations yess")
            
            
//            if let resourceIDCheck = UserDefaults.standard.object(forKey: "resourseID") {
//                resourceID = UserDefaults.standard.value(forKey: "resourseID") as! Int
//            }
            
            
            if(self.latitude == nil || self.longitude == nil){
                // self.latitude = 0.0
                // self.longitude = 0.0
            }else{
                if let isOnlineCheck = UserDefaults.standard.object(forKey: "isOnline") {
                    self.isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
                    if(self.isOnline)!{
                        
                       self.uploadCurrentLocationToServer()
                    }else{
                        self.locationManager.stopUpdatingLocation()
                        self.locationManager.stopMonitoringSignificantLocationChanges()
                    }
                }else{
                    self.locationManager.stopUpdatingLocation()
                    self.locationManager.stopMonitoringSignificantLocationChanges()
                }
                
            }
            
        }
    }
    
    
    
//    func startTimer() {
////
//        NSLog("startTimer", "yes")
//
//
//
//
//        timer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true) { [weak self] _ in
//
//            if(self?.latitude == nil || self?.longitude == nil){
//                // self.latitude = 0.0
//                // self.longitude = 0.0
//            }else{
//                if let isOnlineCheck = UserDefaults.standard.object(forKey: "isOnline") {
//                    self?.isOnline = UserDefaults.standard.value(forKey: "isOnline") as! Bool
//                    if(self?.isOnline)!{
//                        self?.uploadCurrentLocationToServer()
//                    }else{
//                        self?.locationManager.stopMonitoringSignificantLocationChanges()
//                    }
//                }else{
//                    self?.locationManager.stopMonitoringSignificantLocationChanges()
//                }
//
//            }
//
//
//
//        }
//   }
    
    
        
        
        func stopTimer() {
//            if timer != nil {
//                timer!.invalidate()
//                timer = nil
//            }
           timer?.invalidate()
            self.locationManager.stopUpdatingLocation()
           self.locationManager.stopMonitoringSignificantLocationChanges()
        }
        
        deinit {
            stopTimer()
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func uploadCurrentLocationToServer(){
        
        let resourceID = UserDefaults.standard.value(forKey: "resourseID") as! Int
    
//        let updateLocationObject : Dictionary<String,AnyObject> = [
//            "latitude": String(self.latitude) as AnyObject,
//             "longitude": String(self.longitude) as AnyObject,
//             "worker_id": resourceID as AnyObject]
        
        let updateLocationObject : Dictionary<String,AnyObject> = [
            "latitude": String(self.latitude) as AnyObject,
            "longitude": String(self.longitude) as AnyObject]
        
        print("updateLocationObject \(updateLocationObject)")
        
        AlamofireRequest.updateLocation(parameters: updateLocationObject as NSDictionary) { responseObject, statusCode, error in
            
            if(statusCode == 200){
                print("Success to update location")
            }else{
               print("Error to update location")
            }
        }
    }
}
