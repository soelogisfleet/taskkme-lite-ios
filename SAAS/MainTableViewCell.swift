//
//  MainTableViewCell.swift
//  WMG
//
//  Created by Coolasia on 22/12/16.
//  Copyright © 2016 Coolasia. All rights reserved.
//

import UIKit
import M13Checkbox
import MarqueeLabel

class MainTableViewCell: UITableViewCell {
    
    // @IBOutlet weak var cell: UIView!
    @IBOutlet weak var cell: UIView!
    
    @IBOutlet weak var dropOff_contactName: UILabel!
    @IBOutlet weak var dropOff_address: MarqueeLabel!
    @IBOutlet weak var box_cell_image: UIImageView!
    
    @IBOutlet weak var checkbox: M13Checkbox!
    @IBOutlet weak var item_tracking_number: UILabel!
    @IBOutlet weak var checkBoxView: UIView!
    
    @IBOutlet weak var orderStatusLabel: UILabel!
   
    @IBOutlet weak var checkBoxButton: UIButton!
    
    
    @IBOutlet weak var remarksButton: UIButton!
    
    
    @IBOutlet weak var postalCode: UILabel!
    
    @IBOutlet weak var callButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dropOff_address.animationCurve = .curveLinear
        dropOff_address.marqueeType = .MLContinuous
        dropOff_address.scrollDuration = 16.0
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    override func prepareForReuse() {
        
        if checkbox != nil
        {
            checkbox.setCheckState(M13Checkbox.CheckState.unchecked, animated: false)
            
        }
        super.prepareForReuse()
    }
    
}
