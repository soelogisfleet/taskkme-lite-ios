//
//  JobOrder+CoreDataProperties.swift
//  
//
//  Created by Coolasia on 23/3/17.
//
//

import Foundation
import CoreData


extension JobOrder {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<JobOrder> {
        return NSFetchRequest<JobOrder>(entityName: "JobOrder");
    }

    @NSManaged public var body: NSMutableDictionary
    @NSManaged public var id: Int64
    @NSManaged public var orderID: String
    @NSManaged public var orderStatus: [String]

}
